﻿using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BWSystemLibrary2
{
    public enum SortingMethod
    {
        alphabetical,
        consumption,
        presence,
    }

    public abstract class LibUtility
    {
        public static int MinSecurityCodeCount
        {
            get { return (int)LibUtility.getConfigValue("MinSecurityCodeCount", typeof(int)); }
        }


        private static AppSettingsReader config = new AppSettingsReader();

        public static Object getConfigValue(string key, Type type)
        {
            return config.GetValue(key, type);
        }

        public static ArrayOfInt intArray2ArrayOfInt(int[] intArray)
        {
            ArrayOfInt intArrayList = new ArrayOfInt();
            foreach (int number in intArray)
            {
                intArrayList.Add(number);
            }
            return intArrayList;
        }


        public static BW_WebService.SortingMethod convertSortingMethod(BWSystemLibrary2.SortingMethod method)
        {
            switch (method)
            {
                case SortingMethod.alphabetical:
                    return BW_WebService.SortingMethod.alphabetical;
                case SortingMethod.consumption:
                    return BW_WebService.SortingMethod.consumption;
                case SortingMethod.presence:
                    return BW_WebService.SortingMethod.presence;
                default:
                    SimpleLog.Log("Unknown sorting method!");
                    return BW_WebService.SortingMethod.alphabetical;
            }
        }


        public static int getStandardAmount(double size)
        {
            int stdSize = 12;

            if (size >= 1.0)
            {
                stdSize = 12;
            }
            else if (size >= 0.75)
            {
                stdSize = 12;
            }
            else if (size >= 0.5)
            {
                stdSize = 20;
            }
            else if (size >= 0.3)
            {
                stdSize = 24;
            }
            else
            {
                stdSize = 24;
            }

            return stdSize;
        }



        public static bool serializeBookings(List<BookingData> bookings)
        {
            bool serialized = true;
            string backupFile = LibUtility.getBackupFilePath();
            XmlSerializer serializer = new XmlSerializer(typeof(List<BookingData>));

            FileStream fileStream = null;
            if (!File.Exists(backupFile))
            {
                fileStream = File.Create(backupFile);
            }
            else
            {
                fileStream = new FileStream(backupFile, FileMode.Create);
            }

            using (TextWriter writer = new StreamWriter(fileStream))
            {
                //foreach (BookingData booking in bookings)
                try
                {
                    serializer.Serialize(writer, bookings);
                } catch (Exception ex)
                {
                    SimpleLog.Log(ex);
                    serialized = false;
                }
            }
            fileStream.Close();
            return serialized;
        }


        public static List<BookingData> deserializeBookings()
        {
            List<BookingData> bookings = new List<BookingData>();
            string backupFile = LibUtility.getBackupFilePath();
            if (File.Exists(backupFile))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<BookingData>));

                using (TextReader reader = new StreamReader(backupFile))
                {
                    try
                    {
                        bookings = (List<BookingData>)serializer.Deserialize(reader);
                    } catch (Exception ex)
                    {
                        SimpleLog.Log(ex);
                    }
                }
            }

            return bookings;
        }


        private static string getBackupFilePath()
        {
            return (string)LibUtility.getConfigValue("LocalBackupPath", typeof(string)) + Path.DirectorySeparatorChar + "UnbookedBackupBookings.xml";
        }
    }
}
