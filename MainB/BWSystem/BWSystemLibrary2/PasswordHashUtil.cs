﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace BWSystemLibrary2
{
    public class PasswordHashUtil
    {

        /*
         * Salt generation Methdod
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] generateSalt()
        {
            byte[] salt = new byte[32];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(salt);
            return salt;
        }


        /*
         * Hash Generation Methods
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] generateSaltedPasswordHash(string password, byte[] salt)
        {
            byte[] pwInBytes = Encoding.ASCII.GetBytes(password);
            byte[] saltedPW = salt.Concat(pwInBytes).ToArray();

            SHA256 hashAlgo = SHA256.Create();

            return hashAlgo.ComputeHash(saltedPW);
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] generateSaltedPasswordHash(int[] password, byte[] salt)
        {
            byte[] pwInBytes = intArrayToBytesArray(password);
            byte[] saltedPW = salt.Concat(pwInBytes).ToArray();

            SHA256 hashAlgo = SHA256.Create();

            return hashAlgo.ComputeHash(saltedPW);
        }


        /*
         * Validation Methods
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool validatePassword(string password, byte[] salt, byte[] hashedSaltedPassword)
        {
            byte[] enteredSHWP = PasswordHashUtil.generateSaltedPasswordHash(password, salt);

            bool validPW = true;
            for (int i = 0; i < enteredSHWP.Length; i++)
            {
                validPW = validPW && (enteredSHWP[i] == hashedSaltedPassword[i]);
            }
            return validPW;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool validatePassword(int[] password, byte[] salt, byte[] hashedSaltedPassword)
        {
            byte[] enteredSHWP = PasswordHashUtil.generateSaltedPasswordHash(password, salt);

            bool validPW = true;
            for (int i = 0; i < enteredSHWP.Length; i++)
            {
                validPW = validPW && (enteredSHWP[i] == hashedSaltedPassword[i]);
            }
            return validPW;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long getRandomLong()
        {
            byte[] longValues = new byte[8];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(longValues);
            return Convert.ToInt64(longValues);
        }



        /*
         *
         * Helper Methods
         *
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte[] intArrayToBytesArray(int[] password)
        {
            byte[] pwInBytes = new byte[password.Length];
            for (int i = 0; i < password.Length; i++)
            {
                pwInBytes[i] = Convert.ToByte(password[i]);
            }

            return pwInBytes;
        }
    }
}