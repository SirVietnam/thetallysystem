﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using BWSystemLibrary2.BW_WebService;
using System.Windows;

namespace BWSystemLibrary2
{
    abstract public class WebServiceAccess
    {
        private static ClientToken currentToken;
        private static string loginToken = (string)LibUtility.getConfigValue("LoginToken", typeof(string));

        private int wsConnectAttempts;
        private static BWServiceSoapClient service;

        public WebServiceAccess()
        {
            if (WebServiceAccess.service == null)
            {
                service = new BWServiceSoapClient();
                WebServiceAccess.currentToken = new ClientToken();
            }

            this.wsConnectAttempts = (int)LibUtility.getConfigValue("WebServiceReconnectAtempts", typeof(int));
        }


        public BWServiceSoapClient Service
        {
            get { return WebServiceAccess.service; }
        }

        public bool openService()
        {
            try
            {
                bool open = (Service.State == CommunicationState.Opened);

                for (int i = 0; !open && i < this.wsConnectAttempts; i++)
                {
                    //Console.WriteLine("Connection Attmpt {0} from {1} atempts", i, this.wsConnectAttempts);
                    if (Service.State != CommunicationState.Opened)
                    {
                        Service.Open();
                        System.Threading.Thread.Sleep(100);
                    }
                    else
                    {
                        open = true;
                    }
                }
                return (Service.State == CommunicationState.Opened);

            } catch (CommunicationException ex)
            {
                SimpleLog.Log(ex);
                return false;
            } catch (TimeoutException ex)
            {
                SimpleLog.Log(ex);
                return false;
            }

        }


        public ClientToken CurrentToken
        {
            get
            {
                if (WebServiceAccess.currentToken.EndDateTime < DateTime.Now)
                {
                    if (this.openService())
                    {
                        try
                        {
                            WebServiceAccess.currentToken = this.Service.getClientToken(WebServiceAccess.loginToken);
                        } catch (CommunicationException e)
                        {
                            SimpleLog.Log(e);
                        } catch (TimeoutException e)
                        {
                            SimpleLog.Log(e);
                        }
                        finally
                        {
                            if (currentToken.DeviceID == 0 || currentToken.EndDateTime < DateTime.Now)
                            {
                                string errorMsg = "A Security Check failed. The System will shut down. Talk to your admin.";
                                throw new ArgumentException(errorMsg);
                            }
                        }
                    }
                }
                return WebServiceAccess.currentToken;
            }
        }

    }
}
