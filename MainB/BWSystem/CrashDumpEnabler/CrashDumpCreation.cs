﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrashDumpEnabler
{
    class CrashDumpCreation
    {
        struct RegKeys
        {
            public string keyName;
            public object keyValue;
            public RegistryValueKind keyValueKind;

            public RegKeys(string keyName, object keyValue, RegistryValueKind keyValueKind)
            {
                this.keyName = keyName;
                this.keyValue = keyValue;
                this.keyValueKind = keyValueKind;
            }
        }

        private readonly string baseFolder = @"Software\Microsoft\Windows\Windows Error Reporting"; //HKEY_CURRENT_USER
        private readonly string localDumpsFolder = @"LocalDumps";
        private string applicationDumpFolder;
        private RegKeys[] crashDumpRegKeys;
        private RegKeys[] applRegKeys;


        public CrashDumpCreation(string applName, int crahDumpCount, int crahDumpType, string absolutePath)
        {
            this.applicationDumpFolder = applName;

            this.applRegKeys = new RegKeys[] {
                new RegKeys("DumpFolder", absolutePath, RegistryValueKind.ExpandString),
                new RegKeys("DumpCount", crahDumpCount, RegistryValueKind.DWord),
                new RegKeys("DumpType", crahDumpType, RegistryValueKind.DWord)
            };

            this.crashDumpRegKeys = new RegKeys[] {
                new RegKeys("DumpFolder", @"C:\\Temp\\CrashDumps", RegistryValueKind.ExpandString),
                new RegKeys("DumpCount", 0, RegistryValueKind.DWord),
                new RegKeys("DumpType", 1, RegistryValueKind.DWord)
            };
        }


        public void setupRegistry(bool x32 = false)
        {
            RegistryKey localDumpsKey = getLocalDumpsRegistryKey(x32);

            RegistryKey applKey = localDumpsKey.OpenSubKey(applicationDumpFolder, true);
            if (applKey == null)
            {
                applKey = localDumpsKey.CreateSubKey(applicationDumpFolder, RegistryKeyPermissionCheck.ReadWriteSubTree); //, RegistryKeyPermissionCheck.ReadWriteSubTree
            }


            foreach (RegKeys regKey in crashDumpRegKeys)
            {
                if (localDumpsKey != null)
                {
                    if (localDumpsKey.GetValue(regKey.keyName) == null)
                        localDumpsKey.SetValue(regKey.keyName, regKey.keyValue, regKey.keyValueKind);
                }
            }


            foreach (RegKeys regKey in applRegKeys)
            {
                if (localDumpsKey != null)
                {
                    if (localDumpsKey.GetValue(regKey.keyName) == null)
                        localDumpsKey.SetValue(regKey.keyName, regKey.keyValue, regKey.keyValueKind);

                    applKey.SetValue(regKey.keyName, regKey.keyValue, regKey.keyValueKind);
                }
            }
        }



        public void cleanRegistry(bool x32 = false, bool completeClean = false)
        {
            RegistryKey localDumpsKey = getLocalDumpsRegistryKey(x32);

            RegistryKey applKey = localDumpsKey.OpenSubKey(applicationDumpFolder, true);
            if (applKey != null)
            {
                localDumpsKey.DeleteSubKey(applicationDumpFolder); //, RegistryKeyPermissionCheck.ReadWriteSubTree
            }

            if (localDumpsKey.SubKeyCount == 0 && completeClean)
            {
                RegistryKey werKey = getWERRegistyKey(x32);
                werKey.DeleteSubKey(this.localDumpsFolder);
            }
        }



        //////// HELPERS ////////
        private RegistryKey getLocalDumpsRegistryKey(bool x32)
        {
            RegistryKey werKey = getWERRegistyKey(x32);

            RegistryKey localDumpsKey = werKey.OpenSubKey(this.localDumpsFolder, true);
            if (localDumpsKey == null)
            {
                localDumpsKey = werKey.CreateSubKey(this.localDumpsFolder, RegistryKeyPermissionCheck.ReadWriteSubTree); //, RegistryKeyPermissionCheck.ReadWriteSubTree
            }

            return localDumpsKey;
        }

        private RegistryKey getWERRegistyKey(bool x32)
        {
            RegistryKey baseKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, x32 ? RegistryView.Registry32 : RegistryView.Registry64);
            RegistryKey werKey = baseKey.OpenSubKey(baseFolder, true);
            return werKey;
        }
    }
}
