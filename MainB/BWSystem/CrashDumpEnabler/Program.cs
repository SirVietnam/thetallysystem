﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;
using System.Security;

namespace CrashDumpEnabler
{
    class Program
    {
        private static string defaultApplName = @"BWSystemClient2.exe";
        private static string defaultApplPath = @"C:\\CrashDumps\\BWSystemClient";
        private static int defaultCrashDumpCount = 10;
        private static int defaultCrashDumpType = 2;

        static void Main(string[] args)
        {
            Options arguments = new Options();
            Parser parser = new Parser();
            if (parser.ParseArguments(args, arguments))
            {
                if (arguments.Help)
                {
                    Console.WriteLine(arguments.GetUsage());
                }
                else
                {
                    execute(arguments);
                }
            }

            Console.WriteLine("Type any Key to exit");
            Console.ReadKey();
        }

        private static void execute(Options arguments)
        {
            try
            {
                string applicationName = getApplicationName(arguments);
                string applicationPath = getAbsolutePath(arguments);
                int count = getCrashDumpCount(arguments);
                int type = getCrashDumpType(arguments);


                if (arguments.Remove)
                {
                    Console.Write("Starting Removal of Registry ...");
                    CrashDumpCreation creator = new CrashDumpCreation(applicationName, count, type, applicationPath);
                    creator.cleanRegistry(arguments.x32, arguments.ForceRemove);
                }
                else
                {
                    Console.Write("Starting Adding of Registry ...");
                    CrashDumpCreation creator = new CrashDumpCreation(applicationName, count, type, applicationPath);
                    creator.setupRegistry(arguments.x32);
                }
                Console.WriteLine("Done");
            } catch (SecurityException)
            {
                Console.Error.WriteLine("\nERROR\nERROR -- You need to start the application as ADMIN.\nERROR");
            }
        }

        private static int getCrashDumpType(Options arguments)
        {
            if (arguments.CrashDumpType == null || arguments.CrashDumpType < 1 || arguments.CrashDumpType > 2)
            {
                Console.WriteLine("No legal CrashDumpType found \t=> \tUsing default: \t" + Program.defaultCrashDumpType);
                return Program.defaultCrashDumpType;
            }
            return (int)arguments.CrashDumpType;
        }

        private static int getCrashDumpCount(Options arguments)
        {
            if (arguments.CrashDumpCount == null)
            {
                Console.WriteLine("No CrashDumpCount found \t=> \tUsing default: \t" + Program.defaultCrashDumpCount);
                return Program.defaultCrashDumpCount;
            }
            return (int)arguments.CrashDumpCount;
        }

        private static string getAbsolutePath(Options arguments)
        {
            if (arguments.AbsolutePath == null || arguments.Name == "")
            {
                Console.WriteLine("No AbsoluteFilePath found \t=> \tUsing default: \t" + Program.defaultApplPath);
                return Program.defaultApplPath;
            }
            return arguments.Name;
        }

        private static string getApplicationName(Options arguments)
        {
            if (arguments.Name == null || arguments.Name == "")
            {
                Console.WriteLine("No Application name found \t=> \tUsing default: \t" + Program.defaultApplName);
                return Program.defaultApplName;
            }
            return arguments.Name;
        }
    }
}
