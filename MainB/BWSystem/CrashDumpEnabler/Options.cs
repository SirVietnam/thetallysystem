﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrashDumpEnabler
{
    class Options
    {

        [Option('x', "architecture", Required = false, HelpText = "Set to use x86 instead of x64 architecture")]
        public bool x32 { get; set; }

        [Option('r', "remove", Required = false, HelpText = "Remove Crash Dump.")]
        public bool Remove { get; set; }

        [Option('f', "force", Required = false, HelpText = "Use with remove to prevent from creating any crash Dumps at all!")]
        public bool ForceRemove { get; set; }

        [Option('n', "name", Required = false, HelpText = "The application name.")]
        public string Name { get; set; }

        [Option('t', "type", Required = false, HelpText = "The crash dump type: mini(1) OR full(2).")]
        public int? CrashDumpType { get; set; }

        [Option('p', "path", Required = false, HelpText = "The crash dump location in full path.")]
        public string AbsolutePath { get; set; }

        [Option('c', "count", Required = false, HelpText = "The amount of different crash dumps.")]
        public int? CrashDumpCount { get; set; }

        [Option('h', "help", Required = false, HelpText = "The help text.")]
        public bool Help { get; set; }

        [HelpOption(HelpText = "Display this help screen.")]
        public string GetUsage()
        {
            var usage = new StringBuilder();
            usage.AppendLine();
            usage.AppendLine("This application adds or removes the CrashDumpFiles.");
            usage.AppendLine(HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current)));
            return usage.ToString();
        }
    }
}
