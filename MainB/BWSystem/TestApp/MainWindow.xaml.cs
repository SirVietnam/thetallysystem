﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using BWSystemServer.BIZ;
using BWSystemServer.DAL;
using BWSystemServer;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Windows.Media.Animation;

namespace TestApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        [DllImport("Kernel32.dll")]
        public static extern bool AttachConsole(int processId);

        private DateTime lastDateTime;

        public MainWindow()
        {
            InitializeComponent();
            lastDateTime = DateTime.Now;
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
        }


        private void onClick(object sender, MouseButtonEventArgs e)
        {
            //Random random = new Random();
            //double upper = Math.Exp(random.NextDouble() * 5.0);
            //double lower = Math.Exp(3.0);
            //int articleAmount = Convert.ToInt32(Math.Floor(upper / lower)) + 1;
            //Console.WriteLine(articleAmount.ToString());
            //this.addBookings(1, 0.0);

            //this.LoadPictureFromLDAP();

            //this.hashPW("MyPassword1");

            //dbam.makeSettlement(DateTime.Now);

            //MessageBox.Show("Ich liebe dich!");
            //SimpleLogger.SimpleLog.testSimpleLog();
            //this.fadeOut();

            CrashDumpCreation crashDump = new CrashDumpCreation();
            crashDump.setupRegistry();

        }


        private void fadeOut()
        {
            ((Storyboard)FindResource("FadeOut")).Begin(this.clickableHeart);
        }


        private void LoadPictureFromLDAP()
        {
            Authentication auth = new Authentication();
            auth.LoginName = "philipp";
            Account acc = new Account();
            acc.Authentication = auth;

            LDAPSync.getAccountPicture(acc);
        }


        private void hashPW(string password)
        {
            byte[] pwInBytes = Encoding.ASCII.GetBytes(password);

            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            byte[] salt = new byte[32];
            crypto.GetNonZeroBytes(salt);


            byte[] saltedPW = salt.Concat(pwInBytes).ToArray();
            SHA256 hashAlgo = SHA256.Create();

            byte[] hashedSaltedPW = hashAlgo.ComputeHash(saltedPW);

        }
        
    }
}
