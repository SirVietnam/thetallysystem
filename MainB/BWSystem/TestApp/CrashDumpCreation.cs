﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class CrashDumpCreation
    {
        struct RegKeys
        {
            public string keyName;
            public object keyValue;
            public RegistryValueKind keyValueKind;

            public RegKeys(string keyName, object keyValue, RegistryValueKind keyValueKind)
            {
                this.keyName = keyName;
                this.keyValue = keyValue;
                this.keyValueKind = keyValueKind;
            }
        }

        private string baseFolder = @"Software\Microsoft\Windows\Windows Error Reporting"; //HKEY_CURRENT_USER

        private string localDumpsFolder = @"LocalDumps";

        private string applicationDumpFolder = @"BWSystemClient2.exe";

        private RegKeys[] regKeys = {
            new RegKeys("DumpFolder",  @"C:\\Dumps", RegistryValueKind.ExpandString),
            new RegKeys("DumpCount",  10, RegistryValueKind.DWord),
            new RegKeys("DumpType",  2, RegistryValueKind.DWord)
            //new RegKeys("CustomDumpFlags",  @"C:\\Dumps", RegistryValueKind.ExpandString)
        };


        public void setupRegistry()
        {
            string keyPath2 = baseFolder + @"\" + localDumpsFolder; //
            string keyPath = baseFolder + @"\" + localDumpsFolder + @"\" + applicationDumpFolder;// 

            string value64 = string.Empty;
            RegistryKey baseKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey werKey = baseKey.OpenSubKey(baseFolder, true);

            RegistryKey localDumpsKey = werKey.OpenSubKey(localDumpsFolder, true);
            if (localDumpsKey == null)
            {
                localDumpsKey = werKey.CreateSubKey(localDumpsFolder, RegistryKeyPermissionCheck.ReadWriteSubTree); //, RegistryKeyPermissionCheck.ReadWriteSubTree
            }

            RegistryKey applKey = localDumpsKey.OpenSubKey(applicationDumpFolder, true);
            if (applKey == null)
            {
                applKey = localDumpsKey.CreateSubKey(applicationDumpFolder, RegistryKeyPermissionCheck.ReadWriteSubTree); //, RegistryKeyPermissionCheck.ReadWriteSubTree
            }


            foreach (RegKeys regKey in regKeys)
            {
                //Registry.SetValue(keyPath2, regKey.keyName, regKey.keyValue, regKey.keyValueKind);
                //Registry.SetValue(keyPath, regKey.keyName, regKey.keyValue, regKey.keyValueKind);

                if (localDumpsKey != null)
                {
                    localDumpsKey.SetValue(regKey.keyName, regKey.keyValue, regKey.keyValueKind);
                    applKey.SetValue(regKey.keyName, regKey.keyValue, regKey.keyValueKind);
                }
            }
        }
    }
}
