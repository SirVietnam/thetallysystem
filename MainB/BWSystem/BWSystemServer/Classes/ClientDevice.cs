﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Web;

namespace BWSystemServer.Classes
{

    public class ClientToken
    {
        public long ID;
        public int DeviceID;
        public DateTime StartDateTime;
        public DateTime EndDateTime;
    }


    public class ClientDevice
    {
        private static TimeSpan tokenValidationTime = TimeSpan.FromMinutes(5.0);

        bool initialized;
        private int id;
        private string ipAddress;
        private byte[] hashedSaltedPW;
        private byte[] salt;
        ClientToken currentToken;
        byte[] currentTokenHash;


        public ClientDevice()
        {
            this.initialized = false;
        }


        public ClientDevice(int id, string ipAddress, byte[] hashedSaltedPW, byte[] salt)
        {
            this.id = id;
            this.ipAddress = ipAddress;
            this.hashedSaltedPW = hashedSaltedPW;
            this.salt = salt;
            initialized = true;
        }


        internal string generateNewClient(string ipAddress)
        {
            this.ipAddress = ipAddress;

            this.salt = PasswordHashUtil.generateSalt();

            string randomPassword = BitConverter.ToString(PasswordHashUtil.generateSalt());
            randomPassword = randomPassword.Replace("-", "");
            this.hashedSaltedPW = PasswordHashUtil.generateSaltedPasswordHash(randomPassword, salt);

            initialized = true;
            return randomPassword;
        }


        internal ClientToken getToken(string password)
        {
            ClientToken token = new ClientToken();

            if (this.initialized && this.isValidPassword(password))
            {
                token = new ClientToken();

                token.ID = PasswordHashUtil.getRandomLong();
                token.DeviceID = this.id;
                token.StartDateTime = DateTime.Now;
                token.EndDateTime = DateTime.Now + ClientDevice.tokenValidationTime;
                this.currentToken = token;
                this.currentTokenHash = this.createTokenHash(token);
            }
            else
            {
                SimpleLog.Log("The used LoginToken: '" + password + "' was no match to this registered client device.");
            }

            return token;
        }


        private byte[] createTokenHash(ClientToken token)
        {
            byte[] tokenHash = null;

            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter bw = new BinaryWriter(ms, System.Text.Encoding.UTF8))
                {
                    bw.Write(token.ID);
                    bw.Write(token.DeviceID);
                    bw.Write(token.StartDateTime.ToBinary());
                    bw.Write(token.EndDateTime.ToBinary());
                }

                tokenHash = PasswordHashUtil.generateSaltedPasswordHash(ms.ToArray(), new byte[0]);
            }


            return tokenHash;
        }


        internal bool isValidToken(ClientToken recievedToken)
        {
            //Check Time Constrainst
            bool isEqual = DateTime.Now > this.currentToken.StartDateTime;
            isEqual = isEqual && DateTime.Now < this.currentToken.EndDateTime;

            //Check Token equality
            byte[] recievedTokenHash = this.createTokenHash(recievedToken);
            for (int i = 0; isEqual && i < recievedTokenHash.Length && i < this.currentTokenHash[i]; i++)
            {
                isEqual = isEqual && this.currentTokenHash[i] == recievedTokenHash[i];
            }

            return isEqual;
        }

        private bool isValidPassword(string password)
        {
            if (this.initialized)
            {
                return PasswordHashUtil.validatePassword(password, this.salt, this.hashedSaltedPW);
            }
            else
            {
                throw new NullReferenceException("This ClientDevice instance is raw on data!");
            }
        }


        /*
         * GETTER
         */
        public string IpAddress
        {
            get { return ipAddress; }
        }

        public int DeviceID
        {
            get { return id; }
            set { id = value; }
        }

        public byte[] HashedSaltedPW
        {
            get { return hashedSaltedPW; }
        }

        public byte[] Salt
        {
            get { return salt; }
        }
    }
}