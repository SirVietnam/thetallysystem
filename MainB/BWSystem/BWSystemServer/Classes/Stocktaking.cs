﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace BWSystemServer
{
    public class Stocktaking
    {
        DateTime timeStamp;
        List<Tuple<int, int, int>> inventory;

        public Stocktaking(DateTime timeStamp, int articleID, int vStock, int pStock)
        {
            this.timeStamp = timeStamp;
            inventory = new List<Tuple<int, int, int>>();
            inventory.Add(new Tuple<int, int, int>(articleID, vStock, pStock));
        }


        public Stocktaking(DateTime timeStamp)
        {
            this.timeStamp = timeStamp;
            inventory = new List<Tuple<int, int, int>>();
        }


        public void addItem(int articleID, int virtualStock, int physicalStock)
        {
            Tuple<int, int, int> existingArticle = this.inventory.FirstOrDefault(x => x.Item1 == articleID);

            if (existingArticle == null)
            {
                this.inventory.Add(new Tuple<int, int, int>(articleID, virtualStock, physicalStock));
            }
        }



        public DateTime TimeStamp
        {
            get { return timeStamp; }
        }

        public List<Tuple<int, int, int>> Inventory
        {
            get { return inventory; }
            set
            {
                if (value != null)
                {
                    inventory = value;
                }
                else
                {
                    throw new ArgumentNullException("A null list is tried to set!", "Inventory");
                }
            }
        }

        public String toString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Stocktaking:");
            sb.AppendLine("DateTime: " + this.timeStamp.ToString());
            sb.AppendLine("Inventroy hast " + this.Inventory.Count() + " Articles!");

            foreach (Tuple<int, int, int> tuple in this.Inventory)
            {
                sb.AppendLine("Article ID: " + tuple.Item1);
                sb.AppendLine("-Virtual Stock: " + tuple.Item2);
                sb.AppendLine("-Physical Stock: " + tuple.Item3);
            }

            return sb.ToString();
        }
    }
}
