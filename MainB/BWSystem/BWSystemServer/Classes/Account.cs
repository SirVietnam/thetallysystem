﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemServer
{
    public class Account
    {
        private int id;
        private string surname;
        private string name;
        private string nickname;
        private string displayName;
        private string pictureURL;
        private int standardID;
        private int[] favouriteIDs = null;
        private Authentication authentication;
        private bool isSuperuser;
        private bool authenticateAlways;
        private bool isAvailable;


        public Account()
        {
            //this.isAvailable = true;
        }

        public Account(int id, string surname, string name, string nickname, string pictureURL, int standardID,
            int[] favouriteIDs, Authentication authentication, bool isSuperuser, bool authenticateAlways, bool isAvailable)
        {
            this.ID = id;
            this.Surname = surname;
            this.Name = name;
            this.Nickname = nickname;
            this.PictureURL = pictureURL;
            this.Standard = standardID;
            if (favouriteIDs != null)
            {
                this.Favourite = favouriteIDs;
            }
            this.authentication = authentication;
            this.IsSuperuser = isSuperuser;
            this.AuthenticateAlways = authenticateAlways;
            this.IsAvailable = isAvailable;
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public String Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        public String Nickname
        {
            get { return nickname; }
            set { nickname = value; }
        }

        public String PictureURL
        {
            get { return pictureURL; }
            set { pictureURL = value; }
        }

        public int Standard
        {
            get { return standardID; }
            set { standardID = value; }
        }

        public int[] Favourite
        {
            get { return favouriteIDs; }
            set {
                int favSize = (int)ServerUtil.getConfigValue("FavouriteSize", typeof(int));
                if (value.Length == favSize)
                {
                    favouriteIDs = value;
                }
                else
                {
                    throw new System.ArgumentException("Parameter has wrong size!", "favourite");
                }
                }
        }

        public Authentication Authentication
        {
            get { return authentication; }
            set { this.authentication = value; }
        }

        public Boolean IsSuperuser
        {
            get { return isSuperuser; }
            set { isSuperuser = value; }
        }


        public Boolean AuthenticateAlways
        {
            get { return authenticateAlways; }
            set { authenticateAlways = value; }
        }


        public Boolean IsAvailable
        {
            get { return isAvailable; }
            set { isAvailable = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }


        //public String toString()
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendLine("AccountID: " + this.id.ToString());
        //    sb.AppendLine("Surname: " + this.surname);
        //    sb.AppendLine("Name: " + this.name);
        //    sb.AppendLine("PictureURL: " + this.pictureURL);
        //    if (this.standardID == null)
        //    {
        //        sb.AppendLine("StandardArticle: " + "---");
        //    }
        //    else
        //    {
        //        sb.AppendLine("StandardArticle:");
        //        sb.AppendLine(" -ID:" + this.standardID);
        //        sb.AppendLine(" -Name: " + this.standardID);

        //    }
        //    sb.AppendLine("FavouriteArticle: ");
        //    for (int i = 0; this.favouriteIDs != null && i < this.favouriteIDs.Count(); i++)
        //    {
        //        if (this.favouriteIDs[i] != null)
        //        {
        //            sb.AppendLine(" -" + i.ToString() + ". Article: " + this.favouriteIDs[i]);
        //        }
        //    }
        //    if (this.authentication != null)
        //    {
        //        sb.AppendLine("Authentification: " + this.authentication.toString());
        //    }
            
        //    return sb.ToString();
        //}


    }
}
