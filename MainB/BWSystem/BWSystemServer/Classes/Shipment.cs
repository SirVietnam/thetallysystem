﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer
{
    public class Shipment
    {
        private int shipmentID;
        private string vendor;
        private List<Tuple<Article, int>> articleAmountList;
        private List<Tuple<int, int>> articleAmountListInt;
        private DateTime deliveryDatetime;
        private DateTime datetime;

        private bool useArticleList;
        private List<Article> articles;

        public Shipment(int shipmentID, string vendor, DateTime deliveryDatetime)
        {
            this.shipmentID = shipmentID;
            this.vendor = vendor;
            this.useArticleList = false;
            this.deliveryDatetime = deliveryDatetime;
            this.datetime = DateTime.Now;

            this.articleAmountList = new List<Tuple<Article, int>>();
            this.articleAmountListInt = new List<Tuple<int, int>>();
        }


        public Shipment(int shipmentID, string vendor, DateTime deliveryDatetime, DateTime datetime)
        {
            this.shipmentID = shipmentID;
            this.vendor = vendor;
            this.useArticleList = false;
            this.deliveryDatetime = deliveryDatetime;
            this.datetime = datetime;

            this.articleAmountList = new List<Tuple<Article, int>>();
            this.articleAmountListInt = new List<Tuple<int, int>>();
        }


        internal void addArticleList(List<Article> articles)
        {
            this.articles = articles;
        }


        internal void addArticleAmount(Article article, int amount)
        {
            Tuple<Article, int> guard = this.articleAmountList.FirstOrDefault(x => x.Item1.ID == article.ID);
            if (guard == null)
            {
                articleAmountList.Add(new Tuple<Article, int>(article, amount));
                this.addArticleAmount(article.ID, amount);
            }
        }


        internal void addArticleAmount(int articleID, int amount)
        {
            Tuple<int, int> guard = this.ArticleAmountListInt.FirstOrDefault(x => x.Item1 == articleID);
            if (guard == null)
            {
                ArticleAmountListInt.Add(new Tuple<int, int>(articleID, amount));

                Article article = this.articles.FirstOrDefault(x => x.ID == articleID);
                if (article != null)
                {
                    this.addArticleAmount(article, amount);
                }
            }

            if (articleAmountList.Count == ArticleAmountListInt.Count)
            {
                this.useArticleList = true;
            }
            else
            {
                this.useArticleList = false;
            }
        }



        //Getter & Setter
        public int ShipmentID
        {
            get { return shipmentID; }
        }

        public string Vendor
        {
            get { return vendor; }
        }

        public List<Tuple<Article, int>> ArticleAmountList
        {
            get { return articleAmountList; }
        }

        public List<Tuple<int, int>> ArticleAmountListInt
        {
            get { return articleAmountListInt; }
        }

        public bool UseArticleList
        {
            get { return useArticleList; }
        }

        public DateTime DeliveryDatetime
        {
            get { return deliveryDatetime; }
        }

        public DateTime Datetime
        {
            get { return datetime; }
        }
    }
}