﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.Classes
{
    public class CancellationData
    {

        int? cancellationID;
        int user;
        int cancelledBookingID;

        int? newBookingID;
        BookingData newBooking;

        DateTime cancellationTime;
        

        public CancellationData()
        {
        }

        public CancellationData(int? cancellationID, int user, int cancelledBookingID, int? newBookingID, DateTime cancellationTime)
        {
            this.cancellationID = cancellationID;
            this.user = user;
            this.cancelledBookingID = cancelledBookingID;
            this.newBookingID = newBookingID;
            this.newBooking = null;
            this.cancellationTime = cancellationTime;
        }

        public CancellationData(int? cancellationID, int user, int cancelledBookingID, BookingData newBooking, DateTime cancellationTime)
        {
            this.cancellationID = cancellationID;
            this.user = user;
            this.cancelledBookingID = cancelledBookingID;
            this.newBookingID = null;
            this.newBooking = newBooking;
            this.cancellationTime = cancellationTime;
        }


        public int? CancellationID
        {
            get { return cancellationID; }
            set { cancellationID = value; }
        }

        public int User
        {
            get { return user; }
            set { user = value; }
        }

        public int CancelledBookingID
        {
            get { return cancelledBookingID; }
            set { cancelledBookingID = value; }
        }

        public int? NewBookingID
        {
            get { return newBookingID; }
            set { newBookingID = value; }
        }

        public BookingData NewBooking
        {
            get { return newBooking; }
            set { newBooking = value; }
        }


        public DateTime CancellationTime
        {
            get { return cancellationTime; }
            set { cancellationTime = value; }
        }

        //public void addCancelData(int article, int amount)
        //{
        //    this.NewBookingID.Add(new TupleAA(article, amount));
        //}
    }
}