﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemServer
{
    public class Event
    {

        private int eventID;
        private int creatorAccountID;
        private String name;
        private DateTime begin;
        private DateTime end;
        private int[] costAbsorption;
        private bool articlesExclusive;
        private double? eventCosts;

        private Dictionary<int, int> articleToIndex;
        private int[] articleAmount;
        private bool eventIsCalculated;


        public Event()
        {
            this.eventID = -1;
            this.costAbsorption = new int[0];
            articlesExclusive = false;
            this.eventIsCalculated = false;
        }


        public Event(int eventID, int creatorID, String name, DateTime begin, DateTime end, int[] costAbsorption, bool articlesExclusive, double? eventCosts)
        {
            this.eventID = eventID;
            this.creatorAccountID = creatorID;
            this.name = name;
            this.begin = begin;
            this.end = end;
            this.costAbsorption = costAbsorption;
            this.articlesExclusive = articlesExclusive;
            this.eventCosts = eventCosts;
            this.eventIsCalculated = false;
        }

        public int EventID
        {
            get { return eventID; }
            set { eventID = value; }
        }

        public int CreatorAccountID
        {
            get { return creatorAccountID; }
            set { creatorAccountID = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public DateTime Begin
        {
            get { return begin; }
            set { begin = value; }
        }

        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }

        public int[] CostAbsorption
        {
            get { return costAbsorption; }
            set { costAbsorption = value; }
        }

        public double? EventCosts
        {
            get { return eventCosts; }
            set { eventCosts = value; }
        }

        public bool ArticlesExclusive
        {
            get { return articlesExclusive; }
            set { articlesExclusive = value; }
        }


        internal bool eventCalculated()
        {
            return this.eventIsCalculated;
        }


        internal int getAmount(int articleID)
        {
            int amount = -1;

            if (this.eventCalculated())
            {
                int articleIndex = this.articleToIndex[articleID];
                amount = this.articleAmount[articleIndex];
            }

            return amount;
        }



        internal void makeEventSettlement(List<BookingData> bookings, List<Article> allArticles, List<Account> allAccounts)
        {
            this.initSupportStructures(allArticles);
            this.calculateCost(bookings, allArticles);


            this.eventIsCalculated = true;
        }


        internal void writeToExcelFile(string filePath, List<BookingData> bookings, List<Article> allArticles, List<Account> allAccounts)
        {
            if (this.eventIsCalculated)
            {
                using (SLDocument document = new SLDocument())
                {
                    SLStyle styleBold = new SLStyle();
                    styleBold.SetFontBold(true);

                    addOverview(document, allAccounts, styleBold);
                    addBookings(document, styleBold, bookings, allArticles, allAccounts);
                    addArticles(document, styleBold, allArticles);

                    document.SaveAs(filePath);
                }
            }
            else
            {
                SimpleLog.Log("Call EventSettlment was not made befor saving and there for aborted.");
            }

        }


        private void addOverview(SLDocument document, List<Account> allAccounts, SLStyle styleBold)
        {
            bool added = document.AddWorksheet("Overview");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }
            SLStyle styleNumber = new SLStyle();
            styleNumber.FormatCode = "0.00";
            SLStyle styleDateTime = new SLStyle();
            styleDateTime.FormatCode = "yyyy-MM-dd HH:mm:ss";

            int currentRow = 1;
            document.SetCellValue(currentRow, 1, "Event ID");
            document.SetCellValue(currentRow, 2, this.EventID);
            currentRow++;
            document.SetCellValue(currentRow, 1, "Event Name");
            document.SetCellValue(currentRow, 2, this.Name);
            currentRow++;
            document.SetCellValue(currentRow, 1, "Event Start");
            document.SetCellValue(currentRow, 2, this.Begin);
            document.SetCellStyle(currentRow, 2, styleDateTime);
            currentRow++;
            document.SetCellValue(currentRow, 1, "Event End");
            document.SetCellValue(currentRow, 2, this.End);
            document.SetCellStyle(currentRow, 2, styleDateTime);
            currentRow++;
            document.SetCellValue(currentRow, 1, "Event Created By");
            document.SetCellValue(currentRow, 2, ServerUtil.accountToDisplayWebName(allAccounts.First(x => x.ID == this.CreatorAccountID)));
            currentRow++;
            document.SetCellValue(currentRow, 1, "Articles are Exclusive");
            document.SetCellValue(currentRow, 2, this.ArticlesExclusive ? "YES" : "NO");
            currentRow++;
            document.SetCellValue(currentRow, 1, "Event Cost");
            document.SetCellValue(currentRow, 2, (double)this.eventCosts);
            document.SetCellStyle(currentRow, 2, styleNumber);

            document.SetColumnStyle(1, styleBold);
        }



        private void addBookings(SLDocument document, SLStyle styleBold, List<BookingData> bookings, List<Article> allArticles, List<Account> allAccounts)
        {
            bool added = document.AddWorksheet("Bookings");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }
            SLStyle styleTime = new SLStyle();
            styleTime.FormatCode = "HH:mm:ss";


            Dictionary<int, Article> articleDict = new Dictionary<int, Article>();
            foreach (Article article in allArticles)
            {
                articleDict.Add(article.ID, article);
            }

            Dictionary<int, Account> accountDict = new Dictionary<int, Account>();
            foreach (Account account in allAccounts)
            {
                accountDict.Add(account.ID, account);
            }

            int currentRow = 1;
            int currentColumn = 1;

            document.SetCellValue(currentRow, currentColumn++, "Booking ID");
            document.SetCellValue(currentRow, currentColumn++, "Booked By");
            document.SetCellValue(currentRow, currentColumn++, "Article");
            document.SetCellValue(currentRow, currentColumn++, "Amount");
            document.SetCellValue(currentRow, currentColumn++, "Time");
            currentRow++;
            currentColumn = 1;

            foreach (BookingData booking in bookings.OrderBy(x => x.TimeStamp))
            {
                foreach (BookingAA data in booking.Booking)
                {
                    document.SetCellValue(currentRow, currentColumn++, (int)booking.ID);
                    string accountString = booking.User == null ? "--" : accountDict[(int)booking.User].Name;
                    document.SetCellValue(currentRow, currentColumn++, accountString);
                    document.SetCellValue(currentRow, currentColumn++, articleDict[data.Article].Name);
                    document.SetCellValue(currentRow, currentColumn++, data.Amount);

                    document.SetCellValue(currentRow, currentColumn, booking.TimeStamp);
                    document.SetCellStyle(currentRow, currentColumn++, styleTime);

                    currentRow++;
                    currentColumn = 1;
                }
            }
        }


        private void addArticles(SLDocument document, SLStyle styleBold, List<Article> allArticles)
        {
            bool added = document.AddWorksheet("Articles");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }
            SLStyle styleNumber = new SLStyle();
            styleNumber.FormatCode = "0.00";

            int currentRow = 1;
            int currentColumn = 1;

            document.SetRowStyle(currentRow, styleBold);
            document.SetCellValue(currentRow, currentColumn++, "Article ID");
            document.SetCellValue(currentRow, currentColumn++, "Article Name");
            document.SetColumnStyle(currentColumn, styleNumber);
            document.SetCellValue(currentRow, currentColumn++, "Article Size");
            document.SetColumnStyle(currentColumn, styleNumber);
            document.SetCellValue(currentRow, currentColumn++, "Article Prize");
            document.SetCellValue(currentRow, currentColumn++, "Articles Booked");
            currentRow++;
            currentColumn = 1;


            IEnumerable<Article> payedForArticle = allArticles.Where(x => this.CostAbsorption.Contains(x.ID)).OrderBy(x => x.ID);
            document.MergeWorksheetCells(currentRow, currentColumn, currentRow, currentColumn + 3);
            document.SetRowStyle(currentRow, styleBold);
            document.SetCellValue(currentRow++, currentColumn, "Articles payed for by event");

            foreach (Article article in payedForArticle)
            {
                document.SetCellValue(currentRow, currentColumn++, article.ID);
                document.SetCellValue(currentRow, currentColumn++, article.Name);
                document.SetCellValue(currentRow, currentColumn++, article.Size);
                document.SetCellValue(currentRow, currentColumn++, article.Prize);
                document.SetCellValue(currentRow, currentColumn++, this.articleAmount[this.articleToIndex[article.ID]]);

                currentRow++;
                currentColumn = 1;
            }

            if (!this.ArticlesExclusive)
            {
                IEnumerable<Article> notPayedForArticle = allArticles.Where(x => !this.CostAbsorption.Contains(x.ID) && x.IsAvailable).OrderBy(x => x.ID);
                document.MergeWorksheetCells(currentRow, currentColumn, currentRow, currentColumn + 3);
                document.SetRowStyle(currentRow, styleBold);
                document.SetCellValue(currentRow++, currentColumn, "Articles NOT payed for by event");

                foreach (Article article in notPayedForArticle)
                {
                    document.SetCellValue(currentRow, currentColumn++, article.ID);
                    document.SetCellValue(currentRow, currentColumn++, article.Name);
                    document.SetCellValue(currentRow, currentColumn++, article.Size);
                    document.SetCellValue(currentRow, currentColumn++, article.Prize);
                    document.SetCellValue(currentRow, currentColumn++, "--");

                    currentRow++;
                    currentColumn = 1;
                }
            }
        }



        private void calculateCost(List<BookingData> bookings, List<Article> allArticles)
        {
            Dictionary<int, Article> articleDict = new Dictionary<int, Article>();
            foreach (Article article in allArticles)
            {
                articleDict.Add(article.ID, article);
            }

            this.articleAmount = new int[allArticles.Count];

            foreach (BookingData booking in bookings)
            {
                foreach (BookingAA data in booking.Booking)
                {
                    this.articleAmount[this.articleToIndex[data.Article]] += data.Amount;
                }
            }

            this.eventCosts = 0.0;
            foreach (Article article in allArticles)
            {
                double amount = this.articleAmount[this.articleToIndex[article.ID]];
                this.eventCosts += amount * article.Prize;
            }
        }


        private void initSupportStructures(List<Article> articles)
        {
            this.articleToIndex = new Dictionary<int, int>();
            for (int i = 0; i < articles.Count; i++)
            {
                articleToIndex.Add(articles[i].ID, i);
            }
        }
    }
}
