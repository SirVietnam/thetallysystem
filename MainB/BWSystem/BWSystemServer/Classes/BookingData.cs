﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BWSystemServer
{

    public class BookingData : ISerializable, ICloneable
    {
        private int? id;
        private int? userID;
        private int consumerID;
        private List<BookingAA> bookings;
        private DateTime timeStamp;
        private int? eventID;
        private bool dataCancelled;


        public BookingData()
        {
        }

        public BookingData(int? userID, int customerID)
        {
            this.User = userID;
            this.Customer = customerID;
            this.Booking = new List<BookingAA>();
            this.TimeStamp = new DateTime();
        }

        public BookingData(int? id, int? userID, int customerID, List<BookingAA> booking,
            DateTime timeStamp, int? eventID, bool dataCancelled)
        {
            this.ID = id;
            this.User = userID;
            this.Customer = customerID;
            this.Booking = booking;
            this.TimeStamp = timeStamp;
            this.EventBooking = eventID;
            this.DataCancelled = dataCancelled;
        }

        public int? ID
        {
            get { return id; }
            set { id = value; }
        }

        public int? User
        {
            get { return userID; }
            set { userID = value; }
        }

        public int Customer
        {
            get { return consumerID; }
            set { this.consumerID = value; }
        }

        public List<BookingAA> Booking
        {
            get { return this.bookings; }
            set { this.bookings = value; }
        }

        public DateTime TimeStamp
        {
            get { return this.timeStamp; }
            set { this.timeStamp = value; }
        }

        public int? EventBooking
        {
            get { return eventID; }
            set { this.eventID = value; }
        }


        public bool DataCancelled
        {
            get { return dataCancelled; }
            set { dataCancelled = value; }
        }



        public String toString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("BookingData contains the following Data:");
            if (this.ID != null)
            {
                sb.AppendLine("ID: " + this.id);
            }
            sb.AppendLine("User: ");
            if (this.User != null)
            {
                sb.AppendLine(" " + this.User);
            }
            sb.AppendLine("Customer: " + this.Customer);
            sb.AppendLine("Event: ");
            if (this.EventBooking != null)
            {
                sb.AppendLine(" " + this.EventBooking);
            }
            sb.AppendLine("TimeStamp: " + this.TimeStamp.ToString());
            sb.AppendLine("Parts got Cancelled: " + this.DataCancelled.ToString());
            sb.AppendLine("Bookings: ");
            foreach (BookingAA tuple in this.Booking)
            {
                sb.AppendLine(" Article: " + tuple.Article + " --- Amount: " + tuple.Article.ToString());
                //if (tuple.Cancelled != null)
                //{
                //    sb.AppendLine("   --- CancellationID: " + tuple.Cancelled);
                //}
            }
            return sb.ToString();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }

        internal bool changeBooking(int articleID, int amount)
        {
            bool replaced = false;
            for (int i = 0; i < this.Booking.Count() && !replaced; i++)
            {
                if (this.Booking[i].Article == articleID)
                {
                    this.Booking[i].Amount = amount;
                    replaced = true;
                }
            }

            if (!replaced)
            {
                this.Booking.Add(new BookingAA(articleID, amount));
                replaced = true;
            }

            this.TimeStamp = new DateTime(DateTime.Now.Ticks);
            return replaced;
        }

        public object Clone()
        {
            List<BookingAA> bookingsClone = new List<BookingAA>();
            foreach (BookingAA booking in this.bookings)
            {
                bookingsClone.Add((BookingAA)booking.Clone());
            }

            return new BookingData(this.ID, this.userID, this.Customer, bookingsClone, this.timeStamp, this.eventID, this.dataCancelled);
        }
    }
}
