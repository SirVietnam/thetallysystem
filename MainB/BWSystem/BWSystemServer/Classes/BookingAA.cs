﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer
{
    public class BookingAA : ICloneable
    {
        private int articleID;
        private int amount;

        public BookingAA()
        {
        }


        public BookingAA(int articleID, int amount)
        {
            this.Article = articleID;
            this.Amount = amount;
        }


        public int Article
        {
            get { return articleID; }
            set { articleID = value; }
        }

        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public object Clone()
        {
            return new BookingAA(this.articleID, this.amount);
        }
    }
}