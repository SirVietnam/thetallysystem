﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemServer
{
    public class RegAuthentication : Authentication
    {
        private String login;

        public RegAuthentication()
        {
        }

        public RegAuthentication(String login, Int64[] cardCodes, int[] securityCode)
            : base(securityCode, cardCodes)
        {
            this.login = login;
        }

        public RegAuthentication(String login)
        {
            this.login = login;
        }

        public String Login
        {
            get { return login; }
            set { login = value; }
        }

        //new public int[] SecurityCode
        //{
        //    get { return securityCode; }
        //    set { securityCode = value; }
        //}
    }
}
