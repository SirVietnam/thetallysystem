﻿using BWSystemServer;
using System;
using System.Runtime.Serialization;

namespace BWSystemServer
{
    public class Authentication : ISerializable
    {
        private int[] securityCode;

        private Int64[] cardCodes;
        private byte[] securityCodeSalt;
        private byte[] securityCodeHash;
        private String loginName;

        public Authentication()
        {
        }



        public Authentication(int[] securityCode)
        {
            this.securityCode = securityCode;
            if (securityCode.Length < (int)ServerUtil.getConfigValue("MinSecurityCodeCount", typeof(int)))
            {
                throw new ArgumentException("SecurityCode is to short!", "securityCode");
            }
        }


        public Authentication(byte[] securityCodeHash, byte[] securityCodeSalt, Int64[] cardCodes, String loginName)
        {
            this.securityCodeHash = securityCodeHash;
            this.securityCodeSalt = securityCodeSalt;
            this.cardCodes = cardCodes;
            this.loginName = loginName;
        }


        public Authentication(int[] securityCode, Int64[] cardCodes)
        {
            this.securityCode = securityCode;
            this.cardCodes = cardCodes;
        }



        public bool validCode(long uncheckedCardCode)
        {
            bool isValidCode = false;

            if (cardCodes != null)
            {
                foreach (Int64 cardCode in this.cardCodes)
                {
                    if (cardCode == uncheckedCardCode)
                    {
                        isValidCode = true;
                    }
                }
            }
            return isValidCode;
        }

        public bool validSecurityCode(string password)
        {
            return PasswordHashUtil.validatePassword(password, this.securityCodeSalt, this.securityCodeHash);
        }


        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }


        public int[] SecurityCode
        {
            get { return securityCode; }
            set { securityCode = value; }
        }

        public Int64[] CardCode
        {
            get { return cardCodes; }
            set { cardCodes = value; }
        }

        public string LoginName
        {
            get { return loginName; }
            set { loginName = value; }
        }

        public byte[] SecurityCodeHash
        {
            get { return securityCodeHash; }
            set { securityCodeHash = value; }
        }

        public byte[] SecurityCodeSalt
        {
            get { return securityCodeSalt; } 
            set { securityCodeSalt = value; }
        }

        //public String toString()
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("SecurityCode: ");
        //    for (int i = 0; i < this.securityCode.Count(); i++)
        //    {
        //        sb.Append(securityCode[i] + "-");
        //    }
        //    sb.Append("\n");
        //    return sb.ToString();
        //}
    }
}
