﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemServer
{
    public class Article : ISerializable, ICloneable/*, IComparable*/
    {
        public enum ArticleType
        {
            Beer,
            Water,
            Wine,
            SparklingWine,
            Softdrink,
            Mixdrink,
            Sweets,
            Inventory,
            Others,
        }

        private int id;
        private String name;
        private ArticleType type;
        private double prize;
        private double size;
        private int stock;
        private double alcoholicStrength;
        private String manufacturer;
        private String pictureURL;
        private String description;
        private Boolean isInventory;
        private Boolean isAvailable;

        public Article()
        {
            this.ID = -1;
        }

        public Article(String name, double prize, double size)
        {
            this.ID = -1;
            this.name = name;
            this.prize = prize;
            this.size = size;
        }


        public Article(int id, String name, ArticleType type, double prize, double size, int stock, double alcoholicStrength,
            String manufacturer, String pictureURL, String description, Boolean isInventory, Boolean isAvailable)
        {
            this.id = id;
            this.name = name;
            this.type = type;
            this.prize = prize;
            this.size = size;
            this.stock = stock;
            this.alcoholicStrength = alcoholicStrength;
            this.manufacturer = manufacturer;
            this.pictureURL = pictureURL;
            this.description = description;
            this.isInventory = isInventory;
            this.isAvailable = isAvailable;
        }


        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public ArticleType Type
        {
            get { return type; }
            set { type = value; }
        }

        public double Prize
        {
            get { return prize; }
            set { prize = value; }
        }

        public double Size
        {
            get { return size; }
            set { size = value; }
        }

        public int Stock
        {
            get { return stock; }
            set { stock = value; }
        }

        public double AlcoholicStrength
        {
            get { return alcoholicStrength; }
            set { alcoholicStrength = value; }
        }

        public String Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }

        public String PictureURL
        {
            get { return pictureURL; }
            set { pictureURL = value; }
        }

        public String Description
        {
            get { return description; }
            set { description = value; }
        }

        public Boolean IsInventory
        {
            get { return isInventory; }
            set { isInventory = value; }
        }

        public Boolean IsAvailable
        {
            get { return isAvailable; }
            set { isAvailable = value; }
        }

        public String toString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Article with ID: " + this.ID + "\n");
            sb.Append("Name: " + this.Name + "\n");
            sb.Append("Type: " + this.Type + "\n");
            sb.Append("Prize: " + this.Prize + "\n");
            sb.Append("Size: " + this.Size + "\n");
            sb.Append("Stock: " + this.Stock + "\n");
            sb.Append("AlcoholicStrength: " + this.AlcoholicStrength + "\n");
            sb.Append("Manufacturer: " + this.Manufacturer + "\n");
            sb.Append("PictureURL: " + this.PictureURL + "\n");
            sb.Append("Description: " + this.Description + "\n");
            sb.Append("IsInventory: " + this.IsInventory.ToString() + "\n");
            sb.Append("IsAvailable: " + this.IsAvailable.ToString() + "\n");
            return sb.ToString();
        }


        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }

        //public int CompareTo(Object obj)
        //{
        //    int returnVal = -1;

        //    if (obj is Article)
        //    {
        //        Article art = (Article)obj;
        //        if (this.Stock < art.Stock)
        //        {
        //            returnVal = 1;
        //        }
        //        else if (this.Stock == art.Stock)
        //        {
        //            returnVal = 0;
        //        }
        //    }
        //    else
        //    {
        //        throw new ArgumentException("Tried to compare Article to an illegal object!", "obj");
        //    }

        //    return returnVal;
        //}

        public object Clone()
        {
            return new Article(this.ID, this.Name, this.Type, this.Prize, this.Size, this.Stock, this.AlcoholicStrength, this.Manufacturer, this.PictureURL, this.Description, this.IsInventory, this.IsAvailable);
        }
    }
}

