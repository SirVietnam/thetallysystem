﻿using Microsoft.Office.Interop.Excel;
using Microsoft.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SpreadsheetLight;
using BWSystemServer.BIZ.DatabaseAccessManagement;

namespace BWSystemServer
{
    public class Settlement
    {
        private int settlementID;
        private DateTime startDate;
        private DateTime endDate;
        private double settlmentInvoiceValue;

        private bool bookingDataAdded;

        private List<Article> articles;
        private Dictionary<int, int> articleToIndex;

        private List<Account> accounts;
        private Dictionary<int, int> accountToIndex;
        private int[,] user_article_table;
        private double[,] userInvoiceTotal;


        public Settlement(int ID, DateTime startDate, DateTime endDate)
        {
            this.settlementID = ID;
            this.startDate = startDate;
            this.endDate = endDate;
            this.bookingDataAdded = false;
        }


        internal void addSettlementData(List<BookingData> bookings, List<Account> accounts, List<Article> articles)
        {
            this.bookingDataAdded = true;
            this.articles = articles;
            this.articleToIndex = new Dictionary<int, int>();
            for (int i = 0; i < articles.Count; i++)
            {
                articleToIndex.Add(articles[i].ID, i);
            }

            this.accounts = accounts;
            this.accountToIndex = new Dictionary<int, int>();
            for (int i = 0; i < accounts.Count; i++)
            {
                accountToIndex.Add(accounts[i].ID, i);
            }

            this.user_article_table = new int[accounts.Count, articles.Count];

            this.makeBookings(bookings);
            this.createPayments();
        }


        private void makeBookings(List<BookingData> bookings)
        {
            foreach (BookingData booking in bookings)
            {
                if (!booking.DataCancelled)
                {
                    int consumerIndex = this.accountToIndex[booking.Customer];

                    foreach (BookingAA bookingTuple in booking.Booking)
                    {
                        int articleIndex = this.articleToIndex[bookingTuple.Article];
                        this.user_article_table[consumerIndex, articleIndex] += bookingTuple.Amount;
                    }
                }
            }
        }


        private void createPayments()
        {
            this.settlmentInvoiceValue = 0;
            this.userInvoiceTotal = new double[accounts.Count, articles.Count + 1];
            foreach (Account user in accounts)
            {
                double sum = 0.0;
                int userIndex = this.accountToIndex[user.ID];

                foreach (Article article in articles)
                {
                    int articleIndex = this.articleToIndex[article.ID];
                    double amount = (double)this.user_article_table[userIndex, articleIndex];
                    double articleValue = amount * article.Prize;
                    this.userInvoiceTotal[userIndex, articleIndex] = articleValue;
                    sum += articleValue;
                }

                this.userInvoiceTotal[userIndex, articles.Count] = sum;
                this.settlmentInvoiceValue += sum;
            }
        }

        /*
         * 
         */
        public double getPayment(Account account)
        {
            int userIndex = this.accountToIndex[account.ID];
            return this.userInvoiceTotal[userIndex, this.articles.Count];
        }


        public Tuple<int, double>[] getPlaymentDetails(Account account)
        {
            Tuple<int, double>[] paymentDetails = new Tuple<int, double>[this.articles.Count];
            int userIndex = this.accountToIndex[account.ID];

            foreach (Article article in articles)
            {
                int articleIndex = this.articleToIndex[article.ID];
                double amount = (double)this.user_article_table[userIndex, articleIndex];
                double articleValue = amount * article.Prize;
                paymentDetails[articleIndex] = new Tuple<int, double>(article.ID, articleValue);
            }

            return paymentDetails;
        }


        public List<Tuple<int, int>> getUserArticles(Account user)
        {
            List<Tuple<int, int>> articlesAmount = new List<Tuple<int, int>>();

            int userIndex = this.accountToIndex[user.ID];
            for (int i = 0; i < this.articles.Count; i++)
            {
                int articleID = this.articleToIndex.FirstOrDefault(x => x.Value == i).Key;
                articlesAmount.Add(new Tuple<int, int>(articleID, user_article_table[userIndex, i]));
            }

            return articlesAmount;
        }


        public int getArticlesSold(Article article)
        {
            int sum = 0;
            int articleIndex = this.articleToIndex[article.ID];
            for (int i = 0; i < this.user_article_table.GetLength(0); i++)
            {
                sum += this.user_article_table[i, articleIndex];
            }
            return sum;
        }


        internal bool writeToExcelFile(string filePath)
        {
            if (!this.bookingDataAdded)
            {
                throw new ArgumentException("There seam to be no booking data added! Make sure booking data are added BEFOR writing the settlement to file");
            }
            bool written = true;

            using (SLDocument document = new SLDocument())
            {
                SLStyle styleBold = new SLStyle();
                styleBold.SetFontBold(true);

                this.fillOverviewWS(document, styleBold);
                this.fillUserWS(document, styleBold);
                this.fillArticlesWS(document, styleBold);
                this.fillNonOrgAccountWS(document, styleBold);

                document.SaveAs(filePath);
            }

            return written;
        }



        private void fillOverviewWS(SLDocument overviewWS, SLStyle styleBold)
        {
            bool added = overviewWS.AddWorksheet("Overview");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }
            int row = 1;
            overviewWS.SetCellValue(row, 1, "Settlement ID");
            overviewWS.SetCellValue(row, 2, this.SettlementID);
            row++;
            overviewWS.SetCellValue(row, 1, "Start Date");
            overviewWS.SetCellValue(row, 2, ServerUtil.DateTimeToString(this.startDate));
            row++;
            overviewWS.SetCellValue(row, 1, "End Date");
            overviewWS.SetCellValue(row, 2, ServerUtil.DateTimeToString(this.endDate));
            row++;
            overviewWS.SetCellValue(row, 1, "Settlement Invoice Value");
            overviewWS.SetCellValue(row, 2, this.settlmentInvoiceValue);
            row++;

            overviewWS.SetColumnStyle(1, styleBold);
            overviewWS.AutoFitColumn(1);
        }



        private void fillUserWS(SLDocument usersWS, SLStyle styleBold)
        {
            bool added = usersWS.AddWorksheet("UserConsumtions");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }
            bool useAutoSettlement = (bool)ServerUtil.getConfigValue("UseAutoSettlement", typeof(bool));

            int currentColumn = 1;
            usersWS.MergeWorksheetCells(1, currentColumn, 2, currentColumn + 1);
            usersWS.SetCellValue(1, currentColumn, "User + ID\\Article + ID");

            currentColumn += 2;

            usersWS.SetCellValue(1, currentColumn++, "Invoice Total");
            usersWS.SetCellValue(1, currentColumn++, "AutoBooking");

            //int currentColumn = 3;
            foreach (Article article in this.articles)
            {
                usersWS.SetCellValue(1, currentColumn, article.Name);
                usersWS.SetCellValue(2, currentColumn, article.ID);
                currentColumn++;
            }

            int currentRow = 3;
            foreach (Account user in accounts)
            {
                currentColumn = 1;
                usersWS.SetCellValue(currentRow, currentColumn++, user.ID);

                string userNickName = user.Nickname != null ? " v/o " + user.Nickname : "";
                usersWS.SetCellValue(currentRow, currentColumn++, user.Name + ", " + user.Surname + userNickName);

                int userIndex = this.accountToIndex[user.ID];
                usersWS.SetCellValue(currentRow, currentColumn++, this.userInvoiceTotal[userIndex, this.articles.Count]);

                string autoSettlementValue = "";
                SLStyle autoSettlementStyle = new SLStyle();
                if (useAutoSettlement)
                {
                    if (DAM_NonOrgLogin.isNonOrgAccount(user.ID))
                    {
                        autoSettlementValue = "---";
                        autoSettlementStyle.Font.FontColor = System.Drawing.Color.Yellow;
                    }
                    else
                    {
                        autoSettlementValue = "Done";
                        autoSettlementStyle.Font.FontColor = System.Drawing.Color.Green;
                    }
                    usersWS.SetCellStyle(currentRow, currentColumn, autoSettlementStyle);
                }

                usersWS.SetCellValue(currentRow, currentColumn++, autoSettlementValue);

                foreach (Article article in this.articles)
                {
                    int articleIndex = this.articleToIndex[article.ID];
                    usersWS.SetCellValue(currentRow, currentColumn++, user_article_table[userIndex, articleIndex]);
                    //currentColumn++;
                }
                currentRow++;
            }

            usersWS.SetCellStyle(1, 1, styleBold);
            usersWS.SetCellStyle(2, 1, styleBold);
            usersWS.SetCellStyle(1, 1, styleBold);
            usersWS.SetCellStyle(1, 2, styleBold);
            usersWS.AutoFitRow(2);
        }



        private void fillArticlesWS(SLDocument articlesWS, SLStyle styleBold)
        {
            bool added = articlesWS.AddWorksheet("Articles");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }
            int currentRow = 1;
            articlesWS.SetCellValue(currentRow, 1, "ID");
            articlesWS.SetCellValue(currentRow, 2, "Name");
            articlesWS.SetCellValue(currentRow, 3, "Prize");
            articlesWS.SetCellValue(currentRow, 4, "Size");
            currentRow++;

            foreach (Article article in this.articles)
            {
                articlesWS.SetCellValue(currentRow, 1, article.ID);
                articlesWS.SetCellValue(currentRow, 2, article.Name);
                articlesWS.SetCellValue(currentRow, 3, article.Prize);
                articlesWS.SetCellValue(currentRow, 4, article.Size);
                currentRow++;
            }

            articlesWS.SetRowStyle(1, styleBold);
        }


        private void fillNonOrgAccountWS(SLDocument openBillsWS, SLStyle styleBold)
        {
            bool added = openBillsWS.AddWorksheet("Open Bills");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }
            SLStyle prizeStyle = new SLStyle();
            prizeStyle.FormatCode = "0.00";

            int currentRow = 1;
            openBillsWS.SetCellValue(currentRow, 1, "ID");
            openBillsWS.SetCellValue(currentRow, 2, "Name");
            openBillsWS.SetCellValue(currentRow, 3, "Amount");
            openBillsWS.SetCellValue(currentRow, 4, "Login");
            openBillsWS.SetCellValue(currentRow, 5, "Contact");
            openBillsWS.SetRowStyle(currentRow, styleBold);
            currentRow++;

            foreach (Account account in this.accounts.Where(x => DAM_NonOrgLogin.isNonOrgAccount(x.ID)))
            {
                this.fillUser(openBillsWS, styleBold, prizeStyle, currentRow, account);
                currentRow++;
            }
                
        }



        private void fillUser(SLDocument openBillsWS, SLStyle styleBold, SLStyle prizeStyle, int currentRow, Account account)
        {
            openBillsWS.SetCellValue(currentRow, 1, account.ID);

            string userNickName = account.Nickname != null ? " v/o " + account.Nickname : "";
            openBillsWS.SetCellValue(currentRow, 2, account.Name + ", " + account.Surname + userNickName);

            openBillsWS.SetCellValue(currentRow, 3, this.getPayment(account));
            openBillsWS.SetCellStyle(currentRow, 3, prizeStyle);

            openBillsWS.SetCellValue(currentRow, 4, account.Authentication.LoginName);
            openBillsWS.SetCellValue(currentRow, 5, DAM_NonOrgLogin.getMailAddress(account.ID));
        }



        internal void addFailedAutoSettlements(string settlementFilePath, List<Account> failedUsers)
        {
            using (SLDocument userWS = new SLDocument(settlementFilePath, "UserConsumtions"))
            {
                SLStyle errorStyle = new SLStyle();
                errorStyle.SetFontBold(true);
                errorStyle.SetFontColor(System.Drawing.Color.Red);

                foreach (Account failedUser in failedUsers)
                {
                    bool userUpdated = false;
                    int currentRow = 3;
                    while (!userUpdated && userWS.GetCellValueAsString(currentRow, 1) != null && userWS.GetCellValueAsString(currentRow, 1) != "")
                    {
                        if (userWS.GetCellValueAsInt32(currentRow, 1) == failedUser.ID)
                        {
                            userWS.SetCellValue(currentRow, 4, "ERROR");
                            userWS.SetCellStyle(currentRow, 4, errorStyle);
                            userUpdated = true;
                        }
                        currentRow++;
                    }
                }

                userWS.Save();
            }
        }


        // ----- Getter & Setter
        public bool BookingDataAdded
        {
            get
            {
                return bookingDataAdded;
            }
        }

        public int SettlementID
        {
            get
            {
                return settlementID;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
        }
    }
}