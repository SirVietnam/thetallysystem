﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.Classes
{
    public class NonOrgAuthentication
    {
        private int accountID;
        private byte[] passwordSalt;
        private byte[] passwordHash;
        private string mailAddress;


        public NonOrgAuthentication(int accountID, byte[] passwordSalt, byte[] passwordHash, string mailAddress)
        {
            this.accountID = accountID;
            this.passwordSalt = passwordSalt;
            this.passwordHash = passwordHash;
            this.mailAddress = mailAddress;
        }


        public int AccountID
        {
            get { return accountID; }
            set { accountID = value; }
        }

        public byte[] PasswordSalt
        {
            get { return passwordSalt; }
            set { passwordSalt = value; }
        }

        public byte[] PasswordHash
        {
            get { return passwordHash; }
            set { passwordHash = value; }
        }

        public string MailAddress
        {
            get { return mailAddress; }
            set { mailAddress = value; }
        }
    }
}