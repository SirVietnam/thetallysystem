﻿using Microsoft.Office.Interop.Excel;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace BWSystemServer
{
    public class AnnualStatement
    {
        private int annualStatementID;
        private string annualStatementName;
        private DateTime startDate;
        private DateTime endDate;

        private List<Article> articles;
        private Dictionary<int, int> articleToIndex;

        private List<Account> accounts;
        private Dictionary<int, int> accountToIndex;

        private Shipment[] shipments;
        private int[] shipmentsByArticle;

        private Settlement[] settlements;
        private int[] set_soldByArticleAmount;
        private double[] set_soldByArticlePrize;

        private Stocktaking[] stocktakings;
        private int[] lossByArticle;
        private double[] lossByArticlePrize;

        private Event[] events;
        private int[] event_soldByArticleAmount;
        private double[] event_soldByArticlePrize;

        private Stocktaking initialStocktaking;
        private double intialStockValue;
        private Stocktaking finalStocktaking;
        private double finalStockValue;


        public AnnualStatement(string name, DateTime startDate, DateTime endDate)
        {
            this.annualStatementID = int.MinValue;
            this.annualStatementName = name;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public AnnualStatement(int ID, string name, DateTime startDate, DateTime endDate)
        {
            this.annualStatementID = ID;
            this.annualStatementName = name;
            this.startDate = startDate;
            this.endDate = endDate;
        }


        public void addData(List<Article> articles, List<Account> accounts, Shipment[] shipments, Settlement[] settlements, Stocktaking[] stocktakings, Event[] events, Stocktaking initialStock)
        {
            this.initSupportStructures(articles, accounts);

            foreach (Settlement settlement in settlements)
            {
                if (!settlement.BookingDataAdded)
                {
                    string exText = "The settlment you tried to commit to the Annual Statement is not initialized (no booking data were added)!";
                    SimpleLog.Log(exText);
                    Exception ex = new ArgumentException(exText);
                    throw ex;
                }
            }

            this.addShipments(shipments);
            this.addStocktackings(stocktakings);
            this.addSettlements(settlements);
            this.addEvents(events);

            this.initialStocktaking = initialStock;
            this.intialStockValue = this.calcualteStockValue(this.initialStocktaking);
            this.finalStocktaking = stocktakings.OrderByDescending(x => x.TimeStamp.Ticks).First();
            this.finalStockValue = this.calcualteStockValue(this.finalStocktaking);
        }


        private void initSupportStructures(List<Article> articles, List<Account> accounts)
        {
            this.articles = articles;
            this.accounts = accounts;

            this.articleToIndex = new Dictionary<int, int>();
            for (int i = 0; i < articles.Count; i++)
            {
                articleToIndex.Add(articles[i].ID, i);
            }

            this.accountToIndex = new Dictionary<int, int>();
            for (int i = 0; i < accounts.Count; i++)
            {
                accountToIndex.Add(accounts[i].ID, i);
            }
        }


        private void addStocktackings(Stocktaking[] stocktakingsParam)
        {
            this.stocktakings = stocktakingsParam;
            this.lossByArticle = new int[this.articles.Count];
            this.lossByArticlePrize = new double[this.articles.Count];

            Parallel.ForEach(this.stocktakings, (stocktaking) =>
            {
                foreach (Tuple<int, int, int> tuple in stocktaking.Inventory)
                {
                    int articleIndex = articleToIndex[tuple.Item1];
                    int loss = tuple.Item2 - tuple.Item3;
                    Interlocked.Add(ref this.lossByArticle[articleIndex], loss);

                    double lossPrize = (double)loss * articles.First(x => x.ID == tuple.Item1).Prize;
                    double preValue;
                    double postValue;
                    do
                    {
                        preValue = this.lossByArticlePrize[articleIndex];
                        postValue = preValue + lossPrize;
                    } while (preValue != Interlocked.CompareExchange(ref this.lossByArticlePrize[articleIndex], postValue, preValue));
                }
            });
        }


        private void addSettlements(Settlement[] settlementsParam)
        {
            this.settlements = settlementsParam;
            this.set_soldByArticleAmount = new int[this.articles.Count];
            this.set_soldByArticlePrize = new double[this.articles.Count];

            Parallel.ForEach(settlements, (settlement) =>
            {
                foreach (Account account in this.accounts)
                {
                    List<Tuple<int, int>> articleAmountList = settlement.getUserArticles(account);

                    foreach (Tuple<int, int> articleAmount in articleAmountList)
                    {
                        int articleIndex = this.articleToIndex[articleAmount.Item1];
                        Interlocked.Add(ref this.set_soldByArticleAmount[articleIndex], articleAmount.Item2);
                    }

                    Tuple<int, double>[] articleAmountPayment = settlement.getPlaymentDetails(account);
                    foreach (Tuple<int, double> articlePayment in articleAmountPayment)
                    {
                        int articleIndex = this.articleToIndex[articlePayment.Item1];
                        double preValue;
                        double postValue;
                        do
                        {
                            preValue = this.set_soldByArticlePrize[articleIndex];
                            postValue = preValue + articlePayment.Item2;
                        } while (preValue != Interlocked.CompareExchange(ref this.set_soldByArticlePrize[articleIndex], postValue, preValue));
                    }
                }
            });
        }


        private void addShipments(Shipment[] shipmentsParam)
        {
            this.shipments = shipmentsParam;
            this.shipmentsByArticle = new int[this.articles.Count];
            Parallel.ForEach(shipments, (shipment) =>
            {
                List<Tuple<int, int>> articleAmountList = shipment.ArticleAmountListInt;
                foreach (Tuple<int, int> articleAmount in articleAmountList)
                {
                    int articleIndex = this.articleToIndex[articleAmount.Item1];
                    Interlocked.Add(ref this.shipmentsByArticle[articleIndex], articleAmount.Item2);
                }
            });
        }



        private void addEvents(Event[] eventsParam)
        {
            this.events = eventsParam;
            this.event_soldByArticleAmount = new int[this.articles.Count];
            this.event_soldByArticlePrize = new double[this.articles.Count];

            Parallel.ForEach(events, (currentEvent) =>
            {
                foreach (Article article in this.articles)
                {

                    int articleAmount = currentEvent.getAmount(article.ID);
                    int articleIndex = this.articleToIndex[article.ID];
                    Interlocked.Add(ref this.event_soldByArticleAmount[articleIndex], articleAmount);

                    double preValue;
                    double postValue;
                    double articlePrize = articleAmount * article.Prize;
                    do
                    {
                        preValue = this.event_soldByArticlePrize[articleIndex];
                        postValue = preValue + articlePrize;
                    } while (preValue != Interlocked.CompareExchange(ref this.event_soldByArticlePrize[articleIndex], postValue, preValue));
                }
            });
        }


        private double calcualteStockValue(Stocktaking stocktaking)
        {
            double sum = 0.0;
            foreach (Tuple<int, int, int> stock in stocktaking.Inventory)
            {
                Article article = this.articles.First(x => x.ID == stock.Item1);
                sum += (double)stock.Item3 * article.Prize;
            }
            return sum;
        }


        public bool isCompletlyInitialized()
        {
            return this.AnnualStatementID != int.MinValue;
        }


        public int AnnualStatementID
        {
            get
            {
                return annualStatementID;
            }
        }

        public string AnnualStatementName
        {
            get
            {
                return annualStatementName;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
        }


        internal bool writeToExcelFile(string filePath)
        {
            bool written = true;

            using (SLDocument document = new SLDocument())
            {
                SLStyle styleBold = new SLStyle();
                styleBold.SetFontBold(true);

                this.fillOverviewWS(document, styleBold);
                this.fillArticlesWS(document, styleBold);

                document.SaveAs(filePath);
            }

            return written;
        }


        private void fillOverviewWS(SLDocument overviewWS, SLStyle styleBold)
        {
            bool added = overviewWS.AddWorksheet("Overview");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }

            SLStyle financeStyle = new SLStyle();
            financeStyle.FormatCode = "0.00";

            int row = 1;
            overviewWS.SetCellValue(row, 1, "Annual Statement ID");
            overviewWS.SetCellValue(row, 2, this.AnnualStatementID);
            row++;
            overviewWS.SetCellValue(row, 1, "Annual Statement Name");
            overviewWS.SetCellValue(row, 2, this.AnnualStatementName);
            row++;
            overviewWS.SetCellValue(row, 1, "Start Date");
            overviewWS.SetCellValue(row, 2, ServerUtil.DateTimeToString(this.startDate));
            row++;
            overviewWS.SetCellValue(row, 1, "End Date");
            overviewWS.SetCellValue(row, 2, ServerUtil.DateTimeToString(this.endDate));
            row++;
            row++;

            int nameColumn = 1;
            int debitColumn = 2;
            int creditColumn = 3;
            int rowToUse;
            overviewWS.SetCellValue(row, nameColumn, "");
            overviewWS.SetCellValue(row, debitColumn, "Debit");
            overviewWS.SetCellValue(row, creditColumn, "Credit");
            overviewWS.SetRowStyle(row, styleBold);
            row++;

            overviewWS.SetCellValue(row, nameColumn, "Initial Stock");
            overviewWS.SetCellValue(row, creditColumn, this.intialStockValue);
            overviewWS.SetCellStyle(row, creditColumn, financeStyle);
            row++;
            overviewWS.SetCellValue(row, nameColumn, "Purchases");
            overviewWS.SetCellValue(row, creditColumn, @"???");
            //overviewWS.SetCellStyle(row, creditColumn, financeStyle);
            row++;
            overviewWS.SetCellValue(row, nameColumn, "Sales");
            overviewWS.SetCellValue(row, debitColumn, this.set_soldByArticlePrize.Sum());
            overviewWS.SetCellStyle(row, debitColumn, financeStyle);
            row++;

            foreach (Event currentEvent in this.events)
            {
                overviewWS.SetCellValue(row, nameColumn, "Event: " + currentEvent.Name);
                overviewWS.SetCellValue(row, debitColumn, this.event_soldByArticlePrize.Sum());
                overviewWS.SetCellStyle(row, debitColumn, financeStyle);
                row++;
            }

            overviewWS.SetCellValue(row, nameColumn, "Loss");
            double lossSum = this.lossByArticlePrize.Sum();
            rowToUse = lossSum > 0.0 ? creditColumn : debitColumn;
            overviewWS.SetCellValue(row, rowToUse, lossSum);
            overviewWS.SetCellStyle(row, rowToUse, financeStyle);
            row++;
            overviewWS.SetCellValue(row, nameColumn, "Final Stock");
            overviewWS.SetCellValue(row, debitColumn, this.finalStockValue);
            overviewWS.SetCellStyle(row, debitColumn, financeStyle);
            row++;

            overviewWS.SetColumnStyle(nameColumn, styleBold);
        }


        private void fillArticlesWS(SLDocument articlesWS, SLStyle styleBold)
        {
            bool added = articlesWS.AddWorksheet("Articles");
            if (!added)
            {
                throw new ArgumentException("failed to create Worksheet.");
            }

            int currentRow = 1;
            int currentColumn = 1;
            articlesWS.SetCellValue(currentRow, currentColumn, "ID");
            currentColumn++;
            articlesWS.SetCellValue(currentRow, currentColumn, "Name");
            currentColumn++;
            articlesWS.SetCellValue(currentRow, currentColumn, "Prize");
            currentColumn++;
            articlesWS.SetCellValue(currentRow, currentColumn, "Size");
            currentColumn++;
            articlesWS.MergeWorksheetCells(currentRow, currentColumn, currentRow, currentColumn + 2);
            articlesWS.SetCellValue(currentRow, currentColumn, "Stock");
            articlesWS.SetCellValue(currentRow + 1, currentColumn, "Initial");
            articlesWS.SetCellValue(currentRow + 1, currentColumn + 2, "End");
            currentColumn += 2;

            currentColumn += 2;
            articlesWS.MergeWorksheetCells(currentRow, currentColumn, currentRow, currentColumn + this.stocktakings.Length - 1);
            articlesWS.SetCellValue(currentRow, currentColumn, "Losses");

            for (int i = 0; i < this.stocktakings.Length; i++)
            {
                articlesWS.SetCellValue(currentRow + 1, currentColumn, stocktakings[i].TimeStamp);
                currentColumn++;
            }

            currentColumn += 2;
            articlesWS.MergeWorksheetCells(currentRow, currentColumn, currentRow, currentColumn + this.settlements.Length - 1);
            articlesWS.SetCellValue(currentRow, currentColumn, "Sales");

            for (int i = 0; i < this.settlements.Length; i++)
            {
                articlesWS.SetCellValue(currentRow + 1, currentColumn, settlements[i].EndDate);
                currentColumn++;
            }

            currentColumn += 2;
            articlesWS.MergeWorksheetCells(currentRow, currentColumn, currentRow, currentColumn + this.shipments.Length - 1);
            articlesWS.SetCellValue(currentRow, currentColumn, "Deliveries");

            for (int i = 0; i < this.shipments.Length; i++)
            {
                articlesWS.SetCellValue(currentRow + 1, currentColumn, shipments[i].DeliveryDatetime);
                currentColumn++;
            }

            currentColumn += 2;
            articlesWS.MergeWorksheetCells(currentRow, currentColumn, currentRow, currentColumn + this.events.Length - 1);
            articlesWS.SetCellValue(currentRow, currentColumn, "Events");

            for (int i = 0; i < this.shipments.Length; i++)
            {
                articlesWS.SetCellValue(currentRow + 1, currentColumn, this.events[i].Begin);
                currentColumn++;
            }

            SLStyle dateStyle = new SLStyle();
            dateStyle.FormatCode = "yyyy-mm-dd";
            articlesWS.SetRowStyle(currentRow, styleBold);
            articlesWS.SetRowStyle(currentRow + 1, dateStyle);

            currentRow++;
            currentRow++;

            foreach (Article article in this.articles)
            {
                currentColumn = 1;
                articlesWS.SetCellValue(currentRow, currentColumn, article.ID);
                currentColumn++;
                articlesWS.SetCellValue(currentRow, currentColumn, article.Name);
                currentColumn++;
                articlesWS.SetCellValue(currentRow, currentColumn, article.Prize);
                currentColumn++;
                articlesWS.SetCellValue(currentRow, currentColumn, article.Size);
                currentColumn++;
                //Stock Section
                Tuple<int, int, int> initialArticleStock = this.initialStocktaking.Inventory.FirstOrDefault(x => x.Item1 == article.ID);
                if (initialArticleStock != null)
                    articlesWS.SetCellValue(currentRow, currentColumn, initialArticleStock.Item3);
                else
                    articlesWS.SetCellValue(currentRow, currentColumn, "ERR");
                
                currentColumn++;
                articlesWS.SetCellValue(currentRow, currentColumn, "->");
                currentColumn++;
                Tuple<int, int, int> finalArticleStock = this.finalStocktaking.Inventory.FirstOrDefault(x => x.Item1 == article.ID);
                if (finalArticleStock != null)
                    articlesWS.SetCellValue(currentRow, currentColumn, finalArticleStock.Item3);
                else
                    articlesWS.SetCellValue(currentRow, currentColumn, "ERR");


                currentColumn += 2;

                for (int i = 0; i < this.stocktakings.Length; i++)
                {
                    Tuple<int, int, int> itemStock = this.stocktakings[i].Inventory.FirstOrDefault(x => x.Item1 == article.ID);
                    if (itemStock != null)
                        articlesWS.SetCellValue(currentRow, currentColumn, itemStock.Item2 - itemStock.Item3);
                    else
                        articlesWS.SetCellValue(currentRow, currentColumn, "");
                    currentColumn++;
                }

                currentColumn += 2;

                for (int i = 0; i < this.settlements.Length; i++)
                {
                    Settlement settlement = this.settlements[i];
                    articlesWS.SetCellValue(currentRow, currentColumn, settlement.getArticlesSold(article));
                    currentColumn++;
                }

                currentColumn += 2;

                for (int i = 0; i < this.shipments.Length; i++)
                {
                    Shipment shipment = this.shipments[i];
                    Tuple<int, int> articleAmountTuple = shipment.ArticleAmountListInt.FirstOrDefault(x => x.Item1 == article.ID);
                    articlesWS.SetCellValue(currentRow, currentColumn, articleAmountTuple != null ? articleAmountTuple.Item2.ToString() : "");
                    currentColumn++;
                }

                currentColumn += 2;

                for (int i = 0; i < this.events.Length; i++)
                {
                    Event currentEvent = this.events[i];
                    int amount = currentEvent.getAmount(article.ID);
                    articlesWS.SetCellValue(currentRow, currentColumn, amount);
                    currentColumn++;
                }

                currentRow++;
            }
        }

    }
}