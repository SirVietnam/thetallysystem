﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BWSystemServer.WebAccess;
using BWSystemServer.BIZ;

namespace BWSystemServer
{
    public partial class Default : System.Web.UI.Page
    {
        private LoginInfo loginInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected void Login_Authenticate(object sender, AuthenticateEventArgs e)
        {
            bool authenticated = false;
            
            authenticated = checkLogin(Login.UserName, Login.Password);

            if (authenticated)
            {
                this.loginInfo = new LoginInfo(Login.UserName);
                Session["LoginInfo"] = this.loginInfo;
            }

            e.Authenticated = authenticated;
        }


        private bool checkLogin(string userName, string password)
        {
            DBAccessManagement DAM = DBAccessManagement.Instance;
            Account loginAccount = DAM.getAccountToLogin(userName);
            if (loginAccount != null)
            {
                if (userName.ToLower() == "admin" && password == "admin")
                {
                    return true;
                }
                return DBAccessManagement.Instance.isValidPassword(loginAccount, password);
            }
            return false;
        }


        ///Getter & Setter
         
        //public LoginInfo getLoginInfo()
        //{
        //    return this.loginInfo;
        //}
        //public LoginInfo LoginInfo
        //{
        //    get { return loginInfo; }
        //}
    }
}