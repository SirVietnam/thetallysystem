﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class FileUploadPage : System.Web.UI.Page
    {
        private bool isReadOnly;
        private LoginInfo loginInfo;
        private Article article;
        private Account account;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
            HttpContext.Current.Response.AddHeader("Expires", "0");

            this.article = null;
            this.isReadOnly = true;

            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);

                if (this.loginInfo.ActiveArticle == null)
                {
                    this.account = this.loginInfo.LoggedInAccount;
                    this.article = null;
                    this.loadUserData();
                }
                else if (this.loginInfo.EditDataPage != null)
                {
                    this.account = null;
                    this.article = this.loginInfo.ActiveArticle;
                    this.loadArticleData();
                }
                else
                {
                    SimpleLog.Error("NOT IN ANY EDIT MODE");
                }
            }
            else
            {
                SimpleLog.Log("No User is loged in!");
            }

            DataBind();
        }

        private void loadUserData()
        {
            lb_Type.Text = "Account Picture";
            lb_ID.Text = account.ID.ToString();
            if (account.Nickname != null && !account.Nickname.Equals(""))
            {
                lb_Name.Text = account.Nickname;
            }
            else
            {
                lb_Name.Text = account.Surname + " " + account.Name;
            }

            DBAccessManagement DAM = DBAccessManagement.Instance;
            string userImagePath = DAM.getUserImageRelativePath(account.PictureURL);
            img_Picture.ImageUrl = userImagePath;
        }

        private void loadArticleData()
        {
            lb_Type.Text = "Article Picture";
            lb_ID.Text = article.ID.ToString();
            lb_Name.Text = article.Name;

            DBAccessManagement DAM = DBAccessManagement.Instance;
            string articleImagePath = DAM.getArticleImageRelativePath(article.PictureURL);
            img_Picture.ImageUrl = articleImagePath;
        }

        protected void uploadBtn_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile)
            {
                try
                {
                    if (FileUploadControl.PostedFile.ContentLength < 4194304)
                    {
                        if (FileUploadControl.PostedFile.ContentType == "image/jpeg")
                        {
                            string filename = Path.GetFileName(FileUploadControl.FileName);
                            string tempFilePath = Server.MapPath("~/Pictures/") + filename;
                            FileUploadControl.SaveAs(tempFilePath);
                            byte[] picture = File.ReadAllBytes(tempFilePath);
                            DBAccessManagement DAM = DBAccessManagement.Instance;

                            if (this.account != null)
                            {
                                DAM.addUserPicture(picture, this.account);
                            }
                            else if (this.article != null)
                            {
                                DAM.addArticlePicture(picture, this.article);
                            }
                            else
                            {
                                throw new ArgumentException("The FileUploadPage is in no edit mode!");
                            }

                            File.Delete(tempFilePath);
                            //HttpResponse.RemoveOutputCacheItem("/WebAccess/FileUploadPage.aspx");
                            //HttpResponse.RemoveOutputCacheItem("/WebAccess/Article_Page.aspx");
                            //HttpResponse.RemoveOutputCacheItem("/WebAccess/Account_Page.aspx");
                            //Response.Redirect(Request.RawUrl);

                            var enumerator = HttpContext.Current.Cache.GetEnumerator();

                            while (enumerator.MoveNext())
                            {
                                HttpContext.Current.Cache.Remove(enumerator.Key.ToString());
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert('ERROR: The picture must be smaller than 4MB!');</script>");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('ERROR: Only JPEG files are accepted!');</script>");
                    }
                }
                catch (IOException ex)
                {
                    SimpleLog.Log(ex);
                    Response.Write("<script>alert('ERROR: Upload failed!');</script>");
                }
            }
        }

        protected void done_Click(object sender, EventArgs e)
        {
            if (this.account != null)
            {
                this.loginInfo.onEditComplete(this);
                Server.Transfer("~\\WebAccess\\Account_Page.aspx", true);
            }
            else if (this.article != null)
            {
                this.loginInfo.onEditComplete(this);
                Server.Transfer("~\\WebAccess\\Article_Page.aspx", true);
            }
            else
            {
                throw new ArgumentException("Why is loginInfo.editedPage neither Article OR Account ...");
            }
        }
    }
}