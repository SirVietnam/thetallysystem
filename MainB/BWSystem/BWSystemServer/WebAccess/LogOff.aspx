﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogOff.aspx.cs" Inherits="BWSystemServer.WebAccess.LogOff" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Logging Off" SubheaderText="You are now logged off." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <div class="substd_div">
                <p style="width: 55%; float: right; padding: 10px;">
                    <asp:LinkButton ID="lbtn_RelogIn" runat="server" PostBackUrl="~/Default.aspx" BorderColor="Black" BorderStyle="Outset" BorderWidth="1px" style="padding: 5px; margin: 3px;" EnableViewState="False">Log In</asp:LinkButton>
                </p>
                

            </div>
        </form>
    </div>
</body>
</html>
