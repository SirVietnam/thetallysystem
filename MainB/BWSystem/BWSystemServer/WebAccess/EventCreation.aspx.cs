﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class EventCreation : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private Dictionary<int, Button> articleButtons;
        private Event currentEvent;

        public Event CurrentEvent
        {
            get { return currentEvent; }
        }

        //private HashSet<Article> payedArticles;
        //private HashSet<Article> notPayedArticles;
        //private HashSet<Article> disabledArticles;

        protected void Page_Load(object sender, EventArgs e)
        {
            bool editingMode = false;
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.currentEvent = this.loginInfo.ActiveEvent;

            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser && this.currentEvent != null)
            {
                //Returns true if page has been loaded befor
                editingMode = this.loginInfo.onPage_Load(this, this.loginInfo.ActiveEvent.EventID);
                if (!editingMode)
                {
                    this.loginInfo.PayedArticles = new HashSet<Article>();
                    this.loginInfo.NotPayedArticles = new HashSet<Article>();
                    this.loginInfo.DisabledArticles = new HashSet<Article>();
                    this.sortArticles();
                }

                //DataBinding
                //DataBind();
                //filles payedArticles
                if (!this.IsPostBack)
                {
                    this.addDDLs();
                    this.addDates();
                }
                //sets editing now
                editingMode = this.loginInfo.onEditStart(this);
                this.payedArticlesTable.OnButtonClicked = this.OnRemoveButton;
                this.notPayedArticlesTable.OnButtonClicked = this.OnAddButton;
                this.disabledArticlesTable.OnButtonClicked = this.OnAddDisabledButton;
            }
        }


        //Setup Functions
        private void sortArticles()
        {
            List<Article> allArticles = DBAccessManagement.Instance.getArticleList_All();
            foreach (int articleID in this.loginInfo.ActiveEvent.CostAbsorption)
            {
                Article article = allArticles.FirstOrDefault(x => x.ID == articleID);
                if (article != null)
                {
                    this.loginInfo.PayedArticles.Add(article);
                }
            }

            List<Article> articles = new List<Article>(allArticles);
            Event activeEvent = this.loginInfo.ActiveEvent;
            foreach (int articleID in activeEvent.CostAbsorption)
            {
                Article article = articles.FirstOrDefault(x => x.ID == articleID);
                if (article != null)
                {
                    this.loginInfo.PayedArticles.Add(article);
                    articles.Remove(article);
                }
            }

            foreach (Article article in articles)
            {
                if (!article.IsInventory)
                {
                    if (article.IsAvailable)
                    {
                        this.loginInfo.NotPayedArticles.Add(article);
                    }
                    else
                    {
                        this.loginInfo.DisabledArticles.Add(article);
                    }
                }
            }
        }


        private void addDDLs()
        {
            foreach (Article.ArticleType type in Enum.GetValues(typeof(Article.ArticleType)))
            {
                ListItem item = new ListItem(type.ToString(), ((int)type).ToString());
                this.ddl_articleTypes.Items.Add(item);
            }
        }


        private void addDates()
        {
            if (this.currentEvent.EventID >= 0)
            {
                this.tb_eventID.Text = this.currentEvent.EventID.ToString();
            }

            if (this.currentEvent.Name != null)
            {
                this.tb_eventName.Text = this.currentEvent.Name;
            }

            this.cb_isExclusiveArticle.Checked = this.currentEvent.ArticlesExclusive;

            if (this.currentEvent.Begin != null)
            {
                this.tb_startDate.Text = ServerUtil.DateToString(this.currentEvent.Begin);
                string hourString = this.currentEvent.Begin.TimeOfDay.Hours.ToString("00");
                this.ddl_startHour.Text = hourString;
                string minString = this.currentEvent.Begin.TimeOfDay.Minutes.ToString("00");
                this.ddl_startMinutes.Text = minString;
            }

            if (this.currentEvent.End != null)
            {
                this.tb_endDate.Text = ServerUtil.DateToString(this.currentEvent.End);
                string hourString = this.currentEvent.End.TimeOfDay.Hours.ToString("00");
                this.ddl_endHour.Text = hourString;
                string minString = this.currentEvent.End.TimeOfDay.Minutes.ToString("00");
                this.ddl_endMinutes.Text = minString;
            }
        }


        //Button Events
        protected void addType_Btn_Click(object sender, EventArgs e)
        {
            Article.ArticleType selectedArticleType = (Article.ArticleType)Convert.ToInt32(this.ddl_articleTypes.SelectedValue);

            for (int i = this.loginInfo.NotPayedArticles.Count - 1; i >= 0; i--)
            {
                Article article = this.loginInfo.NotPayedArticles.ElementAt(i);

                if (article.Type == selectedArticleType)
                {
                    this.loginInfo.PayedArticles.Add(article);
                    this.loginInfo.NotPayedArticles.Remove(article);
                }
            }
            this.refreshPage();
        }


        private void OnButtonClicked(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button button = (Button)sender;

                if (button.CommandArgument == "payedArticlesTable")
                {
                    this.OnRemoveButton(sender, e);
                }
                else if (button.CommandArgument == "notPayedArticlesTable")
                {
                    this.OnAddButton(sender, e);
                }
                else if (button.CommandArgument == "disabledArticlesTable")
                {
                    this.OnAddDisabledButton(sender, e);
                }
            }
        }

        private void OnRemoveButton(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button removeButton = (Button)sender;
                int articleID = Convert.ToInt32(removeButton.CommandArgument);
                Article article = this.loginInfo.PayedArticles.FirstOrDefault(x => x.ID == articleID);
                if (article != null)
                {
                    this.loginInfo.PayedArticles.Remove(article);
                    if (article.IsAvailable)
                    {
                        this.loginInfo.NotPayedArticles.Add(article);
                    }
                    else
                    {
                        this.loginInfo.DisabledArticles.Add(article);
                    }

                    this.refreshPage();
                }
            }
        }

        private void OnAddButton(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button addButton = (Button)sender;
                int articleID = Convert.ToInt32(addButton.CommandArgument);
                Article article = this.loginInfo.NotPayedArticles.FirstOrDefault(x => x.ID == articleID);
                if (article != null)
                {
                    this.loginInfo.NotPayedArticles.Remove(article);
                    this.loginInfo.PayedArticles.Add(article);

                    this.refreshPage();
                }
            }
        }

        private void OnAddDisabledButton(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button addButton = (Button)sender;
                int articleID = Convert.ToInt32(addButton.CommandArgument);
                Article article = this.loginInfo.DisabledArticles.FirstOrDefault(x => x.ID == articleID);
                if (article != null)
                {
                    this.loginInfo.DisabledArticles.Remove(article);
                    this.loginInfo.PayedArticles.Add(article);

                    this.refreshPage();
                }
            }
        }


        //Event Information Events
        protected void tb_eventName_TextChanged(object sender, EventArgs e)
        {
            if (this.currentEvent != null)
            {
                if (this.tb_eventName.Text != "")
                {
                    this.tb_eventName.CssClass = "std_Textbox";
                }
                else
                {
                    this.tb_eventName.CssClass = "tb_inputError";
                }

                this.currentEvent.Name = tb_eventName.Text;
                DataBind();
            }
        }


        private void refreshPage()
        {
            this.saveData();
            Server.Transfer("~\\WebAccess\\EventCreation.aspx", false);
        }

        protected void tb_startDate_TextChanged(object sender, EventArgs e)
        {
            DateTime startDateTime = Convert.ToDateTime(tb_startDate.Text);
            int startHour = Convert.ToInt32(ddl_startHour.SelectedValue);
            int startMin = Convert.ToInt32(ddl_startMinutes.SelectedValue);
            startDateTime = startDateTime.AddHours(startHour);
            startDateTime = startDateTime.AddMinutes(startMin);

            if (DateTime.Now < startDateTime)
            {
                this.tb_startDate.CssClass = "std_Textbox";
                this.ddl_startHour.CssClass = "std_Textbox";
                this.ddl_startMinutes.CssClass = "std_Textbox";
            }
            else
            {
                this.tb_startDate.CssClass = "tb_inputError";
                this.ddl_startHour.CssClass = "tb_inputError";
                this.ddl_startMinutes.CssClass = "tb_inputError";
            }

            if (currentEvent != null)
            {
                currentEvent.Begin = startDateTime;
            }
        }


        protected void tb_endDate_TextChanged(object sender, EventArgs e)
        {
            DateTime endDateTime = Convert.ToDateTime(tb_endDate.Text);
            int endHour = Convert.ToInt32(ddl_endHour.SelectedValue);
            int endMin = Convert.ToInt32(ddl_endMinutes.SelectedValue);
            endDateTime = endDateTime.AddHours(endHour);
            endDateTime = endDateTime.AddMinutes(endMin);

            if (this.currentEvent != null && this.currentEvent.Begin < endDateTime)
            {
                this.tb_endDate.CssClass = "std_Textbox";
                this.ddl_endHour.CssClass = "std_Textbox";
                this.ddl_endMinutes.CssClass = "std_Textbox";
            }
            else
            {
                this.tb_endDate.CssClass = "tb_inputError";
                this.ddl_endHour.CssClass = "tb_inputError";
                this.ddl_endMinutes.CssClass = "tb_inputError";
            }

            if (this.currentEvent != null)
            {
                currentEvent.End = endDateTime;
            }
        }


        private bool saveData()
        {
            bool correctDateTime = false;
            bool correctName = false;
            if (this.currentEvent != null)
            {
                this.tb_startDate_TextChanged(null, null);
                this.tb_endDate_TextChanged(null, null);
                this.tb_eventName_TextChanged(null, null);

                this.currentEvent.ArticlesExclusive = this.cb_isExclusiveArticle.Checked;

                correctDateTime = this.currentEvent.Begin > DateTime.Now && this.currentEvent.End > this.currentEvent.Begin;
                correctName = this.tb_eventName.Text != "";

                this.currentEvent.CreatorAccountID = this.loginInfo.LoggedInAccount.ID;
                this.currentEvent.CostAbsorption = this.loginInfo.PayedArticles.Select(x => x.ID).ToArray();
            }
            return correctDateTime && correctName;
        }


        protected void save_Btn_Click(object sender, EventArgs e)
        {
            bool dataSaved = this.saveData();

            if (dataSaved)
            {
                bool added = DBAccessManagement.Instance.addEvent(currentEvent);
                if (added)
                {
                    this.loginInfo.onEditComplete(this);
                    this.loginInfo.ActiveEvent = null;
                    Server.Transfer("~\\WebAccess\\Events.aspx", false);
                }

            }
        }


    }
}