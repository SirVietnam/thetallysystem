﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageArticlePrize.aspx.cs" Inherits="BWSystemServer.WebAccess.ManageArticlePrize" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Change Article Prizes" SubheaderText="A change of article prizes requires a settlement." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form runat="server">
        <div class="std_div">
            <asp:Table runat="server" ID="requirementsTable" CssClass="std_table" Width="350">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="150" ColumnSpan="2">Prerequirements*</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="200">Settlement:</asp:TableCell>
                    <asp:TableCell>
                        <asp:Label runat="server" ID="settlementStatusTB" CssClass="tb_inputError"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />

            <asp:Table ID="artTable" runat="server" Width="525px" CssClass="evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="75px">ID</asp:TableCell>
                    <asp:TableCell Width="200px">Name</asp:TableCell>
                    <asp:TableCell Width="125px">Old Prize</asp:TableCell>
                    <asp:TableCell Width="125px">New Prize</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />

            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="Check_Update_Btn" runat="server" Text="Check" OnClick="Check_Update_Btn_Click" />
            </div>
            *The last Settlement must be created less then 30 minutes ago.
        </div>
    </form>
</body>
</html>
