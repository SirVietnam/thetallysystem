﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class DeviceCreation : System.Web.UI.Page
    {
        private bool clientAdded;

        private LoginInfo loginInfo;
        DBAccessManagement DAM;


        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                bool inEditMode = this.loginInfo.onPage_Load(this);

                if (loginInfo.LoggedInAccount.IsSuperuser)
                {
                    this.DAM = DBAccessManagement.Instance;
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
            DataBind();
        }



        protected void Check_Update_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null)
            {
                if (Check_Update_Btn.Text.Equals("Check"))
                {
                    this.loginInfo.onEditStart(this);

                    if (this.checkValues())
                    {
                        Check_Update_Btn.Text = "Add Client";
                    }
                }
                else if (!this.clientAdded)
                {
                    if (this.checkValues())
                    {
                        string loginToken = DAM_ClientDevice.addClientDevice(tb_Login.Text, tb_Password.Text, tb_IP.Text);
                        if (loginToken == null)
                        {
                            this.pwErrorRow.Visible = true;
                            this.tb_Login.CssClass = "tb_inputError";
                            this.tb_Password.CssClass = "tb_inputError";
                        }
                        else
                        {
                            this.pwErrorRow.Visible = false;
                            this.tb_Login.CssClass = "std_Textbox";
                            this.tb_Password.CssClass = "std_Textbox";

                            this.clientAdded = true;
                            this.tb_loginToken.Text = loginToken;
                        }
                        this.loginInfo.onEditComplete(this);
                        DataBind();
                    }
                }
            }
        }


        private bool checkValues()
        {
            bool loginEntered = this.tb_Login.Text != "";
            this.tb_Login.CssClass = loginEntered ? "std_Textbox" : "tb_inputError";

            bool pwEntered = this.tb_Password.Text.Length >= 4;
            this.tb_Password.CssClass = pwEntered ? "std_Textbox" : "tb_inputError";

            bool ipEntered = this.tb_IP.Text != "";
            this.tb_IP.CssClass = ipEntered ? "std_Textbox" : "tb_inputError";

            return loginEntered && pwEntered && ipEntered;
        }



        public bool ClientAdded
        {
            get { return clientAdded; }
        }
    }
}