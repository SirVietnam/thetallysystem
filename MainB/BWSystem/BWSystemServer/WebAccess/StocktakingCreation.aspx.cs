﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ManageInventorySynchronise : System.Web.UI.Page
    {
        private Regex regexIntNumber;
        private Regex regexDate;
        private LoginInfo loginInfo;
        private List<Article> articleList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.regexIntNumber = ServerUtil.RegexIntNumber;
                this.regexDate = ServerUtil.RegexDate;
                //this.allItemsValid = this.loginInfo.AllItemsValid;
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.articleList = DBAManagement.getArticleList_All();

                this.addArticleDataToTable();
            }
        }


        private void addArticleDataToTable()
        {
            GuiWebUtil.addSeperatorRowRow(this.addtbl, "Available Articles", System.Drawing.Color.Green, System.Drawing.Color.Black, 7);
            IEnumerable<Article> availibleArticles = this.articleList.Where(x => !x.IsInventory && x.IsAvailable);
            this.addArticles(availibleArticles);


            IEnumerable<Article> disabledArticles = this.articleList.Where(x => !x.IsInventory && !x.IsAvailable);
            if (disabledArticles.Count() > 0)
            {
                GuiWebUtil.addSeperatorRowRow(this.addtbl, "Disabled Articles", System.Drawing.Color.DarkSlateGray, System.Drawing.Color.White, 7);
                this.addArticles(disabledArticles);
            }

            IEnumerable<Article> inventoryArticles = this.articleList.Where(x => x.IsInventory);
            if (inventoryArticles.Count() > 0)
            {
                GuiWebUtil.addSeperatorRowRow(this.addtbl, "Inventory Articles", System.Drawing.Color.DarkBlue, System.Drawing.Color.White, 7);
                this.addArticles(inventoryArticles);
            }
        }


        private void addArticles(IEnumerable<Article> articles)
        {
            foreach (Article article in articles)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = article.ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell nameCell = new TableCell();
                nameCell.Text = article.Name;
                currentRow.Cells.Add(nameCell);

                TableCell virtualStockCell = new TableCell();
                virtualStockCell.HorizontalAlign = HorizontalAlign.Center;
                virtualStockCell.Text = article.Stock.ToString();
                currentRow.Cells.Add(virtualStockCell);

                TableCell boxesTBCell = new TableCell();
                TextBox boxesTB = new TextBox();
                boxesTB.ID = article.ID.ToString() + "_Box";
                boxesTB.ValidationGroup = "InputTextboxInt";
                boxesTB.CssClass = "std_Textbox";
                boxesTB.Text = "0";
                boxesTB.AutoPostBack = true;
                boxesTB.TextChanged += BoxesTB_TextChanged;
                boxesTBCell.Controls.Add(boxesTB);
                currentRow.Cells.Add(boxesTBCell);

                TableCell bottlesTBCell = new TableCell();
                TextBox bottlesTB = new TextBox();
                bottlesTB.ID = article.ID.ToString() + "_Bot";
                bottlesTB.ValidationGroup = "InputTextboxInt";
                bottlesTB.CssClass = "std_Textbox";
                bottlesTB.Text = "0";
                boxesTB.AutoPostBack = true;
                bottlesTB.TextChanged += BoxesTB_TextChanged;
                bottlesTBCell.Controls.Add(bottlesTB);
                currentRow.Cells.Add(bottlesTBCell);

                TableCell sumCell = new TableCell();
                sumCell.HorizontalAlign = HorizontalAlign.Center;
                sumCell.ID = article.ID.ToString() + "_Sum";
                sumCell.Text = "0";
                currentRow.Cells.Add(sumCell);

                TableCell lossCell = new TableCell();
                lossCell.HorizontalAlign = HorizontalAlign.Center;
                lossCell.ID = article.ID.ToString() + "_Loss";
                lossCell.Text = article.Stock.ToString();
                currentRow.Cells.Add(lossCell);

                addtbl.Rows.Add(currentRow);
            }
        }


        private void BoxesTB_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = null;
            if (sender is TextBox)
            {
                tb = (TextBox)sender;
                string[] idElements = tb.ID.Split("_".ToCharArray());
                int ID = Convert.ToInt32(idElements[0]);

                this.handleArticle(ID);
            }
        }

        private bool handleArticle(int ID)
        {
            bool allEntriesValid = false;
            int amountBoxes = this.handleTextbox(ID.ToString() + "_Box");
            int amountBottles = this.handleTextbox(ID.ToString() + "_Bot");

            TableCell sumCell = (TableCell)FindControl(ID.ToString() + "_Sum");
            TableCell lossCell = (TableCell)FindControl(ID.ToString() + "_Loss");
            if (amountBoxes >= 0 && amountBottles >= 0)
            {
                int boxesSize = ServerUtil.getStandardAmount(articleList.First(x => x.ID == ID).Size);
                int sumCount = amountBoxes * boxesSize + amountBottles;

                sumCell.Text = sumCount.ToString();

                Article article = this.articleList.First(x => x.ID == ID);
                lossCell.Text = (article.Stock - sumCount).ToString();
                allEntriesValid = true;
            }
            else
            {
                sumCell.Text = "ERR";
                sumCell.CssClass = "tb_inputError";
                allEntriesValid = false;
            }
            return allEntriesValid;
        }


        private int handleTextbox(string tbID)
        {
            TextBox tb = (TextBox)FindControl(tbID);
            bool hasValidContent = regexIntNumber.IsMatch(tb.Text);

            int amount = 0;
            if (hasValidContent)
            {
                tb.CssClass = "std_Textbox";
                amount = Convert.ToInt32(tb.Text);
            }
            else
            {
                tb.CssClass = "tb_inputError";
                amount = -1;
            }

            return amount;
        }


        protected void Check_Update_Btn_Click(object sender, EventArgs e)
        {
            if (Check_Update_Btn.Text.Equals("Check"))
            {
                bool allArticleValid = true;
                foreach (Article article in this.articleList)
                {
                    if (article.IsAvailable)
                    {
                        allArticleValid = allArticleValid & this.handleArticle(article.ID);
                    }

                }

                if (allArticleValid)
                {
                    Check_Update_Btn.Text = "Update Stock";
                }
            }
            else
            {
                Stocktaking stocktaking = new Stocktaking(DateTime.Now);
                this.addStocktakintDetails(stocktaking);

                DBAccessManagement DAM = DBAccessManagement.Instance;
                DAM.addStocktaking(stocktaking);

                Server.Transfer("~\\WebAccess\\Stocktakings.aspx", false);
            }
        }



        private void addStocktakintDetails(Stocktaking stocktaking)
        {
            foreach (Article article in this.articleList)
            {
                //if (article.IsAvailable)
                //{
                int amountBoxes = this.handleTextbox(article.ID.ToString() + "_Box");
                int amountBottles = this.handleTextbox(article.ID.ToString() + "_Bot");

                if (amountBoxes >= 0 && amountBottles >= 0)
                {
                    int boxesSize = ServerUtil.getStandardAmount(article.Size);
                    int bottlesCount = amountBoxes * boxesSize + amountBottles;

                    stocktaking.addItem(article.ID, article.Stock, bottlesCount);
                }
                //}

            }
        }
    }
}