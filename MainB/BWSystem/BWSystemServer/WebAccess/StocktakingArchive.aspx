﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StocktakingArchive.aspx.cs" Inherits="BWSystemServer.WebAccess.StocktakingArchive" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Stocktakings" SubheaderText="Here you find all the previous stocktakings!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="stocktakingTable" runat="server" Width="350px">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="70%">Date</asp:TableCell>
                    <asp:TableCell Width="30%">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </form>
    </div>
</body>
</html>
