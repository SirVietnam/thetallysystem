﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class AnualStatementCreation : System.Web.UI.Page
    {
        private LoginInfo loginInfo;


        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                this.fillFields();
            }
        }

        private void fillFields()
        {
            DBAccessManagement DBAManagement = DBAccessManagement.Instance;
            Stocktaking lastStocktaking = DBAManagement.getLatestStocktaking();

            if (lastStocktaking != null)
            {
                if (DBAManagement.newAnnualAccountPrerequirementsMet(lastStocktaking))
                {
                    stocktakingStatusTB.Text = ServerUtil.DateTimeToString(lastStocktaking.TimeStamp);
                    stocktakingStatusTB.CssClass = "tb_inputCorrect";
                }
                else
                {
                    stocktakingStatusTB.Text = ServerUtil.DateTimeToString(lastStocktaking.TimeStamp) + " - Stocktaking to old";
                    stocktakingStatusTB.CssClass = "tb_inputError";
                }
            }
            else
            {
                stocktakingStatusTB.Text = "NA";
                stocktakingStatusTB.CssClass = "tb_inputError";
            }

            Settlement lastSettlement = DBAManagement.getLastSettlementWithoutData();
            
            if (lastSettlement != null)
            {
                if (DBAManagement.newAnnualAccountPrerequirementsMet(lastSettlement))
                {
                    settlementStatusTB.Text = ServerUtil.DateTimeToString(lastSettlement.EndDate);
                    settlementStatusTB.CssClass = "tb_inputCorrect";
                }
                else
                {
                    settlementStatusTB.Text = ServerUtil.DateTimeToString(lastSettlement.EndDate) + " - Settlement to old";
                    settlementStatusTB.CssClass = "tb_inputError";
                }
            }
            else
            {
                settlementStatusTB.Text = "NA";
                settlementStatusTB.CssClass = "tb_inputError";
            }

            if (lastSettlement != null && lastStocktaking != null 
                && DBAManagement.newAnnualAccountPrerequirementsMet(lastStocktaking, lastSettlement))
            {
                this.Check_Update_Btn.Enabled = true;
            }
            else
            {
                this.Check_Update_Btn.Enabled = false;
            }
        }

        protected void Check_Update_Btn_Click(object sender, EventArgs e)
        {
            if (Check_Update_Btn.Text.Equals("Check"))
            {
                bool allArticleValid = !tb_Name.Text.Equals("");

                if (allArticleValid)
                {
                    Check_Update_Btn.Text = "Create Annual Account";
                }
            }
            else
            {
                DBAccessManagement.Instance.makeAnnualStatement(this.tb_Name.Text);

                //HttpResponse.RemoveOutputCacheItem("~\\WebAccess\\StocktakingCreation.aspx");
                //HttpResponse.RemoveOutputCacheItem("~\\WebAccess\\Stocktakings.aspx");
                Server.Transfer("~\\WebAccess\\AnnualAccounts.aspx", false);
            }
        }


    }
}