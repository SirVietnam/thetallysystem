﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ManageAccounts : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Account> accountList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.accountList = DBAManagement.getAccountList_All().OrderBy(x => x.ID).ToList();

                this.addAccountDataToTable();
            }
        }

        private void addAccountDataToTable()
        {
            for (int i = 0; i < accountList.Count; i++)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = accountList[i].ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell nameCell = new TableCell();
                StringBuilder sb = new StringBuilder();
                sb.Append(accountList[i].Name);
                sb.Append(" ");
                sb.Append(accountList[i].Surname);
                if (accountList[i].Nickname != null && !accountList[i].Nickname.Equals(""))
                {
                    sb.Append(" v/o ");
                    sb.Append(accountList[i].Nickname);
                }
                nameCell.Text = sb.ToString();
                currentRow.Cells.Add(nameCell);

                Button de_activateButton = new Button();
                de_activateButton.ID = accountList[i].ID.ToString();
                de_activateButton.CommandArgument = accountList[i].ID.ToString();
                de_activateButton.Text = accountList[i].IsAvailable ? "Deactivate" : "Activate";
                de_activateButton.Command += OnActivateDeactivate_Click;
                de_activateButton.CssClass = "std_Button";
                de_activateButton.Width = 100;

                TableCell actionCell = new TableCell();
                actionCell.Controls.Add(de_activateButton);

                if (!accountList[i].IsAvailable)
                {
                    currentRow.CssClass = "table_row_cancelled";
                }
                currentRow.Cells.Add(actionCell);

                accTable.Rows.Add(currentRow);
            }


        }

        private void OnActivateDeactivate_Click(object sender, CommandEventArgs e)
        {
            DBAccessManagement DAM = DBAccessManagement.Instance;
            Account account = this.accountList.FirstOrDefault(x => x.ID == int.Parse(e.CommandArgument.ToString()));
            if (account != null)
            {
                account.IsAvailable = !account.IsAvailable;
                DAM.addAccount(account);

                HttpResponse.RemoveOutputCacheItem("/WebAccess/ManageAccounts.aspx");
                Response.Redirect(Request.RawUrl);
            }
        }
    }
}