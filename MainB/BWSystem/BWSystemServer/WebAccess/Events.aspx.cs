﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class Events : System.Web.UI.Page
    {
        LoginInfo loginInfo;
        List<Event> events;


        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.events = DBAccessManagement.Instance.getEventList_All();
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (loginInfo != null && loginInfo.LoggedInAccount != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.events = DBAManagement.getEventList_All();

                this.addEventData();

                //TODO: Add Rows!
            }
        }

        private void addEventData()
        {
            foreach (Event eventData in this.events )
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = eventData.EventID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell nameCell = new TableCell();
                nameCell.Text = eventData.Name;
                currentRow.Cells.Add(nameCell);

                TableCell startCell = new TableCell();
                startCell.Text = ServerUtil.DateTimeToString(eventData.Begin);
                currentRow.Cells.Add(startCell);

                TableCell endCell = new TableCell();
                endCell.Text = ServerUtil.DateTimeToString(eventData.End);
                currentRow.Cells.Add(endCell);

                Button detailsButton = new Button();
                detailsButton.ID = eventData.EventID.ToString();
                detailsButton.CommandArgument = eventData.EventID.ToString();
                detailsButton.Text = "Details";
                detailsButton.Command += OnDetailButton_Click;
                detailsButton.CssClass = "std_Button";

                TableCell detailCell = new TableCell();
                detailCell.Controls.Add(detailsButton);
                currentRow.Cells.Add(detailCell);

                eventsTable.Rows.Add(currentRow);
            }
        }

        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            if (this.loginInfo != null)
            {
                Button btn;
                if (sender is Button)
                {
                    btn = (Button)sender;
                }
                else
                {
                    throw new ArgumentException("Sender is not type of Button!");
                }

                int eventID = Convert.ToInt32(btn.ID);
                Event activeEvent = DBAccessManagement.Instance.getEventList_All().FirstOrDefault(x => x.EventID == eventID);
                if (activeEvent == null)
                {
                    throw new ArgumentException("The given Event ID: " + eventID + " was not found!");
                }
                this.loginInfo.ActiveEvent = activeEvent;
                Server.Transfer("~/WebAccess/EventDetails.aspx", true);
            }
        }

        protected void CreateEvent_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                this.loginInfo.ActiveEvent = new Event();
                Server.Transfer("~/WebAccess/EventCreation.aspx", true);
            }
        }

        protected void SeeArchiveData_Btn_Click(object sender, EventArgs e)
        {

        }
    }
}