﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillDetails.aspx.cs" Inherits="BWSystemServer.WebAccess.BillDetails" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>
<%@ Register Src="~/WebAccess/SubElements/ConsumptionTable.ascx" TagPrefix="SE" TagName="ConsumptionTable" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Bill Details" SubheaderText="See all the details on your bill." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form id="form1" runat="server">
        <div class="std_div">
            <asp:Table ID="settlmentTable" runat="server" Width="600px">
                <asp:TableRow>
                    <asp:TableCell Width="30%">Settlment ID:</asp:TableCell>
                    <asp:TableCell Width="70%">
                        <asp:TextBox ID="tb_settlementID" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="30%">Settlment Date:</asp:TableCell>
                    <asp:TableCell Width="70%">
                        <asp:TextBox ID="tb_DateTime" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>My Costs:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_settlementCosts" CssClass="std_table_tb" runat="server" Enabled="false" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            <SE:ConsumptionTable runat="server" id="ConsumptionTable1" TableMode="Settlement" />
        </div>
    </form>
</body>
</html>
