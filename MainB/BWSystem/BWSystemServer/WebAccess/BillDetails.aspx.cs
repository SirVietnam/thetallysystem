﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class BillDetails : System.Web.UI.Page
    {
        private LoginInfo loginInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.addData();
            }
            else
            {
                SimpleLog.Log("LoginInfo not available. Not logged in?");
            }
        }


        private void addData()
        {
            DBAccessManagement DBAManagement = DBAccessManagement.Instance;
            Settlement settlement = this.loginInfo.Settlement;

            this.tb_settlementID.Text = settlement.SettlementID.ToString();
            this.tb_DateTime.Text = ServerUtil.DateTimeToString(this.loginInfo.Settlement.StartDate) + " - " + ServerUtil.DateTimeToString(this.loginInfo.Settlement.EndDate);
            double payment = DBAManagement.getPayment(settlement.SettlementID, this.loginInfo.LoggedInAccount.ID);
            this.tb_settlementCosts.Text = payment.ToString("0.00");
        }
    }
}