﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Dieser Code wurde von einem Tool generiert.
//
//     Änderungen an dieser Datei können fehlerhaftes Verhalten verursachen und gehen verloren, wenn
//     der Code neu generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace BWSystemServer.WebAccess {
    
    
    public partial class BillDetails {
        
        /// <summary>
        /// Head-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::BWSystemServer.WebAccess.SubElements.Head Head;
        
        /// <summary>
        /// Sidebar-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::BWSystemServer.WebAccess.SubElements.Sidebar Sidebar;
        
        /// <summary>
        /// form1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// settlmentTable-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Table settlmentTable;
        
        /// <summary>
        /// tb_settlementID-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_settlementID;
        
        /// <summary>
        /// tb_DateTime-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_DateTime;
        
        /// <summary>
        /// tb_settlementCosts-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_settlementCosts;
        
        /// <summary>
        /// ConsumptionTable1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::BWSystemServer.WebAccess.SubElements.ConsumptionTable ConsumptionTable1;
    }
}
