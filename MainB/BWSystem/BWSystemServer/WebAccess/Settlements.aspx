﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Settlements.aspx.cs" Inherits="BWSystemServer.WebAccess.CreateSettlement" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Manage Settlements" SubheaderText="Create a new settlement or view old settlements." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form runat="server">
        <div class="std_div">
            <asp:Table ID="settlementTable" runat="server" Width="525px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="75px">ID</asp:TableCell>
                    <asp:TableCell Width="150px">From</asp:TableCell>
                    <asp:TableCell Width="150px">To</asp:TableCell>
                    <asp:TableCell Width="150px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br /><br />
            <div class="substd_div">
                <asp:Button CssClass="std_Button" ID="SeeArchiveData_Btn" runat="server" Text="See Archive Data" OnClick="SeeArchiveData_Btn_Click" />
                <asp:Button CssClass="std_Button" ID="CreateSettlement_Btn" runat="server" Text="Create Settlement" OnClick="CreateSettlement_Btn_Click" />
                <asp:CheckBox ID="lastStocktakingCB" runat="server" Text="Last Stocktaking Timestamp*" />
                <br />
                *Check if this is the last settlement at in an Annual Statement. Make sure you already entered the Stocktaking, but less then 30 minutes ago.
            </div>
        </div>
    </form>
</body>
</html>
