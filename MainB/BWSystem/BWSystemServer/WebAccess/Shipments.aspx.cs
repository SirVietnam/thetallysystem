﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BWSystemServer.WebAccess;
using BWSystemServer;
using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;

namespace BWSystemServer.WebAccess
{
    public partial class SeeShipments : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Shipment> shipments;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);

                if (loginInfo.LoggedInAccount.IsSuperuser)
                {
                    this.shipments = DAM_Shipment.getShipmentListWithoutData();
                    this.addData();
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }


        private void addData()
        {

            foreach (Shipment shipment in this.shipments)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = shipment.ShipmentID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell vendorCell = new TableCell();
                vendorCell.Text = shipment.Vendor;
                currentRow.Cells.Add(vendorCell);

                TableCell dateTimeCell = new TableCell();
                dateTimeCell.Text = ServerUtil.DateTimeToString(shipment.DeliveryDatetime);
                currentRow.Cells.Add(dateTimeCell);

                Button detailsButton = new Button();
                detailsButton.ID = shipment.ShipmentID.ToString();
                detailsButton.Text = "Details";
                detailsButton.Command += OnDetailButton_Click;
                detailsButton.CssClass = "std_Button";

                TableCell detailCell = new TableCell();
                detailCell.Controls.Add(detailsButton);
                currentRow.Cells.Add(detailCell);

                shipmentTable.Rows.Add(currentRow);
            }
        }


        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            Button btn;
            if (sender is Button)
            {
                btn = (Button)sender;
            }
            else
            {
                throw new ArgumentException("Sender is not type of Button!");
            }

            int shipmentID = Convert.ToInt32(btn.ID);            
            this.loginInfo.ActiveShipment = DAM_Shipment.getShipment(shipmentID);
            
            Server.Transfer("~/WebAccess/ShipmentDetails.aspx", true);
        }

        protected void CreateShipment_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                Server.Transfer("~/WebAccess/ShipmentCreation.aspx", true);
            }
        }

        protected void SeeArchiveData_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                Server.Transfer("~/WebAccess/ShipmentArchive.aspx", true);
            }
        }
    }
}