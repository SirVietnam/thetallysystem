﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StocktakingCreation.aspx.cs" Inherits="BWSystemServer.WebAccess.ManageInventorySynchronise" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Synchronise Inventory" SubheaderText="Here you are updating your virtual stock to your physical stock." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form id="form1" runat="server">
        <div class="std_div">

            <asp:Table runat="server" ID="addtbl" CssClass="evenOddMarking" Width="725">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell HorizontalAlign="Center" RowSpan="2">
                        <asp:Label runat="server" Text="ID" Width="50"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" RowSpan="2">
                        <asp:Label runat="server" Text="Article" Width="175"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="1" BackColor="Blue">
                        <asp:Label runat="server" Text="Virtual Stock" Width="150"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3" BackColor="Green">
                        <asp:Label runat="server" Text="Physical Stock" Width="250"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="100" RowSpan="2">
                        <asp:Label runat="server" Text="Loss"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow CssClass="table_head">
                    <asp:TableCell HorizontalAlign="Center" Width="150" BackColor="Blue">
                        <asp:Label runat="server" Text="Bottles"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="100" BackColor="Green">
                        <asp:Label runat="server" Text="Boxes"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="100" BackColor="Green">
                        <asp:Label runat="server" Text="Bottles"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="75" BackColor="Green">
                        <asp:Label runat="server" Text="Sum"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />
            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="Check_Update_Btn" runat="server" Text="Check" OnClick="Check_Update_Btn_Click" />
            </div>
        </div>
    </form>
</body>
</html>
