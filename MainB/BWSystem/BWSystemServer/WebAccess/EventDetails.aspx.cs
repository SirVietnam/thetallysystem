﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class EventDetails : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private Event currentEvent;
        private bool eventStarted;
        private bool eventComplete;
        private bool eventSettled;
        private string filePath;
        private string fileName;

        public bool EventStarted
        {
            get { return eventStarted; }
        }

        public bool EventComplete
        {
            get { return eventComplete; }
        }

        public bool EventSettled
        {
            get { return eventSettled; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.currentEvent = loginInfo.ActiveEvent;
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (loginInfo != null && this.currentEvent != null && this.currentEvent.EventID >= 0)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.initHeadData();
                this.initAvailibleArticles();
                this.initPayedBookings();

                //TODO: Add Rows!
            }
        }


        private void initHeadData()
        {
            this.tb_eventID.Text = this.currentEvent.EventID.ToString();

            Account creatorAccount = DBAccessManagement.Instance.getAccount(this.currentEvent.CreatorAccountID);
            this.tb_creatorName.Text = ServerUtil.accountToDisplayWebName(creatorAccount);

            this.tb_eventName.Text = this.currentEvent.Name;
            this.tb_startDateTime.Text = ServerUtil.DateTimeToString(this.currentEvent.Begin);
            this.tb_endDateTime.Text = ServerUtil.DateTimeToString(this.currentEvent.End);

            this.eventStarted = this.currentEvent.Begin.Ticks < DateTime.Now.Ticks;
            this.eventComplete = this.currentEvent.End.Ticks < DateTime.Now.Ticks;

            string configValue = "StandardEventSettlementExportLocation";
            string absoluteFilePath = ServerUtil.getAbsoluteFilePath(configValue);
            this.fileName = ServerUtil.getFileName(currentEvent);
            this.filePath = absoluteFilePath + this.fileName;

            this.eventSettled = this.currentEvent.EventCosts != null && File.Exists(filePath); //this.filePath

            this.cb_isExclusiveArticle.Checked = this.currentEvent.ArticlesExclusive;

            this.tb_eventCosts.Text = this.eventSettled ? ((double)this.currentEvent.EventCosts).ToString("0.00") : "";

            this.DataBind();
        }

        private void initPayedBookings()
        {
            if (DateTime.Now < this.currentEvent.Begin)
            {
                TableRow currentRow = new TableRow();
                TableCell infoCell = new TableCell();
                infoCell.Text = "The event has not yet started!";
                infoCell.BackColor = System.Drawing.Color.Orange;
                infoCell.ColumnSpan = 6;
                currentRow.Cells.Add(infoCell);

                bookingsTable.Rows.Add(currentRow);
            }
            else if (DateTime.Now < this.currentEvent.End)
            {
                TableRow currentRow = new TableRow();
                TableCell infoCell = new TableCell();
                infoCell.Text = "The event is not yet finished!";
                infoCell.BackColor = System.Drawing.Color.Orange;
                infoCell.ColumnSpan = 6;
                currentRow.Cells.Add(infoCell);

                bookingsTable.Rows.Add(currentRow);
            }
            else
            {
                TableRow currentRow = new TableRow();
                TableCell infoCell = new TableCell();
                infoCell.Text = "The event is finished!";
                infoCell.BackColor = System.Drawing.Color.Orange;
                infoCell.ColumnSpan = 6;
                currentRow.Cells.Add(infoCell);

                bookingsTable.Rows.Add(currentRow);
            }

            List<Account> accounts = DBAccessManagement.Instance.getAccountList_All();
            List<Article> articles = DBAccessManagement.Instance.getArticleList_All();
            List<BookingData> bookings = DAM_Booking.getEventBookings(this.currentEvent.EventID);
            foreach (BookingData booking in bookings)
            {
                foreach (BookingAA bookingAA in booking.Booking)
                {
                    TableCell idCell = new TableCell();
                    idCell.Text = booking.ID.ToString();

                    TableCell bookedByCell = new TableCell();
                    string userString = "--";
                    if (booking.User != null)
                    {
                        Account userAccount = accounts.FirstOrDefault(x => x.ID == booking.User);
                        userString = userAccount.DisplayName;
                    }
                    bookedByCell.Text = userString;

                    TableCell dateCell = new TableCell();
                    dateCell.Text = ServerUtil.DateTimeToString(booking.TimeStamp);

                    TableCell itemCell = new TableCell();
                    Article articleBooked = articles.FirstOrDefault(x => x.ID == bookingAA.Article);
                    itemCell.Text = articleBooked.Name;

                    TableCell amountCell = new TableCell();
                    amountCell.Text = bookingAA.Amount.ToString();

                    TableCell prizeCell = new TableCell();
                    double prize = (double)bookingAA.Amount * articleBooked.Prize;
                    prizeCell.Text = prize.ToString("0.00");

                    TableRow currentRow = new TableRow();
                    currentRow.Cells.Add(idCell);
                    currentRow.Cells.Add(bookedByCell);
                    currentRow.Cells.Add(dateCell);
                    currentRow.Cells.Add(itemCell);
                    currentRow.Cells.Add(amountCell);
                    currentRow.Cells.Add(prizeCell);

                    if (booking.DataCancelled)
                    {
                        currentRow.CssClass = "table_row_cancelled";
                    }

                    bookingsTable.Rows.Add(currentRow);

                }
            }
        }


        private void initAvailibleArticles()
        {
            HashSet<Article> availibleArticles = new HashSet<Article>();
            List<Article> articles = DBAccessManagement.Instance.getArticleList_All();
            foreach (int articleID in this.currentEvent.CostAbsorption)
            {
                Article availibleArticle = articles.FirstOrDefault(x => x.ID == articleID);
                if (availibleArticle != null)
                {
                    availibleArticles.Add(availibleArticle);
                }
                else
                {
                    throw new ArgumentException("The given article does not exist. ArticleID: " + articleID);
                }
            }

            this.loginInfo.PayedArticles = availibleArticles;
        }

        protected void editEvent_Btn_Click(object sender, EventArgs e)
        {
            if (this.currentEvent != null)
            {
                this.loginInfo.ActiveEvent = this.currentEvent;
                Server.Transfer("~/WebAccess/EventCreation.aspx", true);
            }
        }

        protected void eventCompleteBtn_Click(object sender, EventArgs e)
        {
            if (this.currentEvent != null)
            {
                this.eventSettled = DBAccessManagement.Instance.makeEventSettlement(this.currentEvent.EventID);
                DataBind();
            }
        }


        protected void eventDownloadBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + this.fileName);
                    Response.TransmitFile(filePath);
                    Response.End();
                }
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
            }
        }

        protected void eventEndNow_Click(object sender, EventArgs e)
        {
            if (this.currentEvent != null)
            {
                this.currentEvent.End = DateTime.Now;
                bool eventUpdated = DBAccessManagement.Instance.addEvent(this.currentEvent);
                Server.Transfer("~/WebAccess/Events.aspx", true);
            }
        }
    }
}