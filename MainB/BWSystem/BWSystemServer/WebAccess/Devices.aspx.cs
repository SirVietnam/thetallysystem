﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using BWSystemServer.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class Devices : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<ClientDevice> devices;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                if (loginInfo.LoggedInAccount.IsSuperuser)
                {
                    DBAccessManagement DBAM = DBAccessManagement.Instance;
                    this.devices = DAM_ClientDevice.getClientDevices();
                    this.addData();
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }


        private void addData()
        {

            foreach (ClientDevice device in this.devices)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = device.DeviceID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell ipAddressCell = new TableCell();
                ipAddressCell.Text = device.IpAddress;
                currentRow.Cells.Add(ipAddressCell);

                Button deleteButton = new Button();
                deleteButton.ID = device.DeviceID.ToString();
                deleteButton.Text = "Delete";
                deleteButton.Command += OnDeleteButton_Click;
                deleteButton.CssClass = "std_Button";

                TableCell deleteCell = new TableCell();
                deleteCell.Controls.Add(deleteButton);
                currentRow.Cells.Add(deleteCell);

                devicesTable.Rows.Add(currentRow);
            }
        }

        private void OnDeleteButton_Click(object sender, CommandEventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedInAccount != null && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                if (sender is Button)
                {
                    Button button = (Button)sender;
                    int clientID = Convert.ToInt32(button.ID);
                    DAM_ClientDevice.deleteClientDevice(clientID);
                    Server.Transfer("~\\WebAccess\\Devices.aspx", false);
                }
            }
        }

        protected void CreateDevice_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null)
            {
                Server.Transfer("~\\WebAccess\\DeviceCreation.aspx", false);
            }
        }
    }
}