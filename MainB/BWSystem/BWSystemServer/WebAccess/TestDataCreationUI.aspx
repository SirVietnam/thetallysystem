﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestDataCreationUI.aspx.cs" Inherits="BWSystemServer.WebAccess.ManageInventory" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Update Inventory" SubheaderText="Here you change your current intventory status. Make a Settlemente befor updateing your Inventory!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form id="form1" runat="server">
        <div class="std_div">
            <asp:Table runat="server" CssClass="std_table" Width="600">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="getLDAP_Data" runat="server" Text="Get LDAP Data" Width="200" OnClick="getLDAP_Data_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="cleanDB" runat="server" Text="Clean Database" Width="200" OnClick="cleanDB_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="getLDAP_Accounts" runat="server" Text="Get LDAP Accounts" Width="200" OnClick="getLDAP_Accounts_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="readAutoSettlement" runat="server" Text="Read Auto Settlement" Width="250" OnClick="readAutoSettlement_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <%--<asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="shipmentBtn" runat="server" Text="Enter Shipment" Width="200" OnClick="shipmentBtn_Click" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="inventoryBtn" runat="server" Text="Synchronise Inventory" Width="200" OnClick="inventoryBtn_Click" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="archiveBtn" runat="server" Text="Finish Anual Account" Width="200" OnClick="archiveBtn_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="seeShipmentBtn" runat="server" Text="See Shipments" Width="200" OnClick="seeShipmentBtn_Click" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="seeInventoryBtn" runat="server" Text="See Stocktakings" Width="200" OnClick="seeInventoryBtn_Click" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="seeAnualAccounts" runat="server" Text="See Anual Accounts" Width="200" OnClick="seeAnualAccounts_Click" />
                    </asp:TableCell>
                </asp:TableRow>--%>
            </asp:Table>
            <br />
            <div>
                <asp:Button ID="tdcBtn" runat="server" Text="Test Data Creation" Width="200" OnClick="tdcBtn_Click" />
                <asp:TextBox ID="bookingsAmount" runat="server" Text="33" />
                <br />
                <asp:Button ID="setBtn" runat="server" Text="Stocktaking Creation" Width="200" OnClick="setBtn_Click" />
                <asp:TextBox ID="setDeviation" runat="server" Text="0,0"  />
                <br />
                <asp:Button ID="shipBtn" runat="server" Text="Shipment Creation" Width="200" OnClick="shipBtn_Click" />
                <asp:TextBox ID="averageAmountShipment" runat="server"  Text="11" />
                <asp:TextBox ID="deviationMaxShipment" runat="server"  Text="0,0"  />
            </div>
        </div>

    </form>
</body>
</html>
