﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ShipmentDetails : System.Web.UI.Page
    {
        LoginInfo loginInfo;
        Shipment shipment;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                if (loginInfo.LoggedIn && this.loginInfo.ActiveShipment != null && loginInfo.LoggedInAccount.IsSuperuser)
                {
                    DBAccessManagement DBAM = DBAccessManagement.Instance;
                    //this.shipment = DBAM.getShipment(this.loginInfo.ActiveShipment.ShipmentID);
                    this.shipment = this.loginInfo.ActiveShipment;

                    //TODO add fields
                    this.fillShipmentGeneralData();
                    this.fillShipmentDetailData();
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }


        private void fillShipmentGeneralData()
        {
            tb_shipmentID.Text = this.shipment.ShipmentID.ToString();
            tb_vendor.Text = this.shipment.Vendor;
            tb_deliveryDateTime.Text = ServerUtil.DateTimeToString(this.shipment.DeliveryDatetime);
        }


        private void fillShipmentDetailData()
        {
            foreach (Tuple<Article, int> articleSupply in this.shipment.ArticleAmountList)
            {
                Article article = articleSupply.Item1;

                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = article.ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell articleNameCell = new TableCell();
                articleNameCell.Text = article.Name;
                currentRow.Cells.Add(articleNameCell);

                TableCell amountCell = new TableCell();
                amountCell.Text = articleSupply.Item2.ToString();
                currentRow.Cells.Add(amountCell);

                shipmentTable.Rows.Add(currentRow);
            }
        }

    }
}