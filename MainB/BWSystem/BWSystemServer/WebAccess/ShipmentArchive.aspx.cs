﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ShipmentArchive : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Shipment> shipments;
        private List<AnnualStatement> annualStatements;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                if (loginInfo.LoggedInAccount.IsSuperuser)
                {
                    this.shipments = DAM_ArchiveManagement.Instance.getArchiveShipmentList();
                    this.annualStatements = DBAccessManagement.Instance.getAnnualStatements();
                    this.addData();
                }
            }
            else
            {
                SimpleLog.Log("No Data for ShipmentArchive");
            }
        }

        private void addData()
        {
            this.shipments = this.shipments.OrderBy(x => x.DeliveryDatetime).ToList();
            this.annualStatements = this.annualStatements.OrderBy(x => x.StartDate).ToList();

            int currentShipmentIndex = 0;
            foreach (AnnualStatement annualStatement in this.annualStatements)
            {
                GuiWebUtil.addAnnualStatementRow(shipmentTable, 4, annualStatement);

                while (currentShipmentIndex < this.shipments.Count
                    && this.shipments[currentShipmentIndex].DeliveryDatetime >= annualStatement.StartDate
                    && this.shipments[currentShipmentIndex].DeliveryDatetime <= annualStatement.EndDate)
                {
                    this.addShipmentRow(this.shipments[currentShipmentIndex]);
                    currentShipmentIndex++;
                }
            }
        }

        //private void addAnnualStatementRow(AnnualStatement annualStatement)
        //{
        //    TableRow currentRow = new TableRow();
        //    TableCell annualStatementCell = new TableCell();
        //    annualStatementCell.Text = annualStatement.AnnualStatementName;
        //    annualStatementCell.ColumnSpan = 4;
        //    currentRow.Cells.Add(annualStatementCell);
        //    shipmentTable.Rows.Add(currentRow);
        //}

        private void addShipmentRow(Shipment shipment)
        {
            TableRow currentRow = new TableRow();
            TableCell idCell = new TableCell();
            idCell.Text = shipment.ShipmentID.ToString();
            currentRow.Cells.Add(idCell);

            TableCell vendorCell = new TableCell();
            vendorCell.Text = shipment.Vendor;
            currentRow.Cells.Add(vendorCell);

            TableCell dateTimeCell = new TableCell();
            dateTimeCell.Text = ServerUtil.DateTimeToString(shipment.DeliveryDatetime);
            currentRow.Cells.Add(dateTimeCell);

            Button detailsButton = new Button();
            detailsButton.ID = shipment.ShipmentID.ToString();
            detailsButton.Text = "Details";
            detailsButton.Command += OnDetailButton_Click;
            detailsButton.CssClass = "std_Button";

            TableCell detailCell = new TableCell();
            detailCell.Controls.Add(detailsButton);
            currentRow.Cells.Add(detailCell);

            shipmentTable.Rows.Add(currentRow);
        }

        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            Button btn;
            if (sender is Button)
            {
                btn = (Button)sender;
            }
            else
            {
                throw new ArgumentException("Sender is not type of Button!");
            }

            int shipmentID = Convert.ToInt32(btn.ID);
            this.loginInfo.ActiveShipment = this.shipments.First(x => x.ShipmentID == shipmentID);

            Server.Transfer("~/WebAccess/ShipmentDetails.aspx", true);
        }
    }
}