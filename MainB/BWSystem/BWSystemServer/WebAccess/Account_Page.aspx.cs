﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BWSystemServer;
using BWSystemServer.BIZ;

namespace BWSystemServer.WebAccess
{
    public partial class Account_Page : System.Web.UI.Page
    {
        private static string authCodeSeperatorSign = ";";
        private bool isReadOnly;
        private LoginInfo loginInfo;
        private Account account;

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.isReadOnly;
            this.account = null;
            this.isReadOnly = true;

            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                if (this.loginInfo.LoggedIn)
                {
                    this.account = this.loginInfo.LoggedInAccount;
                    this.isReadOnly = !this.loginInfo.onPage_Load(this, this.account.ID);
                    if (this.isReadOnly)
                    {
                        this.addAccountDataToPage();
                    }
                }
            }
            else
            {
                this.Edit_Save_Btn.Enabled = false;
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
            DataBind();
        }


        private void addAccountDataToPage()
        {
            tb_AccID.Text = account.ID.ToString();
            tb_Surname.Text = account.Surname;
            tb_Name.Text = account.Name;
            tb_Nickname.Text = account.Nickname;
            tb_StdArticle.Text = account.Standard.ToString();

            if (account.Authentication.SecurityCode != null)
            {
                StringBuilder sb = new StringBuilder();
                foreach (int securityCode in account.Authentication.SecurityCode)
                {
                    sb.Append(securityCode + Account_Page.authCodeSeperatorSign);
                }
                tb_AuthCode.Text = sb.ToString();

                if (account.Authentication.SecurityCodeHash.Length == 0 || account.Authentication.SecurityCodeHash.Length == 1)
                {
                    account.Authentication.SecurityCodeSalt = PasswordHashUtil.generateSalt();
                    account.Authentication.SecurityCodeHash = PasswordHashUtil.generateSaltedPasswordHash(account.Authentication.SecurityCode, account.Authentication.SecurityCodeSalt);
                }
                tb_HashCode.Text = BitConverter.ToString(account.Authentication.SecurityCodeHash).Remove(12) + "...";
                tb_SaltCode.Text = BitConverter.ToString(account.Authentication.SecurityCodeSalt).Remove(12) + "...";

                sb = sb.Clear();
                if (account.Authentication.CardCode != null && account.Authentication.CardCode.Length != 0)
                {
                    foreach (long cardCode in account.Authentication.CardCode)
                    {
                        sb.Append(cardCode.ToString() + "; ");
                    }
                }
                tb_AuthCard.Text = sb.ToString();
            }
            cb_AuthAlways.Checked = account.AuthenticateAlways;
            cb_IsAdmin.Checked = account.IsSuperuser;

            DBAccessManagement DAM = DBAccessManagement.Instance;
            string articleImagePath = DAM.getUserImageRelativePath(account.PictureURL);
            img_Picture.ImageUrl = articleImagePath;
        }


        /// Getter & Setter
        public bool IsReadOnly
        {
            get { return isReadOnly; }
        }

        protected void Edit_Save_Click(object sender, EventArgs e)
        {
            if (this.isReadOnly)
            {
                this.isReadOnly = !this.loginInfo.onEditStart(this);
                Edit_Save_Btn.Text = "Save Changes";

            }
            else
            {
                this.saveAccount();
                this.loginInfo.onEditComplete(this);
                this.isReadOnly = true;
                Edit_Save_Btn.Text = "Edit Data";
                //TODO: Save modified data!
            }
            DataBind();
        }

        internal void saveAccount()
        {
            account.Surname = tb_Surname.Text;
            account.Name = tb_Name.Text;
            account.Nickname = tb_Nickname.Text;
            account.Standard = Convert.ToInt32(tb_StdArticle.Text);

            string[] authCodeString = tb_AuthCode.Text.Split(Account_Page.authCodeSeperatorSign.ToCharArray());
            int codeLength = authCodeString.Length;
            if (authCodeString[codeLength - 1].Equals(""))
            {
                codeLength -= 1;
            }

            int[] authCode = new int[codeLength];
            for (int i = 0; i < authCode.Length; i++)
            {
                authCode[i] = Convert.ToInt32(authCodeString[i]);
            }
            account.Authentication.SecurityCode = authCode;

            account.AuthenticateAlways = cb_AuthAlways.Checked;
            account.IsSuperuser = cb_IsAdmin.Checked;

            DBAccessManagement DAM = DBAccessManagement.Instance;
            DAM.addAccount(this.account);
        }

        protected void changePicture_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null)
            {
                this.loginInfo.onEditComplete(this);
                Server.Transfer("~\\WebAccess\\FileUploadPage.aspx", false);
            }
        }
    }
}