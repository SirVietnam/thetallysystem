﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Shipments.aspx.cs" Inherits="BWSystemServer.WebAccess.SeeShipments" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Shipments" SubheaderText="Here you find all the previous shipments!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="shipmentTable" runat="server" Width="500px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="50px">ID</asp:TableCell>
                    <asp:TableCell Width="200px">Vendor</asp:TableCell>
                    <asp:TableCell Width="200px">Date Time</asp:TableCell>
                    <asp:TableCell Width="50px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />
            <div class="substd_div" style="float: right;">
                <asp:Button CssClass="std_Button" ID="SeeArchiveData_Btn" runat="server" Text="See Archive Data" OnClick="SeeArchiveData_Btn_Click" />
                <asp:Button CssClass="std_Button" ID="CreateShipment_Btn" runat="server" Text="Create Shipment" OnClick="CreateShipment_Btn_Click" />
            </div>
             
        </form>
    </div>
</body>
</html>
