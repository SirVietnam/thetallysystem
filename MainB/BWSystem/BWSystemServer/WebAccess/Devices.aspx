﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Devices.aspx.cs" Inherits="BWSystemServer.WebAccess.Devices" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Manage Devices" SubheaderText="You can see and manage all your registered client devices here." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form runat="server">
        <div class="std_div">
            <asp:Table ID="devicesTable" runat="server" Width="550px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="50px">ID</asp:TableCell>
                    <asp:TableCell Width="350px">IP Address</asp:TableCell>
                    <asp:TableCell Width="150px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />
            <br />
            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="CreateDevice_Btn" runat="server" Text="Create Device" OnClick="CreateDevice_Btn_Click" />
            </div>
        </div>
    </form>
</body>
</html>
