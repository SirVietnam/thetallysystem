﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class CreateSettlement : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Settlement> settlementList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.settlementList = DBAManagement.getSettlementListWithoutData();

                this.addArticleDataToTable();
            }
        }


        private void addArticleDataToTable()
        {
            for (int i = 0; i < settlementList.Count; i++)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = settlementList[i].SettlementID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell fromCell = new TableCell();
                fromCell.Text = ServerUtil.DateTimeToString(settlementList[i].StartDate);
                currentRow.Cells.Add(fromCell);

                TableCell toCell = new TableCell();
                toCell.Text = ServerUtil.DateTimeToString(settlementList[i].EndDate);
                currentRow.Cells.Add(toCell);

                Button detailsButton = new Button();
                detailsButton.ID = ServerUtil.getFileName(settlementList[i]);
                detailsButton.CommandArgument = settlementList[i].SettlementID.ToString();
                detailsButton.Text = "Download";
                detailsButton.Command += OnDownloadButton_Clicked;
                detailsButton.CssClass = "std_Button";

                TableCell detailCell = new TableCell();
                detailCell.Controls.Add(detailsButton);
                currentRow.Cells.Add(detailCell);

                settlementTable.Rows.Add(currentRow);
            }
        }

        private void OnDownloadButton_Clicked(object sender, CommandEventArgs e)
        {
            if (sender is Button)
            {
                Button dlButton = (Button)sender;
                string fileName = dlButton.ID;
                //string relativeFileFolder = (string)ServerUtil.getConfigValue(, typeof(string));
                string absoluteFilePath = ServerUtil.getAbsoluteFilePath("StandardSettlementExportLocation") + fileName;

                try {
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                    Response.TransmitFile(absoluteFilePath);
                    Response.End();
                } catch (Exception ex)
                {
                    SimpleLog.Log(ex);
                }
            }
        }


        protected void CreateSettlement_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement DAM = DBAccessManagement.Instance;
                DAM.makeSettlement(lastStocktakingCB.Checked);
                Server.Transfer("~\\WebAccess\\Settlements.aspx", false);
            }
        }


        protected void SeeArchiveData_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                Server.Transfer("~/WebAccess/SettlementArchive.aspx", true);
            }
        }
    }
}