﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageArticles.aspx.cs" Inherits="BWSystemServer.WebAccess.ManageArticles" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Article Details" SubheaderText="See all the details on the article." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form runat="server">
        <div class="std_div">
            <asp:Table ID="artTable" runat="server" Width="400px" CssClass="evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="75px">ID</asp:TableCell>
                    <asp:TableCell Width="200px">Name</asp:TableCell>
                    <asp:TableCell Width="75px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="NewArticle_Btn" runat="server" Text="Add Article" OnClick="NewArticle_Btn_Click" /> 

                <asp:Button CssClass="std_Button" ID="ChangePrize_Btn" runat="server" Text="Change Prizes" OnClick="ChangePrize_Btn_Click" />
            </div>
        </div>
    </form>

</body>
</html>
