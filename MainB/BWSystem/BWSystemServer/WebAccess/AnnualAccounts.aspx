﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnnualAccounts.aspx.cs" Inherits="BWSystemServer.WebAccess.SeeAnualAccounts" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Anual Accounts" SubheaderText="Here you find all the previous anual accounts!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="annualAccountTable" runat="server" Width="700px" CssClass="evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="50px">ID</asp:TableCell>
                    <asp:TableCell Width="200px">Name</asp:TableCell>
                    <asp:TableCell Width="200px">Start Date</asp:TableCell>
                    <asp:TableCell Width="200px">End Date</asp:TableCell>
                    <asp:TableCell Width="50px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />
            <div class="substd_div" style="float: right;">
                <asp:Button CssClass="std_Button" ID="CreateAnnualAccount_Btn" runat="server" Text="Create Annual Account" OnClick="CreateAnnualAccount_Btn_Click" />
            </div>
        </form>
    </div>
</body>
</html>
