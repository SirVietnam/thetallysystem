﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class UnsettledBookings : System.Web.UI.Page
    {
        private LoginInfo loginInfo;

        private List<BookingData> bookings;
        private List<Account> accounts;
        private List<Article> articles;
        private Dictionary<int, Account> accountDic;
        private Dictionary<int, Article> articleDic;


        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.accounts = DBAManagement.getAccountList_All();
                this.accountDic = new Dictionary<int, Account>();
                foreach (Account acc in this.accounts)
                {
                    accountDic.Add(acc.ID, acc);
                }

                this.articles = DBAManagement.getArticleList_All();
                this.articleDic = new Dictionary<int, Article>();
                foreach (Article art in this.articles)
                {
                    articleDic.Add(art.ID, art);
                }

                DateTime bookingsStartDateTime = DBAManagement.getLastSettlementEndDate();
                this.bookings = DAM_Booking.getBookingDataList(bookingsStartDateTime);

                this.addBookingData();
            }
        }


        private void addBookingData()
        {
            for (int i = 0; i < bookings.Count; i++)
            {
                foreach (BookingAA bookingAA in bookings[i].Booking)
                {
                    TableCell idCell = new TableCell();
                    idCell.Text = bookings[i].ID.ToString();

                    TableCell consumerCell = new TableCell();
                    consumerCell.Text = ServerUtil.accountToDisplayWebName(accountDic[(int)bookings[i].Customer], true);

                    TableCell bookedByCell = new TableCell();
                    bookedByCell.Text = (bookings[i].User != null) ? ServerUtil.accountToDisplayWebName(accountDic[(int)bookings[i].User], true) : "--";

                    TableCell dateCell = new TableCell();
                    dateCell.Text = bookings[i].TimeStamp.ToString();

                    TableCell itemCell = new TableCell();
                    Article articleBooked = articleDic[bookingAA.Article];
                    itemCell.Text = articleBooked.Name;

                    TableCell amountCell = new TableCell();
                    amountCell.Text = bookingAA.Amount.ToString();

                    TableCell prizeCell = new TableCell();
                    double prize = (double)bookingAA.Amount * articleBooked.Prize;
                    prizeCell.Text = prize.ToString("0.00");

                    TableRow currentRow = new TableRow();
                    currentRow.Cells.Add(idCell);
                    currentRow.Cells.Add(consumerCell);
                    currentRow.Cells.Add(bookedByCell);
                    currentRow.Cells.Add(itemCell);
                    currentRow.Cells.Add(amountCell);
                    currentRow.Cells.Add(prizeCell);
                    currentRow.Cells.Add(dateCell);

                    if (bookings[i].DataCancelled)
                    {
                        currentRow.CssClass = "table_row_cancelled";
                    }

                    bookingTable.Rows.Add(currentRow);
                }
            }
        }
    }
}