﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventCreation.aspx.cs" Inherits="BWSystemServer.WebAccess.EventCreation" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>
<%@ Register Src="~/WebAccess/SubElements/Event_ArticleTable.ascx" TagPrefix="SE" TagName="Event_ArticleTable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Event Creation" SubheaderText="Create or modifiy an event." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <asp:Table ID="artTable" runat="server" Width="500px">
                <asp:TableRow>
                    <asp:TableCell Width="125">Event ID:</asp:TableCell>
                    <asp:TableCell Width="375">
                        <asp:TextBox ID="tb_eventID" Width="100%" CssClass="std_table_tb" runat="server" Enabled="false" ReadOnly="true"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Event Name:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_eventName" Width="100%" CssClass="std_table_tb" runat="server" AutoPostBack="true" Text="" OnTextChanged="tb_eventName_TextChanged"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <asp:Table ID="dateTimeTable" runat="server" Width="500px">
                <asp:TableRow>
                    <asp:TableCell Width="125">Start Date:</asp:TableCell>
                    <asp:TableCell Width="225">
                        <asp:TextBox ID="tb_startDate" CssClass="std_table_tb" runat="server" OnTextChanged="tb_startDate_TextChanged"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="startDateCE" runat="server"
                            TargetControlID="tb_startDate" PopupButtonID="tb_Date" Format="yyyy-MM-dd"></ajaxToolkit:CalendarExtender>
                    </asp:TableCell>
                    <asp:TableCell Width="150">
                        <asp:DropDownList ID="ddl_startHour" runat="server" CssClass="std_Textbox" AutoPostBack="true" OnSelectedIndexChanged="tb_startDate_TextChanged">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddl_startMinutes" runat="server" CssClass="std_Textbox" Sytle="margin-left: 10px;" AutoPostBack="true" OnSelectedIndexChanged="tb_startDate_TextChanged">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="125">End Date:</asp:TableCell>
                    <asp:TableCell Width="300">
                        <asp:TextBox ID="tb_endDate" CssClass="std_table_tb" runat="server" OnTextChanged="tb_endDate_TextChanged"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="endDateCE" runat="server"
                            TargetControlID="tb_endDate" PopupButtonID="tb_Date" Format="yyyy-MM-dd"></ajaxToolkit:CalendarExtender>
                    </asp:TableCell>
                    <asp:TableCell Width="150">
                        <asp:DropDownList ID="ddl_endHour" runat="server" CssClass="std_Textbox" AutoPostBack="true" OnSelectedIndexChanged="tb_endDate_TextChanged">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddl_endMinutes" runat="server" CssClass="std_Textbox" Sytle="margin-left: 10px;" AutoPostBack="true" OnSelectedIndexChanged="tb_endDate_TextChanged">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Exclusive Articles:</asp:TableCell>
                    <asp:TableCell>
                        <asp:CheckBox ID="cb_isExclusiveArticle" runat="server" Enabled="true" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            <asp:Table ID="addArticleTypeTable" runat="server" Width="400px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="100%" ColumnSpan="3">Add payed for articles</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="100px">By Type:</asp:TableCell>
                    <asp:TableCell Width="200px">
                        <asp:DropDownList ID="ddl_articleTypes" runat="server" Width="100%"></asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell Width="100px" Style="padding-left: 10px;">
                        <asp:Button CssClass="std_Button" ID="addType_Btn" runat="server" Text="Add" OnClick="addType_Btn_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <%--<asp:TableRow>
                    <asp:TableCell Width="100px">By Article:</asp:TableCell>
                    <asp:TableCell Width="200px">
                        <asp:DropDownList ID="ddl_article" runat="server" Width="100%"></asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell Width="100px" Style="padding-left: 10px;">
                        <asp:Button ID="addArticle_Btn" runat="server" Text="Add" OnClick="addArticle_Btn_Click" CssClass="std_Button" />
                    </asp:TableCell>
                </asp:TableRow>--%>
            </asp:Table>
            <br />
            <SE:Event_ArticleTable runat="server" ID="payedArticlesTable" TableData="payedArticles" ButtonText="Remove" HeadlineText="Payed for Articles" />
            <br />
            <SE:Event_ArticleTable runat="server" ID="notPayedArticlesTable" TableData="notPayedArticles" ButtonText="Add" HeadlineText="Not payed for Articles" />
            <br />
            <SE:Event_ArticleTable runat="server" ID="disabledArticlesTable" TableData="disabledArticles" ButtonText="Add" HeadlineText="Disabled Articles" />
            <br />
            <br />
            <div class="substd_div">
                <asp:Button CssClass="std_Button" ID="save_Btn" runat="server" Text="Save" OnClick="save_Btn_Click" />
            </div>
        </form>
    </div>
</body>
</html>
