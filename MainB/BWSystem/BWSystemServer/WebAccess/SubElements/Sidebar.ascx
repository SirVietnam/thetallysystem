﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sidebar.ascx.cs" Inherits="BWSystemServer.WebAccess.SubElements.Sidebar" %>

<div style="width: 200px;">
    <ul id="sidebar">
        <li><a class="private" id="DefaultPageLink" runat="server" href="~/Default.aspx">Log In</a></li>
        <li><a class="private" id="AccountPageLink" runat="server" href="~/WebAccess/Account_Page.aspx">My Account</a></li>
        <li><a class="private" id="BillsPageLink" runat="server" href="~/WebAccess/Bills.aspx">My Bills</a></li>
        <%--<li><a class="private" id="BillDetailsLink" runat="server" href="~/WebAccess/BillDetails.aspx">Bill Details</a></li>--%>
        <li><a class="private" id="ConsumePageLink" runat="server" href="~/WebAccess/CurrentConsume.aspx">Open Tab</a></li>
        <li><a class="private" id="Statistics" runat="server" href="~/WebAccess/Statistics.aspx">Statistics</a></li>
        <li><a class="admin" id="UnsettledBookingsPageLink" runat="server" href="~/WebAccess/UnsettledBookings.aspx">Open Bookings</a></li>
        <li><a class="admin" id="ShipmentsPageLink" runat="server" href="~/WebAccess/Shipments.aspx">Shipments</a></li>
        <li><a class="admin" id="StocktakingPageLink" runat="server" href="~/WebAccess/Stocktakings.aspx">Stocktakings</a></li>
        <li><a class="admin" id="SettlementsPageLink" runat="server" href="~/WebAccess/Settlements.aspx">Settlements</a></li>
        <li><a class="admin" id="AnnualAccountsPageLink" runat="server" href="~/WebAccess/AnnualAccounts.aspx">Annual Accounts</a></li>
        <li><a class="admin" id="EventsPageLink" runat="server" href="~/WebAccess/Events.aspx">Manage Events</a></li>
        <li><a class="admin" id="ManageArticlePageLink" runat="server" href="~/WebAccess/ManageArticles.aspx">Manage Articles</a></li>
        <li><a class="admin" id="ManageAccountPageLink" runat="server" href="~/WebAccess/ManageAccounts.aspx">Manage Accounts</a></li>
        <li><a class="admin" id="ManageClientPageLink" runat="server" href="~/WebAccess/Devices.aspx">Manage Devices</a></li>
        <li><a class="admin" id="TestDataCreationPageLink" runat="server" href="~/WebAccess/TestDataCreationUI.aspx">Test Data Creation</a></li>
        <li><a class="sign_off" id="LogOffPageLink" runat="server" href="~/WebAccess/LogOff.aspx">Sign Off</a></li>
    </ul>
</div>
