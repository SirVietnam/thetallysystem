﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess.SubElements
{
    public partial class ArticleTable : System.Web.UI.UserControl
    {
        private Regex regexIntNumber;
        private Regex regexFloatNumber;
        private Regex regexPrize;
        private bool isReadOnly;
        private bool isNewArticle;
        private LoginInfo loginInfo;
        private Article article;


        protected void Page_Load(object sender, EventArgs e)
        {
            this.article = null;
            this.isReadOnly = true;
            this.isNewArticle = false;

            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.article = this.loginInfo.ActiveArticle;
                //this.isReadOnly = !this.loginInfo.onPage_Load(this, this.article.ID);
                this.addArtType(article.Type);
                this.regexIntNumber = ServerUtil.RegexIntNumber;
                this.regexFloatNumber = ServerUtil.RegexFloatNumber;
                this.regexPrize = ServerUtil.RegexPrize;

                if (this.article.ID == -1)
                {
                    this.isNewArticle = true;
                    this.Edit_Save_Click(this, null);
                }
            }
            else
            {
                this.Edit_Save_Btn.Enabled = false;
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.article != null)
            {
                if (this.isReadOnly)
                {
                    this.addArticleDataToPage();
                }
            }
            DataBind();
        }

        private void addArticleDataToPage()
        {
            tb_ArtID.Text = article.ID.ToString();
            tb_Name.Text = article.Name.ToString();

            ddl_Type.ClearSelection();
            ddl_Type.Items.FindByValue(article.Type.ToString()).Selected = true;

            tb_Prize.Text = article.Prize.ToString("0.00");
            tb_Size.Text = article.Size.ToString("0.00");
            tb_InStock.Text = article.Stock.ToString();
            tb_Alcohol.Text = article.AlcoholicStrength.ToString("0.00");
            tb_Manufacturer.Text = article.Manufacturer;
            tb_Picture.Text = article.PictureURL;
            tb_Description.Text = article.Description;
            cb_IsInventory.Checked = article.IsInventory;
            cb_IsAvailible.Checked = article.IsAvailable;

            DBAccessManagement DAM = DBAccessManagement.Instance;
            string articleImagePath = DAM.getArticleImageRelativePath(article.PictureURL);
            img_Picture.ImageUrl = articleImagePath;
        }

        private void addArtType(Article.ArticleType currentType)
        {
            if (ddl_Type.Items.Count == 0)
            {
                foreach (Article.ArticleType type in Enum.GetValues(typeof(Article.ArticleType)))
                {
                    ListItem newItem = new ListItem(type.ToString(), type.ToString());
                    if (type == currentType)
                    {
                        this.loginInfo.CurrentArtileType = newItem;
                    }
                    ddl_Type.Items.Add(newItem);
                    System.Diagnostics.Debug.WriteLine(type.ToString());
                }
            }
        }


        protected void Edit_Save_Click(object sender, EventArgs e)
        {
            if (this.isReadOnly)
            {
                //this.isReadOnly = !this.loginInfo.onEditStart(this);
                Edit_Save_Btn.Text = this.isNewArticle ? "Save Article" : "Save Changes";


            }
            else if (this.isNewArticle)
            {
                bool validInput = this.validateInput();
                if (validInput)
                {
                    this.saveArticle();
                    //this.loginInfo.onEditComplete(this);
                    Server.Transfer("~\\WebAccess\\ManageArticles.aspx", false);
                }
            }
            else
            {
                bool validInput = this.validateInput();
                if (validInput)
                {
                    this.saveArticle();
                    //this.loginInfo.onEditComplete(EditDataPage.ArticlePage);
                    this.isReadOnly = true;
                    Edit_Save_Btn.Text = "Edit Data";
                }
                //TODO: Save modified data!
            }
                DataBind();
            }

        private bool validateInput()
        {
            bool validName = !tb_Name.Text.Equals("");
            tb_Name.CssClass = validName ? "std_table_tb" : "std_table_inputError_tb";
            bool validPrize = this.regexPrize.IsMatch(tb_Prize.Text);
            tb_Prize.CssClass = validPrize ? "std_table_tb" : "std_table_inputError_tb";
            bool validSize = this.regexFloatNumber.IsMatch(tb_Size.Text);
            tb_Size.CssClass = validSize ? "std_table_tb" : "std_table_inputError_tb";
            bool validAlcoholicStrength = this.regexFloatNumber.IsMatch(tb_Alcohol.Text);
            tb_Alcohol.CssClass = validAlcoholicStrength ? "std_table_tb" : "std_table_inputError_tb";
            bool validManufacturer = !tb_Manufacturer.Text.Equals("");
            tb_Manufacturer.CssClass = validManufacturer ? "std_table_tb" : "std_table_inputError_tb";
            bool validDescription = !tb_Description.Text.Equals("");
            tb_Description.CssClass = validDescription ? "std_table_tb" : "std_table_inputError_tb";
            return (validName && validPrize && validSize && validAlcoholicStrength && validManufacturer && validDescription);
        }

        internal void saveArticle()
        {
            article.Name = tb_Name.Text;
            article.Type = (Article.ArticleType)Enum.Parse(typeof(Article.ArticleType), ddl_Type.SelectedValue);
            article.Prize = ServerUtil.StringToDouble(tb_Prize.Text);
            article.Size = ServerUtil.StringToDouble(tb_Size.Text);
            article.Stock = tb_InStock.Text.Equals("") ? 0 : Convert.ToInt32(tb_InStock.Text);
            article.AlcoholicStrength = ServerUtil.StringToDouble(tb_Alcohol.Text);
            article.Manufacturer = tb_Manufacturer.Text;
            article.Description = tb_Description.Text;
            article.IsInventory = cb_IsInventory.Checked;
            article.IsAvailable = cb_IsAvailible.Checked;

            DBAccessManagement DAM = DBAccessManagement.Instance;
            DAM.addArticle(this.article);
        }


        protected void changePicture_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null)
            {
                //this.loginInfo.onEditComplete(EditDataPage.ArticlePage);
                //this.loginInfo.onPage_LoadEdit(EditDataPage.ArticlePicture, this.article.ID);
                Server.Transfer("~\\WebAccess\\FileUploadPage.aspx", false);
            }
        }

        /// <summary>
        /// Getter & Setter
        /// </summary>
        public bool IsReadOnly
        {
            get { return isReadOnly; }
        }

        public bool IsNewArticle
        {
            get { return isNewArticle; }
        }
    }
}