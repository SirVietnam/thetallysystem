﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsumptionTable.ascx.cs" Inherits="BWSystemServer.WebAccess.SubElements.ConsumptionTable" %>
<asp:Table ID="bookingTable" runat="server" Width="800px" CssClass="evenOddMarking hooverMarking">
    <asp:TableRow CssClass="table_head">
        <asp:TableCell Width="50px">ID</asp:TableCell>
        <asp:TableCell Width="175px">Booked By</asp:TableCell>
        <asp:TableCell Width="175px">Item</asp:TableCell>
        <asp:TableCell Width="75px">Amount</asp:TableCell>
        <asp:TableCell Width="100px">Prize</asp:TableCell>
        <asp:TableCell Width="200px">Date</asp:TableCell>
    </asp:TableRow>
</asp:Table>
<p>*Grayed out lines are cancelled bookings.</p>
