﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess.SubElements
{
    public partial class Event_ArticleTable : System.Web.UI.UserControl
    {
        private string tableData;
        private LoginInfo loginInfo;
        private HashSet<Article> tableArticles;
        private string buttonText;
        private string headlineText;
        private Color headlineBackgroundColor;
        private CommandEventHandler onButtonClicked;


        public string TableData
        {
            get { return tableData; }
            set { tableData = value; }
        }

        public string ButtonText
        {
            get { return buttonText; }
            set { buttonText = value; }
        }

        public string HeadlineText
        {
            get { return headlineText; }
            set { headlineText = value; }
        }

        public CommandEventHandler OnButtonClicked
        {
            get { return onButtonClicked; }
            set { onButtonClicked = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
                {
                    if (TableData == "payedArticles")
                    {
                        this.tableArticles = this.loginInfo.PayedArticles;
                        this.headlineBackgroundColor = Color.Green;
                    }
                    else if (TableData == "notPayedArticles")
                    {
                        this.tableArticles = this.loginInfo.NotPayedArticles;
                        this.headlineBackgroundColor = Color.Red;
                    }
                    else if (TableData == "disabledArticles")
                    {
                        this.tableArticles = this.loginInfo.DisabledArticles;
                        this.headlineBackgroundColor = Color.Gray;
                    }

                    this.headCell.Text = this.HeadlineText;
                    this.headCell.BackColor = this.headlineBackgroundColor;

                    this.drawArticles();
                }

            }
        }

        public void drawArticles()
        {
            foreach (Article article in this.tableArticles.OrderBy(x => x.Name))
            {
                TableRow currentRow = new TableRow();
                //currentRow.ID = article.ID.ToString() + "_row";
                if (!article.IsAvailable)
                {
                    currentRow.BackColor = System.Drawing.Color.Gray;
                }
                TableCell idCell = new TableCell();
                idCell.Text = article.ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell nameCell = new TableCell();
                nameCell.Text = article.Name;
                currentRow.Cells.Add(nameCell);

                TableCell prizeCell = new TableCell();
                prizeCell.Text = article.Prize.ToString("0.00");
                currentRow.Cells.Add(prizeCell);

                if (this.buttonText != "")
                {
                    TableCell buttonCell = new TableCell();
                    Button button = new Button();
                    button.CommandArgument = article.ID.ToString();
                    button.CssClass = "std_Button";
                    button.Command += this.OnButtonClicked;
                    button.Text = this.buttonText;
                    buttonCell.Controls.Add(button);
                    currentRow.Cells.Add(buttonCell);
                }

                articlesTable.Rows.Add(currentRow);
            }

        }
    }
}