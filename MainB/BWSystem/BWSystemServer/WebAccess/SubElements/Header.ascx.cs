﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess.SubElements
{
    public partial class Head : System.Web.UI.UserControl
    {
        private string headerText;
        private string subheaderText;



        protected void Page_Load(object sender, EventArgs e)
        {

        }


        /// Getter & Setter
        public string HeaderText
        {
            get
            {
                return headerText;
            }

            set
            {
                headerText = value;
            }
        }

        public string SubheaderText
        {
            get
            {
                return subheaderText;
            }

            set
            {
                subheaderText = value;
            }
        }
    }
}