﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess.SubElements
{
    public partial class ConsumptionTable : System.Web.UI.UserControl
    {
        private string tableMode;
        private LoginInfo loginInfo;
        private int settlementID;

        //private Tuple<int, DateTime, DateTime> settlementData;
        private List<BookingData> bookings;
        private List<Account> accounts;
        private List<Article> articles;
        private Dictionary<int, Account> accountDic;
        private Dictionary<int, Article> articleDic;


        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && (this.TableMode == "CurrentConspumtion"
                || (this.TableMode == "Settlement" && this.loginInfo.Settlement != null)))
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.accounts = DBAManagement.getAccountList_All();
                this.accountDic = new Dictionary<int, Account>();
                foreach (Account acc in this.accounts)
                {
                    accountDic.Add(acc.ID, acc);
                }

                this.articles = DBAManagement.getArticleList_All();
                this.articleDic = new Dictionary<int, Article>();
                foreach (Article art in this.articles)
                {
                    articleDic.Add(art.ID, art);
                }

                if (this.TableMode == "Settlement")
                {
                    this.bookings = DAM_Booking.getBookingDataList(this.loginInfo.Settlement.StartDate, this.loginInfo.Settlement.EndDate, this.loginInfo.LoggedInAccount.ID);

                    if (this.bookings.Count == 0)
                    {
                        this.bookings = DAM_ArchiveManagement.Instance.getArchiveBookingData(this.loginInfo.Settlement.StartDate, this.loginInfo.Settlement.EndDate);
                        this.bookings = this.bookings.Where(x => x.Customer == loginInfo.LoggedInAccount.ID).ToList();
                    }
                }
                else if (this.TableMode == "CurrentConspumtion")
                {
                    DateTime lastSettlementEnd = DBAManagement.getLastSettlementEndDate();
                    this.bookings = DAM_Booking.getBookingDataList(lastSettlementEnd, DateTime.Now, this.loginInfo.LoggedInAccount.ID);
                }
                else
                {
                    throw new NotImplementedException("You are trying to use ConsumptionTable without a defined mode!");
                }


                this.addBookingData();

                //TODO: Add Rows!
            }
        }

        private void addBookingData()
        {
            bool isEven = false;
            for (int i = 0; i < bookings.Count; i++)
            {
                foreach (BookingAA bookingAA in bookings[i].Booking)
                {
                    TableCell idCell = new TableCell();
                    idCell.Text = bookings[i].ID.ToString();

                    TableCell bookedByCell = new TableCell();
                    bookedByCell.Text = (bookings[i].User != null) ? ServerUtil.accountToDisplayWebName(accountDic[(int)bookings[i].User]) : "--";

                    TableCell dateCell = new TableCell();
                    dateCell.Text = bookings[i].TimeStamp.ToString();

                    TableCell itemCell = new TableCell();
                    Article articleBooked = articleDic[bookingAA.Article];
                    itemCell.Text = articleBooked.Name;

                    TableCell amountCell = new TableCell();
                    amountCell.Text = bookingAA.Amount.ToString();

                    TableCell prizeCell = new TableCell();
                    double prize = (double)bookingAA.Amount * articleBooked.Prize;
                    prizeCell.Text = prize.ToString("0.00");

                    TableRow currentRow = new TableRow();
                    currentRow.Cells.Add(idCell);
                    currentRow.Cells.Add(bookedByCell);
                    currentRow.Cells.Add(itemCell);
                    currentRow.Cells.Add(amountCell);
                    currentRow.Cells.Add(prizeCell);
                    currentRow.Cells.Add(dateCell);

                    if (isEven = !isEven)
                        currentRow.CssClass = "table_row_grayBackground";

                    if (bookings[i].DataCancelled)
                    {
                        currentRow.CssClass = "table_row_cancelled";
                    }

                    bookingTable.Rows.Add(currentRow);
                }

            }
        }


        /// Getters & Setters
        public int SettlementID
        {
            get
            {
                return settlementID;
            }

            set
            {
                settlementID = value;
            }
        }

        public string TableMode
        {
            get
            {
                return tableMode;
            }

            set
            {
                tableMode = value;
            }
        }
    }

}