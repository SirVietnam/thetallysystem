﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Event_ArticleTable.ascx.cs" Inherits="BWSystemServer.WebAccess.SubElements.Event_ArticleTable" %>
<asp:Table ID="articlesTable" runat="server" Width="525px" CssClass="hooverMarking evenOddMarking">
    <asp:TableRow CssClass="table_head">
        <asp:TableCell Width="100%" ID="headCell" HorizontalAlign="Center" BackColor="Green" ColumnSpan="4" Text="<%# this.HeadlineText %>"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow CssClass="table_head" BackColor="Black">
        <asp:TableCell Width="75px">ID</asp:TableCell>
        <asp:TableCell Width="150px">Name</asp:TableCell>
        <asp:TableCell Width="150px">Prize</asp:TableCell>
        <asp:TableCell Width="150px">&nbsp;</asp:TableCell>
    </asp:TableRow>
</asp:Table>
