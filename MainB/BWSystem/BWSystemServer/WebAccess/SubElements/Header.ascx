﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="BWSystemServer.WebAccess.SubElements.Head" %>
<div class="headbody">
    <p class="head_p"><%= this.HeaderText %></p>
    <p class="subhead_p"><%= this.SubheaderText %></p>
</div>

