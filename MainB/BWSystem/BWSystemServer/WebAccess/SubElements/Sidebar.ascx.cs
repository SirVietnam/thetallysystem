﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess.SubElements
{
    public partial class Sidebar : System.Web.UI.UserControl
    {
        private LoginInfo loginInfo;
        private Account account;

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Object)Session["LoginInfo"] != null && ((LoginInfo)Session["LoginInfo"]).LoggedIn)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.account = loginInfo.LoggedInAccount;
                //this.account.IsSuperuser = true;
                DefaultPageLink.Visible = false;
                AccountPageLink.Visible = true;
                BillsPageLink.Visible = true;
                //BillDetailsLink.Visible = true;
                ConsumePageLink.Visible = true;
                Statistics.Visible = true;

                UnsettledBookingsPageLink.Visible = this.account.IsSuperuser;

                StocktakingPageLink.Visible = this.account.IsSuperuser;
                ShipmentsPageLink.Visible = this.account.IsSuperuser;
                SettlementsPageLink.Visible = this.account.IsSuperuser;
                AnnualAccountsPageLink.Visible = this.account.IsSuperuser;

                EventsPageLink.Visible = this.account.IsSuperuser;

                ManageArticlePageLink.Visible = this.account.IsSuperuser;
                ManageAccountPageLink.Visible = this.account.IsSuperuser;
                ManageClientPageLink.Visible = this.account.IsSuperuser;

#if (DEBUG)
                TestDataCreationPageLink.Visible = this.account.IsSuperuser;
#else
                TestDataCreationPageLink.Visible = false;
#endif
                LogOffPageLink.Visible = true;

            }
            else
            {
                DefaultPageLink.Visible = true;
                AccountPageLink.Visible = false;
                BillsPageLink.Visible = false;
                //BillDetailsLink.Visible = false;
                ConsumePageLink.Visible = false;
                Statistics.Visible = false;

                UnsettledBookingsPageLink.Visible = false;

                StocktakingPageLink.Visible = false;
                ShipmentsPageLink.Visible = false;
                SettlementsPageLink.Visible = false;
                AnnualAccountsPageLink.Visible = false;

                EventsPageLink.Visible = false;

                ManageArticlePageLink.Visible = false;
                ManageAccountPageLink.Visible = false;
                ManageClientPageLink.Visible = false;

                TestDataCreationPageLink.Visible = false;
                LogOffPageLink.Visible = false;
            }
        }
    }
}