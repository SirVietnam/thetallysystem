﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bills.aspx.cs" Inherits="BWSystemServer.WebAccess.Bills" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Bills" SubheaderText="Here you can see your latest bills." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form runat="server">
        <div class="std_div">
            <asp:Table ID="setTable" runat="server" Width="600px" CssClass="evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="50px">ID</asp:TableCell>
                    <asp:TableCell Width="200px">Start Date</asp:TableCell>
                    <asp:TableCell Width="200px">End Date</asp:TableCell>
                    <asp:TableCell Width="150px">Invoice Total</asp:TableCell>
                    <asp:TableCell Width="20px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="SeeArchiveData_Btn" runat="server" Text="See Archive Data" OnClick="SeeArchiveData_Btn_Click" />
                <asp:Button CssClass="std_Button" ID="LoadMore_Btn" runat="server" Text="Load all" OnClick="LoadAll_Btn_Click" />
            </div>
        </div>
    </form>
</body>
</html>
