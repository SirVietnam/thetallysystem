﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShipmentArchive.aspx.cs" Inherits="BWSystemServer.WebAccess.ShipmentArchive" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Shipment Archive" SubheaderText="Here you can find all archived shipments." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">

            <asp:Table ID="shipmentTable" runat="server" Width="500px">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="50px">ID</asp:TableCell>
                    <asp:TableCell Width="200px">Vendor</asp:TableCell>
                    <asp:TableCell Width="200px">Date Time</asp:TableCell>
                    <asp:TableCell Width="50px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </form>
    </div>
</body>
</html>
