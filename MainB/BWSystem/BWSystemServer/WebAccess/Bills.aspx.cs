﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BWSystemServer.WebAccess;
using BWSystemServer;
using BWSystemServer.BIZ;

namespace BWSystemServer.WebAccess
{
    public partial class Bills : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Settlement> settlementsData;
        private int nextSetData;

        protected void Page_Load(object sender, EventArgs e)
        {
            nextSetData = 0;
            Account account = null;

            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);
                account = this.loginInfo.LoggedInAccount;
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (account != null)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.settlementsData = DBAManagement.getSettlementListWithoutData();

                this.addSettlementDataToTable(10);

                //TODO: Add Rows!
            }
        }

        protected void LoadAll_Btn_Click(object sender, EventArgs e)
        {
            this.addSettlementDataToTable(settlementsData.Count);
        }


        private void addSettlementDataToTable(int count)
        {
            int maxDataSets = (nextSetData + count);
            for (int i = this.nextSetData; i < settlementsData.Count && i < maxDataSets; i++)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = settlementsData[i].SettlementID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell startCell = new TableCell();
                startCell.Text = ServerUtil.DateTimeToString(settlementsData[i].StartDate);
                currentRow.Cells.Add(startCell);

                TableCell endCell = new TableCell();
                endCell.Text = ServerUtil.DateTimeToString(settlementsData[i].EndDate);
                currentRow.Cells.Add(endCell);

                TableCell prizeCell = new TableCell();
                double payment = DBAccessManagement.Instance.getPayment(settlementsData[i].SettlementID, this.loginInfo.LoggedInAccount.ID);
                prizeCell.Text = payment.ToString("0.00");
                currentRow.Cells.Add(prizeCell);

                Button detailsButton = new Button();
                detailsButton.ID = settlementsData[i].SettlementID.ToString();
                detailsButton.CommandArgument = settlementsData[i].SettlementID.ToString();
                detailsButton.Text = "Details";
                detailsButton.Command += OnDetailButton_Click;
                detailsButton.CssClass = "std_Button";

                TableCell detailCell = new TableCell();
                detailCell.Controls.Add(detailsButton);
                currentRow.Cells.Add(detailCell);

                setTable.Rows.Add(currentRow);
                this.nextSetData++;
            }

        }

        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            int settlementID = int.Parse(e.CommandArgument.ToString());
            this.loginInfo.Settlement = this.settlementsData.First(x => x.SettlementID == settlementID);
            Server.Transfer("~/WebAccess/BillDetails.aspx", true);
        }

        protected void SeeArchiveData_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn)
            {
                Server.Transfer("~/WebAccess/BillArchive.aspx", true);
            }
        }

    }
}