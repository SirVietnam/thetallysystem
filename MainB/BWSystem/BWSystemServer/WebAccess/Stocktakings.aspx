﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Stocktakings.aspx.cs" Inherits="BWSystemServer.WebAccess.SeeStocktakings" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Stocktakings" SubheaderText="Here you find all the previous stocktakings!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="stocktakingTable" runat="server" Width="350px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="70%">Date</asp:TableCell>
                    <asp:TableCell Width="30%">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="SeeArchiveData_Btn" runat="server" Text="See Archive Data" OnClick="SeeArchiveData_Btn_Click" />
                <asp:Button CssClass="std_Button" ID="CreateStocktaking_Btn" runat="server" Text="Add Stocktaking" OnClick="CreateStocktaking_Btn_Click" />
            </div>
        </form>
    </div>
</body>
</html>
