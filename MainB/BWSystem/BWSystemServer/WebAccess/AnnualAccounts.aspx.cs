﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class SeeAnualAccounts : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        List<AnnualStatement> annualAccounts;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                if (loginInfo.LoggedInAccount.IsSuperuser)
                {
                    DBAccessManagement DBAM = DBAccessManagement.Instance;
                    this.annualAccounts = DBAM.getAnnualStatements();
                    this.addData();
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }

        private void addData()
        {
            foreach (AnnualStatement annualStatement in this.annualAccounts)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = annualStatement.AnnualStatementID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell nameCell = new TableCell();
                nameCell.Text = annualStatement.AnnualStatementName;
                currentRow.Cells.Add(nameCell);

                TableCell startCell = new TableCell();
                startCell.Text = ServerUtil.DateTimeToString(annualStatement.StartDate);
                currentRow.Cells.Add(startCell);

                TableCell endCell = new TableCell();
                endCell.Text = ServerUtil.DateTimeToString(annualStatement.EndDate);
                currentRow.Cells.Add(endCell);

                Button detailsButton = new Button();
                detailsButton.ID = ServerUtil.getFileName(annualStatement);
                detailsButton.CommandArgument = annualStatement.AnnualStatementID.ToString();
                detailsButton.Text = "Download";
                detailsButton.Command += OnDownloadButton_Clicked;
                detailsButton.CssClass = "std_Button";

                TableCell detailCell = new TableCell();
                detailCell.Controls.Add(detailsButton);
                currentRow.Cells.Add(detailCell);

                annualAccountTable.Rows.Add(currentRow);
            }
        }


        private void OnDownloadButton_Clicked(object sender, CommandEventArgs e)
        {
            if (sender is Button)
            {
                Button dlButton = (Button)sender;
                string fileName = dlButton.ID;
                string filePath = ServerUtil.getAbsoluteFilePath("StandardAnnualStatementExportLocation");
                filePath += fileName;

                try
                {
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                    Response.TransmitFile(filePath);
                    Response.End();
                } catch (Exception ex)
                {
                    SimpleLog.Log(ex);
                }
            }
        }

        protected void CreateAnnualAccount_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                Server.Transfer("~/WebAccess/AnnualAccountCreation.aspx", true);
            }
        }
    }
}