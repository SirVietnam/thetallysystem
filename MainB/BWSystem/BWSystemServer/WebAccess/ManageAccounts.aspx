﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageAccounts.aspx.cs" Inherits="BWSystemServer.WebAccess.ManageAccounts" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Manage Accounts" SubheaderText="You can can activate or deactivate accounts here." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form id="form1" runat="server">
        <div class="std_div">
            <asp:Table ID="accTable" runat="server" Width="600px" CssClass="evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="75px">ID</asp:TableCell>
                    <asp:TableCell Width="300px">Name</asp:TableCell>
                    <asp:TableCell Width="100px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
