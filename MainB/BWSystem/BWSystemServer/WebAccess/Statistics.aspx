﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="BWSystemServer.WebAccess.Statistics" %>

<%--<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp2" %>--%>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Statistics" SubheaderText="Get your datas right." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form runat="server">
        <div class="std_div">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <ajaxToolkit:CalendarExtender ID="startScopeCE" runat="server"
                TargetControlID="startTB" PopupButtonID="tb_Date" Format="yyyy-MM-dd"></ajaxToolkit:CalendarExtender>

            <ajaxToolkit:CalendarExtender ID="endScopeCE" runat="server"
                TargetControlID="endTB" PopupButtonID="tb_Date" Format="yyyy-MM-dd"></ajaxToolkit:CalendarExtender>

            <asp:Table ID="graphSelectionTbl" runat="server" Width="700px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow>
                    <asp:TableCell Width="75px">Data</asp:TableCell>
                    <asp:TableCell Width="450px" ColumnSpan="2">
                        <asp:DropDownList ID="ddl_data" Width="200" runat="server" CssClass="std_Textbox" AutoPostBack="true" OnSelectedIndexChanged="ddl_data_SelectedIndexChanged">
                            <asp:ListItem Value="my">My Data</asp:ListItem>
                            <asp:ListItem Value="all">All Data</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                    <asp:TableCell RowSpan="3" Width="150px">
                        <asp:Button CssClass="std_Button" Width="70px" Height="70px" ID="goBtn" runat="server" Text="Go" OnClick="goBtn_Click" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Width="75px">Scope</asp:TableCell>
                    <asp:TableCell ColumnSpan="2">
                        <asp:DropDownList ID="ddl_scope" Width="200" runat="server" CssClass="std_Textbox" AutoPostBack="true" OnSelectedIndexChanged="ddl_scope_SelectedIndexChanged">
                            <asp:ListItem Value="tab">Open Tab</asp:ListItem>
                            <asp:ListItem Value="today">Today</asp:ListItem>
                            <asp:ListItem Value="settlement">Last Settlement</asp:ListItem>
                            <asp:ListItem Value="currentAnnual">Current Annual Account</asp:ListItem>
                            <asp:ListItem Value="lastAnnual">Last Annual Account</asp:ListItem>
                            <asp:ListItem Value="custom">Custom</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="75px">Custom</asp:TableCell>
                    <asp:TableCell Width="500px">
                        <div class="horizontal_div">
                            <asp:TextBox ID="startTB" Width="48%" CssClass="std_table_tb" runat="server" OnTextChanged="startTB_TextChanged" Enabled="<%# this.InCustomeMode %>"></asp:TextBox>
                            <asp:TextBox ID="endTB" Width="48%" CssClass="std_table_tb" runat="server" OnTextChanged="endTB_TextChanged" Enabled="<%# this.InCustomeMode %>"></asp:TextBox>
                        </div>
                    </asp:TableCell>

                </asp:TableRow>
            </asp:Table>

            <div>
                <asp:Chart runat="server" ID="chart" Width="1000">
                    <Series>
                        <asp:Series Name="Series1"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div>
                <h1>Value of the selected range is comming SOON!</h1>
            </div>

        </div>
    </form>
</body>
</html>
