﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventDetails.aspx.cs" Inherits="BWSystemServer.WebAccess.EventDetails" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>
<%@ Register Src="~/WebAccess/SubElements/Event_ArticleTable.ascx" TagPrefix="SE" TagName="Event_ArticleTable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Events Details" SubheaderText="See all the details of your event." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="artTable" runat="server" Width="600px">
                <asp:TableRow>
                    <asp:TableCell Width="30%">Event ID:</asp:TableCell>
                    <asp:TableCell Width="70%">
                        <asp:TextBox ID="tb_eventID" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="30%">Event Creator:</asp:TableCell>
                    <asp:TableCell Width="70%">
                        <asp:TextBox ID="tb_creatorName" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Event Name:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_eventName" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Start Date:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_startDateTime" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>End Date:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_endDateTime" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Exclusive Articles:</asp:TableCell>
                    <asp:TableCell>
                        <asp:CheckBox ID="cb_isExclusiveArticle" runat="server" Enabled="false" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Event Costs:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_eventCosts" CssClass="std_table_tb" runat="server" Enabled="false" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />
            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="eventEndNow" runat="server" Text="End Event Now" Visible="<%# this.EventStarted && !this.EventComplete%>" OnClick="eventEndNow_Click" />
                <asp:Button CssClass="std_Button" ID="eventCompleteBtn" runat="server" Text="Create Event Settlement" Visible="<%# this.EventComplete && !this.EventSettled %>" OnClick="eventCompleteBtn_Click" />
                <asp:Button CssClass="std_Button" ID="eventDownloadBtn" runat="server" Text="Download Event Settlement" Visible="<%# this.EventComplete && this.EventSettled %>" OnClick="eventDownloadBtn_Click" />
            </div>

            <br />

            <asp:Table ID="bookingsTable" runat="server" Width="700px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="100%" ColumnSpan="6" HorizontalAlign="Center">Articles Consumend</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow CssClass="table_head" BackColor="Black">
                    <asp:TableCell Width="10%">ID</asp:TableCell>
                    <asp:TableCell Width="20%">BookedBy</asp:TableCell>
                    <asp:TableCell Width="20%">Date & Time</asp:TableCell>
                    <asp:TableCell Width="20%">Article</asp:TableCell>
                    <asp:TableCell Width="10%">Amount</asp:TableCell>
                    <asp:TableCell Width="10%">Prize</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />

            <SE:Event_ArticleTable runat="server" ID="payedArticlesTable" TableData="payedArticles" ButtonText="" HeadlineText="Availible Articles" />

            <br />
            <div class="substd_div">
                <div style="float: right">
                    <asp:Button CssClass="std_Button" ID="editEvent_Btn" runat="server" Text="Edit Event" Visible="<%# !this.EventComplete %>" OnClick="editEvent_Btn_Click" />
                </div>
            </div>
        </form>
    </div>
</body>
</html>
