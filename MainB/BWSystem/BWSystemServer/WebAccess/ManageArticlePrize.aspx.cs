﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ManageArticlePrize : System.Web.UI.Page
    {
        private Regex regexPrize;
        private LoginInfo loginInfo;
        private List<Article> articleList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.regexPrize = ServerUtil.RegexPrize;
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement DAM = DBAccessManagement.Instance;

                DateTime lastSettlmentEnd = DAM.getLastSettlementEndDate();
                this.settlementStatusTB.Text = ServerUtil.DateTimeToString(lastSettlmentEnd);
                this.settlementStatusTB.CssClass = DAM.updatePrizesPreRequirementsMet() ? "tb_inputCorrect" : "tb_inputError";

                this.articleList = DAM.getArticleList_All();

                this.addArticlePrizeToTable();
            }
        }

        private void addArticlePrizeToTable()
        {
            for (int i = 0; i < articleList.Count; i++)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = articleList[i].ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell nameCell = new TableCell();
                nameCell.Text = articleList[i].Name;
                currentRow.Cells.Add(nameCell);

                TableCell oldPrizeCell = new TableCell();
                oldPrizeCell.Text = articleList[i].Prize.ToString("0.00");
                currentRow.Cells.Add(oldPrizeCell);

                TableCell boxesTBCell = new TableCell();
                TextBox boxesTB = new TextBox();
                boxesTB.ID = articleList[i].ID.ToString();
                boxesTB.ValidationGroup = "InputTextboxInt";
                boxesTB.CssClass = "std_Textbox";
                boxesTB.Text = "";
                boxesTB.AutoPostBack = true;
                boxesTB.TextChanged += BoxesTB_TextChanged;
                boxesTBCell.Controls.Add(boxesTB);
                currentRow.Cells.Add(boxesTBCell);

                artTable.Rows.Add(currentRow);
            }
        }

        private void BoxesTB_TextChanged(object sender, EventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox tb = (TextBox)sender;

                bool hasValidContent = regexPrize.IsMatch(tb.Text);

                float newPrize = 0;
                if (hasValidContent)
                {
                    tb.CssClass = "std_Textbox";
                    newPrize = (float)ServerUtil.StringToDouble(tb.Text);
                }
                else
                {
                    tb.CssClass = "tb_inputError";
                    newPrize = -1;
                }

                if (Check_Update_Btn.Text.Equals("Update"))
                {
                    Check_Update_Btn.Text = "Check";
                }
            }
        }

        private bool checkTextBox(TextBox tb)
        {
            bool hasValidContent = regexPrize.IsMatch(tb.Text);
            tb.CssClass = hasValidContent ? "std_Textbox" : "tb_inputError";
            return hasValidContent;
        }

        protected void Check_Update_Btn_Click(object sender, EventArgs e)
        {
            if (Check_Update_Btn.Text.Equals("Check"))
            {
                bool allNumbersValid = true;
                foreach (Article article in this.articleList)
                {
                    TextBox tb = (TextBox)FindControl(article.ID.ToString());
                    if (tb != null)
                    {
                        allNumbersValid = allNumbersValid && this.checkTextBox(tb);
                    }
                    else
                    {
                        throw new ArgumentNullException("The Textbox with ID: " + article.ID + " does not exist, while it should exist");
                    }
                }

                if (allNumbersValid && DBAccessManagement.Instance.updatePrizesPreRequirementsMet())
                {
                    Check_Update_Btn.Text = "Update Prizes";
                }
            }
            else
            {
                this.updatePrizes();
            }
        }


        private void updatePrizes()
        {
            List<Tuple<int, float>> prizeUpdates = new List<Tuple<int, float>>();

            foreach (Article article in this.articleList)
            {
                TextBox tb = (TextBox)FindControl(article.ID.ToString());

                if (!tb.Text.Equals(""))
                {
                    float newPrize = (float)ServerUtil.StringToDouble(tb.Text);
                    prizeUpdates.Add(new Tuple<int, float>(article.ID, newPrize));
                }
            }

            DBAccessManagement DBAManagement = DBAccessManagement.Instance;
            DBAManagement.updatePrizes(prizeUpdates);

            Server.Transfer("~\\WebAccess\\ManageArticles.aspx", false);
        }
    }
}