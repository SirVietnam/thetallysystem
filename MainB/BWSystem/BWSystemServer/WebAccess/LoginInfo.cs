﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWSystemServer.BIZ;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace BWSystemServer.WebAccess
{
    public class LoginInfo
    {
        private bool loggedIn;
        private DateTime logInTime;

        private Account loggedInAccount;

        private Article activeArticle;
        private Shipment activeShipment;
        private Stocktaking activeStocktaking;
        private Event activeEvent;
        private HashSet<Article> payedArticles;
        private HashSet<Article> notPayedArticles;
        private HashSet<Article> disabledArticles;

        private Page editDataPage;
        private int editID;
        private bool editData;

        //Page Infos
        private Settlement settlement;

        //Other Infos
        private Article.ArticleType currentType;
        private ListItem currentArtileType;
        private bool allItemsValid;
        private bool deviceAdded;

        //Admin Infos
        private bool settlementDone;
        private bool annualStatementDone;

        public LoginInfo(string username)
        {
            DBAccessManagement dbAccess = DBAccessManagement.Instance;
            this.loggedInAccount = dbAccess.getAccountToLogin(username);
            if (this.LoggedInAccount != null)
            {
                loggedIn = true;
                logInTime = DateTime.Now;
            }
            this.settlement = null;

            this.editDataPage = null;
            this.editID = -1;
            this.editData = false;
        }


        /// <summary>
        /// Call this method on "Page_Load" when data are possibly modified.
        /// </summary>
        /// <param name="pageLoaded">The page, that executed "Page_Load"</param>
        /// <returns>If the page is in editing mode</returns>
        public bool onPage_Load(Page pageLoaded, int editID = -1)
        {
            bool equalPages = this.EditDataPage != null && this.EditDataPage.GetType() == pageLoaded.GetType();
            if (!equalPages || this.editID != editID)
            {
                this.editData = false;
                this.editDataPage = pageLoaded;
                this.editID = editID;
            }
            return this.editData;
        }

        /// <summary>
        /// Call this method, after you called "onPage_LoadEdit".
        /// </summary>
        /// <param name="pageLoaded">The current Page</param>
        /// <returns>If it is valid to start editing data</returns>
        public bool onEditStart(Page pageLoaded)
        {
            if (EditDataPage.GetType() == pageLoaded.GetType() && !this.editData)
            {
                this.editData = true;
            }
            return this.editData;
        }

        /// <summary>
        /// Call this method, after your data modification is complete
        /// </summary>
        /// <param name="pageLoaded">The current page</param>
        public void onEditComplete(Page pageLoaded)
        {
            if (EditDataPage.GetType() == pageLoaded.GetType() && editData)
            {
                this.editData = false;
                this.editDataPage = null;
                this.editID = -1;
            }
        }

        public bool LoggedIn
        {
            get { return loggedIn; }
            //set { loggedIn = value; }
        }

        public Account LoggedInAccount
        {
            get { return loggedInAccount; }
            //set { logedInAccount = value; }
        }

        public Settlement Settlement
        {
            get { return settlement; }
            set { settlement = value; }
        }

        public bool EditData
        {
            get { return editData; }
            //set { editData = value; }
        }

        public Article ActiveArticle
        {
            get { return activeArticle; }
            set { activeArticle = value; }
        }

        public Page EditDataPage
        {
            get { return editDataPage; }
        }

        public Article.ArticleType CurrentType
        {
            get { return currentType; }
            set { currentType = value; }
        }

        public ListItem CurrentArtileType
        {
            get { return currentArtileType; }
            set { currentArtileType = value; }
        }

        public bool AllItemsValid
        {
            get { return allItemsValid; }
            set { allItemsValid = value; }
        }

        public bool DeviceAdded
        {
            get { return deviceAdded; }
            set { deviceAdded = value; }
        }

        public bool SettlementDone
        {
            get { return settlementDone; }
            set { settlementDone = value; }
        }

        public bool AnnualStatementDone
        {
            get { return annualStatementDone; }
            set { annualStatementDone = value; }
        }

        public Shipment ActiveShipment
        {
            get { return activeShipment; }
            set { activeShipment = value; }
        }

        public Stocktaking ActiveStocktaking
        {
            get { return activeStocktaking; }
            set { activeStocktaking = value; }
        }

        public Event ActiveEvent
        {
            get { return activeEvent; }
            set { activeEvent = value; }
        }

        public HashSet<Article> PayedArticles
        {
            get { return payedArticles; }

            set { payedArticles = value; }
        }

        public HashSet<Article> NotPayedArticles
        {
            get { return notPayedArticles; }
            set { notPayedArticles = value; }
        }

        public HashSet<Article> DisabledArticles
        {
            get { return disabledArticles; }
            set { disabledArticles = value; }
        }
    }
}