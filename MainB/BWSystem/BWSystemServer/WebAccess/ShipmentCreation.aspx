﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShipmentCreation.aspx.cs" Inherits="BWSystemServer.WebAccess.ManageInventoryShipment" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Enter Shipment" SubheaderText="Here you can enter the stockchanges when a shipment came." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="std_div">

            <asp:Table runat="server" ID="inputTable" CssClass="std_table" Width="550">
                <asp:TableRow>
                    <asp:TableCell Width="150">Vendor:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Vendor" CssClass="std_Textbox" runat="server" Width="100%" AutoPostBack="true" OnTextChanged="onInputDataChanged"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="150">Delivery Date:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Date" CssClass="std_Textbox" runat="server" Width="100%" AutoPostBack="true" OnTextChanged="onInputDataChanged" />
                        <ajaxToolkit:CalendarExtender ID="DeliveryDate" runat="server"
                            TargetControlID="tb_Date" PopupButtonID="tb_Date" Format="yyyy-MM-dd"></ajaxToolkit:CalendarExtender>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="150">Delivery Time:</asp:TableCell>
                    <asp:TableCell Width="200" CssClass="evenOddMarking">
                        <asp:DropDownList ID="ddl_Hour" runat="server" CssClass="std_Textbox" AutoPostBack="true" OnSelectedIndexChanged="onInputDataChanged">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddl_Minutes" runat="server" CssClass="std_Textbox" Sytle="margin-left: 10px;" AutoPostBack="true" OnSelectedIndexChanged="onInputDataChanged">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="150" Text=" " />
                    <asp:TableCell>
                        <asp:CheckBox ID="disabledCB" runat="server" AutoPostBack="true" Text="Include Disabled Articles" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="150" Text=" " />
                    <asp:TableCell>
                        <asp:CheckBox ID="inventoryCB" runat="server" AutoPostBack="true" Text="Include Inventory Articles" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            <br />

            <asp:Table runat="server" ID="addtbl" CssClass="std_table" Width="600">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label runat="server" Text="ID" Width="50"></asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Center">
                        <asp:Label runat="server" Text="Article" Width="175"></asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Center" Width="100">
                        <asp:Label runat="server" Text="Boxes"></asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Center" Width="100">
                        <asp:Label runat="server" Text="Single Bottles"></asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Center" Width="100">
                        <asp:Label runat="server" Text="Sum in Bottles"></asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Center" Width="50">
                        &nbsp;
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="Check_Update_Btn" runat="server" Text="Check" OnClick="Check_Update_Btn_Click" />
            </div>
        </div>

    </form>
</body>
</html>
