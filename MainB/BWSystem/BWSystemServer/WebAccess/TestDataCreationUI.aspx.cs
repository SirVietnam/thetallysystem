﻿using BWSystemServer.BIZ;
using BWSystemServer.DAL.AutoSettlement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ManageInventory : System.Web.UI.Page
    {
        private LoginInfo loginInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }


        protected void tdcBtn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                TestDataCreation tdc = new TestDataCreation();
                tdc.addBookings(Convert.ToInt32(bookingsAmount.Text), 0.0);
            }
        }


        protected void setBtn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                TestDataCreation tdc = new TestDataCreation();
                tdc.addInventory(Convert.ToDouble(setDeviation.Text));
            }
        }


        protected void shipBtn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                TestDataCreation tdc = new TestDataCreation();
                tdc.addShipment(Convert.ToInt32(averageAmountShipment.Text), Convert.ToDouble(deviationMaxShipment.Text));
            }
        }


        protected void getLDAP_Data_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement.Instance.syncLDAP_DB();
            }
        }


        protected void cleanDB_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement.Instance.cleanDBs();
            }
            //int i = 0;
        }


        protected void getLDAP_Accounts_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement.Instance.addMissingLDAPAccounts();
                //DBAccessManagement.Instance.syncLDAP_DB();
            }
        }


        protected void readAutoSettlement_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                NmAutoSettlement autoSet = new NmAutoSettlement();
            }
        }
    }
}