﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Account_Page.aspx.cs" Inherits="BWSystemServer.WebAccess.Account_Page" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <SE:Header runat="server" ID="Head" HeaderText="Account Information" SubheaderText="You can only change BWSystem relevant information." />
        <SE:Sidebar runat="server" ID="Sidebar" />

        <div class="std_div">

            <asp:Table runat="server" Width="825px">
                <asp:TableRow>
                    <asp:TableCell>

                        <asp:Table CssClass="std_table" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>AccountID:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_AccID" CssClass="std_table_tb" runat="server" ReadOnly="True" Enabled="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Surname:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_Surname" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Name:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_Name" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Nickname:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_Nickname" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Standard Article:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_StdArticle" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Authentication Code:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_AuthCode" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Hash Code:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_HashCode" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Salt Code:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_SaltCode" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Authentication Cards:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="tb_AuthCard" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="false"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Authenticate Always:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:CheckBox ID="cb_AuthAlways" runat="server" Enabled="<%# !this.IsReadOnly %>" />
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>Admin:</asp:TableCell>
                                <asp:TableCell>
                                    <asp:CheckBox ID="cb_IsAdmin" runat="server" Enabled="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>

                    </asp:TableCell>

                    <asp:TableCell>
                        <div style="width: 125px;">
                            <asp:Image ID="img_Picture" runat="server" ImageUrl="~/Pictures/Users/Nm_Zirkel.jpg" Height="150px" /><br />
                            <asp:Button ID="changePicture_Btn" CssClass="std_Btn" runat="server" Text="Change Picture" Width="125px" Visible="<%# !this.IsReadOnly %>" OnClick="changePicture_Click" />
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="Edit_Save_Btn" runat="server" Text="Edit Data" OnClick="Edit_Save_Click" />
            </div>
        </div>

    </form>

</body>
</html>
