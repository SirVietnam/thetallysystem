﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileUploadPage.aspx.cs" Inherits="BWSystemServer.WebAccess.FileUploadPage" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Manage Settlements" SubheaderText="Create a new settlement or view old settlements." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <div style="text-align: center;">
                <asp:Table CssClass="std_table" runat="server">
                    <asp:TableRow CssClass="table_head">
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                            <asp:Label ID="lb_Type" runat="server">Type</asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" Font-Italic="true" Enabled="false">ID: </asp:Label>
                            <asp:Label ID="lb_ID" runat="server" Enabled="false">ID</asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label runat="server" Font-Italic="true" Enabled="false">Name: </asp:Label>
                            <asp:Label ID="lb_Name" runat="server" Enabled="false">Name</asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>


                <asp:Image ID="img_Picture" runat="server" ImageUrl="F:/BWSystemPics/Client/Articles/Nm_Zirkel.jpg" Width="300px" BorderWidth="3px" />
                <br />
                <br />
                <asp:Table CssClass="std_table" runat="server">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center" Width="50%">
                            <asp:FileUpload CssClass="std_Button" ID="FileUploadControl" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell Width="50%">
                            <asp:Button ID="uploadBtn" CssClass="std_Button" runat="server" OnClick="uploadBtn_Click" Text="Upload Picture" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>

            <div style="margin-left: 80%;">
                <asp:Button CssClass="std_Button" ID="done_btn" runat="server" Text="Done" OnClick="done_Click" />
            </div>

        </form>
    </div>
</body>
</html>
