﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class SettlementArchive : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Settlement> settlements;
        private List<AnnualStatement> annualStatements;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                this.settlements = BIZ.DatabaseAccessManagement.DAM_ArchiveManagement.Instance.getArchiveSettlementList();
                this.annualStatements = DBAccessManagement.Instance.getAnnualStatements();

                this.settlements = this.settlements.OrderBy(x => x.StartDate).ToList();
                this.annualStatements = this.annualStatements.OrderBy(x => x.StartDate).ToList();

                this.addData();
            }
        }


        private void addData()
        {
            int currentStocktakingIndex = 0;
            foreach (AnnualStatement annualStatement in this.annualStatements)
            {
                GuiWebUtil.addAnnualStatementRow(settlementTable, 4, annualStatement);

                while (currentStocktakingIndex < this.settlements.Count
                    && this.settlements[currentStocktakingIndex].StartDate >= annualStatement.StartDate
                    && this.settlements[currentStocktakingIndex].EndDate <= annualStatement.EndDate)
                {
                    this.addSettlementDataRow(this.settlements[currentStocktakingIndex]);
                    currentStocktakingIndex++;
                }
            }
        }


        private void addSettlementDataRow(Settlement settlement)
        {
            TableRow currentRow = new TableRow();
            TableCell idCell = new TableCell();
            idCell.Text = settlement.SettlementID.ToString();
            currentRow.Cells.Add(idCell);

            TableCell fromCell = new TableCell();
            fromCell.Text = ServerUtil.DateTimeToString(settlement.StartDate);
            currentRow.Cells.Add(fromCell);

            TableCell toCell = new TableCell();
            toCell.Text = ServerUtil.DateTimeToString(settlement.EndDate);
            currentRow.Cells.Add(toCell);

            Button detailsButton = new Button();
            detailsButton.ID = settlement.SettlementID.ToString() + ServerUtil.DateTimeToFileString(settlement.EndDate, ".xlsx");
            detailsButton.CommandArgument = settlement.SettlementID.ToString();
            detailsButton.Text = "Download";
            detailsButton.Command += OnDownloadButton_Clicked;
            detailsButton.CssClass = "std_Button";

            TableCell detailCell = new TableCell();
            detailCell.Controls.Add(detailsButton);
            currentRow.Cells.Add(detailCell);

            settlementTable.Rows.Add(currentRow);

        }

        private void OnDownloadButton_Clicked(object sender, CommandEventArgs e)
        {
            if (sender is Button)
            {
                Button dlButton = (Button)sender;
                string fileName = dlButton.ID;
                string filePath = (string)ServerUtil.getConfigValue("StandardSettlementExportLocation", typeof(string));
                filePath += fileName;

                try
                {
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                    Response.TransmitFile(Server.MapPath(filePath));
                    Response.End();
                } catch (Exception ex)
                {
                    SimpleLog.Log(ex);
                }
            }
        }

    }
}