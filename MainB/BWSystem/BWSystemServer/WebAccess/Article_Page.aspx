﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Article_Page.aspx.cs" Inherits="BWSystemServer.WebAccess.Article_Page" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Bills" SubheaderText="Here you can see your latest bills." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="artTable" runat="server" Width="700px">
                <asp:TableRow>
                    <asp:TableCell Width="35%">ID:</asp:TableCell>
                    <asp:TableCell Width="30%">
                        <asp:TextBox ID="tb_ArtID" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell RowSpan="9" HorizontalAlign="Center">
                        <div style="width: 125px;">
                            <asp:Image ID="img_Picture" runat="server" ImageUrl="~/Pictures/Articles/Nm_Zirkel.jpg" Height="150px" /><br />
                            <asp:Button ID="changePicture_Btn" CssClass="std_Btn" runat="server" Text="Change Picture" Width="125px" Visible="<%# !this.IsReadOnly && !this.IsNewArticle %>" OnClick="changePicture_Click" />
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Name:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Name" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Type:</asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="ddl_Type" runat="server" Enabled="<%# !this.IsReadOnly %>">
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Prize:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Prize" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly && this.IsNewArticle %>"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Size:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Size" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>In Stock:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_InStock" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Alcoholic Strength:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Alcohol" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Manufacturer:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Manufacturer" CssClass="std_table_tb" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Picture:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Picture" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Description:</asp:TableCell>
                    <asp:TableCell ColumnSpan="2">
                        <asp:TextBox Width="100%" ID="tb_Description" runat="server" ReadOnly="<%# this.IsReadOnly %>" Enabled="<%# !this.IsReadOnly %>"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Is Inventory:</asp:TableCell>
                    <asp:TableCell>
                        <asp:CheckBox ID="cb_IsInventory" runat="server" Enabled="<%# !this.IsReadOnly %>" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Is Availible:</asp:TableCell>
                    <asp:TableCell>
                        <asp:CheckBox ID="cb_IsAvailible" runat="server" Enabled="<%# !this.IsReadOnly %>" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <div class="substd_div" style="float: right">
                <asp:Button CssClass="std_Button" ID="Edit_Save_Btn" runat="server" Text="Edit Data" OnClick="Edit_Save_Click" />
            </div>
        </form>
    </div>
</body>
</html>
