﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ManageArticles : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Article> articleList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.articleList = DBAManagement.getArticleList_All().OrderBy(x => x.Name).ToList();

                this.addArticleDataToTable();
            }
        }

        private void addArticleDataToTable()
        {
            for (int i = 0; i < articleList.Count; i++)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = articleList[i].ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell startCell = new TableCell();
                startCell.Text = articleList[i].Name;
                currentRow.Cells.Add(startCell);

                Button detailsButton = new Button();
                detailsButton.ID = articleList[i].ID.ToString();
                detailsButton.CommandArgument = articleList[i].ID.ToString();
                detailsButton.Text = "Details";
                detailsButton.Command += OnDetailButton_Click;
                detailsButton.CssClass = "std_Button";

                TableCell detailCell = new TableCell();
                detailCell.Controls.Add(detailsButton);
                currentRow.Cells.Add(detailCell);

                artTable.Rows.Add(currentRow);
            }
        }

        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            this.loginInfo.ActiveArticle = this.articleList.FirstOrDefault(x => x.ID == int.Parse(e.CommandArgument.ToString()));
            Server.Transfer("~\\WebAccess\\Article_Page.aspx", true);
        }
        

        protected void ChangePrize_Btn_Click(object sender, EventArgs e)
        {
            Server.Transfer("~\\WebAccess\\ManageArticlePrize.aspx", true);
        }

        protected void NewArticle_Btn_Click(object sender, EventArgs e)
        {
            this.loginInfo.ActiveArticle = new Article();
            Server.Transfer("~\\WebAccess\\Article_Page.aspx", true);
        }
    }
}