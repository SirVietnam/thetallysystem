﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SettlementArchive.aspx.cs" Inherits="BWSystemServer.WebAccess.SettlementArchive" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Manage Settlements" SubheaderText="Create a new settlement or view old settlements." />
    <SE:Sidebar runat="server" ID="Sidebar" />

     <form runat="server">
        <div class="std_div">
            <asp:Table ID="settlementTable" runat="server" Width="525px" CssClass="hooverMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="75px">ID</asp:TableCell>
                    <asp:TableCell Width="150px">From</asp:TableCell>
                    <asp:TableCell Width="150px">To</asp:TableCell>
                    <asp:TableCell Width="150px">&nbsp;</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
