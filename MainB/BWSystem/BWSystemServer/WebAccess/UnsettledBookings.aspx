﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnsettledBookings.aspx.cs" Inherits="BWSystemServer.WebAccess.UnsettledBookings" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Update Inventory" SubheaderText="Here you change your current intventory status. Make a Settlemente befor updateing your Inventory!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form id="form1" runat="server">
        <div class="std_div">
            <asp:Table ID="bookingTable" runat="server" Width="950px" CssClass="evenOddMarking hooverMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="50px">ID</asp:TableCell>
                    <asp:TableCell Width="175px">Consumer</asp:TableCell>
                    <asp:TableCell Width="175px">Booked By</asp:TableCell>
                    <asp:TableCell Width="175px">Item</asp:TableCell>
                    <asp:TableCell Width="50px">Amount</asp:TableCell>
                    <asp:TableCell Width="75px">Prize</asp:TableCell>
                    <asp:TableCell Width="200px">Date</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
