﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public class GuiWebUtil
    {
        internal static void addAnnualStatementRow(Table table, int columnSpan, AnnualStatement annualStatement)
        {
            TableRow currentRow = new TableRow();
            currentRow.CssClass = "table_row_archivedAnnualAccount";

            TableCell annualStatementCell = new TableCell();
            annualStatementCell.Text = annualStatement.AnnualStatementName;
            annualStatementCell.ColumnSpan = columnSpan;
            currentRow.Cells.Add(annualStatementCell);
            table.Rows.Add(currentRow);
        }


        internal static void addSeperatorRowRow(Table table, string text, System.Drawing.Color backgroundColor, System.Drawing.Color fontColor, int columnSpan)
        {
            TableRow currentRow = new TableRow();
            currentRow.CssClass = "table_seperatorRow";
            currentRow.BackColor = backgroundColor;

            TableCell annualStatementCell = new TableCell();
            annualStatementCell.ForeColor = fontColor;
            annualStatementCell.Text = text;
            annualStatementCell.ColumnSpan = columnSpan;
            currentRow.Cells.Add(annualStatementCell);

            table.Rows.Add(currentRow);
        }
    }
}