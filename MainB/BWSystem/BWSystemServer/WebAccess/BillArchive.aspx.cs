﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class BillArchive : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Settlement> settlements;
        private List<AnnualStatement> annualStatements;


        protected void Page_Load(object sender, EventArgs e)
        {
            Account account = null;

            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                account = this.loginInfo.LoggedInAccount;
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (account != null)
            {
                this.settlements = DAM_ArchiveManagement.Instance.getArchiveSettlementList();
                this.annualStatements = DBAccessManagement.Instance.getAnnualStatements();
                this.settlements = this.settlements.OrderBy(x => x.EndDate).ToList();
                this.annualStatements = this.annualStatements.OrderBy(x => x.StartDate).ToList();

                this.addSettlementDataToTable(10);
            }
        }


        private void addSettlementDataToTable(int count)
        {
            int currentSettlementIndex = 0;
            foreach (AnnualStatement annualStatement in this.annualStatements)
            {
                GuiWebUtil.addAnnualStatementRow(setTable, 5, annualStatement);

                while (currentSettlementIndex < this.settlements.Count
                    && this.settlements[currentSettlementIndex].StartDate >= annualStatement.StartDate
                    && this.settlements[currentSettlementIndex].EndDate <= annualStatement.EndDate)
                {
                    this.addSettlmentRow(currentSettlementIndex);
                    currentSettlementIndex++;
                }
            }

        }

        private void addSettlmentRow(int i)
        {
            TableRow currentRow = new TableRow();
            TableCell idCell = new TableCell();
            idCell.Text = settlements[i].SettlementID.ToString();
            currentRow.Cells.Add(idCell);

            TableCell startCell = new TableCell();
            startCell.Text = ServerUtil.DateTimeToString(settlements[i].StartDate);
            currentRow.Cells.Add(startCell);

            TableCell endCell = new TableCell();
            endCell.Text = ServerUtil.DateTimeToString(settlements[i].EndDate);
            currentRow.Cells.Add(endCell);

            TableCell prizeCell = new TableCell();
            double payment = DAM_ArchiveManagement.Instance.getArchivePayment(settlements[i].SettlementID, this.loginInfo.LoggedInAccount.ID);
            prizeCell.Text = payment.ToString("0.00");
            currentRow.Cells.Add(prizeCell);

            Button detailsButton = new Button();
            detailsButton.ID = settlements[i].SettlementID.ToString();
            detailsButton.CommandArgument = settlements[i].SettlementID.ToString();
            detailsButton.Text = "Details";
            detailsButton.Command += OnDetailButton_Click;
            detailsButton.CssClass = "std_Button";

            TableCell detailCell = new TableCell();
            detailCell.Controls.Add(detailsButton);
            currentRow.Cells.Add(detailCell);

            setTable.Rows.Add(currentRow);
        }


        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            int settlementID = int.Parse(e.CommandArgument.ToString());
            this.loginInfo.Settlement = this.settlements.First(x => x.SettlementID == settlementID);
            Server.Transfer("~/WebAccess/BillDetails.aspx", true);
        }
    }
}