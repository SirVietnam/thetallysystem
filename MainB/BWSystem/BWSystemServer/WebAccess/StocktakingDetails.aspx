﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StocktakingDetails.aspx.cs" Inherits="BWSystemServer.WebAccess.StocktakingDetails" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Stocktakings" SubheaderText="Here you find all the previous stocktakings!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="artTable" runat="server" Width="550px">
                <asp:TableRow>
                    <asp:TableCell Width="50%">Stocktaking Date:</asp:TableCell>
                    <asp:TableCell Width="50%">
                        <asp:TextBox ID="tb_date" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="50%">Stocktaking Time:</asp:TableCell>
                    <asp:TableCell Width="50%">
                        <asp:TextBox ID="tb_time" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />

            <asp:Table ID="stocktakingTable" runat="server" Width="650px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="10%">ID</asp:TableCell>
                    <asp:TableCell Width="25%">Article</asp:TableCell>
                    <asp:TableCell Width="20%">Virtual Stock</asp:TableCell>
                    <asp:TableCell Width="20%">Physical Stock</asp:TableCell>
                    <asp:TableCell Width="15%">Loss*</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </form>
        <br />
        *A negative value means that your count was higher than the systems count.
    </div>
</body>
</html>
