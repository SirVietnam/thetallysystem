﻿using BWSystemServer.BIZ;
using BWSystemServer.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class ManageInventoryShipment : System.Web.UI.Page
    {
        private Regex regexIntNumber;
        private Regex regexDate;
        private LoginInfo loginInfo;
        private List<Article> articleList;


        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.regexIntNumber = ServerUtil.RegexIntNumber;
                this.regexDate = ServerUtil.RegexDate;
                //this.allItemsValid = this.loginInfo.AllItemsValid;
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }

            if (this.loginInfo != null && loginInfo.LoggedInAccount.IsSuperuser)
            {
                DBAccessManagement DBAManagement = DBAccessManagement.Instance;
                this.articleList = DBAManagement.getArticleList_All();

                this.addArticleDataToTable();
            }
        }

        private void addArticleDataToTable()
        {
            GuiWebUtil.addSeperatorRowRow(this.addtbl, "Available Articles", System.Drawing.Color.Green, System.Drawing.Color.Black, 6);
            IEnumerable<Article> availibleArticles = this.articleList.Where(x => !x.IsInventory && x.IsAvailable);
            this.addArticles(availibleArticles);

            if (this.disabledCB.Checked)
            {
                GuiWebUtil.addSeperatorRowRow(this.addtbl, "Disabled Articles", System.Drawing.Color.DarkSlateGray, System.Drawing.Color.White, 6);
                IEnumerable<Article> disabledArticles = this.articleList.Where(x => !x.IsInventory && !x.IsAvailable);
                this.addArticles(disabledArticles);
            }
            if (this.inventoryCB.Checked)
            {
                GuiWebUtil.addSeperatorRowRow(this.addtbl, "Inventory Articles", System.Drawing.Color.DarkBlue, System.Drawing.Color.White, 6);
                IEnumerable<Article> inventoryArticles = this.articleList.Where(x => x.IsInventory);
                this.addArticles(inventoryArticles);
            }
        }



        private void addArticles(IEnumerable<Article> articles)
        {
            foreach (Article article in articles)
            {
                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = article.ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell nameCell = new TableCell();
                nameCell.Text = article.Name;
                currentRow.Cells.Add(nameCell);

                TableCell boxesTBCell = new TableCell();
                TextBox boxesTB = new TextBox();
                boxesTB.ID = article.ID.ToString() + "_Box";
                boxesTB.ValidationGroup = "InputTextboxInt";
                boxesTB.CssClass = "std_Textbox";
                boxesTB.Text = "0";
                boxesTB.AutoPostBack = true;
                boxesTB.TextChanged += BoxesTB_TextChanged;
                boxesTBCell.Controls.Add(boxesTB);
                currentRow.Cells.Add(boxesTBCell);

                TableCell bottlesTBCell = new TableCell();
                TextBox bottlesTB = new TextBox();
                bottlesTB.ID = article.ID.ToString() + "_Bot";
                bottlesTB.ValidationGroup = "InputTextboxInt";
                bottlesTB.CssClass = "std_Textbox";
                bottlesTB.Text = "0";
                boxesTB.AutoPostBack = true;
                bottlesTB.TextChanged += BoxesTB_TextChanged;
                bottlesTBCell.Controls.Add(bottlesTB);
                currentRow.Cells.Add(bottlesTBCell);

                TableCell sumCell = new TableCell();
                sumCell.HorizontalAlign = HorizontalAlign.Center;
                sumCell.ID = article.ID.ToString() + "_Sum";
                sumCell.Text = "0";
                currentRow.Cells.Add(sumCell);

                addtbl.Rows.Add(currentRow);
            }
        }



        private void BoxesTB_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = null;
            if (sender is TextBox)
            {
                tb = (TextBox)sender;
                string[] idElements = tb.ID.Split("_".ToCharArray());
                int ID = Convert.ToInt32(idElements[0]);

                this.handleArticle(ID);
            }
        }

        private bool handleArticle(int ID)
        {
            bool allEntriesValid = true;
            int amountBoxes = this.handleTextbox(ID.ToString() + "_Box");
            int amountBottles = this.handleTextbox(ID.ToString() + "_Bot");

            TableCell sumCell = (TableCell)FindControl(ID.ToString() + "_Sum");
            if (amountBoxes >= 0 && amountBottles >= 0)
            {
                int boxesSize = ServerUtil.getStandardAmount(articleList.First(x => x.ID == ID).Size);
                int sumCount = amountBoxes * boxesSize + amountBottles;

                sumCell.Text = sumCount.ToString();
                allEntriesValid = true;
            }
            else if (sumCell != null)
            {
                sumCell.Text = "ERR";
                sumCell.CssClass = "tb_inputError";
                allEntriesValid = false;
            }
            return allEntriesValid;
        }


        private int handleTextbox(string tbID)
        {
            TextBox tb = (TextBox)FindControl(tbID);
            int amount = -1;
            if (tb != null)
            {
                bool hasValidContent = regexIntNumber.IsMatch(tb.Text);
                
                if (hasValidContent)
                {
                    tb.CssClass = "std_Textbox";
                    amount = Convert.ToInt32(tb.Text);
                }
                else
                {
                    tb.CssClass = "tb_inputError";
                    amount = -1;
                }
            }
            return amount;
        }


        protected void Check_Update_Btn_Click(object sender, EventArgs e)
        {
            if (Check_Update_Btn.Text.Equals("Check"))
            {
                bool allArticleValid = true;
                foreach (Article article in this.articleList)
                {
                    allArticleValid = allArticleValid && this.handleArticle(article.ID);
                }

                bool inputDataValid = this.checkInputData();
                if (allArticleValid && inputDataValid)
                {
                    Check_Update_Btn.Text = "Update Inventory";
                }
            }
            else
            {
                string vendorName = tb_Vendor.Text;
                string deliveryDateString = tb_Date.Text;
                DateTime deliveryDateTime = Convert.ToDateTime(tb_Date.Text);
                int hour = Convert.ToInt32(ddl_Hour.SelectedValue);
                int min = Convert.ToInt32(ddl_Minutes.SelectedValue);
                deliveryDateTime = deliveryDateTime.AddHours(hour);
                deliveryDateTime = deliveryDateTime.AddMinutes(min);

                Shipment shipment = new Shipment(-1, vendorName, deliveryDateTime);
                shipment.addArticleList(this.articleList);
                this.addShipmentDetails(shipment);

                DBAccessManagement DAM = DBAccessManagement.Instance;
                DAM.reportShipment(shipment);

                Server.Transfer("~\\WebAccess\\Shipments.aspx", false);
            }
        }


        private void addShipmentDetails(Shipment shipment)
        {
            foreach (Article article in this.articleList)
            {
                int amountBoxes = this.handleTextbox(article.ID.ToString() + "_Box");
                int amountBottles = this.handleTextbox(article.ID.ToString() + "_Bot");

                if (amountBoxes > 0 || amountBottles > 0)
                {
                    int boxesSize = ServerUtil.getStandardAmount(article.Size);
                    int bottlesCount = amountBoxes * boxesSize + amountBottles;

                    shipment.addArticleAmount(article, bottlesCount);
                }
            }
        }



        private bool checkInputData()
        {
            bool vendorOK = false;
            bool dateOK = false;
            bool timeOK = false;

            if (tb_Vendor.Text != null && !tb_Vendor.Text.Equals(""))
            {
                vendorOK = true;
                tb_Vendor.CssClass = "std_Textbox";
            }
            else
            {
                vendorOK = false;
                tb_Vendor.CssClass = "tb_inputError";
            }

            if (this.regexDate.IsMatch(tb_Date.Text))
            {
                dateOK = true;
                tb_Date.CssClass = "std_Textbox";
            }
            else
            {
                dateOK = false;
                tb_Date.CssClass = "tb_inputError";
            }

            int hour = Convert.ToInt32(ddl_Hour.SelectedValue);
            int min = Convert.ToInt32(ddl_Minutes.SelectedValue);

            if (hour != 0 || min != 0)
            {
                timeOK = true;
                ddl_Hour.CssClass = "std_Textbox";
                ddl_Minutes.CssClass = "std_Textbox";
            }
            else
            {
                timeOK = false;
                ddl_Hour.CssClass = "tb_inputError";
                ddl_Minutes.CssClass = "tb_inputError";
            }

            return (vendorOK && dateOK && timeOK);
        }


        protected void onInputDataChanged(object sender, EventArgs e)
        {
            Check_Update_Btn.Text = "Check";
        }
    }
}