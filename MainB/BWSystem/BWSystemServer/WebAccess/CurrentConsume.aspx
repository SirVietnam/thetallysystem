﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CurrentConsume.aspx.cs" Inherits="BWSystemServer.WebAccess.CurrentConsume" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>
<%@ Register Src="~/WebAccess/SubElements/ConsumptionTable.ascx" TagPrefix="SE" TagName="ConsumptionTable" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Consumption" SubheaderText="Here you can see all your current consumption." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <form id="form1" runat="server">
        <div class="std_div">
            <SE:ConsumptionTable runat="server" ID="ConsumptionTable" TableMode="CurrentConspumtion" />
        </div>
    </form>
</body>
</html>
