﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{

    public partial class Statistics : System.Web.UI.Page
    {
        private LoginInfo loginInfo;

        private Account user;
        private DateTime scopeStart;
        private DateTime scopeEnd;

        private bool inCustomeMode;



        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                try
                {
                    if (!this.IsPostBack)
                    {
                        this.ddl_data_SelectedIndexChanged(null, null);
                        this.ddl_scope_SelectedIndexChanged(null, null);
                    }
                } catch (Exception ex)
                {
                    SimpleLog.Log(ex, false);
                }

            }
            else
            {
                SimpleLog.Log("No login info avaiable.");
            }
            DataBind();
        }


        protected void ddl_data_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.ddl_data.SelectedValue)
            {
                case "my":
                    this.user = this.loginInfo.LoggedInAccount;
                    break;
                case "all":
                    this.user = null;
                    break;
                default:
                    this.user = null;
                    break;
            }
        }

        protected void ddl_scope_SelectedIndexChanged(object sender, EventArgs e)
        {
            DBAccessManagement DAM = DBAccessManagement.Instance;

            switch (this.ddl_scope.SelectedValue)
            {
                case "tab":
                    this.scopeStart = DAM.getLastSettlementEndDate();
                    this.scopeEnd = DateTime.Now;
                    break;
                case "today":
                    DateTime now = DateTime.Now;
                    this.scopeStart = DateTime.Now - (TimeSpan.FromHours(now.Hour) + TimeSpan.FromMinutes(now.Minute) + TimeSpan.FromSeconds(now.Second));
                    this.scopeEnd = DateTime.Now;
                    break;
                case "settlement":
                    Settlement lastSettlement = DAM.getLastSettlementWithoutData();
                    this.scopeStart = lastSettlement != null ? lastSettlement.StartDate : DBAccessManagement.zeroDateTime;
                    this.scopeEnd = lastSettlement != null ? lastSettlement.EndDate : DateTime.Now;
                    break;
                case "currentAnnual":
                    AnnualStatement lastAS = DAM.getLastAnnualAccount();
                    this.scopeStart = lastAS != null ? lastAS.EndDate : DBAccessManagement.zeroDateTime;
                    this.scopeEnd = DateTime.Now;
                    break;
                case "lastAnnual":
                    lastAS = DAM.getLastAnnualAccount();
                    this.scopeStart = lastAS != null ? lastAS.StartDate : DBAccessManagement.zeroDateTime;
                    this.scopeEnd = lastAS != null ? lastAS.EndDate : DateTime.Now;
                    break;
                case "custom":
                    this.inCustomeMode = true;
                    if (this.IsPostBack)
                    {
                        this.startTB_TextChanged(null, null);
                        this.endTB_TextChanged(null, null);
                    }
                    else
                    {
                        this.scopeStart = DateTime.Now - TimeSpan.FromDays(10);
                        this.scopeEnd = DateTime.Now;
                    }
                    break;
                default:
                    this.user = null;
                    break;
            }

            if (this.ddl_scope.SelectedValue != "custom")
            {
                this.inCustomeMode = false;
            }
            this.updateScopes();
            this.DataBind();
        }


        private void updateScopes()
        {
            this.startTB.Text = this.scopeStart.ToString("yyyy-MM-dd");
            this.endTB.Text = this.scopeEnd.ToString("yyyy-MM-dd");
        }


        protected void startTB_TextChanged(object sender, EventArgs e)
        {
            this.scopeStart = Convert.ToDateTime(this.startTB.Text);
        }

        protected void endTB_TextChanged(object sender, EventArgs e)
        {
            this.scopeEnd = Convert.ToDateTime(this.endTB.Text);
            this.scopeEnd += (TimeSpan.FromDays(1) - TimeSpan.FromMilliseconds(1));
        }

        protected void goBtn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null)
            {
                this.ddl_data_SelectedIndexChanged(null, null);
                this.ddl_scope_SelectedIndexChanged(null, null);
                this.createChart();
            }
        }

        private void createChart()
        {
            List<Article> articles = DBAccessManagement.Instance.getArticleList_All();

            Dictionary<int, Int64> articleCounts = new Dictionary<int, long>();
            foreach (Article article in articles)
            {
                articleCounts.Add(article.ID, 0);
            }

            List<BookingData> bookings = null;
            if (this.user != null)
                bookings = DAM_Booking.getBookingDataList(this.scopeStart, this.scopeEnd, this.loginInfo.LoggedInAccount.ID, false);
            else
                bookings = DAM_Booking.getBookingDataList(this.scopeStart, this.scopeEnd, false);

            foreach (BookingData booking in bookings)
            {
                BookingAA bookingData = booking.Booking.First();
                articleCounts[bookingData.Article]++;
            }

            this.chart.Series[0].Points.Clear();
            this.chart.Series[0].YValueType = ChartValueType.Int32;
            this.chart.Titles.Add("Consumption: " + ServerUtil.DateTimeToString(this.scopeStart) + " - " + ServerUtil.DateTimeToString(this.scopeEnd));
            this.chart.Titles[0].Font = new Font(FontFamily.GenericSansSerif, 20.0F, FontStyle.Bold);
            foreach (Article article in articles)
            {
                Int64 amount = articleCounts[article.ID];
                if (amount != 0)
                    this.chart.Series[0].Points.AddXY(article.Name, (int)amount);
            }
            this.chart.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
            this.chart.ChartAreas[0].AxisX.LabelStyle.Angle = -45;
            this.chart.DataBind();
        }



        /*
         * GETTERS
         */
        public bool InCustomeMode
        {
            get
            {
                return inCustomeMode;
            }
        }
    }
}