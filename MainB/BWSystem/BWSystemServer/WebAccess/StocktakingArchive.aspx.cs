﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class StocktakingArchive : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Stocktaking> stocktakings;
        private List<AnnualStatement> annualStatements;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                if (loginInfo.LoggedInAccount.IsSuperuser)
                {
                    this.annualStatements = DBAccessManagement.Instance.getAnnualStatements();
                    this.stocktakings = BIZ.DatabaseAccessManagement.DAM_ArchiveManagement.Instance.getArchiveStocktakingList();

                    this.stocktakings = this.stocktakings.OrderBy(x => x.TimeStamp).ToList();
                    this.annualStatements = this.annualStatements.OrderBy(x => x.StartDate).ToList();

                    this.addData();
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }

        private void addData()
        {
            int currentStocktakingIndex = 0;
            foreach (AnnualStatement annualStatement in this.annualStatements)
            {
                GuiWebUtil.addAnnualStatementRow(stocktakingTable, 2, annualStatement);

                while (currentStocktakingIndex < this.stocktakings.Count
                    && this.stocktakings[currentStocktakingIndex].TimeStamp >= annualStatement.StartDate
                    && this.stocktakings[currentStocktakingIndex].TimeStamp <= annualStatement.EndDate)
                {
                    this.addStocktakingRow(currentStocktakingIndex);
                    currentStocktakingIndex++;
                }
            }
        }

        private void addStocktakingRow(int stocktakingIndex)
        {
            TableRow currentRow = new TableRow();
            Stocktaking currentStocktaking = this.stocktakings[stocktakingIndex];

            TableCell dateTimeCell = new TableCell();
            dateTimeCell.Text = ServerUtil.DateTimeToString(currentStocktaking.TimeStamp);
            currentRow.Cells.Add(dateTimeCell);

            Button detailsButton = new Button();
            detailsButton.ID = stocktakingIndex.ToString();
            detailsButton.Text = "Details";
            detailsButton.Command += OnDetailButton_Click;
            detailsButton.CssClass = "std_Button";

            TableCell detailCell = new TableCell();
            detailCell.Controls.Add(detailsButton);
            currentRow.Cells.Add(detailCell);

            stocktakingTable.Rows.Add(currentRow);
        }

        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            Button btn;
            if (sender is Button)
            {
                btn = (Button)sender;
            }
            else
            {
                throw new ArgumentException("Sender is not type of Button!");
            }

            int index = Convert.ToInt32(btn.ID);
            this.loginInfo.ActiveStocktaking = this.stocktakings[index];

            Server.Transfer("~/WebAccess/StocktakingDetails.aspx", true);
        }
    }
}