﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShipmentDetails.aspx.cs" Inherits="BWSystemServer.WebAccess.ShipmentDetails" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Shipments" SubheaderText="Here you find all the previous shipments!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table ID="artTable" runat="server" Width="600px">
                <asp:TableRow>
                    <asp:TableCell Width="40%">Shipment ID:</asp:TableCell>
                    <asp:TableCell Width="60%">
                        <asp:TextBox ID="tb_shipmentID" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Vendor:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_vendor" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Delivery Date:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_deliveryDateTime" CssClass="std_table_tb" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />

            <asp:Table ID="shipmentTable" runat="server" Width="500px" CssClass="hooverMarking evenOddMarking">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="20%">ID</asp:TableCell>
                    <asp:TableCell Width="40%">Article</asp:TableCell>
                    <asp:TableCell Width="40%">Bottles</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </form>
    </div>
</body>
</html>
