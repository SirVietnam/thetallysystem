﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeviceCreation.aspx.cs" Inherits="BWSystemServer.WebAccess.DeviceCreation" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BWSystem</title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Manage Devices" SubheaderText="You can see and manage all your registered client devices here." />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table runat="server" ID="deviceCreationTbl" CssClass="std_table" Width="500">
                <asp:TableRow CssClass="">
                    <asp:TableCell Width="200" ColumnSpan="2" CssClass="table_head">Required Data</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow CssClass="">
                    <asp:TableCell Width="200">Login:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Login" CssClass="std_Textbox" runat="server" Width="100%"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="200">Password:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Password" TextMode="Password" CssClass="std_Textbox" runat="server" Width="100%"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="std" Width="200">Client IP4 Address:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_IP" CssClass="std_Textbox" runat="server" Width="100%"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="pwErrorRow" Visible="false">
                    <asp:TableCell CssClass="tb_inputError" ColumnSpan="2" Width="300">The Login-Password combination seams to be wrong.</asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />


            <asp:Table runat="server" ID="table_Results" CssClass="std_table" Width="525" Visible="<%# this.ClientAdded %>">
                <asp:TableRow CssClass="">
                    <asp:TableCell Width="100%" ColumnSpan="2" CssClass="table_head">Results</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="175">Login Token:*</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox Width="350" ID="tb_loginToken" CssClass="std_Textbox" runat="server" ReadOnly="true"></asp:TextBox>
                    </asp:TableCell>
                    <%--<asp:TableCell>
                        <asp:Button ID="copyBtn" runat="server" CssClass="std_Button" OnClick="CopyText()" Text="Copy Token"></asp:Button>
                    </asp:TableCell>--%>

                </asp:TableRow>
            </asp:Table>

            <br />

            <div class="substd_div">
                <div style="float: right">
                    <asp:Button CssClass="std_Button" ID="Check_Update_Btn" runat="server" Text="Check" OnClick="Check_Update_Btn_Click" Visible="<%# !this.ClientAdded %>" />
                </div>
                <asp:Label ID="lb_ResultInfo" runat="server">
                *Copy this value into the "LoginToken" field at your clients application.config file.
                </asp:Label>
            </div>
        </form>
    </div>
</body>
</html>
