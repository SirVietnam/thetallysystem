﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnnualAccountCreation.aspx.cs" Inherits="BWSystemServer.WebAccess.AnualStatementCreation" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Design.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <SE:Header runat="server" ID="Head" HeaderText="Anual Accounts" SubheaderText="Here you find all the previous anual accounts!" />
    <SE:Sidebar runat="server" ID="Sidebar" />

    <div class="std_div">
        <form id="form1" runat="server">
            <asp:Table runat="server" ID="requirementsTable" CssClass="std_table" Width="350">
                <asp:TableRow CssClass="table_head">
                    <asp:TableCell Width="150" ColumnSpan="2">Prerequirements*</asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="150">Stocktaking:</asp:TableCell>
                    <asp:TableCell>
                        <asp:Label runat="server" ID="stocktakingStatusTB" CssClass="tb_inputError"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="200">Settlement:</asp:TableCell>
                    <asp:TableCell>
                        <asp:Label runat="server" ID="settlementStatusTB" CssClass="tb_inputError"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <br />

            <asp:Table runat="server" ID="DataTable" CssClass="std_table" Width="350">
                <asp:TableRow>
                    <asp:TableCell Width="150">Name:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="tb_Name" CssClass="std_Textbox" runat="server" Width="200"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="150">Start Date:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="startDateTimeTB" CssClass="std_Textbox" runat="server" Width="200" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="150">End Date:</asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="endDateTimeTB" CssClass="std_Textbox" runat="server" Width="200" Enabled="false"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            
            <div class="substd_div">
                <div style="float: right">
                    <asp:Button CssClass="std_Button" ID="Check_Update_Btn" runat="server" Text="Check" OnClick="Check_Update_Btn_Click" />
                </div>
                *A complete Stocktaking (1st) and a Settlement (2nd) must be created befor creating an Annual Account.
            </div>
        </form>
    </div>
</body>
</html>
