﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class StocktakingDetails : System.Web.UI.Page
    {
        LoginInfo loginInfo;
        Stocktaking stocktaking;
        Article[] articles;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];

                if (loginInfo.LoggedIn && this.loginInfo.ActiveStocktaking != null && loginInfo.LoggedInAccount.IsSuperuser)
                {
                    this.stocktaking = loginInfo.ActiveStocktaking;
                    articles = DBAccessManagement.Instance.getArticleList_All().ToArray();

                    //TODO add fields
                    this.fillShipmentGeneralData();
                    this.fillShipmentDetailData();
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }

        private void fillShipmentGeneralData()
        {
            tb_date.Text = ServerUtil.DateToString(this.stocktaking.TimeStamp.Date);
            tb_time.Text = ServerUtil.TimeToString(this.stocktaking.TimeStamp);
        }


        private void fillShipmentDetailData()
        {
            foreach (Tuple<int, int, int> articleStock in this.stocktaking.Inventory)
            {
                Article article = articles.First(x => x.ID == articleStock.Item1);

                TableRow currentRow = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = article.ID.ToString();
                currentRow.Cells.Add(idCell);

                TableCell articleNameCell = new TableCell();
                articleNameCell.Text = article.Name;
                currentRow.Cells.Add(articleNameCell);

                TableCell virtualStockCell = new TableCell();
                virtualStockCell.Text = articleStock.Item2.ToString();
                currentRow.Cells.Add(virtualStockCell);

                TableCell physicalStockCell = new TableCell();
                physicalStockCell.Text = articleStock.Item3.ToString();
                currentRow.Cells.Add(physicalStockCell);

                TableCell lossCell = new TableCell();
                int lossCount = articleStock.Item2 - articleStock.Item3;
                lossCell.Text = lossCount.ToString();
                currentRow.Cells.Add(lossCell);

                stocktakingTable.Rows.Add(currentRow);
            }
        }
    }
}