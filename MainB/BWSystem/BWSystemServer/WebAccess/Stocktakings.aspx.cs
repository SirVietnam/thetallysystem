﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BWSystemServer.WebAccess
{
    public partial class SeeStocktakings : System.Web.UI.Page
    {
        private LoginInfo loginInfo;
        private List<Stocktaking> stocktakings;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Object)Session["LoginInfo"] != null)
            {
                this.loginInfo = (LoginInfo)Session["LoginInfo"];
                this.loginInfo.onPage_Load(this);

                if (loginInfo.LoggedInAccount.IsSuperuser)
                {
                    DBAccessManagement DBAM = DBAccessManagement.Instance;
                    this.stocktakings = DBAM.getStocktakingList();
                    this.addData();
                }
            }
            else
            {
                Console.Error.WriteLine("NO DATA PROVIDED OR COULD NOT BE READ");
            }
        }

        private void addData()
        {
            for (int i = 0; i < this.stocktakings.Count; i++)
            {
                Stocktaking stocktaking = this.stocktakings[i];
                TableRow currentRow = new TableRow();

                TableCell dateTimeCell = new TableCell();
                dateTimeCell.Text = ServerUtil.DateTimeToString(stocktaking.TimeStamp);
                currentRow.Cells.Add(dateTimeCell);

                Button detailsButton = new Button();
                detailsButton.ID = i.ToString();
                detailsButton.Text = "Details";
                detailsButton.Command += OnDetailButton_Click;
                detailsButton.CssClass = "std_Button";

                TableCell detailCell = new TableCell();
                detailCell.Controls.Add(detailsButton);
                currentRow.Cells.Add(detailCell);

                stocktakingTable.Rows.Add(currentRow);
            }
        }

        private void OnDetailButton_Click(object sender, CommandEventArgs e)
        {
            Button btn;
            if (sender is Button)
            {
                btn = (Button)sender;
            }
            else
            {
                throw new ArgumentException("Sender is not type of Button!");
            }

            int index = Convert.ToInt32(btn.ID);
            this.loginInfo.ActiveStocktaking = this.stocktakings[index];

            Server.Transfer("~/WebAccess/StocktakingDetails.aspx", true);
        }

        protected void CreateStocktaking_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                Server.Transfer("~/WebAccess/StocktakingCreation.aspx", true);
            }
        }

        protected void SeeArchiveData_Btn_Click(object sender, EventArgs e)
        {
            if (this.loginInfo != null && this.loginInfo.LoggedIn && this.loginInfo.LoggedInAccount.IsSuperuser)
            {
                Server.Transfer("~/WebAccess/StocktakingArchive.aspx", true);
            }
        }
    }
}