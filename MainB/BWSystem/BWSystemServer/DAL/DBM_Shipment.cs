﻿using BWSystemServer.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_Shipment : DataBaseManagement
    {

        public DBM_Shipment() : base()
        {
        }

        public List<Shipment> getShipmentList()
        {
            DateTime startTime = new DateTime(1999, 01, 01);
            return this.getShipmentList(startTime);
        }

        //GET Data
        public List<Shipment> getShipmentList(DateTime startTime)
        {
            List<Shipment> shipments = new List<Shipment>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM Shipment WHERE TimeStamp BETWEEN  @StartDate AND @EndDate", connection);

                    this.Database.addParameter(command, "@StartDate", DBAdapter.DBParamType.DateTime, startTime);
                    //SqlParameter startDateParam = new SqlParameter("@StartDate", SqlDbType.DateTime);
                    //startDateParam.Value = startTime.ToString();
                    //command.Parameters.Add(startDateParam);

                    this.Database.addParameter(command, "@EndDate", DBAdapter.DBParamType.DateTime, new DateTime(9999, 01, 01));
                    //SqlParameter endDateParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
                    //endDateParam.Value = (new DateTime(9999, 01, 01)).ToString();
                    //command.Parameters.Add(endDateParam);

                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        shipments.Add(this.buildShipment(dataReader));
                    }
                }
            }

            return shipments;
        }

        public Shipment getShipment(int shipmentID)
        {
            Shipment shipmentToID = null;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM Shipment WHERE ShipmentID = @ShipmentID", connection);

                    this.Database.addParameter(command, "@ShipmentID", DBAdapter.DBParamType.Int, shipmentID);
                    //SqlParameter idParam = new SqlParameter("@ShipmentID", SqlDbType.Int);
                    //idParam.Value = shipmentID;
                    //command.Parameters.Add(idParam);

                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        shipmentToID = this.buildShipment(dataReader);
                    }
                }
            }

            return shipmentToID;
        }


        private Shipment buildShipment(DbDataReader dataReader)
        {
            int shipmentID = (int)dataReader["ShipmentID"];

            String vendorString = (String)dataReader["Vendor"];
            string vendor = vendorString.TrimEnd(null);

            DateTime deliveryDate = (DateTime)dataReader["DeliveryDate"];
            DateTime datetime = (DateTime)dataReader["TimeStamp"];
            return new Shipment(shipmentID, vendor, deliveryDate, datetime);
        }


        //ADD Data
        public int addShipment(Shipment shipmentToAdd)
        {
            int shipmentIdInt = -1;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO Shipment (Vendor, DeliveryDate, TimeStamp) VALUES (@Vendor, @DeliveryDate, @Datetime); " + this.Database.getLastAutoValueCommand(), connection);

                this.Database.addParameter(insertCom, "@Vendor", DBAdapter.DBParamType.VarChar, 50, shipmentToAdd.Vendor);
                //SqlParameter eventName = new SqlParameter("@Vendor", SqlDbType.VarChar, 50);
                //eventName.Value = shipmentToAdd.Vendor;
                //insertCom.Parameters.Add(eventName);

                this.Database.addParameter(insertCom, "@DeliveryDate", DBAdapter.DBParamType.DateTime, shipmentToAdd.DeliveryDatetime);
                //SqlParameter deliveryDateParam = new SqlParameter("@DeliveryDate", SqlDbType.DateTime);
                //deliveryDateParam.Value = shipmentToAdd.DeliveryDatetime.ToString();
                //insertCom.Parameters.Add(deliveryDateParam);

                this.Database.addParameter(insertCom, "@Datetime", DBAdapter.DBParamType.DateTime, shipmentToAdd.Datetime);
                //SqlParameter datetimeParam = new SqlParameter("@Datetime", SqlDbType.DateTime);
                //datetimeParam.Value = shipmentToAdd.Datetime.ToString();
                //insertCom.Parameters.Add(datetimeParam);

                Decimal accIdDecimal = Convert.ToDecimal(insertCom.ExecuteScalar());
                shipmentIdInt = (int)accIdDecimal;
            }

            return shipmentIdInt;
        }

        //Archive Data
        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            base.copyDataToArchive(startDateTime, endDateTime, Tables.Shipment, Tables_Archives.Shipment);
        }


        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            base.deleteDataFromTable(startDateTime, endDateTime, Tables.Shipment);
        }

        public List<Shipment> getArchiveShipmentList()
        {
            List<Shipment> schipmentList = new List<Shipment>();

            using (DbConnection connection = base.Database.getOpenConnection())
            { 
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + this.getTableName(Tables_Archives.Shipment), connection);

                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        schipmentList.Add(this.buildShipment(dataReader));
                    }
                }
            }

            return schipmentList;
        }
    }
}