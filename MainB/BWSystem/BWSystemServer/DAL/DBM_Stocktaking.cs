﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_Stocktaking : DataBaseManagement
    {

        public DBM_Stocktaking()
            : base()
        {
        }

        //GET
        public List<Stocktaking> getStocktakingList()
        {
            List<Stocktaking> stocktakings = this.getStocktakingList(this.getTableName(Tables.Stocktaking));
            return stocktakings;
        }


        private List<Stocktaking> getStocktakingList(string tableName)
        {
            List<Stocktaking> stocktakings = new List<Stocktaking>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + tableName + " ORDER BY(TimeStamp) ASC ", connection);

                    DbDataReader dataReader = command.ExecuteReader();
                    Stocktaking lastStocktaking = null;
                    while (dataReader.Read())
                    {
                        Stocktaking nextStocktaking = this.buildStocktaking(dataReader);
                        if (lastStocktaking != null &&
                            lastStocktaking.TimeStamp.Equals(nextStocktaking.TimeStamp))
                        {
                            lastStocktaking.Inventory.Add(nextStocktaking.Inventory.First());
                        }
                        else
                        {
                            stocktakings.Add(nextStocktaking);
                            lastStocktaking = nextStocktaking;
                        }
                    }
                }
            }

            return stocktakings;
        }


        private Stocktaking buildStocktaking(DbDataReader dataReader)
        {
            DateTime timeStamp = (DateTime)dataReader["TimeStamp"];

            int articleID = (int)dataReader["Article"];
            //Article article = this.getArticle(articleID);
            int vStock = (int)dataReader["VirtualStock"];
            //article.Stock = vStock;
            int pStock = (int)dataReader["PhysicalStock"];

            return new Stocktaking(timeStamp, articleID, vStock, pStock);
        }


        //ADD Data
        public void addStocktaking(Stocktaking stocktaking)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                List<Tuple<int, int, int>> inventory = stocktaking.Inventory;
                foreach (Tuple<int, int, int> items in inventory)
                {
                    DbCommand insertCom = this.Database.getCommand("INSERT INTO Stocktaking (TimeStamp, Article, VirtualStock, PhysicalStock) "
                        + "VALUES (@TimeStamp, @ArticleID, @VStock, @PStock);", connection);

                    this.Database.addParameter(insertCom, "@TimeStamp", DBAdapter.DBParamType.DateTime, stocktaking.TimeStamp);
                    this.Database.addParameter(insertCom, "@ArticleID", DBAdapter.DBParamType.Int, items.Item1);
                    this.Database.addParameter(insertCom, "@VStock", DBAdapter.DBParamType.Int, items.Item2);
                    this.Database.addParameter(insertCom, "@PStock", DBAdapter.DBParamType.Int, items.Item3);

                    insertCom.ExecuteNonQuery();
                }
            }
        }

        //Archive Data
        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            base.copyDataToArchive(startDateTime, endDateTime, Tables.Stocktaking, Tables_Archives.Stocktaking);
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            base.deleteDataFromTable(startDateTime, endDateTime, Tables.Stocktaking);
        }

        public List<Stocktaking> getArchiveStocktakingList()
        {
            List<Stocktaking> stocktakings = this.getStocktakingList(this.getTableName(Tables_Archives.Stocktaking));
            return stocktakings;
        }
    }
}