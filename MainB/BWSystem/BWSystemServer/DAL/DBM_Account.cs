﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_Account : DataBaseManagement
    {
        private string standardPicture;

        public DBM_Account()
            : base()
        {
            //TODO
            //BUG?!?!
            this.standardPicture = (string)ServerUtil.getConfigValue("StandardAccountURL", typeof(string));
        }


        public List<Account> getAccountList()
        {
            List<Account> users = new List<Account>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + this.getTableName(Tables.Account), connection);
                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Account nextAccount = this.buildAccount(dataReader);
                        users.Add(nextAccount);
                    }
                }

            }
            return users;
        }

        private Account buildAccount(DbDataReader dataReader)
        {
            int id = (int)dataReader["AccountID"];
            String surname = (String)dataReader["Surname"];
            surname = surname.TrimEnd(null);
            String name = (String)dataReader["Name"];
            name = name.TrimEnd(null);
            String nickname = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("Nickname")))
            {
                nickname = (String)dataReader["Nickname"];
                nickname = nickname.TrimEnd(null);
            }

            String pictureURL;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("PictureURL")))
            {
                pictureURL = (String)dataReader["PictureURL"];
                pictureURL = pictureURL.TrimEnd(null);
            }
            else
            {
                pictureURL = null;
            }


            int standardArticleID = (int)dataReader["StandardArticle"];
            //Article standardArticle = this.getArticle(standardArticleID);

            Boolean isSU = Convert.ToBoolean(dataReader["IsSuperuser"]);
            Boolean authAlways = Convert.ToBoolean(dataReader["AuthenticateAlways"]);
            Boolean isAvailable = Convert.ToBoolean(dataReader["IsAvailable"]);

            Authentication auth = this.getAuthentification(id);

            return new Account(id, surname, name, nickname, pictureURL, standardArticleID, null, auth, isSU, authAlways, isAvailable);
        }



        private Authentication getAuthentification(int userID)
        {
            //TODO convert RegAuthentification to Authentification
            Authentication auth = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {

                    DbCommand command = this.Database.getCommand("SELECT * FROM Authentification WHERE AccountID = @SpecAccountID", connection);

                    this.Database.addParameter(command, "@SpecAccountID", DBAdapter.DBParamType.Int, userID);
                    //SqlParameter idParam = new SqlParameter("@SpecAccountID", SqlDbType.Int);
                    //idParam.Value = userID;
                    //command.Parameters.Add(idParam);

                    DbDataReader dataReader = command.ExecuteReader();
                    dataReader.Read();

                    auth = buildAuthentification(dataReader);
                }
            }

            return auth;
        }

        private Authentication buildAuthentification(DbDataReader dataReader)
        {
            String login = null;

            if (!dataReader.IsDBNull(dataReader.GetOrdinal("Login")))
            {
                login = (string)dataReader["Login"];
            }

            int[] securityCode = this.getSecurityCode(dataReader);

            Int64[] cardCodes = this.getCardCodes(dataReader);

            byte[] securityCodeHash = !dataReader.IsDBNull(dataReader.GetOrdinal("Hash")) ? (byte[])dataReader["Hash"] : new byte[0];
            byte[] securityCodeSalt = !dataReader.IsDBNull(dataReader.GetOrdinal("Salt")) ? (byte[])dataReader["Salt"] : new byte[0];

            Authentication auth = new Authentication(securityCodeHash, securityCodeSalt, cardCodes, login);
            auth.SecurityCode = securityCode;
            return auth;

        }


        private int[] getSecurityCode(DbDataReader dataReader)
        {
            string securityCodeString = !dataReader.IsDBNull(dataReader.GetOrdinal("SecurityCode")) ? (string)dataReader["SecurityCode"] : null;
            int[] securityCode = null;
            if (securityCodeString != null)
            {
                securityCodeString = securityCodeString.TrimEnd(null);
                securityCode = ServerUtil.eliminateSeperatorSign(securityCodeString);
            }
            return securityCode;
        }

        private Int64[] getCardCodes(DbDataReader dataReader)
        {
            Int64[] cardCodes = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("CardCode")))
            {
                string cardCodeString = (string)dataReader["CardCode"];
                cardCodes = this.activeDirToCardCode(cardCodeString);
            }
            return cardCodes;
        }



        public Boolean addAccount(Account newAccount)
        {
            int accountId = -1;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                this.makeAccountDbViable(newAccount);

                DbCommand insertCom = this.Database.getCommand("INSERT INTO Account (Surname, Name, Nickname, PictureURL, StandardArticle, IsSuperuser, AuthenticateAlways, IsAvailable) "
                    + " VALUES (@Surname, @Name, @Nickname, @PictureURL, @StandardArticle, @IsSuperuser, @AuthenticateAlways, @IsAvailable)"
                + "; " + this.Database.getLastAutoValueCommand(), connection);

                this.Database.addParameter(insertCom, "@Surname", DBAdapter.DBParamType.VarChar, 50, newAccount.Surname);
                this.Database.addParameter(insertCom, "@Name", DBAdapter.DBParamType.VarChar, 50, newAccount.Name);
                this.Database.addParameter(insertCom, "@Nickname", DBAdapter.DBParamType.VarChar, 50, newAccount.Nickname);
                this.Database.addParameter(insertCom, "@PictureURL", DBAdapter.DBParamType.VarChar, 50, newAccount.PictureURL);
                this.Database.addParameter(insertCom, "@StandardArticle", DBAdapter.DBParamType.Int, newAccount.Standard);
                this.Database.addParameter(insertCom, "@IsSuperuser", DBAdapter.DBParamType.Bit, newAccount.IsSuperuser);
                this.Database.addParameter(insertCom, "@AuthenticateAlways", DBAdapter.DBParamType.Bit, newAccount.AuthenticateAlways);
                this.Database.addParameter(insertCom, "@IsAvailable", DBAdapter.DBParamType.Bit, newAccount.IsAvailable);
                Decimal accIdDecimal = Convert.ToDecimal(insertCom.ExecuteScalar());
                accountId = (int)accIdDecimal;
            }

            if (accountId != -1)
            {
                this.addAuthentification(accountId, newAccount.Authentication);
                newAccount.ID = accountId;
            }

            return (accountId != -1);
        }

        private void addAuthentification(int accountID, Authentication aut)
        {
            this.addInstituteAuthentication(accountID, aut);
        }



        private void addInstituteAuthentication(int accountID, Authentication regAut)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO Authentification VALUES (@AccountID, " +
                "@SecurityCode, @CardCode, @Login, @Salt, @Hash)", connection);

                this.Database.addParameter(insertCom, "@AccountID", DBAdapter.DBParamType.Int, accountID);
                this.Database.addParameter(insertCom, "@SecurityCode", DBAdapter.DBParamType.VarChar, 50, ServerUtil.insertSeperatorSign(regAut.SecurityCode));
                this.Database.addParameter(insertCom, "@CardCode", DBAdapter.DBParamType.VarChar, 100, this.cardNumbersToString(regAut.CardCode));
                if (regAut.LoginName != null)
                {
                    regAut.LoginName = regAut.LoginName.ToLower();
                }
                this.Database.addParameter(insertCom, "@Login", DBAdapter.DBParamType.VarChar, 100, regAut.LoginName);
                this.Database.addParameter(insertCom, "@Salt", DBAdapter.DBParamType.VarBinary, 32, regAut.SecurityCodeSalt);
                this.Database.addParameter(insertCom, "@Hash", DBAdapter.DBParamType.VarBinary, 32, regAut.SecurityCodeHash);

                insertCom.ExecuteNonQuery();
            }
        }




        private object cardNumbersToString(Int64[] cardCodes)
        {
            string cardCodesString = null;
            if (cardCodes != null)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                for (int i = 0; i < cardCodes.Length; i++)
                {
                    sb.Append(cardCodes[i].ToString());
                    if (i != cardCodes.Length - 1)
                    {
                        sb.Append(';');
                    }
                }
                cardCodesString = sb.ToString();
            }
            return cardCodesString;
        }



        private Int64[] activeDirToCardCode(string cardCodesString)
        {
            //TODO exchange ";"
            Int64[] cardNumbers = null;
            if (cardCodesString.Contains(';'))
            {
                LinkedList<Int64> cardNumbersList = new LinkedList<long>();

                string[] cardNumbersSplit = cardCodesString.Split(';');
                foreach (string cardNumberString in cardNumbersSplit)
                {
                    cardNumbersList.AddLast(this.stringToCardNumber(cardNumberString));
                }
                cardNumbers = cardNumbersList.ToArray<Int64>();
            }
            else
            {
                cardNumbers = new Int64[1];
                cardNumbers[0] = this.stringToCardNumber(cardCodesString);
            }

            return cardNumbers;
        }

        private Int64 stringToCardNumber(string cardNumberString)
        {
            Int64 cardNumber = -999;
            try
            {
                cardNumber = Convert.ToInt64(cardNumberString);
            } catch (OverflowException e)
            {
                //TODO Logging
                Console.Error.WriteLine("Exception trying to parse number: " + cardNumberString[0] + "\n" + e.Message.ToString());
            } catch (FormatException e)
            {
                //TODO Logging
                Console.Error.WriteLine("Exception trying to parse number: " + cardNumberString[0] + "\n" + e.Message.ToString());
            }
            return cardNumber;
        }

        /*
        private Account getAccount(int accountID)
        {
            Account account = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            if (connection.State == ConnectionState.Open)
            {
                DbCommand command = this.Database.getCommand("SELECT * FROM Account WHERE AccountID = @SpecAccountID", connection);
                SqlParameter idParam = new SqlParameter("@SpecAccountID", SqlDbType.Int);
                idParam.Value = accountID;
                command.Parameters.Add(idParam);
                DbDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    account = this.buildAccount(dataReader);
                }
            }
            this.closeConnection(connection);
            return account;
        }
        */

        internal bool updateAccount(Account oldAccount)
        {
            int rows = 0;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                makeAccountDbViable(oldAccount);

                DbCommand changeCom = this.Database.getCommand("UPDATE Account SET" +
                " Surname = @Surname, Name = @Name, Nickname = @Nickname, PictureURL = @PicURL, StandardArticle = @StandArticle, IsSuperuser = @IsSU, AuthenticateAlways = @AuthAlways, IsAvailable = @IsAv" +
                " WHERE AccountID = @AccID ;", connection);


                this.Database.addParameter(changeCom, "@Surname", DBAdapter.DBParamType.VarChar, 50, oldAccount.Surname);
                this.Database.addParameter(changeCom, "@Name", DBAdapter.DBParamType.VarChar, 50, oldAccount.Name);
                this.Database.addParameter(changeCom, "@Nickname", DBAdapter.DBParamType.VarChar, 50, oldAccount.Nickname);
                this.Database.addParameter(changeCom, "@PicURL", DBAdapter.DBParamType.VarChar, 50, oldAccount.PictureURL);
                this.Database.addParameter(changeCom, "@StandArticle", DBAdapter.DBParamType.Int, oldAccount.Standard);
                this.Database.addParameter(changeCom, "@IsSU", DBAdapter.DBParamType.Bit, oldAccount.IsSuperuser);
                this.Database.addParameter(changeCom, "@AuthAlways", DBAdapter.DBParamType.Bit, oldAccount.AuthenticateAlways);
                this.Database.addParameter(changeCom, "@IsAv", DBAdapter.DBParamType.Bit, oldAccount.IsAvailable);

                this.Database.addParameter(changeCom, "@AccID", DBAdapter.DBParamType.Int, oldAccount.ID);

                rows = changeCom.ExecuteNonQuery();
            }

            bool authUpdated = this.updateAuthentification(oldAccount.ID, oldAccount.Authentication);
            return (rows == 1) && authUpdated;
        }

        private bool updateAuthentification(int accountID, Authentication authentication)
        {
            int rows = 0;
            bool authUpdated = true;
            Authentication oldAuth = this.getAuthentification(accountID);

            string sqlComString = null;
            //if (!oldAuth.SecurityCode.Equals(authentication.SecurityCode) ||
            //     !oldAuth.CardCode.Equals(authentication.CardCode))
            //{

            sqlComString = "UPDATE Authentification SET " +
            "SecurityCode = @SecurityCode, CardCode = @CardCode, Login = @Login, Salt = @Salt, Hash = @Hash " +
            "WHERE AccountID = @AccountID ;";

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand(sqlComString, connection);

                this.Database.addParameter(insertCom, "@AccountID", DBAdapter.DBParamType.Int, accountID);
                this.Database.addParameter(insertCom, "@SecurityCode", DBAdapter.DBParamType.VarChar, 50, ServerUtil.insertSeperatorSign(authentication.SecurityCode));
                this.Database.addParameter(insertCom, "@CardCode", DBAdapter.DBParamType.VarChar, 100, this.cardNumbersToString(authentication.CardCode));
                if (authentication.LoginName != null)
                {
                    authentication.LoginName = authentication.LoginName.ToLower();
                }
                this.Database.addParameter(insertCom, "@Login", DBAdapter.DBParamType.VarChar, 100, authentication.LoginName);
                this.Database.addParameter(insertCom, "@Salt", DBAdapter.DBParamType.VarBinary, 32, authentication.SecurityCodeSalt);
                this.Database.addParameter(insertCom, "@Hash", DBAdapter.DBParamType.VarBinary, 32, authentication.SecurityCodeHash);

                rows = insertCom.ExecuteNonQuery();
            }

            authUpdated = rows == 1;
            return authUpdated;
        }


        internal Tuple<int, string>[] getID_LoginList()
        {
            LinkedList<Tuple<int, string>> infoList = new LinkedList<Tuple<int, string>>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {

                    DbCommand command = this.Database.getCommand("SELECT AccountID, Login FROM Authentification", connection);

                    DbDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        Tuple<int, string> tuple = this.getID_Login(dataReader);
                        infoList.AddLast(tuple);
                    }

                }
            }

            return infoList.ToArray<Tuple<int, string>>();
        }


        private Tuple<int, string> getID_Login(DbDataReader dataReader)
        {

            int id = (int)dataReader["AccountID"];
            string login = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("Login")))
            {
                login = (String)dataReader["Login"];
            }
            return new Tuple<int, string>(id, login);
        }

        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }


        /*
         * Helper
         */
        private void makeAccountDbViable(Account newAccount)
        {
            if (newAccount.Nickname != null && newAccount.Nickname.TrimEnd() == "")
                newAccount.Nickname = null;
        }
    }
}