﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL.PictureManager
{
    public class PictureManager
    {
        private string picturesPath;
        private string thumbnailPath;
        private string picturesRelativePath;
        //private string[] pictures;
        //private DirectoryInfo dirInfo;

        bool cutImages;

        public PictureManager(string pictureSubFolter, bool cutImages)
        {
            this.cutImages = cutImages;
            string picturePath = (string)ServerUtil.getConfigValue("PicturePath", typeof(string));

            this.picturesRelativePath = picturePath + Path.DirectorySeparatorChar + pictureSubFolter;
            this.picturesPath = HttpContext.Current.Server.MapPath(picturesRelativePath);
            this.thumbnailPath = HttpContext.Current.Server.MapPath(picturesRelativePath + Path.DirectorySeparatorChar + "Thumbnails");

            //dirInfo = new DirectoryInfo(this.picturesPath);

            this.createFolder();
        }


        /*
         * Main Methods
         */
        public string addPicture(byte[] file, string pictureName)
        {
            this.createFolder();

            string completePicturePath = this.getCompletePicturePath(pictureName);
            this.deleteFile(pictureName);

            bool pictureAdded = this.savePicture(file, completePicturePath);
            if (pictureAdded)
            {
                pictureAdded = this.validateAddedPicture(completePicturePath);
                if (!pictureAdded)
                {
                    File.Delete(completePicturePath);
                }
            }
            return pictureAdded ? pictureName : null;
        }

        internal void deletePicture(string pictureName)
        {
            this.deleteFile(pictureName);
        }


        public byte[] getDefalutImage(int size)
        {
            string pictureName = (string)ServerUtil.getConfigValue("DefaultPictureName", typeof(string));

            return this.getImage(pictureName, size);
        }


        public byte[] getImage(string pictureName, int size)
        {
            byte[] pictureBytes = null;

            string completePicturePath = this.getCompletePicturePath(pictureName);

            if (File.Exists(completePicturePath))
            {
                string completeThumbnailPath = this.getCompleteThumbnailPath(completePicturePath, new Size(size, size));

                if (!File.Exists(completeThumbnailPath))
                {
                    this.createThumbnail(completePicturePath, completeThumbnailPath, size, size);
                }
                pictureBytes = File.ReadAllBytes(completeThumbnailPath);
            }
            else
            {
                SimpleLog.Error("The picture for user: " + pictureName + " does not exist. But was requested ...", false);
            }
            return pictureBytes;
        }


        internal string getImageRelativePath(string pictureName)
        {
            string relImagePath = this.picturesRelativePath + Path.DirectorySeparatorChar + pictureName;
            if (!relImagePath.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase))
            {
                relImagePath += ".jpg";
            }
            return relImagePath;
        }

        internal DateTime getImageLastModified(string fileName)
        {
            string completeFilePath = this.getCompletePicturePath(fileName);
            FileInfo info = new FileInfo(completeFilePath);
            return info.LastWriteTime;
        }

        /////////////// HELPERS ////////////////
        #region Helper
        private string getCompletePicturePath(string pictureName)
        {
            string completePicturePath = this.picturesPath + Path.DirectorySeparatorChar + pictureName;
            if (!pictureName.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase))
            {
                completePicturePath += ".jpg";
            }
            return completePicturePath;
        }

        private string getCompleteThumbnailPath(string picturePath, Size imateSize)
        {

            int startIndex = picturePath.LastIndexOf(Path.DirectorySeparatorChar);
            int endIndex = picturePath.LastIndexOf('.');

            string pictureFolder = picturePath.Substring(0, startIndex);
            string pictureName = picturePath.Substring(startIndex + 1, endIndex - startIndex - 1);
            string fileEnding = picturePath.Substring(endIndex, picturePath.Length - endIndex);

            int maxSize = Math.Max(imateSize.Height, imateSize.Width);

            string thumbPicturePath = pictureFolder + Path.DirectorySeparatorChar + "Thumbnails" + Path.DirectorySeparatorChar + pictureName + '_' + maxSize.ToString() + fileEnding;
            return thumbPicturePath;
        }


        private void createFolder()
        {
            if (!Directory.Exists(this.picturesPath))
            {
                Directory.CreateDirectory(this.picturesPath);
            }
            if (!Directory.Exists(this.thumbnailPath))
            {
                Directory.CreateDirectory(this.thumbnailPath);
            }
        }

        private void deleteFile(string fileName)
        {
            string filePath = this.getCompletePicturePath(fileName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            string thumbnailName = fileName;
            if (fileName.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase))
            {
                thumbnailName = thumbnailName.Replace(".jpg", "");
            }

            string[] thumbnails = Directory.GetFiles(this.thumbnailPath);
            foreach (string thumbnail in thumbnails)
            {
                if (thumbnail.Contains(thumbnailName))
                {
                    File.Delete(thumbnail);
                }
            }
        }


        private bool savePicture(byte[] file, string filePath)
        {
            MemoryStream ms = new MemoryStream(file);
            Image image = Image.FromStream(ms);
            string completeFilePath = filePath;

            if (!filePath.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase))
            {
                completeFilePath += ".jpg";
            }

            image.Save(completeFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            image.Dispose();
            ms.Dispose();

            return File.Exists(completeFilePath);
        }

        private bool validateAddedPicture(string picturePath)
        {
            bool validPicture = false;
            try
            {
                Image fileImage = Image.FromFile(picturePath);
                validPicture = true;
                fileImage.Dispose();
            } catch (Exception ex)
            {
                SimpleLog.Log(ex, false);
            }
            return validPicture;
        }


        public void createThumbnail(string completeFilePath, string completeThumbnailPath, int width, int hight)
        {
            string thumbFolderPath = completeThumbnailPath;

            if (File.Exists(completeFilePath))
            {
                Image fileImage = Image.FromFile(completeFilePath);

                if (this.cutImages)
                {
                    Image quadImage = this.cutImageToQuad(fileImage);
                    fileImage.Dispose();
                    fileImage = quadImage;
                }

                using (Image thumb = this.resizeImage(fileImage, new Size(width, hight)))
                {
                    thumb.Save(completeThumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                fileImage.Dispose();
            }
        }


        private Image resizeImage(Image image, Size size)
        {
            int newWidth;
            int newHeight;
            int originalWidth = image.Width;
            int originalHeight = image.Height;
            double percentWidth = (double)size.Width / (double)originalWidth;
            double percentHeight = (double)size.Height / (double)originalHeight;
            double percent = percentHeight < percentWidth ? percentHeight : percentWidth;
            newWidth = (int)(originalWidth * percent);
            newHeight = (int)(originalHeight * percent);

            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }


        private Image cutImageToQuad(Image image)
        {
            int size;
            Rectangle sourceRect;
            if (image.Width < image.Height)
            {
                size = image.Width;
                double middle = (double)image.Height / 2.0;
                double topPoint = middle - ((double)size / 2.0);
                sourceRect = new Rectangle(0, (int)topPoint, size, size);
            }
            else
            {
                size = image.Height;
                double middle = (double)image.Width / 2.0;
                double leftPoint = middle - (double)(size / 2.0);
                sourceRect = new Rectangle((int)leftPoint, 0, size, size);
            }

            Bitmap newImage = new Bitmap(size, size);
            newImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBilinear;
                graphicsHandle.DrawImage(image, 0, 0, sourceRect, GraphicsUnit.Pixel);
            }
            return newImage;
        }
        #endregion
    }
}