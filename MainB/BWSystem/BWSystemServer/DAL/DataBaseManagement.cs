﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.Common;
using MySql.Data.MySqlClient;
using BWSystemServer.DAL.DBAdapter;
using System.Diagnostics;

namespace BWSystemServer.DAL
{
    public abstract class DataBaseManagement
    {
        private static bool TABLES_CREATED = false;
        private static DataBaseType dbType;

        private string SqlTableNameVariable;
        private IDBAdapter database;


        public enum Tables
        {
            Account,
            Authentification,
            NonOrgLogin,
            Article,
            AnnualStatement,
            Event,
            BookingData,
            Cancellation,
            PrizeHistory,
            Settlement,
            SettlementData,
            Shipment,
            ShipmentData,
            Stocktaking,
            ClientDevice
        }

        public enum Tables_Archives
        {
            BookingData,
            Cancellation,
            Event,
            PrizeHistory,
            Settlement,
            SettlementData,
            Shipment,
            ShipmentData,
            Stocktaking
        }

        private enum DataBaseType
        {
            MSSql,
            MySql
        }

        public DataBaseManagement()
        {
            DataBaseManagement.dbType = this.getDbType();

            switch (DataBaseManagement.dbType)
            {
                case DataBaseType.MSSql:
                    this.database = new MSSqlAdapter(ServerUtil.getConnectionString());
                    break;
                case DataBaseType.MySql:
                    this.database = new MySqlAdapter(ServerUtil.getConnectionString());
                    break;
                default:
                    ArgumentException ex = new ArgumentException("The defined DBType is unknow: " + DataBaseManagement.dbType.ToString());
                    SimpleLog.Log(ex);
                    throw ex;
            }

            if (!DataBaseManagement.TABLES_CREATED)
            {
                this.SqlTableNameVariable = (string)ServerUtil.getConfigValue("SqlTableNameVariable", typeof(string));
                this.createAllDatabases();
                DataBaseManagement.TABLES_CREATED = true;
            }
        }

        /*
         * Connection Management
         */
        #region ConnectionManagement
        protected IDBAdapter Database
        {
            get
            {
                return database;
            }
        }

        private DataBaseType getDbType()
        {
            string typeString = (string)ServerUtil.getConfigValue("DBType", typeof(string));
            DataBaseType type = (DataBaseType)Enum.Parse(typeof(DataBaseType), typeString);
            return type;
        }
        #endregion

        /*
         * AutoCreation of Tables
         */
        #region TableAutoCreation
        private void createAllDatabases()
        {
            string sqlDirPath = (string)ServerUtil.getConfigValue("SqlFilePath", typeof(string));
            sqlDirPath = System.Web.HttpContext.Current.Server.MapPath(sqlDirPath);
            sqlDirPath += Path.DirectorySeparatorChar + DataBaseManagement.dbType.ToString();

            if (!Directory.Exists(sqlDirPath))
            {
                string msg = "The file " + sqlDirPath + " could not be found!";
                SimpleLog.Log(msg);
                throw new FileNotFoundException(msg);
            }

            string[] tableFiles = Directory.GetFiles(sqlDirPath);

            if (tableFiles.Length != Enum.GetValues(typeof(Tables)).Length)
            {
                throw new FileNotFoundException("The amount of file ins your SqlFilePath is not correct!");
            }

            List<string> existingTables = this.getExistingTables();

            this.createStandardTable(tableFiles, existingTables);
            this.createArchiveTables(tableFiles, existingTables);
        }

        private void createStandardTable(string[] tableFiles, List<string> existingTables)
        {
            string[] tables = Enum.GetNames(typeof(Tables));

            foreach (string tableName in tables)
            {
                string filePath = tableFiles.FirstOrDefault(x => x.Contains(tableName));

                string existingTable = existingTables.FirstOrDefault(x => x.ToLower().Equals(tableName.ToLower()));

                if (filePath != null && existingTable == null)
                {
                    string tableCreationString = File.ReadAllText(filePath);
                    tableCreationString = tableCreationString.Replace(this.SqlTableNameVariable, tableName);
                    this.createTable(tableCreationString);

                    SimpleLog.Log("DB Table created: " + tableName);
                }
            }
        }

        private void createArchiveTables(string[] tableFiles, List<string> existingTables)
        {
            string[] archiveTables = Enum.GetNames(typeof(Tables_Archives));
            string archivePrefix = (string)ServerUtil.getConfigValue("SqlArchivePrefix", typeof(string));

            foreach (string archiveTable in archiveTables)
            {
                string filePath = tableFiles.FirstOrDefault(x => x.Contains(archiveTable));
                string archiveTableName = archivePrefix + archiveTable;

                if (filePath != null && !existingTables.Contains(archiveTableName.ToLower()))
                {
                    string tableCreationString = "";

                    using (StreamReader reader = new StreamReader(filePath))
                    {
                        tableCreationString = reader.ReadToEnd();
                    }

                    tableCreationString = this.Database.converToArchiveTable(tableCreationString);
                    tableCreationString = tableCreationString.Replace(this.SqlTableNameVariable, archiveTableName);

                    this.createTable(tableCreationString);

                    SimpleLog.Log("DB Table created: " + archiveTable);
                }
            }
        }




        private void createTable(string tableCreationString)
        {
            using (DbConnection connection = this.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    Console.WriteLine(tableCreationString);

                    DbCommand command = this.Database.getCommand(tableCreationString, connection);
                    command.ExecuteNonQuery();
                }
            }
        }


        private List<string> getExistingTables()
        {
            List<string> tables = new List<string>();
            using (DbConnection connection = this.Database.getOpenConnection())
            {
                DbCommand command = this.Database.getCommand("SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE';", connection);

                using (DbDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        string table = (string)dataReader[0];
                        tables.Add(table.ToLower());
                    }
                }
            }
            return tables;
        }
        #endregion


        /*
         * Archiving of TableData
         */
        #region Archiving
        internal abstract void copyDataToArchive(DateTime startDateTime, DateTime endDateTime);

        protected void copyDataToArchive(DateTime startDateTime, DateTime endDateTime, Tables sourceTable, Tables_Archives targetTable)
        {
            using (DbConnection connection = this.Database.getOpenConnection())
            {
                //SET IDENTITY_INSERT {1} ON;;
                string copyComString = "INSERT INTO {1} SELECT * FROM {2} WHERE TimeStamp >= @StartDate AND TimeStamp <= @EndDate;";
                string tableName = this.getTableName(sourceTable);
                string archiveTableName = this.getTableName(targetTable);
                copyComString = copyComString.Replace(@"{1}", archiveTableName);
                copyComString = copyComString.Replace(@"{2}", tableName);
                DbCommand copyCom = this.Database.getCommand(copyComString, connection);

                this.Database.addParameter(copyCom, "@StartDate", DBAdapter.DBParamType.DateTime, startDateTime);
                //SqlParameter startDateParam = new SqlParameter("@StartDate", SqlDbType.DateTime);
                //startDateParam.Value = startDateTime;
                //copyCom.Parameters.Add(startDateParam);

                this.Database.addParameter(copyCom, "@EndDate", DBAdapter.DBParamType.DateTime, endDateTime);
                //SqlParameter endDateParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
                //endDateParam.Value = endDateTime;
                //copyCom.Parameters.Add(endDateParam);

                int copiedRows = copyCom.ExecuteNonQuery();

            }
        }

        internal abstract void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime);

        protected void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime, Tables sourceTable)
        {
            using (DbConnection connection = this.Database.getOpenConnection())
            {
                //SET IDENTITY_INSERT {1} ON;
                string deleteComString = "DELETE FROM {1} WHERE TimeStamp >= @StartDate AND TimeStamp <= @EndDate;";
                string tableName = this.getTableName(sourceTable);
                deleteComString = deleteComString.Replace(@"{1}", tableName);
                DbCommand deleteCom = this.Database.getCommand(deleteComString, connection);

                this.Database.addParameter(deleteCom, "@StartDate", DBAdapter.DBParamType.DateTime, startDateTime);
                //SqlParameter startDateParam = new SqlParameter("@StartDate", SqlDbType.DateTime);
                //startDateParam.Value = startDateTime;
                //deleteCom.Parameters.Add(startDateParam);

                this.Database.addParameter(deleteCom, "@EndDate", DBAdapter.DBParamType.DateTime, endDateTime);
                //SqlParameter endDateParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
                //endDateParam.Value = endDateTime;
                //deleteCom.Parameters.Add(endDateParam);

                int copiedRows = deleteCom.ExecuteNonQuery();
            }
        }

        internal void deleteAllContentFromArchive()
        {
            string[] archiveTables = Enum.GetNames(typeof(Tables_Archives));

            string contentDeletionString = "DELETE FROM {1}";
            using (DbConnection connection = this.Database.getOpenConnection())
            {
                foreach (string archiveTable in archiveTables)
                {
                    string archiveTableName = (string)ServerUtil.getConfigValue("SqlArchivePrefix", typeof(string)) + archiveTable;
                    string sqlString = contentDeletionString.Replace("{1}", archiveTableName);
                    DbCommand copyCom = this.Database.getCommand(sqlString, connection);
                    int copiedRows = copyCom.ExecuteNonQuery();

                }
            }
        }
        #endregion


        /*
         * Helpers
         */
        #region Helpers
        internal string getTableName(Tables table)
        {
            return table.ToString();
        }

        internal string getTableName(Tables_Archives table)
        {
            return (string)ServerUtil.getConfigValue("SqlArchivePrefix", typeof(string)) + table.ToString();
        }


        [Conditional("DEBUG")]
        internal void resetDatabases()
        {
            List<string> tablesToDelete = new List<string>();
            tablesToDelete.Add(this.getTableName(Tables.ShipmentData));
            tablesToDelete.Add(this.getTableName(Tables.Shipment));
            tablesToDelete.Add(this.getTableName(Tables.Stocktaking));
            tablesToDelete.Add(this.getTableName(Tables.SettlementData));
            tablesToDelete.Add(this.getTableName(Tables.Settlement));
            tablesToDelete.Add(this.getTableName(Tables.Cancellation));
            tablesToDelete.Add(this.getTableName(Tables.BookingData));
            tablesToDelete.Add(this.getTableName(Tables.Event));
            tablesToDelete.Add(this.getTableName(Tables.AnnualStatement));
            tablesToDelete.Add(this.getTableName(Tables.PrizeHistory));

            tablesToDelete.Add(this.getTableName(Tables_Archives.ShipmentData));
            tablesToDelete.Add(this.getTableName(Tables_Archives.Shipment));
            tablesToDelete.Add(this.getTableName(Tables_Archives.Stocktaking));
            tablesToDelete.Add(this.getTableName(Tables_Archives.SettlementData));
            tablesToDelete.Add(this.getTableName(Tables_Archives.Settlement));
            tablesToDelete.Add(this.getTableName(Tables_Archives.Event));
            tablesToDelete.Add(this.getTableName(Tables_Archives.Cancellation));
            tablesToDelete.Add(this.getTableName(Tables_Archives.BookingData));
            //tablesToDelete.Add(this.getTableName(Tables_Archives.PrizeHistory));

            //tablesToDelete.Add(this.getTableName(Tables.Article));
            tablesToDelete.Add(this.getTableName(Tables.NonOrgLogin));
            tablesToDelete.Add(this.getTableName(Tables.Authentification));
            tablesToDelete.Add(this.getTableName(Tables.Account));

            //string contentDeletionString = "DELETE FROM {1};"; //DROP TABLE 
            string contentDeletionString = "DROP TABLE {1};";
            using (DbConnection connection = this.Database.getOpenConnection())
            {
                foreach (string table in tablesToDelete)
                {
                    string sqlString = contentDeletionString.Replace("{1}", table);
                    DbCommand copyCom = this.Database.getCommand(sqlString, connection);
                    copyCom.ExecuteNonQuery();
                }
            }
        }
        #endregion
    }
}
