﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemServer.DAL.AutoSettlement
{
    interface IAutoSettlement
    {
        DbConnection getOpenConnection();
        List<Account> makeAutoSettlement(List<Tuple<Account, int>> accounts, Settlement settlement, DbConnection connection);
    }
}
