﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Data;

namespace BWSystemServer.DAL.AutoSettlement
{
    public class NmAutoSettlement : IAutoSettlement
    {
        private string connectionString;
        private MySqlConnection dbConnection;

        public NmAutoSettlement()
        {
            
        }


        public DbConnection getOpenConnection()
        {
            connectionString = (string)ServerUtil.getConfigValue("AutoSettlement_SqlConnectionString", typeof(string));
            dbConnection = new MySqlConnection(connectionString);
            dbConnection.Open();
            return dbConnection;
        }


        public List<Account> makeAutoSettlement(List<Tuple<Account, int>> accounts, Settlement settlement, DbConnection openConnection)
        {
            List<Account> unbookedAccounts = new List<Account>();

            string tableName = this.getTableName((MySqlConnection)openConnection);
            string usage = "Abrechnung Bierwart " + ServerUtil.DateToString(settlement.EndDate);
            string bookedBy = "Barsystem";

            int tempListID = 8800;

            int prizeSum = 0;

            foreach (Tuple<Account, int> account in accounts)
            {
                double prizeDouble = settlement.getPayment(account.Item1) * 100;
                int prizeInCent = (int)Math.Floor(prizeDouble);

                bool booked = this.enterSettlement(tableName, account.Item2, tempListID, prizeInCent, usage, bookedBy, settlement.EndDate, openConnection);

                if (booked)
                {
                    prizeSum += prizeInCent;
                }
                else
                {
                    unbookedAccounts.Add(account.Item1);
                }
            }

            int bierwartID = 5999;
            this.enterSettlement(tableName, tempListID, bierwartID, prizeSum, usage, bookedBy, settlement.EndDate, openConnection);

            openConnection.Close();
            openConnection.Dispose();
            return unbookedAccounts;
        }


        private string getTableName(MySqlConnection connection)
        {
            List<string> tables = new List<string>();

            MySqlCommand command = new MySqlCommand("SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE'", connection);

            using (MySqlDataReader dataReader = command.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    string table = (string)dataReader[0];
                    tables.Add(table);
                }
            }

            tables = tables.Where(x => x.Contains("jahr")).ToList();

            string currentTable = null;
            int currentID = -1;
            foreach (string tableName in tables)
            {
                try
                {
                    int yearID = int.Parse(tableName.Remove(0, 4));
                    if (yearID > currentID)
                    {
                        currentID = yearID;
                        currentTable = tableName;
                    }
                } catch (FormatException ex)
                {
                    SimpleLog.Log(ex);
                }
            }
            connection.Clone();
            return currentTable;
        }



        private bool enterSettlement(string tableName, int sollId, int habenId, int amountInCent, string usage, string bookedBy, DateTime timestamp, DbConnection connection)
        {
            MySqlCommand command = new MySqlCommand("INSERT INTO " + tableName + " (soll, haben, betrag, zweck, datum, geb_von) "
                + "VALUES ( @soll, @haben, @betrag, @zweck, @datum, @geb_von );", (MySqlConnection)connection);

            MySqlParameter sollParam = new MySqlParameter("@soll", MySqlDbType.Int32);
            sollParam.Value = sollId;
            command.Parameters.Add(sollParam);

            MySqlParameter habenParam = new MySqlParameter("@haben", MySqlDbType.Int32);
            habenParam.Value = habenId;
            command.Parameters.Add(habenParam);

            MySqlParameter betragParam = new MySqlParameter("@betrag", MySqlDbType.Int32);
            betragParam.Value = amountInCent;
            command.Parameters.Add(betragParam);

            MySqlParameter zweckParam = new MySqlParameter("@zweck", MySqlDbType.VarChar);
            zweckParam.Value = usage;
            command.Parameters.Add(zweckParam);

            MySqlParameter dateTimeParam = new MySqlParameter("@datum", MySqlDbType.Timestamp);
            dateTimeParam.Value = timestamp;
            command.Parameters.Add(dateTimeParam);

            MySqlParameter bookedByParam = new MySqlParameter("@geb_von", MySqlDbType.Text);
            bookedByParam.Value = bookedBy;
            command.Parameters.Add(bookedByParam);

            int linesAffected = command.ExecuteNonQuery();

            return linesAffected == 1;
        }
    }
}