﻿using BWSystemServer.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_ShipmentData : DataBaseManagement
    {

        public DBM_ShipmentData() : base()
        {
        }

        //GET Data
        public Shipment getShipmentData(Shipment shipment)
        {
            Shipment shipmentData = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM ShipmentData WHERE ShipmentID = @ParamID", connection);

                    this.Database.addParameter(command, "@ParamID", DBAdapter.DBParamType.Int, shipment.ShipmentID);

                    DbDataReader dataReader = command.ExecuteReader();
                    shipmentData = this.buildShipmentData(shipment, dataReader);

                }
            }
            return shipmentData;
        }


        private Shipment buildShipmentData(Shipment shipment, DbDataReader dataReader)
        {
            while (dataReader.Read())
            {
                int articleID = (int)dataReader["ArticleID"];
                int articleInput = (int)dataReader["ArticleInput"];
                shipment.addArticleAmount(articleID, articleInput);
            }
            return shipment;
        }


        //Add Data
        public void addShipment(int shipmentID, List<Tuple<int, int>> shipmentData)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                foreach (Tuple<int, int> tuple in shipmentData)
                {
                    this.addArticleAmount(shipmentID, tuple.Item1, tuple.Item2, connection);
                }
            }
        }

        private bool addArticleAmount(int shipmentID, int articleID, int amount, DbConnection connection)
        {
            DbCommand insertCom = this.Database.getCommand("INSERT INTO ShipmentData (ShipmentID, ArticleID, ArticleInput) " + 
                "VALUES (@ShipmentID, @ArticleID, @ArticleInput)", connection);

            this.Database.addParameter(insertCom, "@ShipmentID", DBAdapter.DBParamType.Int, shipmentID);
            this.Database.addParameter(insertCom, "@ArticleID", DBAdapter.DBParamType.Int, articleID);
            this.Database.addParameter(insertCom, "@ArticleInput", DBAdapter.DBParamType.Int, amount);

            int rows = insertCom.ExecuteNonQuery();
            return (rows == 1);
        }


        //Archive
        internal void copyDataToArchive(List<Shipment> shipments)
        {
            foreach (Shipment shipment in shipments)
            {
                this.moveDataToArchive(shipment.ShipmentID);
            }
        }

        private void moveDataToArchive(int shipmentID)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                string copyComString = "INSERT INTO {1} SELECT * FROM {2} WHERE ShipmentID = @ShipID;";
                string tableName = this.getTableName(Tables.ShipmentData);
                string archiveTableName = this.getTableName(Tables_Archives.ShipmentData);
                copyComString = copyComString.Replace(@"{1}", archiveTableName);
                copyComString = copyComString.Replace(@"{2}", tableName);
                DbCommand copyCom = this.Database.getCommand(copyComString, connection);

                this.Database.addParameter(copyCom, "@ShipID", DBAdapter.DBParamType.Int, shipmentID);

                copyCom.ExecuteNonQuery();
            }
        }

        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException("Use the other Method instead!");
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }

        internal void deleteDataFromTable(List<Shipment> shipments)
        {
            foreach (Shipment shipment in shipments)
            {
                this.deleteDataFromTable(shipment.ShipmentID);
            }
        }

        private void deleteDataFromTable(int shipmentID)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                string deleteComString = "DELETE FROM {1} WHERE ShipmentID = @ShipID;";
                string tableName = this.getTableName(Tables.ShipmentData);
                deleteComString = deleteComString.Replace(@"{1}", tableName);
                DbCommand deleteCom = this.Database.getCommand(deleteComString, connection);

                this.Database.addParameter(deleteCom, "@ShipID", DBAdapter.DBParamType.Int, shipmentID);

                deleteCom.ExecuteNonQuery();
            }
        }


        public Shipment getArchiveShipmentData(Shipment shipment)
        {
            Shipment shipmentData = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + this.getTableName(Tables_Archives.ShipmentData) + " WHERE ShipmentID = @ParamID", connection);

                    this.Database.addParameter(command, "@ParamID", DBAdapter.DBParamType.Int, shipment.ShipmentID);

                    DbDataReader dataReader = command.ExecuteReader();
                    shipmentData = this.buildShipmentData(shipment, dataReader);
                }
            }
            return shipmentData;
        }
    }
}