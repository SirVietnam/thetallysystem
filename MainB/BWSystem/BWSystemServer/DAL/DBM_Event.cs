﻿using BWSystemServer.DAL.DBAdapter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_Event : DataBaseManagement
    {

        public DBM_Event() : base()
        {
        }


        public List<Event> getEventList()
        {
            DateTime startTime = new DateTime(1999, 01, 01);
            return this.getEventList(startTime);
        }

        public List<Event> getEventList(DateTime startTime)
        {
            List<Event> events = this.getEventList(startTime, this.getTableName(Tables.Event));
            return events;
        }


        private List<Event> getEventList(DateTime startTime, string tableName)
        {
            List<Event> events = new List<Event>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + tableName + " WHERE EndDateTime BETWEEN @StartDate AND @EndDate", connection);

                    this.Database.addParameter(command, "@StartDate", DBParamType.DateTime, startTime);
                    this.Database.addParameter(command, "@EndDate", DBParamType.DateTime, new DateTime(9999, 01, 01));

                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        events.Add(this.buildEvent(dataReader));
                    }
                }
            }

            return events;
        }

        public Event getEvent(int eventID)
        {
            Event eventToID = null;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM Event WHERE EventID = @IDParam", connection);

                    this.Database.addParameter(command, "@IDParam", DBParamType.Int, eventID);

                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        eventToID = this.buildEvent(dataReader);
                    }
                }
            }
            return eventToID;
        }


        private Event buildEvent(DbDataReader dataReader)
        {
            //var test = dataReader["EventID"];
            int eventID = (int)dataReader["EventID"];
            int creatorID = (int)dataReader["CreatorID"];

            String eventName = (String)dataReader["Name"];
            eventName = eventName.TrimEnd(null);

            DateTime start = (DateTime)dataReader["StartDateTime"];
            DateTime end = (DateTime)dataReader["EndDateTime"];

            String costAbsorptionString = (String)dataReader["CostAbsorption"];
            costAbsorptionString = costAbsorptionString.TrimEnd(null);
            int[] costAbsorption = ServerUtil.eliminateSeperatorSign(costAbsorptionString);

            bool articlesExclusive = Convert.ToBoolean(dataReader["ArticlesExclusive"]);

            double? eventCosts = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("EventCosts")))
            {
                eventCosts = ServerUtil.convertToDoubleFormDB((int)dataReader["EventCosts"]);
            }

            return new Event(eventID, creatorID, eventName, start, end, costAbsorption, articlesExclusive, eventCosts);
        }


        public bool addEvent(Event newEvent)
        {
            int affectedRows = 0;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO Event (Name, CreatorID, StartDateTime, EndDateTime, CostAbsorption, ArticlesExclusive, EventCosts) "
                    + "VALUES (@EventName, @CreatorID, @StartDate, @EndDate, @CostAbsorption, @ArticlesExclusive, @EventCosts)", connection);

                this.Database.addParameter(insertCom, "@CreatorID", DBParamType.Int, newEvent.CreatorAccountID);
                this.Database.addParameter(insertCom, "@EventName", DBParamType.VarChar, 50, newEvent.Name);
                this.Database.addParameter(insertCom, "@StartDate", DBParamType.DateTime, newEvent.Begin);
                this.Database.addParameter(insertCom, "@EndDate", DBParamType.DateTime, newEvent.End);
                this.Database.addParameter(insertCom, "@CostAbsorption", DBParamType.VarChar, 100, ServerUtil.insertSeperatorSign(newEvent.CostAbsorption));
                this.Database.addParameter(insertCom, "@ArticlesExclusive", DBParamType.Bit, newEvent.ArticlesExclusive);

                if (newEvent.EventCosts != null)
                {
                    this.Database.addParameter(insertCom, "@EventCosts", DBParamType.Int, ServerUtil.convertDoubleForDB((double)newEvent.EventCosts));
                }
                else
                {
                    this.Database.addParameter(insertCom, "@EventCosts", DBParamType.Int, null);
                }

                affectedRows = insertCom.ExecuteNonQuery();
            }
            return affectedRows == 1;
        }


        public bool updateEvent(Event eventToUpdate)
        {
            int affectedRows = 0;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("UPDATE Event SET" +
                    " CreatorID = @CreatorID, Name = @EventName, StartDateTime = @StartDate, EndDateTime = @EndDate, CostAbsorption = @CostAbsorption, ArticlesExclusive = @ArticlesExclusive, EventCosts = @EventCosts "
                    + "WHERE EventID = @EventID", connection);

                this.Database.addParameter(insertCom, "@EventID", DBParamType.Int, eventToUpdate.EventID);
                this.Database.addParameter(insertCom, "@CreatorID", DBParamType.Int, eventToUpdate.CreatorAccountID);
                this.Database.addParameter(insertCom, "@EventName", DBParamType.VarChar, 50, eventToUpdate.Name);
                this.Database.addParameter(insertCom, "@StartDate", DBParamType.DateTime, eventToUpdate.Begin);
                this.Database.addParameter(insertCom, "@EndDate", DBParamType.DateTime, eventToUpdate.End);
                this.Database.addParameter(insertCom, "@CostAbsorption", DBParamType.VarChar, 100, ServerUtil.insertSeperatorSign(eventToUpdate.CostAbsorption));
                this.Database.addParameter(insertCom, "@ArticlesExclusive", DBParamType.Bit, eventToUpdate.ArticlesExclusive);

                if (eventToUpdate.EventCosts != null)
                { 
                    this.Database.addParameter(insertCom, "@EventCosts", DBParamType.Int, ServerUtil.convertDoubleForDB((double)eventToUpdate.EventCosts));
                }
                else
                {
                    this.Database.addParameter(insertCom, "@EventCosts", DBParamType.Int, null);
                }

                affectedRows = insertCom.ExecuteNonQuery();
            }

            return affectedRows == 1;
        }


        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                //SET IDENTITY_INSERT {1} ON;;
                string copyComString = "INSERT INTO {1} SELECT * FROM {2} WHERE StartDateTime >= @StartDate AND EndDateTime <= @EndDate;";
                string tableName = this.getTableName(Tables.Event);
                string archiveTableName = this.getTableName(Tables_Archives.Event);
                copyComString = copyComString.Replace(@"{1}", archiveTableName);
                copyComString = copyComString.Replace(@"{2}", tableName);
                DbCommand copyCom = this.Database.getCommand(copyComString, connection);

                this.Database.addParameter(copyCom, "@StartDate", DBParamType.DateTime, startDateTime);
                //SqlParameter startDateParam = new SqlParameter("@StartDate", SqlDbType.DateTime);
                //startDateParam.Value = startDateTime;
                //copyCom.Parameters.Add(startDateParam);

                this.Database.addParameter(copyCom, "@EndDate", DBParamType.DateTime, endDateTime);
                //SqlParameter endDateParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
                //endDateParam.Value = endDateTime;
                //copyCom.Parameters.Add(endDateParam);

                int copiedRows = copyCom.ExecuteNonQuery();
            }
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                //SET IDENTITY_INSERT {1} ON;;
                string deleteComString = "DELETE FROM {1} WHERE StartDateTime >= @StartDate AND EndDateTime <= @EndDate;";
                string tableName = this.getTableName(Tables.Event);
                string archiveTableName = this.getTableName(Tables_Archives.Event);
                deleteComString = deleteComString.Replace(@"{1}", tableName);
                DbCommand copyCom = this.Database.getCommand(deleteComString, connection);

                this.Database.addParameter(copyCom, "@StartDate", DBParamType.DateTime, startDateTime);
                //SqlParameter startDateParam = new SqlParameter("@StartDate", SqlDbType.DateTime);
                //startDateParam.Value = startDateTime;
                //copyCom.Parameters.Add(startDateParam);

                this.Database.addParameter(copyCom, "@EndDate", DBParamType.DateTime, endDateTime);
                //SqlParameter endDateParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
                //endDateParam.Value = endDateTime;
                //copyCom.Parameters.Add(endDateParam);

                int copiedRows = copyCom.ExecuteNonQuery();
            }
        }


        internal List<Event> getArchiveEventList()
        {
            return this.getEventList(new DateTime(1970, 01, 01), this.getTableName(Tables_Archives.Event));
        }
    }
}