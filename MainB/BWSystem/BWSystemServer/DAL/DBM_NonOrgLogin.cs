﻿using BWSystemServer.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_NonOrgLogin : DataBaseManagement
    {

        public DBM_NonOrgLogin()
            : base()
        {
        }



        /*
         * Get
         */
        internal NonOrgAuthentication getNonOrgLogin(int userID)
        {
            NonOrgAuthentication nonOrgAuth = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {

                    DbCommand command = this.Database.getCommand("SELECT * FROM NonOrgLogin WHERE AccountID = @SpecAccountID", connection);

                    this.Database.addParameter(command, "@SpecAccountID", DBAdapter.DBParamType.Int, userID);

                    DbDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        nonOrgAuth = buildNonOrgAuthentication(dataReader);
                    }
                }
            }

            return nonOrgAuth;
        }


        private NonOrgAuthentication buildNonOrgAuthentication(DbDataReader dataReader)
        {
            int accountID = (int)dataReader["AccountID"];
            byte[] passwordHash = (byte[])dataReader["PasswordHash"];
            byte[] passwordSalt = (byte[])dataReader["PasswordSalt"];
            string mailAddress = (string)dataReader["MailAddress"];

            return new NonOrgAuthentication(accountID, passwordSalt, passwordHash, mailAddress);
        }


        internal bool isEntryAvailable(int accountID)
        {
            int id = -1;

            using(DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {

                    DbCommand command = this.Database.getCommand("SELECT * FROM NonOrgLogin WHERE AccountID = @SpecAccountID", connection);

                    this.Database.addParameter(command, "@SpecAccountID", DBAdapter.DBParamType.Int, accountID);

                    Decimal accIdDecimal = Convert.ToDecimal(command.ExecuteScalar());
                    id = (int)accIdDecimal;
                }
            }

            return id == accountID && id != 0;
        }


        /*
         * Add
         */
        public Boolean addNonOrgLogin(NonOrgAuthentication newNonOrgAuth)
        {
            int rows = 0;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO NonOrgLogin (AccountID, PasswordSalt, PasswordHash, MailAddress) "
                    + "VALUES (@AccountID, @PWSalt, @PWHash, @MailAddress)", connection);

                this.Database.addParameter(insertCom, "@AccountID", DBAdapter.DBParamType.Int, newNonOrgAuth.AccountID);
                this.Database.addParameter(insertCom, "@PWSalt", DBAdapter.DBParamType.VarBinary, 32, newNonOrgAuth.PasswordSalt);
                this.Database.addParameter(insertCom, "@PWHash", DBAdapter.DBParamType.VarBinary, 32, newNonOrgAuth.PasswordHash);
                this.Database.addParameter(insertCom, "@MailAddress", DBAdapter.DBParamType.VarChar, 100, newNonOrgAuth.MailAddress);

                rows = insertCom.ExecuteNonQuery();
            }

            return rows == 1;
        }


        private bool updateNonOrgLogin(NonOrgAuthentication nonOrgAuth)
        {
            int rows = 0;
            bool authUpdated = true;

            string sqlComString = "UPDATE NonOrgLogin SET " +
            "PasswordSalt = @PasswordSalt, PasswordHash = @PasswordHash, MailAddress = @MailAddress " +
            "WHERE AccountID = @AccountID ;";

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand(sqlComString, connection);

                this.Database.addParameter(insertCom, "@AccountID", DBAdapter.DBParamType.Int, nonOrgAuth.AccountID);
                this.Database.addParameter(insertCom, "@PasswordSalt", DBAdapter.DBParamType.VarChar, 64, nonOrgAuth.PasswordSalt);
                this.Database.addParameter(insertCom, "@PasswordHash", DBAdapter.DBParamType.VarChar, 64, nonOrgAuth.PasswordHash);
                this.Database.addParameter(insertCom, "@MailAddress", DBAdapter.DBParamType.VarChar, 100, nonOrgAuth.MailAddress);

                rows = insertCom.ExecuteNonQuery();
            }

            authUpdated = rows == 1;
            return authUpdated;
        }



        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }
    }
}