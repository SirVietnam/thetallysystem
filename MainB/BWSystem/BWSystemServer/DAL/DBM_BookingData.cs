﻿using BWSystemServer.Classes;
using BWSystemServer.DAL.DBAdapter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_BookingData : DataBaseManagement
    {

        public DBM_BookingData()
            : base()
        { }


        //GET Data
        public List<BookingData> getPostingDataList(bool grouped)
        {
            return this.getPostingDataList(new DateTime(1971, 01, 01), new DateTime(DateTime.Now.Ticks), grouped);
        }

        public List<BookingData> getPostingDataList(DateTime startTime, bool grouped)
        {
            return this.getPostingDataList(startTime, new DateTime(DateTime.Now.Ticks), grouped);
        }

        public List<BookingData> getPostingDataList(DateTime startTime, DateTime endTime, bool grouped)
        {
            List<BookingData> postingDatas = this.getPostingDataList(startTime, endTime, grouped, this.getTableName(Tables.BookingData));
            return postingDatas;
        }


        public List<BookingData> getPostingDataList(int eventID)
        {
            return this.getPostingDataList(eventID, this.getTableName(Tables.BookingData));
        }


        public List<BookingData> getArticePostingDataList(int eventID)
        {
            return this.getPostingDataList(eventID, this.getTableName(Tables_Archives.BookingData));
        }


        private List<BookingData> getPostingDataList(int eventID, string tableName)
        {
            List<BookingData> postingDatas = new List<BookingData>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + tableName + " WHERE EventID = @EventID;", connection);
                    this.Database.addParameter(command, "@EventID", DBParamType.Int, eventID);

                    DbDataReader dataReader = command.ExecuteReader();
                    postingDatas = this.getGroupedPostingDatas(dataReader, postingDatas);
                }
            }
            return postingDatas;
        }


        public List<BookingData> getPostingDataList(DateTime startTime, DateTime endTime, bool grouped, string tableName)
        {
            List<BookingData> postingDatas = new List<BookingData>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + tableName + " WHERE TimeStamp BETWEEN  @StartDate AND @EndDate;", connection);
                    this.Database.addParameter(command, "@StartDate", DBParamType.DateTime, startTime);
                    this.Database.addParameter(command, "@EndDate", DBParamType.DateTime, endTime);

                    DbDataReader dataReader = command.ExecuteReader();

                    if (grouped)
                    {
                        postingDatas = this.getGroupedPostingDatas(dataReader, postingDatas);
                    }
                    else
                    {
                        postingDatas = this.getUnGroupedPostingDatas(dataReader, postingDatas);
                    }
                }
            }
            return postingDatas;
        }

        private List<BookingData> getGroupedPostingDatas(DbDataReader dataReader, List<BookingData> postingDatas)
        {
            BookingData lastData = null;
            while (dataReader.Read())
            {
                BookingData nextPostingData = this.buildPostingData(dataReader, true);
                if (lastData != null &&
                    lastData.ID == nextPostingData.ID &&
                    lastData.Customer == nextPostingData.Customer)
                //&& lastData.TimeStamp.Equals(nextPostingData.TimeStamp)
                {
                    List<BookingAA> datas = nextPostingData.Booking;
                    lastData.Booking.AddRange(datas);
                    lastData.DataCancelled = (lastData.DataCancelled || nextPostingData.DataCancelled);
                }
                else
                {
                    postingDatas.Add(nextPostingData);
                    lastData = nextPostingData;
                }
            }
            return postingDatas;
        }


        private List<BookingData> getUnGroupedPostingDatas(DbDataReader dataReader, List<BookingData> postingDatas)
        {
            while (dataReader.Read())
            {
                postingDatas.Add(this.buildPostingData(dataReader, false));
            }
            return postingDatas;
        }

        //TODO: verify this method!
        public List<BookingData> getPostingDatasToBookingID(int id, bool grouped)
        {
            List<BookingData> postingDatas = new List<BookingData>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM BookingData WHERE BookingID = @PrimID OR BookingSubID = @SubID", connection);

                    this.Database.addParameter(command, "@PrimID", DBParamType.Int, id);
                    this.Database.addParameter(command, "@SubID", DBParamType.Int, id);

                    DbDataReader dataReader = command.ExecuteReader();
                    if (grouped)
                    {
                        postingDatas = this.getGroupedPostingDatas(dataReader, postingDatas);
                    }
                    else
                    {
                        postingDatas = this.getUnGroupedPostingDatas(dataReader, postingDatas);
                    }
                }

            }
            return postingDatas;
        }


        //TODO: verify this method!
        private BookingData buildPostingData(DbDataReader dataReader, bool grouped)
        {
            int dataID = (int)dataReader["BookingID"];

            int? dataSubID = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("BookingSubID")))
            {
                dataSubID = (int)dataReader["BookingSubID"];
            }

            int? userID = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("UserID")))
            {
                userID = (int)dataReader["UserID"];
            }
            int customerID = (int)dataReader["CustomerID"];

            int? eventID = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("EventID")))
            {
                eventID = (int)dataReader["EventID"];
            }
            DateTime timeStamp = (DateTime)dataReader["TimeStamp"];

            int articleID = (int)dataReader["ArticleID"];
            int amount = (int)dataReader["Amount"];

            Boolean cancelled = Convert.ToBoolean(dataReader["Cancellation"]);

            List<BookingAA> bookings = new List<BookingAA>();
            bookings.Add(new BookingAA(articleID, amount));

            BookingData data;
            if (grouped && dataSubID != null)
            {
                data = new BookingData(dataSubID, userID, customerID, bookings, timeStamp, eventID, cancelled);
            }
            else
            {
                data = new BookingData(dataID, userID, customerID, bookings, timeStamp, eventID, cancelled);
            }

            return data;
        }



        //ADD Data
        //TODO: verify this method!
        public int addPostingData(BookingData booking)
        {
            int bookingID = -1;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                int rows = 0;
                int? subBookingID = null;
                foreach (BookingAA tuple in booking.Booking)
                {
                    DbCommand insertCom = this.Database.getCommand("INSERT INTO BookingData (BookingSubID, UserID, CustomerID, ArticleID, Amount, EventID, TimeStamp, Cancellation)" +
                    " VALUES (@BookingSubID, @User, @Customer, @Article, @Amount, @Event, @TimeStamp, @Cancellation);" +
                    this.Database.getLastAutoValueCommand(), connection);

                    this.Database.addParameter(insertCom, "@BookingSubID", DBParamType.Int, subBookingID);
                    this.Database.addParameter(insertCom, "@User", DBParamType.Int, booking.User);
                    this.Database.addParameter(insertCom, "@Customer", DBParamType.Int, booking.Customer);
                    this.Database.addParameter(insertCom, "@Event", DBParamType.Int, booking.EventBooking);
                    this.Database.addParameter(insertCom, "@TimeStamp", DBParamType.DateTime, booking.TimeStamp);
                    this.Database.addParameter(insertCom, "@Cancellation", DBParamType.Bit, false);

                    this.Database.addParameter(insertCom, "@Article", DBParamType.Int, tuple.Article);
                    this.Database.addParameter(insertCom, "@Amount", DBParamType.Int, tuple.Amount);

                    if (subBookingID == null)
                    {
                        decimal obj = Convert.ToDecimal(insertCom.ExecuteScalar());
                        bookingID = (int)obj;
                        subBookingID = bookingID;
                    }
                    else
                    {
                        insertCom.ExecuteNonQuery();
                    }
                    rows++;
                }

                if (rows != booking.Booking.Count())
                {
                    bookingID = -1;
                }

            }
            return bookingID;
        }


        //TODO Test this method!
        internal bool enterCancellation(int bookingID, int bookingSize)
        {
            int rows = 0;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand changeCom = this.Database.getCommand("UPDATE BookingData SET Cancellation = @CanellationValue WHERE BookingID = @BookingID OR BookingSubID = @BookingSubID", connection);

                this.Database.addParameter(changeCom, "@BookingID", DBParamType.Int, bookingID);
                this.Database.addParameter(changeCom, "@BookingSubID", DBParamType.Int, bookingID);
                this.Database.addParameter(changeCom, "@CanellationValue", DBParamType.Bit, true);

                rows = changeCom.ExecuteNonQuery();

            }
            return (rows == bookingSize);
        }


        //internal bool changeBooking(int bookingID, int amount, int cancelID)
        //{
        //    int rows = 0;
        //    using (DbConnection connection = base.Database.getOpenConnection())
        //    {
        //        DbCommand changeCom = this.Database.getCommand("UPDATE BookingData SET Amount = Amount - @CanceledVal WHERE BookingID = @BookingID AND CancellationID = @CancelIDParam", connection);

        //        SqlParameter cancelIdParam = new SqlParameter("@CancelIDParam", SqlDbType.Int);
        //        cancelIdParam.Value = cancelID;
        //        changeCom.Parameters.Add(cancelIdParam);

        //        SqlParameter bookingIdParam = new SqlParameter("@BookingID", SqlDbType.Int);
        //        bookingIdParam.Value = bookingID;
        //        changeCom.Parameters.Add(bookingIdParam);

        //        SqlParameter articleIdParam = new SqlParameter("@CanceledVal", SqlDbType.Int);
        //        articleIdParam.Value = amount;
        //        changeCom.Parameters.Add(articleIdParam);

        //        rows = changeCom.ExecuteNonQuery();

        //    }
        //    return (rows == 1);
        //}



        //Archive Data
        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            base.copyDataToArchive(startDateTime, endDateTime, Tables.BookingData, Tables_Archives.BookingData);
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            base.deleteDataFromTable(startDateTime, endDateTime, Tables.BookingData);
        }


        internal List<BookingData> getArchivePostingDataList(DateTime startDateTime, DateTime endDateTime)
        {
            List<BookingData> postingDatas = this.getPostingDataList(startDateTime, endDateTime, true, this.getTableName(Tables_Archives.BookingData));
            return postingDatas;
        }
    }
}