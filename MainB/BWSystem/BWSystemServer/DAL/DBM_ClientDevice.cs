﻿using BWSystemServer.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_ClientDevice : DataBaseManagement
    {

        public DBM_ClientDevice()
            : base()
        {
        }


        public List<ClientDevice> getClietDeviceList()
        {
            List<ClientDevice> devices = new List<ClientDevice>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + this.getTableName(Tables.ClientDevice), connection);
                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        ClientDevice nextDevice = this.buildClientDevice(dataReader);
                        devices.Add(nextDevice);
                    }
                }
            }

            return devices;
        }


        private ClientDevice buildClientDevice(DbDataReader dataReader)
        {
            int id = (int)dataReader["ClientID"];
            string ipAddress = (string)dataReader["IpAddress"];
            byte[] salt = (byte[])dataReader["Salt"];
            byte[] hash = (byte[])dataReader["Hash"];
            return new ClientDevice(id, ipAddress, hash, salt);
        }


        internal bool addClientDevice(ClientDevice device)
        {
            SimpleLog.Info("Request Adding-Devide to DB");
            int deviceID = -1;

            using (DbConnection connection = base.Database.getOpenConnection())
            {

                DbCommand insertCom = this.Database.getCommand("INSERT INTO " + this.getTableName(Tables.ClientDevice)
                    + " (IpAddress, Salt, Hash) VALUES (@IpAddress, @Salt, @Hash);"
                    + this.Database.getLastAutoValueCommand(), connection);

                this.Database.addParameter(insertCom, "@IpAddress", DBAdapter.DBParamType.VarChar, 50, device.IpAddress);
                this.Database.addParameter(insertCom, "@Salt", DBAdapter.DBParamType.VarBinary, 32, device.Salt);
                this.Database.addParameter(insertCom, "@Hash", DBAdapter.DBParamType.VarBinary, 32, device.HashedSaltedPW);
                Decimal accIdDecimal = Convert.ToDecimal(insertCom.ExecuteScalar());
                deviceID = (int)accIdDecimal;
                device.DeviceID = deviceID;
                SimpleLog.Log("Device with ID: " + deviceID + " was added!");
            }

            return deviceID != -1;
        }


        internal bool removeDevice(int deviceID)
        {
            SimpleLog.Info("Requesting Device-Delete from DB");
            int rows = 0;

            using (DbConnection connection = base.Database.getOpenConnection())
            {

                DbCommand insertCom = this.Database.getCommand("DELETE FROM " + this.getTableName(Tables.ClientDevice)
                    + " WHERE (ClientID = @ID);", connection);
                this.Database.addParameter(insertCom, "@ID", DBAdapter.DBParamType.Int, deviceID);
                rows = insertCom.ExecuteNonQuery();
            }

            bool deleted = (rows == 1);
            SimpleLog.Info("Device was delted: " + deleted);
            return deleted;
        }


        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }
    }
}