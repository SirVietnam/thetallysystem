﻿using BWSystemServer.BIZ;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_Settlement : DataBaseManagement
    {

        public DBM_Settlement()
            : base()
        {
        }


        public List<Settlement> getSettlementList()
        {
            List<Settlement> settlements = this.getSettlementList(this.getTableName(Tables.Settlement));

            return settlements;
        }

        #region Settlement
        private List<Settlement> getSettlementList(string tableName)
        {
            List<Settlement> settlements = new List<Settlement>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + tableName + " ORDER BY(EndDateTime) ASC", connection);


                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        int id = (int)dataReader["SettlementID"];
                        DateTime startDate = (DateTime)dataReader["StartDateTime"];
                        DateTime endStamp = (DateTime)dataReader["EndDateTime"];
                        Settlement settlement = new Settlement(id, startDate, endStamp);
                        settlements.Add(settlement);
                        startDate = endStamp;
                    }
                }
            }

            return settlements;
        }



        public int addSettlement(DateTime startDateTime, DateTime endDateTime)
        {
            int settlementID = -1;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO Settlement (StartDateTime, EndDateTime) VALUES (@StartDateTime, @EndDateTime); " + this.Database.getLastAutoValueCommand(), connection);

                this.Database.addParameter(insertCom, "@StartDateTime", DBAdapter.DBParamType.DateTime, startDateTime);
                this.Database.addParameter(insertCom, "@EndDateTime", DBAdapter.DBParamType.DateTime, endDateTime);

                Decimal settlementIdDecimal = Convert.ToDecimal(insertCom.ExecuteScalar());
                settlementID = (int)settlementIdDecimal;
            }

            return settlementID;
        }
        #endregion


        #region SettlementData
        public void addSettlementData(Settlement settlementData, List<Account> accounts)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                foreach (Account account in accounts)
                {
                    int paymentInt = ServerUtil.convertDoubleForDB(settlementData.getPayment(account));

                    if (paymentInt > 0)
                    {
                        DbCommand insertCom = this.Database.getCommand("INSERT INTO SettlementData (SettlementID, AccountID, Payment) "
                            + "VALUES (@SettlementID, @AccountID, @Payment);", connection);

                        this.Database.addParameter(insertCom, "@SettlementID", DBAdapter.DBParamType.Int, settlementData.SettlementID);
                        this.Database.addParameter(insertCom, "@AccountID", DBAdapter.DBParamType.Int, account.ID);
                        this.Database.addParameter(insertCom, "@Payment", DBAdapter.DBParamType.Int, paymentInt);

                        int rows = insertCom.ExecuteNonQuery();
                        if (rows != 1)
                        {
                            throw new DataException("Something went wrong at adding data ...");
                        }
                    }
                }
            }
        }



        public double getPaymentData(int settlementID, int accountID)
        {
            double paymentDouble = this.getPaymentData(settlementID, accountID, this.getTableName(Tables.SettlementData));
            return paymentDouble;
        }

        private double getPaymentData(int settlementID, int accountID, string tableName)
        {
            double paymentDouble = 0.0;
            int paymentInt = -1;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT Payment FROM " + tableName + " WHERE (SettlementID = @SetID AND AccountID = @AccID);", connection);

                    this.Database.addParameter(command, "@SetID", DBAdapter.DBParamType.Int, settlementID);
                    this.Database.addParameter(command, "@AccID", DBAdapter.DBParamType.Int, accountID);

                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        paymentInt = (int)dataReader["Payment"];
                    }
                }
            }

            if (paymentInt >= 0)
            {
                paymentDouble = Convert.ToDouble(paymentInt) / 100;
            }

            return paymentDouble;
        }
        #endregion

        //Archive Data
        #region SettlementArchive
        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            List<Settlement> settlments = this.getSettlementList();
            List<Settlement> sublist = settlments.Where(x => x.StartDate.Ticks >= startDateTime.Ticks && x.EndDate.Ticks <= endDateTime.Ticks).ToList();
            foreach (Settlement settlement in sublist)
            {
                this.copyDataToArchive(settlement.SettlementID, Tables.SettlementData, Tables_Archives.SettlementData);
            }
            foreach (Settlement settlement in sublist)
            {
                this.copyDataToArchive(settlement.SettlementID, Tables.Settlement, Tables_Archives.Settlement);
            }
        }

        private void copyDataToArchive(int settlementID, Tables sourceTable, Tables_Archives targetTable)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                string copyComString = "INSERT INTO {1} SELECT * FROM {2} WHERE SettlementID = @SettID;";
                string tableName = this.getTableName(sourceTable);
                string archiveTableName = this.getTableName(targetTable);
                copyComString = copyComString.Replace(@"{1}", archiveTableName);
                copyComString = copyComString.Replace(@"{2}", tableName);
                DbCommand copyCom = this.Database.getCommand(copyComString, connection);

                this.Database.addParameter(copyCom, "@SettID", DBAdapter.DBParamType.Int, settlementID);

                copyCom.ExecuteNonQuery();
            }
        }


        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            List<Settlement> settlments = this.getSettlementList();
            List<Settlement> sublist = settlments.Where(x => x.StartDate.Ticks >= startDateTime.Ticks && x.EndDate.Ticks <= endDateTime.Ticks).ToList();
            foreach (Settlement settlement in sublist)
            {
                this.deleteDataFromTable(settlement.SettlementID, Tables.SettlementData);
            }
            foreach (Settlement settlement in sublist)
            {
                this.deleteDataFromTable(settlement.SettlementID, Tables.Settlement);
            }
        }


        private void deleteDataFromTable(int settlementID, Tables sourceTable)
        {
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                string deleteComString = "DELETE FROM {1} WHERE SettlementID = @SettID;";
                string tableName = this.getTableName(sourceTable);
                deleteComString = deleteComString.Replace(@"{1}", tableName);
                DbCommand deleteCom = this.Database.getCommand(deleteComString, connection);

                this.Database.addParameter(deleteCom, "@SettID", DBAdapter.DBParamType.Int, settlementID);

                int deletedRows = deleteCom.ExecuteNonQuery();
            }
        }


        internal List<Settlement> getArchiveSettlementList()
        {
            return this.getSettlementList(this.getTableName(Tables_Archives.Settlement));
        }

        public double getArchivePaymentData(int settlementID, int accountID)
        {
            double paymentDouble = this.getPaymentData(settlementID, accountID, this.getTableName(Tables_Archives.SettlementData));
            return paymentDouble;
        }
        #endregion
    }
}