﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace BWSystemServer.DAL
{

    public struct LDAP_Data
    {
        public bool filledWithData;
        public string loginName;
        public string preName;
        public string surName;
        public string nickName;
        public Int64[] cardNumbers;
        public bool isAdmin;
        public bool isActive;
        public System.Drawing.Image picture;
    }


    public class LDAPAccess
    {
        private static string loginName = "sAMAccountName";
        private static string nickName = "description";
        private static string preName = "givenName";
        private static string surName = "sn";
        private static string cardNumbers = "info";
        private static string completeName = "displayName";
        private static string pictureName = "thumbnailphoto";
        private static string memberOfName = "memberOf";
        private static string deactivatedName = "userAccountControl";

        private static string picture = "thumbnailphoto";

        private LinkedList<string> requiredInfos;

        private string technicalUser = "tu007";
        private string technicalPassword = "CQXC4kG2SNh34zh";
        private string server = "normannia.stud.uni-karlsruhe.de";
        private string domain = "DC=normannia,DC=stud,DC=uni-karlsruhe,DC=de"; //OU=Normannen,

        public LDAPAccess()
        {
            this.requiredInfos = new LinkedList<string>();
            this.requiredInfos.AddLast(loginName);
            this.requiredInfos.AddLast(nickName);
            this.requiredInfos.AddLast(preName);
            this.requiredInfos.AddLast(surName);
            this.requiredInfos.AddLast(cardNumbers);
            this.requiredInfos.AddLast(completeName);
            this.requiredInfos.AddLast(picture);
            this.requiredInfos.AddLast(memberOfName);
            this.requiredInfos.AddLast(deactivatedName);
        }

        /**
         * Creat LDAP-Directory Entries
         */
        private DirectoryEntry createDirectoryEntry()
        {
            return this.createDirectoryEntry(this.technicalUser, this.technicalPassword);
        }

        private DirectoryEntry createDirectoryEntry(string user, string password)
        {
            // create and return new LDAP connection with desired settings  
            DirectoryEntry ldapConnection = new DirectoryEntry();
            ldapConnection.Username = user;
            ldapConnection.Password = password;
            ldapConnection.Path = "LDAP://" + this.server + "/" + this.domain;
            ldapConnection.AuthenticationType = AuthenticationTypes.Secure;

            return ldapConnection;
        }


        internal List<String> getLoginOfGroup(string LDAP_Group)
        {
            List<string> logins = new List<string>();

            DirectoryEntry ldapConnection = this.createDirectoryEntry();
            ldapConnection.Path = "LDAP://" + this.server + "/" + LDAP_Group;

            foreach (string dn in ldapConnection.Properties["member"]) //
            {
                DirectoryEntry userLdapConnection = this.createDirectoryEntry();
                userLdapConnection.Path = "LDAP://" + this.server + "/" + dn;

                string phoneNumber = (string)userLdapConnection.Properties["telephoneNumber"].Value;
                if (phoneNumber != null && phoneNumber != "")
                {
                    try
                    {
                        string userLogin = (string)userLdapConnection.Properties[loginName][0];
                        logins.Add(userLogin);
                    } catch (ArgumentOutOfRangeException)
                    {
                        SimpleLog.Log("User: '" + dn + " has no " + loginName + ". He can't be added automatically.");
                    } //[loginName]
                }
            }
            return logins;
        }

        internal bool isValidPassword(string userLogin, string userPassword)
        {
            bool validPassword = false;

            DirectoryEntry myLdapConnection = createDirectoryEntry(userLogin, userPassword);
            DirectorySearcher search = new DirectorySearcher(myLdapConnection);
            search.PropertiesToLoad.Add(LDAPAccess.loginName);
            search.Filter = "(" + LDAPAccess.loginName + "=" + userLogin + ")";

            try
            {
                search.FindOne();
                validPassword = true;
            } catch (DirectoryServicesCOMException)
            {
                SimpleLog.Warning("User: " + userLogin + " tried to login with the wrong password!");
            } catch (System.Runtime.InteropServices.COMException ex)
            {
                SimpleLog.Log(ex);
            }

            myLdapConnection.Close();

            return validPassword;
        }


        /**
         * Get Methods
         */
        public LDAP_Data getLDAP_Data(string userLogin)
        {
            DirectoryEntry myLdapConnection = createDirectoryEntry(this.technicalUser, this.technicalPassword);

            SearchResult result = this.getSearchResult(userLogin, myLdapConnection);

            LDAP_Data data = new LDAP_Data();
            data.filledWithData = false;
            if (result != null)
            {
                data = this.buildLDAP_Data(result);
            }

            myLdapConnection.Close();
            return data;
        }

        public LDAP_Data getLDAP_Data(string user, string password)
        {
            DirectoryEntry myLdapConnection = createDirectoryEntry(user, password);

            SearchResult result = this.getSearchResult(user, myLdapConnection);

            LDAP_Data data = new LDAP_Data();
            data.filledWithData = false;
            if (result != null)
            {
                data = this.buildLDAP_Data(result);
            }

            myLdapConnection.Close();
            return data;
        }


        private SearchResult getSearchResult(string userName, DirectoryEntry myLdapConnection)
        {
            DirectorySearcher search = new DirectorySearcher(myLdapConnection);

            foreach (string property in this.requiredInfos)
            {
                search.PropertiesToLoad.Add(property);
            }

            search.Filter = "(" + LDAPAccess.loginName + "=" + userName + ")";

            SearchResult result = null;
            try
            {
                result = search.FindOne();
            } catch (DirectoryServicesCOMException)
            {
                SimpleLog.Warning("User: " + userName + " tried to login with the wrong password!");
            } catch (ArgumentException ex)
            {
                SimpleLog.Log("Seams like user: " + userName + " does not exist!");
                SimpleLog.Log(ex);
            }
            return result;
        }

        public LDAP_Data[] getLDAP_Data(string[] loginDatas)
        {
            LinkedList<LDAP_Data> datas = new LinkedList<LDAP_Data>();

            DirectoryEntry myLdapConnection = createDirectoryEntry();
            try
            {
                DirectorySearcher search = new DirectorySearcher(myLdapConnection);

                foreach (string property in this.requiredInfos)
                {
                    search.PropertiesToLoad.Add(property);
                }

                foreach (string loginData in loginDatas)
                {
                    search.Filter = "(" + loginName + "=" + loginData + ")";

                    SearchResult result = search.FindOne();

                    LDAP_Data data = this.buildLDAP_Data(result);
                    if (data.surName != null && data.preName != null)
                    {
                        datas.AddLast(data);
                    }
                }
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
            } finally
            {
                myLdapConnection.Close();
            }

            return datas.ToArray<LDAP_Data>();
        }



        public LDAP_Data[] getAllUserLDAP_Data()
        {
            LinkedList<LDAP_Data> datas = new LinkedList<LDAP_Data>();

            try
            {
                DirectoryEntry myLdapConnection = createDirectoryEntry();

                DirectorySearcher search = new DirectorySearcher(myLdapConnection);

                foreach (string property in this.requiredInfos)
                {
                    search.PropertiesToLoad.Add(property);
                }

                SearchResultCollection allUsers = search.FindAll();

                foreach (SearchResult result in allUsers)
                {

                    LDAP_Data data = this.buildLDAP_Data(result);
                    if (data.surName != null && data.preName != null)
                    {
                        datas.AddLast(data);
                    }
                }
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
            }

            return datas.ToArray<LDAP_Data>();
        }


        internal string getUserProperty(string loginName, string property)
        {
            string propertyValue = null;

            DirectoryEntry myLdapConnection = createDirectoryEntry();
            try
            {
                DirectorySearcher search = new DirectorySearcher(myLdapConnection);
                search.PropertiesToLoad.Add(property);
                search.Filter = "(" + LDAPAccess.loginName + "=" + loginName + ")";
                SearchResult result = search.FindOne();
                propertyValue = (string)result.Properties[property][0];
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
            } finally
            {
                myLdapConnection.Close();
            }

            return propertyValue;
        }


        /**
         * Helper Methods
         */
        private LDAP_Data buildLDAP_Data(SearchResult result)
        {
            LDAP_Data data = new LDAP_Data();
            if (result == null)
            {
                data.filledWithData = false;
            }
            else if (result.Properties[surName].Count > 0 && result.Properties[preName].Count > 0)
            {
                data.filledWithData = true;
                data.loginName = result.Properties[loginName].Count > 0 ? result.Properties[loginName][0].ToString() : null;
                data.surName = result.Properties[surName].Count > 0 ? result.Properties[surName][0].ToString() : null;
                data.preName = result.Properties[preName].Count > 0 ? result.Properties[preName][0].ToString() : null;
                data.nickName = result.Properties[nickName].Count > 0 ? result.Properties[nickName][0].ToString() : null;

                if (result.Properties[cardNumbers].Count > 0)
                {
                    data.cardNumbers = this.stringToCardNumber(result.Properties[cardNumbers][0].ToString());
                }
                else
                {
                    data.cardNumbers = null;
                }

                byte[] pictureData = result.Properties[picture].Count > 0 ? (byte[])result.Properties[picture][0] : null;
                if (pictureData != null)
                {
                    data.picture = Image.FromStream(new MemoryStream(pictureData));
                }
                data.isActive = this.checkIsActive(result);
                data.isAdmin = this.checkIsAdmin(result);
            }
            return data;
        }


        private bool checkIsActive(SearchResult result)
        {
            int flags = (int)result.Properties[deactivatedName][0];
            bool isDeactivated = Convert.ToBoolean(flags & 0x0002);
            return !isDeactivated;
        }

        private bool checkIsAdmin(SearchResult result)
        {
            bool isAdmin = false;
            string adminGroup = (string)ServerUtil.getConfigValue("LDAP_AdminGroup", typeof(string));
            if (result.Properties[memberOfName].Count > 0)
            {
                foreach (string group in result.Properties[memberOfName])
                {
                    isAdmin = group.Contains(adminGroup);
                    if (isAdmin)
                        break;
                }
            }
            return isAdmin;
        }

        private Int64[] stringToCardNumber(string cardNumbers)
        {
            LinkedList<Int64> cardList = new LinkedList<Int64>();
            if (cardNumbers.Contains(';'))
            {
                string[] cardNumbersSplit = cardNumbers.Split(';');
                foreach (string cardNumberUnclean in cardNumbersSplit)
                {
                    string[] cardNumberClean = cardNumberUnclean.Split('=');
                    try
                    {
                        cardList.AddLast(Convert.ToInt64(cardNumberClean[0]));
                    } catch (OverflowException e)
                    {
                        Console.Error.WriteLine("Exception trying to parse number: " + cardNumberClean[0] + "\n" + e.Message.ToString());
                    } catch (FormatException e)
                    {
                        Console.Error.WriteLine("Exception trying to parse number: " + cardNumberClean[0] + "\n" + e.Message.ToString());
                    }
                }
            }
            else
            {
                string[] cardNumberClean = cardNumbers.Split('=');
                cardList.AddLast(Convert.ToInt64(cardNumberClean[0]));
            }
            return cardList.ToArray<Int64>();
        }

        private string cardNumberToString(string preNumbers, Int64 cardNumber, string cardNumberName)
        {
            StringBuilder sb = new StringBuilder(preNumbers);

            sb.Append(";");
            sb.Append(cardNumberName);
            sb.Append("=");
            sb.Append(cardNumber);

            return sb.ToString();
        }




        /*
         * Get Methods
         */
        internal Byte[] getAccountPicture(string loginName)
        {
            DirectoryEntry myLdapConnection = createDirectoryEntry();
            DirectorySearcher search = new DirectorySearcher(myLdapConnection);

            search.PropertiesToLoad.Add(LDAPAccess.pictureName);

            search.Filter = "(" + LDAPAccess.loginName + "=" + loginName + ")";

            SearchResult result = search.FindOne();

            Byte[] pictureData = null;
            if (result != null)
            {
                try
                {
                    pictureData = (byte[])result.Properties[pictureName][0];
                } catch (ArgumentOutOfRangeException)
                {
                    SimpleLog.Log("User " + loginName + " has no picture in LDAP!");
                }
            }

            return pictureData;
        }




        /*
         * Add Methods
         */
        public bool addPictureToLDAP(string userName, byte[] pictureData)
        {
            DirectoryEntry myLdapConnection = createDirectoryEntry();

            SearchResult result = this.getSearchResult(userName, myLdapConnection);

            DirectoryEntry entryToUpdate = result.GetDirectoryEntry();

            entryToUpdate.Properties[LDAPAccess.picture].Value = pictureData;
            entryToUpdate.CommitChanges();

            return true;
        }

        public bool addCardNumber(string userName, Int64 newCardNumber)
        {
            bool added = true;
            DirectoryEntry myLdapConnection = createDirectoryEntry();
            SearchResult result = this.getSearchResult(userName, myLdapConnection);
            DirectoryEntry entryToUpdate = result.GetDirectoryEntry();

            string currentCardNumbers = (string)entryToUpdate.Properties[LDAPAccess.cardNumbers].Value;

            Int64[] oldCardNumbers = this.stringToCardNumber(currentCardNumbers);

            if (!oldCardNumbers.Contains(newCardNumber))
            {
                string newCardNumberName = "BWadd" + oldCardNumbers.Length;
                string newCompleteCardString = this.cardNumberToString(currentCardNumbers, newCardNumber, newCardNumberName);
                entryToUpdate.Properties[LDAPAccess.cardNumbers].Value = newCompleteCardString;
                entryToUpdate.CommitChanges();
            }
            else
            {
                SimpleLog.Error("User: " + userName + " already has CardNumber: " + newCardNumber);
                added = false;
            }


            return added;
        }
    }
}