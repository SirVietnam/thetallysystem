﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemServer.DAL.DBAdapter
{
    public enum DBParamType
    {
        Int,
        Float, //Only use with Precision (53)
        Bit,
        VarChar,
        VarBinary,
        DateTime,
        Text
    }

    public interface IDBAdapter
    {
        DbConnection getOpenConnection();

        DbCommand getCommand(string commandString, DbConnection connection);

        void addParameter(DbCommand command, string param, DBParamType type, object value);
        void addParameter(DbCommand command, string param, DBParamType type, int length, object value);

        string getLastAutoValueCommand();
        object convertToSpecificParamType(DBParamType type);

        string converToArchiveTable(string tableCreationString);
    }
}
