﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BWSystemServer.DAL.DBAdapter
{
    public class MySqlAdapter : IDBAdapter
    {
        private string connectionString;

        public MySqlAdapter(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DbConnection getOpenConnection()
        {
            DbConnection connection = null;
            try
            {
                connection = new MySqlConnection(this.connectionString);
                connection.Open();
            } catch (Exception exc)
            {
                SimpleLog.Log(exc);
                /*
                 * TODO: Decent Exception catching!
                 */
            }
            return connection;
        }

        public DbCommand getCommand(string commandString, DbConnection connection)
        {
            return new MySqlCommand(commandString, (MySqlConnection)connection);
        }

        public void addParameter(DbCommand command, string param, DBParamType type, object value)
        {
            MySqlCommand sqlCommand = (MySqlCommand)command;
            MySqlParameter parameter = new MySqlParameter(param, (MySqlDbType)this.convertToSpecificParamType(type));
            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }
            sqlCommand.Parameters.Add(parameter);
        }

        public void addParameter(DbCommand command, string param, DBParamType type, int length, object value)
        {
            MySqlCommand sqlCommand = (MySqlCommand)command;
            MySqlParameter parameter = new MySqlParameter(param, (MySqlDbType)this.convertToSpecificParamType(type), length);
            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }
            sqlCommand.Parameters.Add(parameter);
        }

        public object convertToSpecificParamType(DBParamType type)
        {
            switch (type)
            {
                case DBParamType.Int:
                    return MySqlDbType.Int32;
                case DBParamType.Float:
                    return MySqlDbType.Float;
                case DBParamType.Bit:
                    return MySqlDbType.Bit;
                case DBParamType.VarChar:
                    return MySqlDbType.VarChar;
                case DBParamType.VarBinary:
                    return MySqlDbType.VarBinary;
                case DBParamType.DateTime:
                    return MySqlDbType.DateTime;
                case DBParamType.Text:
                    return MySqlDbType.Text;
                default:
                    throw new ArgumentException("The enum value : " + type.ToString() + " is unknown.");
            }
        }


        public string getLastAutoValueCommand()
        {
            return "SELECT LAST_INSERT_ID();";
        }

        public string converToArchiveTable(string tableCreationString)
        {

            string convertedTableCreationString = null;

            using (StringReader reader = new StringReader(tableCreationString))
            {
                StringBuilder sb = new StringBuilder();
                string line;

                //Read string line by line
                while ((line = reader.ReadLine()) != null)
                {
                    //Remove CONSTRAINT lines
                    if (!line.Contains("CONSTRAINT"))
                    {
                        sb.Append(line);
                    }
                }
                convertedTableCreationString = sb.ToString();
            }

            //Remove 'AUTO_INCREMENT' so there is no autoincerement
            string regexString = @".* AUTO_INCREMENT.*";
            Regex identitiyRegex = new Regex(regexString);
            if (identitiyRegex.IsMatch(convertedTableCreationString))
            {
                string autoIncrementString = "AUTO_INCREMENT";
                int startPos = convertedTableCreationString.IndexOf(autoIncrementString);
                int count = autoIncrementString.Length;
                convertedTableCreationString = convertedTableCreationString.Remove(startPos, count);
            }

            int lastCommaPos = convertedTableCreationString.LastIndexOf(',');
            if (convertedTableCreationString.Length - lastCommaPos <= 5)
            {
                convertedTableCreationString = convertedTableCreationString = convertedTableCreationString.Remove(lastCommaPos, 1);
            }

            return convertedTableCreationString;
        }
    }
}