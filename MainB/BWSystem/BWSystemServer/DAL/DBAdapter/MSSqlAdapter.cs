﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BWSystemServer.DAL.DBAdapter
{
    public class MSSqlAdapter : IDBAdapter
    {
        private string connectionString;

        public MSSqlAdapter(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DbConnection getOpenConnection()
        {
            DbConnection connection = new SqlConnection(this.connectionString);
            try
            {
                connection.Open();
            } catch (Exception exc)
            {
                SimpleLog.Log(exc);
                /*
                 * TODO: Decent Exception catching!
                 */
            }
            return connection;
        }


        public DbCommand getCommand(string commandString, DbConnection connection)
        {
            return new SqlCommand(commandString, (SqlConnection)connection);
        }


        public void addParameter(DbCommand command, string param, DBParamType type, object value)
        {
            SqlCommand sqlCommand = (SqlCommand)command;
            SqlParameter parameter = new SqlParameter(param, (SqlDbType)this.convertToSpecificParamType(type));
            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }
            sqlCommand.Parameters.Add(parameter);
        }

        public void addParameter(DbCommand command, string param, DBParamType type, int length, object value)
        {
            SqlCommand sqlCommand = (SqlCommand)command;
            SqlParameter parameter = new SqlParameter(param, (SqlDbType)this.convertToSpecificParamType(type), length);
            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }
            sqlCommand.Parameters.Add(parameter);
        }

        public object convertToSpecificParamType(DBParamType type)
        {
            switch (type)
            {
                case DBParamType.Int:
                    return SqlDbType.Int;
                case DBParamType.Float:
                    return SqlDbType.Float;
                case DBParamType.Bit:
                    return SqlDbType.Bit;
                case DBParamType.VarChar:
                    return SqlDbType.VarChar;
                case DBParamType.VarBinary:
                    return SqlDbType.VarBinary;
                case DBParamType.DateTime:
                    return SqlDbType.DateTime;
                case DBParamType.Text:
                    return SqlDbType.Text;
                default:
                    throw new ArgumentException("The enum value : " + type.ToString() + " is unknown.");
            }

        }

        public string getLastAutoValueCommand()
        {
            return "SELECT SCOPE_IDENTITY();";
        }

        public string converToArchiveTable(string tableCreationString)
        {
            string convertedTableCreationString = null;

            using (StringReader reader = new StringReader(tableCreationString))
            {
                StringBuilder sb = new StringBuilder();
                string line;

                //Read string line by line
                while ((line = reader.ReadLine()) != null)
                {
                    //Remove CONSTRAINT lines
                    if (!line.Contains("CONSTRAINT"))
                    {
                        sb.Append(line);
                    }
                }
                convertedTableCreationString = sb.ToString();
            }

            //Remove 'IDENTITY (x, x)' so there is no autoincerement
            string regexString = @".* IDENTITY.*(\d+.+\d+).*";
            Regex identitiyRegex = new Regex(regexString);
            if (identitiyRegex.IsMatch(convertedTableCreationString))
            {
                int startPos = convertedTableCreationString.IndexOf(" IDENTITY ");
                int endPos = convertedTableCreationString.IndexOf(") ");
                convertedTableCreationString = convertedTableCreationString.Remove(startPos + 1, (endPos - startPos) + 1);
            }

            return convertedTableCreationString;
        }
    }
}