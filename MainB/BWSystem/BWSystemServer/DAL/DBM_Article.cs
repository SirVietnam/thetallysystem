﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_Article : DataBaseManagement
    {
        private string standardPicture;
        private string standardDesc;

        public DBM_Article()
            : base()
        {
            this.standardPicture = (string)ServerUtil.getConfigValue("StandardArticleURL", typeof(string));
            this.standardDesc = (string)ServerUtil.getConfigValue("StandardArticleDes", typeof(string));
        }



        public List<Article> getArticleList()
        {
            List<Article> articles = new List<Article>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM Article", connection);
                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Article nextArticle = this.buildArticle(dataReader);
                        articles.Add(nextArticle);
                    }
                }
            }
            return articles;
        }



        private Article buildArticle(DbDataReader dataReader)
        {
            int id = (int)dataReader["ArticleID"];
            String name = (String)dataReader["Name"];
            name = name.TrimEnd(null);
            Article.ArticleType type = (Article.ArticleType)dataReader["Type"];
            double prize = ServerUtil.convertToDoubleFormDB((int)dataReader["Prize"]);
            double size = ServerUtil.convertToDoubleFormDB((int)dataReader["Size"]);
            int stock = (int)dataReader["Stock"];
            double alcoholicStrength = ServerUtil.convertToDoubleFormDB((int)dataReader["AlcoholicStrength"]);
            String manufacturer = (String)dataReader["Manufacturer"];
            manufacturer = manufacturer.TrimEnd(null);
            String pictureURL;
            if (dataReader.IsDBNull(dataReader.GetOrdinal("PictureURL")))
            {
                //pictureURL = this.standardPicture;
                pictureURL = null;
            }
            else
            {
                pictureURL = (String)dataReader["PictureURL"];
                pictureURL = pictureURL.TrimEnd(null);
            }

            String description;
            if (dataReader.IsDBNull(dataReader.GetOrdinal("Description")))
            {
                description = this.standardDesc;
            }
            else
            {
                description = (String)dataReader["Description"];
                description = description.TrimEnd(null);
            }

            Boolean isInventory = Convert.ToBoolean(dataReader["IsInventory"]);
            //int ord = -1000;
            //ord = dataReader.GetOrdinal("IsAvailable");
            //(Boolean)dataReader["IsAvailable"];
            Boolean isAvailable = Convert.ToBoolean(dataReader["IsAvailable"]);
            //Boolean isAvailable = true;

            return new Article(id, name, type, prize, size, stock, alcoholicStrength, manufacturer,
                pictureURL, description, isInventory, isAvailable);
        }



        public Article getArticle(int articleID)
        {
            Article article = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM Article WHERE ArticleID = @SpecArticleID", connection);
                    this.Database.addParameter(command, "@SpecArticleID", DBAdapter.DBParamType.Int, articleID);

                    DbDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        article = this.buildArticle(dataReader);
                    }
                }
            }
            return article;
        }



        public Boolean addArticle(Article newArticle)
        {
            int rows = 0;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO Article (Name, Type, Prize, Size, Stock, AlcoholicStrength, Manufacturer, PictureURL, Description, IsInventory, IsAvailable) "
                    + "VALUES (@Name, @Type, @Prize, @Size, @Stock, @AlcoholicStrength, @Manufacturer, @PictureURL, @Description, @IsInventory, @IsAvailable)", connection);

                this.Database.addParameter(insertCom, "@Name", DBAdapter.DBParamType.VarChar, 50, newArticle.Name);
                this.Database.addParameter(insertCom, "@Type", DBAdapter.DBParamType.Int, newArticle.Type);
                this.Database.addParameter(insertCom, "@Prize", DBAdapter.DBParamType.Int, ServerUtil.convertDoubleForDB(newArticle.Prize));
                this.Database.addParameter(insertCom, "@Size", DBAdapter.DBParamType.Int, ServerUtil.convertDoubleForDB(newArticle.Size));
                this.Database.addParameter(insertCom, "@Stock", DBAdapter.DBParamType.Int, newArticle.Stock);
                this.Database.addParameter(insertCom, "@AlcoholicStrength", DBAdapter.DBParamType.Int, ServerUtil.convertDoubleForDB(newArticle.AlcoholicStrength));
                this.Database.addParameter(insertCom, "@Manufacturer", DBAdapter.DBParamType.VarChar, 50, newArticle.Manufacturer);
                this.Database.addParameter(insertCom, "@PictureURL", DBAdapter.DBParamType.VarChar, 50, newArticle.PictureURL);
                this.Database.addParameter(insertCom, "@Description", DBAdapter.DBParamType.Text, newArticle.Description);
                this.Database.addParameter(insertCom, "@IsInventory", DBAdapter.DBParamType.Bit, newArticle.IsInventory);
                this.Database.addParameter(insertCom, "@IsAvailable", DBAdapter.DBParamType.Bit, newArticle.IsAvailable);

                rows = insertCom.ExecuteNonQuery();
            }

            return rows == 1;
        }


        public bool changeStock(int articleID, int amountChange)
        {
            bool stockChanged = false;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("UPDATE Article SET Stock = Stock - @stockChange WHERE ArticleID = @ArticleID", connection);

                this.Database.addParameter(insertCom, "@stockChange", DBAdapter.DBParamType.Int, amountChange);
                //SqlParameter newStockParamm = new SqlParameter("@stockChange", SqlDbType.Int);
                //newStockParamm.Value = amountChange;
                //insertCom.Parameters.Add(newStockParamm);

                this.Database.addParameter(insertCom, "@ArticleID", DBAdapter.DBParamType.Int, articleID);
                //SqlParameter idParam = new SqlParameter("@ArticleID", SqlDbType.Int);
                //idParam.Value = articleID;
                //insertCom.Parameters.Add(idParam);

                int rows = insertCom.ExecuteNonQuery();

                if (rows == 1)
                {
                    stockChanged = true;
                }
            }
            return stockChanged;
        }

        internal bool updateArticle(Article oldArticle)
        {
            int rows = 0;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand changeCom = this.Database.getCommand("UPDATE Article SET" +
                    " Name = @Name, Type = @Type, Prize = @Prize, Size = @Size, AlcoholicStrength = @AlcStr, Manufacturer = @Manu," +
                    " PictureURL = @PicURL, Description = @Desc, IsInventory = @IsInventory, IsAvailable = @IsAvailable" +
                    " WHERE ArticleID = @ArticleID", connection);

                this.Database.addParameter(changeCom, "@Name", DBAdapter.DBParamType.VarChar, 50, oldArticle.Name);
                this.Database.addParameter(changeCom, "@Type", DBAdapter.DBParamType.Int, oldArticle.Type);
                this.Database.addParameter(changeCom, "@Prize", DBAdapter.DBParamType.Int, ServerUtil.convertDoubleForDB(oldArticle.Prize));
                this.Database.addParameter(changeCom, "@Size", DBAdapter.DBParamType.Int, ServerUtil.convertDoubleForDB(oldArticle.Size));
                this.Database.addParameter(changeCom, "@AlcStr", DBAdapter.DBParamType.Int, ServerUtil.convertDoubleForDB(oldArticle.AlcoholicStrength));
                this.Database.addParameter(changeCom, "@Manu", DBAdapter.DBParamType.VarChar, 50, oldArticle.Manufacturer);
                this.Database.addParameter(changeCom, "@PicURL", DBAdapter.DBParamType.VarChar, 50, oldArticle.PictureURL);
                this.Database.addParameter(changeCom, "@Desc", DBAdapter.DBParamType.Text, oldArticle.Description);
                this.Database.addParameter(changeCom, "@IsInventory", DBAdapter.DBParamType.Bit, oldArticle.IsInventory);
                this.Database.addParameter(changeCom, "@IsAvailable", DBAdapter.DBParamType.Bit, oldArticle.IsAvailable);

                this.Database.addParameter(changeCom, "@ArticleID", DBAdapter.DBParamType.Int, oldArticle.ID);

                rows = changeCom.ExecuteNonQuery();

            }
            return (rows == 1);
        }


        internal bool changePrize(int articleID, float newPrize)
        {
            int rows = 0;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO PrizeHistory (ArticleID, Prize, PrizeUntil) VALUES (@ArticleID, @Prize, @PrizeUntil);", connection);

                Article article = this.getArticle(articleID);
                double oldPrize = article.Prize;
                DateTime validUntilDate = DateTime.Now;

                this.Database.addParameter(insertCom, "@ArticleID", DBAdapter.DBParamType.Int, articleID);
                this.Database.addParameter(insertCom, "@Prize", DBAdapter.DBParamType.Int, ServerUtil.convertDoubleForDB(oldPrize));
                this.Database.addParameter(insertCom, "@PrizeUntil", DBAdapter.DBParamType.DateTime, validUntilDate);

                rows = insertCom.ExecuteNonQuery();
            }
            return (rows == 1);
        }


        internal List<Tuple<float, DateTime>> getPrizeHistory(int articleID)
        {
            List<Tuple<float, DateTime>> prizeHistory = new List<Tuple<float, DateTime>>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM PrizeHistory WHERE ArticleID = @ArticleID ORDER BY PrizeUntil", connection);

                    this.Database.addParameter(command, "@ArticleID", DBAdapter.DBParamType.Int, articleID);

                    DbDataReader dataReader = command.ExecuteReader();

                    if (dataReader.Read())
                    {
                        float prize = (float)dataReader["Prize"];
                        DateTime validUntil = (DateTime)dataReader["PrizeUntil"];
                        prizeHistory.Add(new Tuple<float, DateTime>(prize, validUntil));
                    }
                }
            }
            return prizeHistory;
        }


        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            base.copyDataToArchive(startDateTime, endDateTime, Tables.PrizeHistory, Tables_Archives.PrizeHistory);
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }
    }
}