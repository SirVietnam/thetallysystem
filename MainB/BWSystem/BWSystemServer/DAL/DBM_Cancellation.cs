﻿using BWSystemServer.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_Cancellation : DataBaseManagement
    {

        //TODO: Fix Cancellation! 
        public DBM_Cancellation()
            : base()
        {
        }

        //GET Data
        public List<CancellationData> getCancellationList()
        {
            return this.getCancellationList(new DateTime(1971, 01, 01), new DateTime(DateTime.Now.Ticks));
        }

        public List<CancellationData> getCancellationList(DateTime startTime)
        {
            return this.getCancellationList(startTime, new DateTime(DateTime.Now.Ticks));
        }

        //TODO Rework this method!
        public List<CancellationData> getCancellationList(DateTime startTime, DateTime endTime)
        {
            List<CancellationData> cancellationDatas = this.getCancellationList(startTime, endTime, this.getTableName(Tables.Cancellation));
            return cancellationDatas;
        }


        private List<CancellationData> getCancellationList(DateTime startTime, DateTime endTime, string tableName)
        {
            List<CancellationData> cancellationDatas = new List<CancellationData>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM " + tableName + " WHERE TimeStamp BETWEEN  @StartDate AND @EndDate", connection);

                    this.Database.addParameter(command, "@StartDate", DBAdapter.DBParamType.DateTime, startTime);
                    this.Database.addParameter(command, "@EndDate", DBAdapter.DBParamType.DateTime, endTime);

                    DbDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        CancellationData nextCancellationData = this.buildCancellationData(dataReader);
                        cancellationDatas.Add(nextCancellationData);
                    }
                }

            }
            return cancellationDatas;
        }


        //TODO: verify this method!
        public CancellationData getCancellationDatasToCancellationID(int id)
        {
            CancellationData cancellationData = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM Cancellation WHERE CancellationID = @ParamID", connection);

                    this.Database.addParameter(command, "@ParamID", DBAdapter.DBParamType.Int, id);

                    DbDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        cancellationData = this.buildCancellationData(dataReader);
                    }
                }
            }
            return cancellationData;
        }


        private CancellationData buildCancellationData(DbDataReader dataReader)
        {
            int cancellationID = (int)dataReader["CancellationID"];
            int cancelledBookingID = (int)dataReader["CancelledBookingID"];

            int? newBookingID = null;
            if (!dataReader.IsDBNull(dataReader.GetOrdinal("NewBookingID")))
            {
                newBookingID = (int)dataReader["NewBookingID"];
            }

            int userID = (int)dataReader["User"];
            DateTime timeStamp = (DateTime)dataReader["TimeStamp"];

            return new CancellationData(cancellationID, userID, cancelledBookingID, newBookingID, timeStamp);
        }


        //ADD Data
        public int addCancellation(int cancelledBookingID, int? newBookingID, int userID, DateTime timeStamp)
        {
            //TODO: Fix Cancellation! 
            int cancellationID = -1;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO Cancellation (CancelledBookingID, NewBookingID, UserID, TimeStamp)" +
                    " VALUES (@CancelledBookingIDParam, @NewBookingIDParam, @UserIDParam, @TimeStampParam); " +
                    this.Database.getLastAutoValueCommand(), connection); //" SELECT CancellationID FROM Cancellation WHERE CancellationID = @@identity;"

                this.Database.addParameter(insertCom, "@CancelledBookingIDParam", DBAdapter.DBParamType.Int, cancelledBookingID);
                this.Database.addParameter(insertCom, "@NewBookingIDParam", DBAdapter.DBParamType.Int, newBookingID);
                this.Database.addParameter(insertCom, "@UserIDParam", DBAdapter.DBParamType.Int, userID);
                this.Database.addParameter(insertCom, "@TimeStampParam", DBAdapter.DBParamType.DateTime, timeStamp);

                //DbDataReader idReader = insertCom.ExecuteReader();
                //idReader.Read();
                //cancellationID = (int)idReader["CancellationID"];
                //idReader.Close();
                decimal obj = Convert.ToDecimal(insertCom.ExecuteScalar());
                cancellationID = (int)obj;
            }
            return cancellationID;
        }


        //Archive Data
        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            base.copyDataToArchive(startDateTime, endDateTime, Tables.Cancellation, Tables_Archives.Cancellation);
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            base.deleteDataFromTable(startDateTime, endDateTime, Tables.Cancellation);
        }

        public List<CancellationData> getArchiveCancellationList()
        {
            List<CancellationData> cancellationDatas = this.getCancellationList(new DateTime(1970, 01, 01), DateTime.Now, this.getTableName(Tables_Archives.Cancellation));
            return cancellationDatas;
        }

    }
}