﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWSystemServer.DAL
{
    public class DBM_AnnualStatement : DataBaseManagement
    {

        public DBM_AnnualStatement()
            : base()
        {
        }

        //Read Methods
        public AnnualStatement getLastAnnualStatement()
        {
            AnnualStatement statement = null;

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM AnnualStatement WHERE EndDate IN (SELECT max(EndDate) FROM AnnualStatement);", connection);
                    DbDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        statement = this.buildAnnualStatement(dataReader);
                    }
                }
            }
            return statement;
        }


        public List<AnnualStatement> getAnnualStatementList()
        {
            List<AnnualStatement> statements = new List<AnnualStatement>();

            using (DbConnection connection = base.Database.getOpenConnection())
            {
                if (connection.State == ConnectionState.Open)
                {
                    DbCommand command = this.Database.getCommand("SELECT * FROM AnnualStatement", connection);
                    DbDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        AnnualStatement statement = this.buildAnnualStatement(dataReader);
                        statements.Add(statement);
                    }
                }
            }
            return statements;
        }


        private AnnualStatement buildAnnualStatement(DbDataReader dataReader)
        {
            int id = (int)dataReader["AnnualStatementID"];
            DateTime startDate = (DateTime)dataReader["StartDate"];
            DateTime endDate = (DateTime)dataReader["EndDate"];
            String name = (String)dataReader["Name"];
            return new AnnualStatement(id, name, startDate, endDate);
        }



        //Write Methods
        public AnnualStatement addAnnualStatement(AnnualStatement annualStatement)
        {
            Decimal? annualAccountIdDecimal = null;
            using (DbConnection connection = base.Database.getOpenConnection())
            {
                DbCommand insertCom = this.Database.getCommand("INSERT INTO AnnualStatement (StartDate, EndDate, Name) " 
                    + "VALUES (@StartDate, @EndDate, @Name); " + this.Database.getLastAutoValueCommand(), connection);

                this.Database.addParameter(insertCom, "@StartDate", DBAdapter.DBParamType.DateTime, annualStatement.StartDate);
                this.Database.addParameter(insertCom, "@EndDate", DBAdapter.DBParamType.DateTime, annualStatement.EndDate);
                this.Database.addParameter(insertCom, "@Name", DBAdapter.DBParamType.VarChar, 50, annualStatement.AnnualStatementName);

                annualAccountIdDecimal = Convert.ToDecimal(insertCom.ExecuteScalar());
            }
            return new AnnualStatement((int)annualAccountIdDecimal, annualStatement.AnnualStatementName, annualStatement.StartDate, annualStatement.EndDate);
        }

        //Inherrited Methods
        internal override void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }

        internal override void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }
    }
}