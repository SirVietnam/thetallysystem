﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWSystemServer.BIZ;
using BWSystemServer.Classes;
using System.Xml.Serialization;
using BWSystemServer.BIZ.DatabaseAccessManagement;

namespace BWSystemServer
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für "BWService"
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Wenn der Aufruf dieses Webdiensts aus einem Skript zulässig sein soll, heben Sie mithilfe von ASP.NET AJAX die Kommentarmarkierung für die folgende Zeile auf. 
    // [System.Web.Script.Services.ScriptService]
    public class BWService : System.Web.Services.WebService
    {
        private static DAM_ClientDevice ClientDeviceManagement = new DAM_ClientDevice();
        private DBAccessManagement DAM;


        public BWService()
        {
            this.DAM = DBAccessManagement.Instance;
        }


        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public ClientToken getClientToken(string loginToken)
        {
            string ipAddress = HttpContext.Current.Request.UserHostAddress;
            ClientToken token = ClientDeviceManagement.getToken(loginToken, ipAddress);
            return token;
        }

        [WebMethod]
        public bool checkClientToken(ClientToken clientToken)
        {
            bool validToken = ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress);
            return validToken;

        }

        [WebMethod]
        public bool updateRequired(DateTime lastDataUpdate)
        {
            return this.DAM.updateRequired(lastDataUpdate);
        }


        /*
         * Account Operations
         */
        [WebMethod]
        public List<Account> getAccountList(ClientToken clientToken)
        {
            List<Account> accounts = null;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                accounts = this.DAM.getAccountList_Active();
            }
            return accounts;
        }


        [WebMethod]
        public Account getAccount(ClientToken clientToken, string userID, string userPW)
        {
            Account account = null;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                account = this.DAM.getOrgAccount(userID, userPW);
            }
            return account;
        }


        [WebMethod]
        public bool addAccount(ClientToken clientToken, Account user)
        {
            bool userAdded = false;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                userAdded = this.DAM.addAccount(user);
            }
            return userAdded;
        }


        [WebMethod]
        public bool addNonOrgAccount(ClientToken clientToken, Account user, string password, string mailAddress)
        {
            bool userAdded = false;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                userAdded = this.DAM.addAccount(user, password, mailAddress);
            }
            return userAdded;
        }


        [WebMethod]
        public InputRegex[] getNonOrgLoginPrefix(ClientToken clientToken)
        {
            InputRegex[] loginRegex = null;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                loginRegex = DAM_NonOrgLogin.getLoginRegex();
            }
            return loginRegex;
        }


        [WebMethod]
        public InputRegex[] getMailRegex(ClientToken clientToken)
        {
            InputRegex[] mailRegex = null;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                mailRegex = DAM_NonOrgLogin.getMailRegexWithExplanations();
            }
            return mailRegex;
        }


        [WebMethod]
        public InputRegex[] getPasswordRegex(ClientToken clientToken)
        {
            InputRegex[] pwRegex = null;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                pwRegex = PasswordHashUtil.getPasswordRegexWithExplanations();
            }
            return pwRegex;
        }


        [WebMethod]
        public bool isValidLogin(ClientToken clientToken, string login)
        {
            bool validLogin = false;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                validLogin = this.DAM.isValidLogin(login);
            }
            return validLogin;
        }


        /*
         * Article Operations
         */
        [WebMethod]
        public List<Article> getArticleList()
        {
            return this.DAM.getArticleList_Active();
        }


        [WebMethod]
        public List<Article> getEventArticleList(int eventID)
        {
            return this.DAM.getEventArticleList(eventID);
        }


        /*
         * Event Operations
         */
        [WebMethod]
        public List<Event> getFutureEvents()
        {
            return this.DAM.getEventList_Future();
        }


        /*
         * Booking Operations
         */
        [WebMethod]
        public List<BookingData> getPostingDataList(ClientToken clientToken, DateTime timeStamp)
        {
            List<BookingData> bookings = null;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                bookings = DAM_Booking.getBookingDataList(timeStamp);
            }
            return bookings;
        }

        [WebMethod]
        public bool addPostingData(ClientToken clientToken, BookingData bookingData)
        {
            bool bookingAdded = false;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                int bookingID = DAM_Booking.addBooking(bookingData);
                bookingAdded = bookingID != -1;
            }
            return bookingAdded;
        }


        /*
         * Cancellation Operations
         */
        [WebMethod]
        public bool cancelBooking(ClientToken clientToken, BWSystemServer.Classes.CancellationData cancellation)
        {
            bool bookingCancelled = false;
            if (ClientDeviceManagement.isValidToken(clientToken, HttpContext.Current.Request.UserHostAddress))
            {
                bookingCancelled = this.DAM.cancelBooking(cancellation);
            }
            return bookingCancelled;
        }


        /*
         * Image Operations
         */
        [WebMethod]
        public byte[] getDefaultUserImage(int maxSize)
        {
            byte[] imageBytes = null;
            try
            {
                imageBytes = this.DAM.getDefaultUserImage(maxSize);
            } catch (Exception ex)
            {
                SimpleLog.Log(ex, false);
            }
            return imageBytes;
        }

        [WebMethod]
        public byte[] getDefaultArticleImage(int maxSize)
        {
            byte[] imageBytes = null;
            try
            {
                imageBytes = this.DAM.getDefaultArticleImage(maxSize);
            } catch (Exception ex)
            {
                SimpleLog.Log(ex, false);
            }
            return imageBytes;
        }



        [WebMethod]
        public byte[] getUserImage(string pictureName, int maxSize)
        {
            byte[] userImageBytes = null;
            try
            {
                userImageBytes = this.DAM.getUserImage(pictureName, maxSize);
            } catch (Exception ex)
            {
                SimpleLog.Log(ex, false);
            }
            return userImageBytes;
        }

        [WebMethod]
        public byte[] getArticleImage(string pictureName, int maxSize)
        {
            return this.DAM.getArticleImage(pictureName, maxSize);
        }

        //[WebMethod]
        //public DateTime lastPictureUpdate(bool users, bool pictures)
        //{
        //    DateTime lastModified;
        //    if (users && !pictures)
        //    {
        //        lastModified = DAM.lastAccountPictureModification();
        //    }
        //    else if (!users && pictures)
        //    {
        //        lastModified = DAM.lastArticlePictureModification();
        //    }
        //    else
        //    {
        //        lastModified = new DateTime(DateTime.Now.Ticks);
        //    }
        //    return lastModified;
        //}

        [WebMethod]
        public DateTime getAccountImageDateTime(int accountID)
        {
            return DAM.getUserImageDateTime(accountID);
        }

        [WebMethod]
        public DateTime getArticleImageDateTime(int articleID)
        {
            return DAM.getArticleImageDateTime(articleID);
        }

        /*
         * Additional Operations
         */
        [WebMethod]
        public int[] getOrder(BWSystemServer.BIZ.SortingMethod sortingMethod, bool user)
        {
            return this.DAM.getOrder(sortingMethod, user);
        }
    }
}