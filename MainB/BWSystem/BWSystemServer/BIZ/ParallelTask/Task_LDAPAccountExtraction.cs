﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.ParallelTask
{
    public class Task_LDAPAccountExtraction : PeriodicTask
    {
        public Task_LDAPAccountExtraction()
            : base((int)ServerUtil.getConfigValue("LDAPAccEx_Start_Hour", typeof(int))
                  , (int)ServerUtil.getConfigValue("LDAPAccEx_Start_Min", typeof(int))
                  , TimeSpan.FromMinutes((int)ServerUtil.getConfigValue("LDAPAccEx_Intervall_Min", typeof(int))))
        {
        }


        public override void run()
        {
            SimpleLog.Log("Starting run of LDAP Account Extraction");
            DBAccessManagement.Instance.addMissingLDAPAccounts();
        }
    }
}