﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.ParallelTask
{
    public abstract class PeriodicTask
    {
        private DateTime nextExecutionTime;
        private TimeSpan executionIntervall;

        public PeriodicTask(int hour, int minute, TimeSpan executionIntervall)
        {
            this.executionIntervall = executionIntervall;

            if (hour > 23 && minute > 59)
            {
                throw new ArgumentException("Hour and Minute must be time compliant!");
            }
            DateTime currentDateTime = DateTime.Now;
            this.nextExecutionTime = new DateTime(currentDateTime.Year, currentDateTime.Month, Math.Max(currentDateTime.Day - 1, 1), hour, minute, 0);

            this.calculateNextExecutionTime();
        }


        public abstract void run();

        public DateTime NextExecutionTime
        {
            get
            {
                return nextExecutionTime;
            }
        }

        internal void calculateNextExecutionTime()
        {
            while (this.nextExecutionTime < DateTime.Now)
            {
                this.nextExecutionTime = this.nextExecutionTime.Add(executionIntervall);
            }
        }
    }
}