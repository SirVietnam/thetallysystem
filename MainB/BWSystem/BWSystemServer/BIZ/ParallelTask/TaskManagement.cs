﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Web;

namespace BWSystemServer.BIZ.ParallelTask
{
    public class TaskManagement
    {
        private List<PeriodicTask> tasks;
        private System.Timers.Timer taskTimer;

        public TaskManagement()
        {
            taskTimer = new System.Timers.Timer();
            taskTimer.Elapsed += timerInvoke;
            tasks = new List<PeriodicTask>();

            tasks.Add(new Task_LDAPSync());
            tasks.Add(new Task_LDAPAccountExtraction());

            this.setupTimer();
        }

        private void timerInvoke(object sender, ElapsedEventArgs e)
        {
            taskTimer.Enabled = false;

            
            PeriodicTask nextTask = tasks.First();
            while (nextTask.NextExecutionTime <= DateTime.Now)
            {
                //Start the Task Parallel
                Thread taskRunner = new Thread(new ThreadStart(nextTask.run));
                taskRunner.Start();
                nextTask.calculateNextExecutionTime();
                this.tasks.OrderBy(x => x.NextExecutionTime);
                nextTask = tasks.First();
            }

            this.setupTimer();
        }

        private void setupTimer()
        {
            //Calculate the next Task
            this.tasks = this.tasks.OrderBy(x => x.NextExecutionTime.Ticks).ToList();

            //Setup Timer to invoke on next Event
            TimeSpan timeUntilNextInvoke = TimeSpan.FromTicks(this.tasks.First().NextExecutionTime.Ticks - DateTime.Now.Ticks);
            taskTimer.Interval = timeUntilNextInvoke.TotalMilliseconds;
            taskTimer.Enabled = true;
        }
    }
}