﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.ParallelTask
{
    public class Task_LDAPSync : PeriodicTask
    {

        public Task_LDAPSync()
            : base((int)ServerUtil.getConfigValue("LDAPSync_Start_Hour", typeof(int))
                  , (int)ServerUtil.getConfigValue("LDAPSync_Start_Min", typeof(int))
                  , TimeSpan.FromMinutes((int)ServerUtil.getConfigValue("LDAPSync_Intervall_Min", typeof(int))))
        {
        }


        public override void run()
        {
            SimpleLog.Log("Starting run of LDAPSync");
            DBAccessManagement.Instance.syncLDAP_DB();
        }
    }
}