﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWSystemServer.DAL;

namespace BWSystemServer.BIZ
{
    public static class LDAPSync
    {
        private static LDAPAccess LDAP = new LDAPAccess();


        /////////////////// Main Methods /////////////////////
        internal static bool isValidPassword(string user, string password)
        {
            return LDAP.isValidPassword(user, password);
        }


        internal static List<Account> getModifiedAccounts(IEnumerable<Account> accountList)
        {
            List<Account> modifiedAccounts = new List<Account>();

            foreach (Account account in accountList)
            {
                LDAP_Data data = LDAP.getLDAP_Data(account.Authentication.LoginName);
                if (data.filledWithData)
                {
                    bool equals = LDAPSync.equalAccounts(account, data);
                    if (!equals)
                    {
                        Account modifiedAccount = LDAPSync.fillAccountData(account, data);
                        modifiedAccounts.Add(modifiedAccount);
                    }
                }
                else
                {
                    SimpleLog.Log("The LDAP-Querry for UserLogin: " + account.Authentication.LoginName + " did not bring any results. Account will be deactivated!");
                    account.IsAvailable = false;
                    modifiedAccounts.Add(account);
                }
            }

            return modifiedAccounts;
        }


        public static Account getAccountData(string userID, string userPW)
        {
            LDAP_Data data = LDAP.getLDAP_Data(userID, userPW);
            Account acc = null;
            if (data.filledWithData)
            {
                acc = LDAPSync.fillAccountData(new Account(), data);
            }
            return acc;
        }


        public static List<Account> getMissingAccounts(List<Account> accounts)
        {
            List<Account> missingAccounts = new List<Account>();

            string ldapGroupsConfig = (string)ServerUtil.getConfigValue("LDAP_Account_Group", typeof(string));
            string[] ldapGroups = ldapGroupsConfig.Split((char)ServerUtil.getConfigValue("SecurityCodeSeperatorSign", typeof(char)));

            HashSet<string> logins = new HashSet<string>();
            foreach (string ldapGroup in ldapGroups)
            {
                List<string> loginsInGroup = LDAP.getLoginOfGroup(ldapGroup);
                foreach (string login in loginsInGroup)
                {
                    bool added = logins.Add(login.ToLower());
                    if (!added)
                    {
                        int i = 0;
                    }
                }
            }

            foreach (string login in logins)
            {
                Account existingAccount = accounts.FirstOrDefault(x => x.Authentication.LoginName.Equals(login, StringComparison.OrdinalIgnoreCase));
                //Account accountInAdding = missingAccounts.FirstOrDefault(x => x.Authentication.LoginName.Equals(login, StringComparison.OrdinalIgnoreCase));

                if (existingAccount == null) //&& accountInAdding == null
                {
                    Account newAccount = LDAPSync.getAccountData(login);
                    if (newAccount != null)
                    {
                        missingAccounts.Add(newAccount);
                    }
                }

            }

            return missingAccounts;
        }


        private static Account getAccountData(string userID)
        {
            LDAP_Data data = LDAP.getLDAP_Data(userID);
            Account acc = null;
            if (data.filledWithData)
            {
                acc = LDAPSync.fillAccountData(new Account(), data);
            }
            return acc;
        }


        internal static List<Tuple<Account, int>> getAutoSettlementIDs(List<Account> accounts)
        {
            List<Tuple<Account, int>> extraInfos = new List<Tuple<Account, int>>();
            string propertyField = (string)ServerUtil.getConfigValue("LDAP_AutoSettlment_Property", typeof(string));

            foreach (Account account in accounts)
            {
                string autoSettlementID_String = LDAP.getUserProperty(account.Authentication.LoginName, propertyField);

                if (autoSettlementID_String != null)
                {
                    try
                    {
                        int autoSettlementID = int.Parse(autoSettlementID_String);
                        extraInfos.Add(new Tuple<Account, int>(account, autoSettlementID));
                    } catch (FormatException ex)
                    {
                        SimpleLog.Log(ex);
                    }
                }
                else
                {
                    SimpleLog.Log("The value: " + propertyField + "is availible for user: " + account.Authentication.LoginName);
                }
            }

            return extraInfos;
        }



        /************* Picture Interaction Methods ******************/
        #region LDAP-Picture
        public static Byte[] getAccountPicture(Account user)
        {
            Byte[] picture = null;
            if (user.Authentication.LoginName != null)
            {
                picture = LDAP.getAccountPicture(user.Authentication.LoginName);
            }
            else
            {
                SimpleLog.Error("The user: " + user.ID + " - " + user.Name + " has no login, so no picture could be loaded from LDAP!");
            }
            return picture;
        }


        public static bool addPicture(Account user, byte[] picture)
        {
            bool pictureAdded = true;
            if (user.Authentication.LoginName != null)
            {
                pictureAdded = LDAP.addPictureToLDAP(user.Authentication.LoginName, picture);

                if (!pictureAdded)
                {
                    SimpleLog.Error("Picture add for user : " + user.ID + " - " + user.Name + " FAILED! -- Reason unkonwn!");
                }
            }
            else
            {
                SimpleLog.Error("Picture add for user : " + user.ID + " - " + user.Name + " because authentication has no LoginName!");
                pictureAdded = false;
            }
            return pictureAdded;
        }
        #endregion

        //////////////// HELPER METHODS //////////////////////////////
        #region HelperMethods
        private static bool equalAccounts(Account systemAccount, LDAP_Data ldapData)
        {
            bool equals = true;
            equals = equals && systemAccount.Name == ldapData.preName;
            equals = equals && systemAccount.Surname == ldapData.surName;
            equals = equals && systemAccount.Nickname == ldapData.nickName;

            if (systemAccount.Authentication.CardCode != null && ldapData.cardNumbers != null)
            {
                equals = equals && systemAccount.Authentication.CardCode.Length == ldapData.cardNumbers.Length;
            }

            if (equals)
            {
                equals = new HashSet<Int64>(systemAccount.Authentication.CardCode).SetEquals(ldapData.cardNumbers);
            }

            equals = equals && systemAccount.IsAvailable == ldapData.isActive;
            equals = equals && systemAccount.IsSuperuser == ldapData.isAdmin;

            return equals;
        }


        private static Account fillAccountData(Account accountToFill, LDAP_Data ldapData)
        {
            if (ldapData.filledWithData)
            {
                accountToFill.Name = ldapData.preName;
                accountToFill.Surname = ldapData.surName;
                accountToFill.Nickname = ldapData.nickName;
                if (accountToFill.Authentication == null)
                {
                    accountToFill.Authentication = new Authentication();
                }
                accountToFill.Authentication.LoginName = ldapData.loginName;
                accountToFill.Authentication.CardCode = ldapData.cardNumbers;
                accountToFill.IsAvailable = ldapData.isActive;
                accountToFill.IsSuperuser = ldapData.isAdmin;
            }

            return accountToFill;
        }
        #endregion
    }
}