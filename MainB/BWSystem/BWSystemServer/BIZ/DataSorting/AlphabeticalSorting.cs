﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ
{
    internal struct alphabeticalStruct
    {
        internal int id;
        internal string name;
    }

    public class AlphabeticalSorting : DataSorting
    {

        private alphabeticalStruct[] accountStructs;
        private alphabeticalStruct[] articleStructs;

        public AlphabeticalSorting(DBAccessManagement DAM)
            : base(DAM)
        {
        }


        public override void sort()
        {
            this.updateFields();
            accountStructs = new alphabeticalStruct[this.Accounts.Count];

            for (int i = 0; i < accountStructs.Count(); i++)
            {
                accountStructs[i].name = this.Accounts[i].DisplayName;
                accountStructs[i].id = this.Accounts[i].ID;
            }

            articleStructs = new alphabeticalStruct[this.Articles.Count];

            for (int i = 0; i < articleStructs.Count(); i++)
            {
                articleStructs[i].name = this.Articles[i].Name;
                articleStructs[i].id = this.Articles[i].ID;
            }

            Array.Sort(accountStructs, (x, y) => string.Compare(x.name, y.name));
            Array.Sort(articleStructs, (x, y) => string.Compare(x.name, y.name));

            this.SortTime = new DateTime(DateTime.Now.Ticks);
        }

        public override int[] getAccountOrder()
        {
            int[] accOrder = new int[this.accountStructs.Count()];

            for (int i = 0; i < accOrder.Count(); i++)
            {
                accOrder[i] = this.accountStructs[i].id;
            }
            return accOrder;
        }

        public override int[] getArticleOrder()
        {
            int[] artOrder = new int[this.articleStructs.Count()];

            for (int i = 0; i < artOrder.Count(); i++)
            {
                artOrder[i] = this.articleStructs[i].id;
            }

            return artOrder;
        }
    }
}