﻿using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ
{
    internal class ConsumptionStruct
    {
        public int id;
        public int amount;
    }

    public class ConsumptionSorting : DataSorting
    {
        private List<BookingData> bookings;
        private DBAccessManagement DAM;


        private List<ConsumptionStruct> userStructs;
        private Dictionary<int, ConsumptionStruct> userDict;

        private List<ConsumptionStruct> articleStructs;
        private Dictionary<int, ConsumptionStruct> articleDict;

        private TimeSpan comsumptionTimeSpan;

        public ConsumptionSorting(DBAccessManagement DAM)
            : base(DAM)
        {
            this.DAM = DAM;

            int days = (int)ServerUtil.getConfigValue("SortingBookingIntervalDay", typeof(int));
            int hours = (int)ServerUtil.getConfigValue("SortingBookingIntervalHour", typeof(int));
            int mins = (int)ServerUtil.getConfigValue("SortingBookingIntervalMin", typeof(int));
            this.comsumptionTimeSpan = new TimeSpan(days, hours, mins, 0);

            this.userStructs = new List<ConsumptionStruct>();
            this.userDict = new Dictionary<int, ConsumptionStruct>();

            this.articleStructs = new List<ConsumptionStruct>();
            this.articleDict = new Dictionary<int, ConsumptionStruct>();
        }

        public override void sort()
        {
            this.initFields();

            DateTime bookingIntervall = DateTime.Now.Subtract(this.comsumptionTimeSpan);
            this.bookings = DAM_Booking.getBookingDataList(bookingIntervall);

            foreach (BookingData booking in bookings)
            {
                int user = booking.Customer;
                //int userIndex = this.userDict[user];
                //ConsumptionStruct userStruct = this.userStructs.ElementAt(userIndex);
                ConsumptionStruct userStruct = this.userDict[user];

                foreach (BookingAA tuple in booking.Booking)
                {
                    int article = tuple.Article;
                    int amount = tuple.Amount;

                    //int articleIndex = this.articleDict[article];
                    //ConsumptionStruct articleStruct = this.articleStructs.ElementAt(articleIndex);
                    ConsumptionStruct articleStruct = this.articleDict[article];
    
                    userStruct.amount += amount;
                    articleStruct.amount += amount;
                }
            }

            this.userStructs = this.userStructs.OrderByDescending(x => x.amount).ToList();
            this.articleStructs = this.articleStructs.OrderByDescending(x => x.amount).ToList();

            this.SortTime = DateTime.Now;
        }



        private void initFields()
        {
            this.updateFields();
            this.userDict.Clear();
            this.userStructs.Clear();
            this.articleDict.Clear();
            this.articleStructs.Clear();

            for (int userIndex = 0; userIndex < this.Accounts.Count(); userIndex++)
            {
                int userID = this.Accounts[userIndex].ID;
                ConsumptionStruct userStruct = new ConsumptionStruct();
                userStruct.id = userID;
                //this.userDict.Add(userID, userIndex);
                this.userDict.Add(userID, userStruct);
                this.userStructs.Add(userStruct);
            }

            for (int articleIndex = 0; articleIndex < this.Articles.Count(); articleIndex++)
            {
                int articleID = this.Articles[articleIndex].ID;
                ConsumptionStruct articleStruct = new ConsumptionStruct();
                articleStruct.id = articleID;
                //this.articleDict.Add(articleID, articleIndex);
                this.articleDict.Add(articleID, articleStruct);
                this.articleStructs.Add(articleStruct);
            }
        }



        public override int[] getAccountOrder()
        {
            int[] users = new int[this.userStructs.Count];
            for (int i = 0; i < this.userStructs.Count; i++)
            {
                users[i] = this.userStructs.ElementAt(i).id;
            }
            return users;
        }

        public override int[] getArticleOrder()
        {
            int[] articles = new int[this.articleStructs.Count];
            for (int i = 0; i < this.articleStructs.Count; i++)
            {
                articles[i] = this.articleStructs.ElementAt(i).id;
            }
            return articles;
        }
    }
}