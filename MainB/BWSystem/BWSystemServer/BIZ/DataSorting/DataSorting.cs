﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ
{
    abstract public class DataSorting
    {
        private DateTime sortTime;
        private List<Account> accounts;
        private List<Article> articles;
        private DBAccessManagement DAM;
        private int resortInterval;


        public DataSorting(DBAccessManagement DAM)
        {
            this.DAM = DAM;
            this.sortTime = new DateTime(1999, 01, 01);
            this.resortInterval = (int)ServerUtil.getConfigValue("ResortInterval", typeof(int));
        }

        

        public List<Article> Articles
        {
            get { return articles; }
        }


        public List<Account> Accounts
        {
            get { return accounts; }
        }


        public DateTime SortTime
        {
            get { return sortTime; }
            set { sortTime = value; }
        }


        public abstract void sort();

        public abstract int[] getAccountOrder();

        public abstract int[] getArticleOrder();

        public void updateFields()
        {
            this.accounts = this.DAM.getAccountList_Active();
            this.articles = this.DAM.getArticleList_Active();
        }

        public bool isSorted
        {
            get
            {
                bool sorted = false;
                TimeSpan timeSpan = DateTime.Now - this.sortTime;
                
                if (this.resortInterval > (int)timeSpan.TotalSeconds)
                {
                    sorted = true;
                }
                return sorted;
            }
        }
    }
}