﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ
{
    internal struct PresenceStruct
    {
        internal int id;
        internal DateTime timeStamp;
    }


    public class PresenceSorting : DataSorting
    {
        private DBAccessManagement DAM;
        private List<PresenceStruct> userStructs;
        private List<PresenceStruct> articleStructs;


        public PresenceSorting(DBAccessManagement DAM)
            : base(DAM)
        {
            this.DAM = DAM;

            this.initFields();
            this.syncWithBooking();
        }


        public void addBooking(BookingData booking, Object o)
        {
            this.initFields();
            PresenceStruct userStruct = userStructs.Find(x => x.id == booking.Customer);
            userStruct.timeStamp = DateTime.Now; //TODO does this work?

            foreach (BookingAA tuple in booking.Booking)
            {
                PresenceStruct artStruct = articleStructs.Find(x => x.id == tuple.Article);
                artStruct.timeStamp = DateTime.Now;
            }
        }

        public void userCame(Account account, Object o)
        {
            this.syncAccounts();
            PresenceStruct userStruct = userStructs.Find(x => x.id == account.ID);
            userStruct.timeStamp = DateTime.Now; //TODO does this work?
        }
        

        public override void sort()
        {
            userStructs.OrderByDescending(x => x.timeStamp.Ticks);
            articleStructs.OrderByDescending(x => x.timeStamp.Ticks);

            this.SortTime = new DateTime(DateTime.Now.Ticks);
        }



        private void initFields()
        {
            if (this.articleStructs == null && this.userStructs == null)
            {
                this.userStructs = new List<PresenceStruct>();
                this.articleStructs = new List<PresenceStruct>();
            }

            this.updateFields();
            this.syncAccounts();
            this.syncArticles();
        }


        private void syncAccounts()
        {
            this.userStructs.OrderBy(x => x.id);
            this.Accounts.OrderBy(x => x.ID);

            int i = 0;
            while (i < this.Accounts.Count)
            {
                if (i >= this.userStructs.Count)
                {
                    PresenceStruct newStruct = new PresenceStruct();
                    newStruct.id = this.Accounts[i].ID;
                    newStruct.timeStamp = new DateTime(DateTime.Now.Ticks);
                    userStructs.Add(newStruct);

                    i++;
                }
                else if (this.userStructs[i].id == this.Accounts[i].ID)
                {
                    i++;
                }
                else if (this.userStructs[i].id > this.Accounts[i].ID)
                {
                    PresenceStruct newStruct = new PresenceStruct();
                    newStruct.id = this.Accounts[i].ID;
                    newStruct.timeStamp = new DateTime(DateTime.Now.Ticks);
                    userStructs.Add(newStruct);
                    this.userStructs.Add(newStruct);
                    this.userStructs.OrderBy(x => x.id);
                }
                else if (this.userStructs[i].id < this.Accounts[i].ID)
                {
                    this.userStructs.RemoveAt(i);
                }
            }
        }

        
        private void syncArticles()
        {
            this.articleStructs.OrderBy(x => x.id);
            this.Articles.OrderBy(x => x.ID);

            int i = 0;
            while (i < this.Articles.Count)
            {
                if (i >= this.articleStructs.Count)
                {
                    PresenceStruct newStruct = new PresenceStruct();
                    newStruct.id = this.Articles[i].ID;
                    newStruct.timeStamp = new DateTime(DateTime.Now.Ticks);
                    articleStructs.Add(newStruct);

                    i++;
                }
                else if (this.articleStructs[i].id == this.Articles[i].ID)
                {
                    i++;
                }
                else if (this.articleStructs[i].id > this.Articles[i].ID)
                {
                    PresenceStruct newStruct = new PresenceStruct();
                    newStruct.id = this.Articles[i].ID;
                    newStruct.timeStamp = new DateTime(DateTime.Now.Ticks);
                    articleStructs.Add(newStruct);
                    this.articleStructs.Add(newStruct);
                    this.articleStructs.OrderBy(x => x.id);
                }
                else if (this.articleStructs[i].id < this.Articles[i].ID)
                {
                    this.articleStructs.RemoveAt(i);
                }
            }
        }


        private void syncWithBooking()
        {
            DateTime oldTime = new DateTime(1890, 10, 4);
            for (int i = 0; i < this.userStructs.Count; i++)
            {
                PresenceStruct presStruct = userStructs[i];
                presStruct.timeStamp = oldTime;
            }

            for (int i = 0; i < this.articleStructs.Count; i++)
            {
                PresenceStruct presStruct = articleStructs[i];
                presStruct.timeStamp = oldTime;
            }

            int days = (int)ServerUtil.getConfigValue("SortingBookingIntervalDay", typeof(int));
            int hours = (int)ServerUtil.getConfigValue("SortingBookingIntervalHour", typeof(int));
            int mins = (int)ServerUtil.getConfigValue("SortingBookingIntervalMin", typeof(int));
            DateTime bookingIntervall = DateTime.Now.Subtract(new TimeSpan(days, hours, mins, 0));
            List<BookingData> bookings = DatabaseAccessManagement.DAM_Booking.getBookingDataList(bookingIntervall);
            bookings.OrderByDescending(x => x.TimeStamp);

            for (int i = 0; i < bookings.Count && i < 100; i++)
            {
                int account = bookings[i].Customer;
                if (account != 0)
                {
                    PresenceStruct presStruct = this.userStructs.Find(x => x.id == account);
                    presStruct.timeStamp = bookings[i].TimeStamp;
                }
                foreach (BookingAA tuple in bookings[i].Booking)
                {
                    int article = tuple.Article;
                    if (article != 0)
                    {
                        PresenceStruct presStruct = this.articleStructs.Find(x => x.id == article);
                        presStruct.timeStamp = bookings[i].TimeStamp;
                    }
                }
            }
        }


        public override int[] getAccountOrder()
        {
            int[] users = new int[this.userStructs.Count()];
            for (int i = 0; i < this.userStructs.Count(); i++)
            {
                users[i] = userStructs[i].id;
            }
            return users;
        }


        public override int[] getArticleOrder()
        {
            int[] articles = new int[this.articleStructs.Count()];
            for (int i = 0; i < this.articleStructs.Count(); i++)
            {
                articles[i] = articleStructs[i].id;
            }
            return articles;
        }
    }
}