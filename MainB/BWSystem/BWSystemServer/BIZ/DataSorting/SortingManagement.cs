﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ
{
    public enum SortingMethod
    {
        alphabetical,
        consumption,
        presence,
    }


    public class SortingManagement
    {
        private AlphabeticalSorting alpaSort;
        private ConsumptionSorting consumpSort;
        private PresenceSorting presSort;

        public SortingManagement(DBAccessManagement DAM)
        {
            this.alpaSort = new AlphabeticalSorting(DAM);
            this.consumpSort = new ConsumptionSorting(DAM);
            this.presSort = new PresenceSorting(DAM);
        }

        public int[] sort(SortingMethod sortingMethod, bool user)
        {
            int[] order;
            DataSorting sortingAlgo;
            switch (sortingMethod)
            {
                case SortingMethod.alphabetical:
                    sortingAlgo = this.alpaSort;
                    break;
                case SortingMethod.consumption:
                    sortingAlgo = this.consumpSort;
                    break;
                case SortingMethod.presence:
                    sortingAlgo = this.presSort;
                    break;
                default:
                    sortingAlgo = this.alpaSort;
                    break;
            }

            if (!sortingAlgo.isSorted)
            {
                sortingAlgo.sort();
            }
            order = user ? sortingAlgo.getAccountOrder() : sortingAlgo.getArticleOrder();
            return order;
        }
    }
}