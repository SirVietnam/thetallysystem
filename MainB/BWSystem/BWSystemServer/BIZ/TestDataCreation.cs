﻿using BWSystemServer.BIZ;
using BWSystemServer.BIZ.DatabaseAccessManagement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace BWSystemServer
{
    internal class TestDataCreation
    {

        internal TestDataCreation()
        {

        }

        [Conditional("DEBUG")]
        internal void addBookings(int bookingCount, double cancelChance)
        {
            //this.randomBookings(bookingCount);
            this.equalBookings(bookingCount);
        }

        /*
            BOOKINGS SECTION
        */
        [Conditional("DEBUG")]
        private void equalBookings(int bookingCount)
        {
            DBAccessManagement dbam = DBAccessManagement.Instance;
            List<Account> accounts = dbam.getAccountList_Active();
            List<Article> articles = dbam.getArticleList_Active();
            int accountCount = accounts.Count;
            int articleCount = articles.Count;

            Random random = new Random();

            for (int i = 0; i < bookingCount; i++)
            {
                int customerID = accounts.ElementAt(i % accountCount).ID;
                int? userID = null;

                BookingData booking = new BookingData(userID, customerID);
                int articleID = articles.ElementAt(i % articleCount).ID;
                int articleAmount = 1;
                booking.Booking.Add(new BookingAA(articleID, articleAmount));


                booking.TimeStamp = DateTime.Now;
                DAM_Booking.addBooking(booking);
            }
        }

        [Conditional("DEBUG")]
        private void randomBookings(int bookingCount)
        {
            DBAccessManagement dbam = DBAccessManagement.Instance;
            List<Account> accounts = dbam.getAccountList_Active();
            List<Article> articles = dbam.getArticleList_Active();
            int accountCount = accounts.Count;
            int articleCount = articles.Count;

            Random random = new Random();

            for (int i = 0; i < bookingCount; i++)
            {
                int customerID = accounts.ElementAt(random.Next(0, accountCount)).ID;
                int? userID = null;
                if (random.NextDouble() < 0.5)
                {
                    userID = accounts.ElementAt(random.Next(0, accountCount)).ID;
                }

                int bookings = Convert.ToInt32(Math.Floor(Math.Exp(random.NextDouble() * 5.0) / Math.Exp(3.0))) + 1;

                BookingData booking = new BookingData(userID, customerID);
                for (int j = 0; j < bookings; j++)
                {
                    int articleID = articles.ElementAt(random.Next(0, articleCount)).ID;
                    int articleAmount = Convert.ToInt32(Math.Floor(Math.Exp(random.NextDouble() * 5.0) / Math.Exp(3.0))) + 1;
                    booking.Booking.Add(new BookingAA(articleID, articleAmount));
                }

                booking.TimeStamp = DateTime.Now;
                DAM_Booking.addBooking(booking);
            }
        }



        /*
            INVENTORY SECTION
        */
        [Conditional("DEBUG")]
        internal void addInventory(double maxDeviation)
        {
            this.addLossOneInventory();
            //this.addRandomInventory(maxDeviation);
        }

        [Conditional("DEBUG")]
        internal void addLossOneInventory()
        {
            DBAccessManagement dbam = DBAccessManagement.Instance;
            List<Article> articles = dbam.getArticleList_Active();

            Stocktaking stocktaking = new Stocktaking(DateTime.Now);

            foreach (Article article in articles)
            {
                stocktaking.addItem(article.ID, article.Stock, article.Stock - 1);
            }

            dbam.addStocktaking(stocktaking);
        }

        [Conditional("DEBUG")]
        internal void addRandomInventory(double maxDeviation)
        {
            DBAccessManagement dbam = DBAccessManagement.Instance;
            List<Article> articles = dbam.getArticleList_Active();
            Random random = new Random();

            Stocktaking stocktaking = new Stocktaking(DateTime.Now);

            foreach (Article article in articles)
            {
                int preSign = random.NextDouble() > 0.00 ? 1 : -1;
                int deviation = Convert.ToInt32(random.NextDouble() * maxDeviation * Convert.ToDouble(article.Stock));
                int finalStock = article.Stock + deviation * preSign;
                stocktaking.addItem(article.ID, article.Stock, finalStock);
            }

            dbam.addStocktaking(stocktaking);
        }



        /*
            SHIPMENT SECTION
        */
        [Conditional("DEBUG")]
        internal void addShipment(int averageAmount, double maxDeviation)
        {
            this.addAllShipment(averageAmount);
            //this.addRandomShipment(averageAmount, maxDeviation);
        }


        [Conditional("DEBUG")]
        internal void addAllShipment(int amountToAdd)
        {
            DBAccessManagement dbam = DBAccessManagement.Instance;
            List<Article> articles = dbam.getArticleList_All();

            Shipment shipment = new Shipment(0, "TestDataCreator", DateTime.Now);
            shipment.addArticleList(articles);

            foreach (Article article in articles)
            {
                shipment.addArticleAmount(article, amountToAdd);
            }

            dbam.reportShipment(shipment);
        }

        [Conditional("DEBUG")]
        internal void addRandomShipment(int averageAmount, double maxDeviation)
        {
            DBAccessManagement dbam = DBAccessManagement.Instance;
            List<Article> articles = dbam.getArticleList_Active();
            Random random = new Random();

            Shipment shipment = new Shipment(0, "TestDataCreator", DateTime.Now);
            shipment.addArticleList(articles);

            foreach (Article article in articles)
            {
                double preSign = random.NextDouble() > 0.30 ? 1.0 : -1.0;
                double deviation = ((double)averageAmount) * random.NextDouble() * maxDeviation;
                shipment.addArticleAmount(article, averageAmount + Convert.ToInt32(deviation * preSign));
            }

            dbam.reportShipment(shipment);
        }
    }
}