﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BWSystemServer.DAL;
using BWSystemServer.Classes;
using System.Diagnostics;
using System.IO;
using System.Web;
using BWSystemServer.BIZ.ParallelTask;
using BWSystemServer.DAL.AutoSettlement;
using System.Collections.Concurrent;
using BWSystemServer.DAL.PictureManager;
using BWSystemServer.BIZ.DatabaseAccessManagement;

namespace BWSystemServer.BIZ
{
    public class DBAccessManagement
    {
        private static DBAccessManagement instance;
        private static Account adminAccount = new Account(-1, "Admin", "istartor", "admin", null, -1, null, new Authentication(new byte[0], new byte[0], new long[0], "admin"), true, true, true);
        public static readonly DateTime zeroDateTime = new DateTime(1970, 01, 01);
        private static TaskManagement taskManager;



        private DAM_Account accountMan;
        private DAM_NonOrgLogin nonOrgLoginMan;
        private DAM_Article articleMan;
        private DAM_Event eventMan;
        private DAM_Booking bookingMan;
        private DAM_Shipment shipmentMan;

        private DBM_Cancellation DBM_Can;
        private DBM_Settlement DBM_Set;
        private DBM_Stocktaking DBM_Stock;
        //private DBM_Shipment DBM_Shipment;
        private DBM_ShipmentData DBM_ShipmentData;
        private DBM_AnnualStatement DBM_AnnualStatement;
        //private DAM_ClientDevice clientDeviceMan;

        private SortingManagement SM;
        private PictureManager userPictures;
        private PictureManager articlePictures;


        private bool accountUpdateRequired;
        private bool articleUpdateRequired;
        //private bool useLDAP;
        private bool pushToLDAP_Fotos;
        //private bool pushToLDAP_CardNumber;
        //private bool useTokenSecurity;


        private DBAccessManagement()
        {
            this.initLogging();

            accountMan = new DAM_Account(this.mapAccountsAndFavourites);
            nonOrgLoginMan = new DAM_NonOrgLogin();
            articleMan = new DAM_Article(this.updatePrizesPreRequirementsMet);
            eventMan = new DAM_Event();
            bookingMan = new DAM_Booking(this.articleMan.changeStock);
            shipmentMan = new DAM_Shipment(this.articleMan.changeStock);
            //clientDeviceMan = new DAM_ClientDevice();

            DBM_Can = new DBM_Cancellation();
            DBM_Set = new DBM_Settlement();
            DBM_Stock = new DBM_Stocktaking();
            DBM_AnnualStatement = new DBM_AnnualStatement();

            this.accountUpdateRequired = true;
            this.articleUpdateRequired = true;

            SM = new SortingManagement(this);
            taskManager = new TaskManagement();

            this.userPictures = new PictureManager("Users", true);
            this.articlePictures = new PictureManager("Articles", false);

            bool useLDAP = (bool)ServerUtil.getConfigValue("UseLDAP", typeof(bool));
            this.pushToLDAP_Fotos = useLDAP && (bool)ServerUtil.getConfigValue("PushToLDAP_Fotos", typeof(bool));
            //this.pushToLDAP_CardNumber = this.useLDAP && (bool)ServerUtil.getConfigValue("PushToLDAP_CardNumber", typeof(bool));
            //this.useTokenSecurity = (bool)ServerUtil.getConfigValue("UseSecurityTokenEvaluation", typeof(bool));
        }


        private void initLogging()
        {
            string serverLogPath = (string)ServerUtil.getAbsoluteFilePath("ServerLogPath");
            SimpleLog.SetLogFile(serverLogPath, @"ServerLog_");
            SimpleLog.Info("------------------- Starting the Server ------------------------", false);
        }


        private void registerCrashHandling()
        {
            AppDomain.CurrentDomain.UnhandledException += GlobalCrashHandler;
        }

        private void GlobalCrashHandler(object sender, UnhandledExceptionEventArgs e)
        {
            string preSeperator =
                    "-----------------------------------------------------------------------------------------------------------------\n" +
                    "-----------------------------------------------------------------------------------------------------------------\n" +
                    "--------------------------------------------- GLOBAL CRASH REPORT -----------------------------------------------\n" +
                    "-----------------------------------------------------------------------------------------------------------------\n" +
                    "-----------------------------------------------------------------------------------------------------------------";
            SimpleLog.Log(preSeperator, SimpleLog.Severity.Error, false);
            SimpleLog.Log((Exception)e.ExceptionObject, false);

            if (e.IsTerminating)
            {
                SimpleLog.Log("Application will TERMINATE!", SimpleLog.Severity.Error, false, 0);
            }
        }

        /*
         * Singelton
         */
        public static DBAccessManagement Instance
        {
            get
            {
                if (DBAccessManagement.instance == null)
                {
                    DBAccessManagement.instance = new DBAccessManagement();
                }
                return DBAccessManagement.instance;
            }
        }


        /*
         * Account
         */
        #region Account
        internal List<Account> getAccountList_All()
        {
            return this.accountMan.AccountList_All;
        }


        internal List<Account> getAccountList_Active()
        {
            return this.accountMan.AccountList_Active;
        }

        internal Account getAccount(int accountID)
        {
            return this.accountMan.getAccount(accountID);
        }

        /// <summary>
        /// Adds an user account. Use this one for organization accounts.
        /// </summary>
        /// <param name="user">Contains all account informations</param>
        /// <returns></returns>
        internal bool addAccount(Account user)
        {
            bool accountAdded = false;
            byte[] userPicture = null;

            if (user.PictureURL == null && user.ID <= 0 && !DAM_NonOrgLogin.isNonOrgAccount(user.ID))
            {
                userPicture = LDAPSync.getAccountPicture(user);
                if (userPicture != null)
                {
                    string pictureName = this.userPictures.addPicture(userPicture, ServerUtil.accountToFilename(user));
                    user.PictureURL = pictureName != null ? pictureName : null;
                }
            }

            accountAdded = this.accountMan.addAccount(user);

            if (!accountAdded && userPicture != null)
            {
                this.userPictures.deletePicture(user.PictureURL);
            }

            return accountAdded;
        }

        /// <summary>
        /// Adds an user account. Use this one for Non-Organization accounts.
        /// </summary>
        /// <param name="user">Contains all account informations</param>
        /// <param name="password">The users password</param>
        /// <param name="mailAddress">The users mail address</param>
        /// <returns></returns>
        internal bool addAccount(Account user, string password, string mailAddress)
        {
            bool accountAdded = false;
            bool nonOrgAuthAdded = false;

            accountAdded = this.accountMan.addAccount(user);
            if (accountAdded)
                nonOrgAuthAdded = this.nonOrgLoginMan.addLogin(user.ID, password, mailAddress);
            if (!nonOrgAuthAdded)
            {
                //TODO: Delete account
            }
            return accountAdded && nonOrgAuthAdded;
        }


        internal bool isValidLogin(string login)
        {
            return DAM_NonOrgLogin.isValidLogin(login) && !this.accountMan.loginAlreadyExists(login);
        }
        #endregion


        /*
         * Article
         */
        #region Articles
        internal List<Article> getArticleList_All()
        {
            return this.articleMan.ArticleList_All;
        }

        internal List<Article> getArticleList_Active()
        {
            return this.articleMan.ArticleList_Active;
        }

        internal List<Article> getEventArticleList(int eventID)
        {
            return this.articleMan.getEventArticleList(eventID);
        }

        internal bool addArticle(Article newArticle)
        {
            return this.articleMan.addArticle(newArticle);
        }

        internal void updatePrizes(List<Tuple<int, float>> prizeUpdates)
        {
            this.articleMan.updatePrizes(prizeUpdates);
        }
        #endregion


        /*
         * Event
         */
        #region Events
        internal List<Event> getEventList_All()
        {
            return this.eventMan.EventList_All;
        }

        internal List<Event> getEventList_Future()
        {
            return this.eventMan.EventList_Future;
        }

        internal bool addEvent(Event eventToAdd)
        {
            return this.eventMan.addEvent(eventToAdd);
        }

        internal bool makeEventSettlement(int eventID)
        {
            return this.eventMan.makeEventSettlement(eventID);
        }
        #endregion


        /*
         * LOGIN
         */
        internal Account getAccountToLogin(string login)
        {
            string lowerLogin = login.ToLower();

            Account accToID = null;
            Tuple<int, string>[] id_loginList = this.accountMan.getAccountID_LoginList();

            IEnumerable<Tuple<int, string>> ids = id_loginList.Where(x => x.Item2 != null && x.Item2.ToLower() == lowerLogin);

            if (ids.Count() == 1)
            {
                int accID = ids.First().Item1;
                accToID = accountMan.getAccount(accID);
            }
            else if (login == "admin")
            {
                if (this.accountMan.AccountList_All.FirstOrDefault(x => x.IsSuperuser) == null)
                {
                    accToID = DBAccessManagement.adminAccount;
                }
            }

            return accToID;
        }


        internal bool isValidPassword(Account user, string password)
        {
            if (DAM_NonOrgLogin.isNonOrgAccount(user.ID))
            {
                return this.nonOrgLoginMan.isCorrectPassword(user.ID, password);
            }
            else
            {
                return LDAPSync.isValidPassword(user.Authentication.LoginName, password);
            }
        }


        /*
         * LDAP Section
         */
        #region LDAP
        public void syncLDAP_DB()
        {
            IEnumerable<Account> currentAccounts = this.accountMan.AccountList_All.Where(x => !DAM_NonOrgLogin.isNonOrgAccount(x.ID));
            List<Account> accsToUpdate = LDAPSync.getModifiedAccounts(currentAccounts);

            foreach (Account account in accsToUpdate)
            {
                this.accountMan.addAccount(account);
            }
            this.accountUpdateRequired = true;
        }


        internal Account getOrgAccount(string userID, string userPW)
        {
            Account ldapAccount = LDAPSync.getAccountData(userID, userPW);
            if (ldapAccount != null)
            {
                Account existingAccount = this.getAccountToLogin(userID);
                if (existingAccount != null)
                {
                    ldapAccount = ServerUtil.mergeAccounts(existingAccount, ldapAccount, existingAccount.IsAvailable, ldapAccount.IsSuperuser);
                }
            }
            return ldapAccount;
        }


        internal void addMissingLDAPAccounts()
        {
            //Get all org accounts
            List<Account> currentOrgAccounts = this.accountMan.AccountList_All.Where(x => x.Authentication.LoginName != null && !DAM_NonOrgLogin.isNonOrgAccount(x.ID)).ToList();
            //Get missing accounts
            List<Account> missingAccounts = LDAPSync.getMissingAccounts(currentOrgAccounts);

            foreach (Account account in missingAccounts)
            {
                this.addAccount(account);
            }
        }


        internal List<Tuple<Account, int>> getAutoSettlementIDs()
        {
            //Get all org accounts
            List<Account> accounts = this.accountMan.AccountList_All.Where(x => x.Authentication.LoginName != null && !DAM_NonOrgLogin.isNonOrgAccount(x.ID)).ToList();
            return LDAPSync.getAutoSettlementIDs(accounts);
        }
        #endregion



        /*
         * AnnualStatement Section
         */
        #region AnnualStatement
        internal List<AnnualStatement> getAnnualStatements()
        {
            return DBM_AnnualStatement.getAnnualStatementList();
        }


        public bool makeAnnualStatement(string name)
        {
            //Get Start Time
            DateTime startDate = this.getLastAnnualAccountStartDate();

            //Check if AnualStatement can be made
            Settlement lastSettlement = this.getSettlementListWithoutData().Last();
            Stocktaking lastStocktaking = this.getStocktakingList().Last();

            if (!this.newAnnualAccountPrerequirementsMet(lastStocktaking, lastSettlement)
                || !DBAccessManagement.timeRequirementMet(lastStocktaking.TimeStamp))
            {
                return false;
            }

            //Get End Date
            DateTime endDate = lastStocktaking.TimeStamp;

            //Create AnnualStatement & add to DB
            AnnualStatement currentAnnualStatement = new AnnualStatement(name, startDate, endDate);
            currentAnnualStatement = this.DBM_AnnualStatement.addAnnualStatement(currentAnnualStatement);

            //Collect Data
            Stocktaking initialStock = this.getInitialStock(startDate);
            Shipment[] shipments = DAM_Shipment.getShipmentList().Where(x => x.Datetime.Ticks > startDate.Ticks && x.Datetime.Ticks <= endDate.Ticks).ToArray();
            Stocktaking[] stocktakings = this.getStocktakingList().Where(x => x.TimeStamp.Ticks > startDate.Ticks && x.TimeStamp.Ticks <= endDate.Ticks).ToArray();
            Settlement[] settlements = this.getSettlementListWithoutData().Where(x => x.StartDate.Ticks >= startDate.Ticks && x.EndDate.Ticks <= endDate.Ticks).ToArray();
            Event[] events = eventMan.EventList_All.Where(x => x.End.Ticks >= startDate.Ticks && x.End.Ticks <= endDate.Ticks).ToArray();

            //Add Settlement Data
            for (int i = 0; i < settlements.Length; i++)
            {
                int settlementID = settlements[i].SettlementID;
                settlements[i] = this.getSettlement(settlementID);
            }

            //Add Event Data
            foreach (Event currentEvent in events)
            {
                List<BookingData> bookings = DAM_Booking.getEventBookings(currentEvent.EventID);
                if (bookings.Count > 0)
                {
                    currentEvent.makeEventSettlement(bookings, this.articleMan.ArticleList_All, this.accountMan.AccountList_All);
                }
            }
            //Add Data
            currentAnnualStatement.addData(this.articleMan.ArticleList_All, this.accountMan.AccountList_All, shipments, settlements, stocktakings, events, initialStock);

            //Write AnnualStatement to Disk
            string configValue = "StandardAnnualStatementExportLocation";
            string absoluteFilePath = ServerUtil.getAbsoluteFilePath(configValue);
            string fileName = ServerUtil.getFileName(currentAnnualStatement);
            currentAnnualStatement.writeToExcelFile(absoluteFilePath + fileName);

            //Migrate Data to Archive
            this.moveToArchive(startDate, endDate);

            return true;
        }


        private Stocktaking getInitialStock(DateTime startDate)
        {
            Stocktaking initialStock = null;

            //TODO: Does this fix work?
            List<Stocktaking> potentialStocks = DAM_ArchiveManagement.Instance.getArchiveStocktakingList().Where(x => x.TimeStamp.Ticks <= startDate.Ticks).ToList();
            if (potentialStocks.Count > 0)
            {
                initialStock = potentialStocks.OrderByDescending(x => x.TimeStamp.Ticks).First();
            }
            else
            {
                initialStock = new Stocktaking(startDate);
                foreach (Article article in this.articleMan.ArticleList_All)
                {
                    initialStock.addItem(article.ID, 0, 0);
                }
            }

            return initialStock;
        }


        internal DateTime getLastAnnualAccountStartDate()
        {
            AnnualStatement lastAnnualStatement = this.DBM_AnnualStatement.getLastAnnualStatement();
            DateTime startDate = lastAnnualStatement != null ? startDate = lastAnnualStatement.EndDate : DBAccessManagement.zeroDateTime;
            return startDate;
        }

        internal AnnualStatement getLastAnnualAccount()
        {
            return this.DBM_AnnualStatement.getLastAnnualStatement();
        }


        internal bool newAnnualAccountPrerequirementsMet(Settlement lastSettlement)
        {
            return DBAccessManagement.timeRequirementMet(lastSettlement.EndDate);
        }


        internal bool newAnnualAccountPrerequirementsMet(Stocktaking lastStocktaking)
        {
            return DBAccessManagement.timeRequirementMet(lastStocktaking.TimeStamp);
        }


        internal bool newAnnualAccountPrerequirementsMet(Stocktaking lastStocktaking, Settlement lastSettlement)
        {
            double maxInterActionTime = (double)ServerUtil.getConfigValue("InterAction_MaxTime_Minutes", typeof(double));
            return (lastStocktaking.TimeStamp - lastSettlement.EndDate).Ticks <= TimeSpan.FromMinutes(maxInterActionTime).Ticks;
        }
        #endregion


        /*
        * Settlement Section
        */
        #region Settelement
        public bool makeSettlement(bool useLastStocktakingDate) //set Flag true IF you are makeing a settlement for AnnualAccount
        {
            //Determine EndDate
            DateTime endDateTime = DateTime.Now;
            if (useLastStocktakingDate)
            {
                Stocktaking lastStocktaking = this.getStocktakingList().Last();
                if (DBAccessManagement.timeRequirementMet(lastStocktaking.TimeStamp))
                {
                    endDateTime = lastStocktaking.TimeStamp;
                }
                else
                {
                    SimpleLog.Log("Requirements for 'useLlastStocktakingDate' were not matched!");
                    return false;
                }
            }

            //Determine StartDate
            DateTime startDateTime = this.getLastSettlementEndDate();

            //Create Settlement at DB and get ID
            int settlementID = DBM_Set.addSettlement(startDateTime, endDateTime);

            //Fill Settlement with booking data
            List<BookingData> allBookings = DAM_Booking.getBookingDataList(startDateTime, endDateTime);
            List<BookingData> validBookings = allBookings.Where(x => x.EventBooking == null).ToList();
            Settlement settlement = new Settlement(settlementID, startDateTime, endDateTime);
            settlement.addSettlementData(validBookings, this.accountMan.AccountList_All, this.articleMan.ArticleList_All);

            //Add SettlementData to DB
            DBM_Set.addSettlementData(settlement, this.accountMan.AccountList_All);

            //Write File to Disk
            string configValue = "StandardSettlementExportLocation";
            string absoluteFilePath = ServerUtil.getAbsoluteFilePath(configValue);
            string fileName = ServerUtil.getFileName(settlement);
            string completeFilePath = absoluteFilePath + fileName;
            try
            {
                settlement.writeToExcelFile(completeFilePath);
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
            }

            //Make AutoSettlement
            if ((bool)ServerUtil.getConfigValue("UseAutoSettlement", typeof(bool)))
            {
                IAutoSettlement autoSettlement = this.getAutoSettlement();
                List<Tuple<Account, int>> accountInfos = this.getAutoSettlementIDs();
                List<Account> unbookedAccounts = autoSettlement.makeAutoSettlement(accountInfos, settlement, autoSettlement.getOpenConnection());

                foreach (Account acc in unbookedAccounts)
                {
                    SimpleLog.Log("Account with Login: " + acc.Authentication.LoginName + " was not autobooked correctly on Settlement with ID: " + settlement.SettlementID);
                }
                settlement.addFailedAutoSettlements(completeFilePath, unbookedAccounts);
            }
            return true;
        }


        public Settlement getSettlementWithoutData(int settlementID)
        {
            List<Settlement> settlements = this.getSettlementListWithoutData();
            Settlement settlement = settlements.First(x => x.SettlementID == settlementID);
            return settlement;
        }


        public Settlement getSettlement(int settlementID)
        {
            Settlement settlement = this.getSettlementWithoutData(settlementID);
            List<BookingData> bookingsToSetID = DAM_Booking.getBookingDataList(settlement.StartDate, settlement.EndDate);
            settlement.addSettlementData(bookingsToSetID, this.accountMan.AccountList_All, this.articleMan.ArticleList_All);
            return settlement;
        }


        public List<Settlement> getSettlementListWithoutData()
        {
            return DBM_Set.getSettlementList();
        }


        public Settlement getLastSettlementWithoutData()
        {
            List<Settlement> settlements = this.getSettlementListWithoutData();
            Settlement lastSettlement = settlements.Count == 0 ? null : settlements.Last();
            return lastSettlement;
        }


        public DateTime getLastSettlementEndDate()
        {
            DateTime endDateTime = DBAccessManagement.zeroDateTime;

            List<Settlement> oldSettlements = this.DBM_Set.getSettlementList();
            if (oldSettlements.Count == 0)
            {
                oldSettlements = this.DBM_Set.getArchiveSettlementList();
            }

            if (oldSettlements.Count != 0)
            {
                endDateTime = oldSettlements.Last().EndDate;
            }
            return endDateTime;
        }


        public double getPayment(int settlementID, int accountID)
        {
            return this.DBM_Set.getPaymentData(settlementID, accountID);
        }


        private IAutoSettlement getAutoSettlement()
        {
            IAutoSettlement autoSet = null;
            string autoSetName = (string)ServerUtil.getConfigValue("LDAP_AutoSettlement_Type", typeof(string));
            switch (autoSetName)
            {
                case "Normannia":
                    autoSet = new NmAutoSettlement();
                    break;
            }
            return autoSet;
        }


        internal bool updatePrizesPreRequirementsMet()
        {
            DateTime lastSettlementEnd = this.getLastSettlementEndDate();
            return DBAccessManagement.timeRequirementMet(lastSettlementEnd);
        }
        #endregion


        /*
         * Stocktaking Section
         */
        #region Stocktaking
        public bool addStocktaking(Stocktaking stocktaking)
        {
            this.articleUpdateRequired = true;
            DBM_Stock.addStocktaking(stocktaking);

            foreach (Tuple<int, int, int> stocktakingData in stocktaking.Inventory)
            {
                int loss = stocktakingData.Item2 - stocktakingData.Item3;
                this.articleMan.changeStock(stocktakingData.Item1, loss);
            }
            return true;
        }

        //public List<Stocktaking> getArchiveStocktakingList()
        //{

        //}

        public List<Stocktaking> getStocktakingList()
        {
            return DBM_Stock.getStocktakingList();
        }

        public Stocktaking getLatestStocktaking()
        {
            List<Stocktaking> stocktakings = this.getStocktakingList();
            Stocktaking lastStocktaking = stocktakings.Count == 0 ? null : stocktakings.Last();
            return lastStocktaking;
        }
        #endregion


        /*
         * Shipment Section
         */
        #region Shipment
        public void reportShipment(Shipment shipment)
        {
            this.shipmentMan.reportShipment(shipment);
        }
        #endregion


        /*
         * Favourites Section
         */
        #region Favourites
        private void mapAccountsAndFavourites(List<Account> accounts)
        {
            int favDaysIntervall = (int)ServerUtil.getConfigValue("FavouriteDayInterval", typeof(int));
            DateTime startTimeStamp = new DateTime(DateTime.Now.Ticks);
            startTimeStamp = startTimeStamp.Subtract(new TimeSpan(favDaysIntervall, 0, 0, 0));

            List<BookingData> bookings = DAM_Booking.getBookingDataList(startTimeStamp, DateTime.Now);

            foreach (Account account in accounts)
            {
                account.Favourite = this.getFavourites(account, bookings);
            }
        }

        public int[] getFavourites(Account account, List<BookingData> bookings)
        {
            int accountID = account.ID;
            int favSize = (int)ServerUtil.getConfigValue("FavouriteSize", typeof(int));
            int[] favourites = new int[favSize];

            List<Article> articles = new List<Article>();
            foreach (Article article in this.articleMan.ArticleList_All)
            {
                Article articleClone = (Article)article.Clone();
                articleClone.Stock = 0;
                articles.Add(articleClone);
            }

            foreach (BookingData booking in bookings)
            {
                if (booking.Customer == accountID)
                {
                    foreach (BookingAA tuple in booking.Booking)
                    {
                        Article article = articles.Find(item => item.ID == tuple.Article);
                        article.Stock += tuple.Amount;
                    }
                }
            }
            articles = articles.OrderBy(x => x.Stock).ToList();

            for (int i = 0; i < favSize; i++)
            {
                favourites[i] = articles[i].ID;
            }
            return favourites;
        }
        #endregion

        /*
         * Cancellation Section
         */
        #region Cancellation
        public bool cancelBooking(CancellationData cancellation)
        {
            bool cancelledOldBooking = false;
            bool enteredNewBooking = false;
            bool enteredCancellation = false;


            //Cancel Old Booking!
            List<BookingData> bookingToCancel = DAM_Booking.getBookingDataList(cancellation.CancelledBookingID, true);
            foreach (BookingData booking in bookingToCancel)
            {
                if (booking.ID == cancellation.CancelledBookingID && !booking.DataCancelled)
                {
                    cancelledOldBooking = this.bookingMan.enterCancellation(cancellation.CancelledBookingID, booking.Booking.Count);
                }
            }

            //Ener new Booking!
            if (cancellation.NewBooking != null && cancellation.NewBooking.Booking.Count > 0)
            {
                if (cancelledOldBooking && cancellation.NewBooking != null)
                {
                    int newBookingID = DAM_Booking.addBooking(cancellation.NewBooking);
                    if (newBookingID != -1)
                    {
                        enteredNewBooking = true;
                        cancellation.NewBookingID = newBookingID;
                    }

                }
            }

            //Enter Cancellation
            if (cancelledOldBooking)
            {
                int cancelID = this.DBM_Can.addCancellation(cancellation.CancelledBookingID, cancellation.NewBookingID, cancellation.User, cancellation.CancellationTime);
                enteredCancellation = (cancelID != -1);
            }

            return enteredCancellation;
        }
        #endregion


        /*
         * 
         * Sorting Order Part
         * 
         */
        #region Sorting
        internal int[] getOrder(SortingMethod sortingMethod, bool account)
        {
            return this.SM.sort(sortingMethod, account);
        }
        #endregion


        /*
         * Archiving
         */
        #region Archiving
        internal void moveToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            Debug.Assert(startDateTime.Ticks < endDateTime.Ticks);

            //COPY to Archive
            this.DBM_Can.copyDataToArchive(startDateTime, endDateTime);         //Cancellation,
            this.bookingMan.copyDataToArchive(startDateTime, endDateTime);      //BookingData,
                                                                                //this.DBM_Art.moveDataToArchive(startDateTime, endDateTime);       //PrizeHistory,
            this.DBM_Set.copyDataToArchive(startDateTime, endDateTime);         //SettlementData,//Settlement,
            this.shipmentMan.copyDataToArchive(startDateTime, endDateTime);     //ShipmentData & Shipment
            this.DBM_Stock.copyDataToArchive(startDateTime, endDateTime);       //Stocktaking
            this.eventMan.copyDataToArchive(startDateTime, endDateTime);        //Event,

            //DELETE from current Table
            this.DBM_Can.deleteDataFromTable(startDateTime, endDateTime);       //Cancellation,
            this.bookingMan.deleteDataFromTable(startDateTime, endDateTime);    //BookingData,
                                                                                //this.DBM_Art.moveDataToArchive(startDateTime, endDateTime);         //PrizeHistory,
            this.DBM_Set.deleteDataFromTable(startDateTime, endDateTime);       //SettlementData & Settlement,
            this.shipmentMan.deleteDataFromTable(startDateTime, endDateTime);   //ShipmentData & Shipment
            this.DBM_Stock.deleteDataFromTable(startDateTime, endDateTime);     //Stocktaking
            this.eventMan.deleteDataFromTable(startDateTime, endDateTime);      //Event,
        }
        #endregion


        /*
         * Picture Adding & Getting for Account & Article 
         */
        //////////////// USERS ////////////////////
        #region PictureUsers
        internal bool addUserPicture(byte[] picture, Account user)
        {
            bool userUpdated = false;
            string pictureName = this.userPictures.addPicture(picture, ServerUtil.accountToFilename(user));


            if (pictureName != null)
            {
                user.PictureURL = pictureName;
                userUpdated = this.accountMan.addAccount(user);

                if (this.pushToLDAP_Fotos && !DAM_NonOrgLogin.isNonOrgAccount(user.ID))
                {
                    //TODO: COMException (No connection to LDAP)
                    LDAPSync.addPicture(user, picture);
                }
                this.accountUpdateRequired = userUpdated;
            }

            return (userUpdated && pictureName != null);
        }


        internal byte[] getDefaultUserImage(int maxSize)
        {
            return this.userPictures.getDefalutImage(maxSize);
        }


        internal byte[] getUserImage(string pictureName, int maxSize)
        {
            return this.userPictures.getImage(pictureName, maxSize);
        }

        internal string getUserImageRelativePath(string pictureName)
        {
            return this.userPictures.getImageRelativePath(pictureName);
        }

        internal DateTime getUserImageDateTime(int accountID)
        {
            DateTime pictureDateTime = DateTime.MinValue;

            Account account = this.accountMan.AccountList_Active.FirstOrDefault(x => x.ID == accountID);
            if (account != null)
            {
                pictureDateTime = this.userPictures.getImageLastModified(ServerUtil.accountToFilename(account));
            }
            return pictureDateTime;
        }
        #endregion


        //////////////// ARTICLES ////////////////////
        #region ArticlePictures
        internal bool addArticlePicture(byte[] picture, Article product)
        {
            bool articleUpdated = false;
            string pictureName = this.articlePictures.addPicture(picture, ServerUtil.articleToFilename(product));

            if (pictureName != null)
            {
                product.PictureURL = pictureName;
                articleUpdated = this.articleMan.addArticle(product);

                this.articleUpdateRequired = articleUpdated;
            }

            return (articleUpdated && pictureName != null);
        }


        internal byte[] getDefaultArticleImage(int maxSize)
        {
            return this.articlePictures.getDefalutImage(maxSize);
        }


        internal byte[] getArticleImage(string pictureName, int maxSize)
        {
            return this.articlePictures.getImage(pictureName, maxSize);
        }


        internal string getArticleImageRelativePath(string pictureName)
        {
            return this.articlePictures.getImageRelativePath(pictureName);
        }

        internal DateTime getArticleImageDateTime(int articleID)
        {
            DateTime pictureDateTime = DateTime.MinValue;

            Article article = this.articleMan.ArticleList_All.FirstOrDefault(x => x.ID == articleID);
            if (article != null)
            {
                pictureDateTime = this.articlePictures.getImageLastModified(ServerUtil.articleToFilename(article));
            }
            return pictureDateTime;
        }
        #endregion


        /*
         * Helper/Tester
         */
        #region Tester&Helper
        internal bool updateRequired(DateTime lastDataUpdate)
        {
            //TODO: Check if this works properly
            return this.accountMan.isUpdateRequired() || this.articleMan.isUpdateRequired() || this.eventMan.isUpdateRequired();
        }

        private static bool timeRequirementMet(DateTime endDateTime)
        {
            double maxInterActionTime = (double)ServerUtil.getConfigValue("InterAction_MaxTime_Minutes", typeof(double));
            return (DateTime.Now - endDateTime).Ticks <= TimeSpan.FromMinutes(maxInterActionTime).Ticks;
        }

        [Conditional("DEBUG")]
        internal void cleanDBs()
        {
            this.DBM_Set.resetDatabases();
        }
        #endregion
    }
}
