﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public class BIZ_NameManagement
    {


        internal static List<Account> fillWithDisplayNames(ICollection<Account> unfilledAccounts)
        {
            HashSet<string> displayNames = new HashSet<string>();
            Dictionary<string, Account> accounts = new Dictionary<string, Account>();

            Queue<Account> orderedAccounts = new Queue<Account>();

            //Queue all accounts
            foreach (Account account in unfilledAccounts)
                orderedAccounts.Enqueue(account);

            //Handle all queued elements
            while (orderedAccounts.Any())
            {
                Account account = orderedAccounts.Dequeue();
                bool nameUnique = false;
                string currentDisplayName = account.DisplayName;

                while (!nameUnique)
                {
                    //Get displayName & check for existance
                    currentDisplayName = BIZ_NameManagement.getDisplayName(account, currentDisplayName);
                    nameUnique = displayNames.Add(currentDisplayName);

                    if (nameUnique)
                    {
                        //Add if displayName is unique
                        account.DisplayName = currentDisplayName;
                        accounts.Add(currentDisplayName, account);
                    }
                    else if (accounts.ContainsKey(currentDisplayName))
                    {
                        //If displayName does already exist && a element with the same name enlisted => requeue that element
                        Account requeueAccount = accounts[currentDisplayName];
                        orderedAccounts.Enqueue(requeueAccount);
                        accounts.Remove(currentDisplayName);
                    }
                    //else
                    //{
                    //    int i = 0;
                    //}
                }

            }

            return accounts.Values.ToList();
        }

        private static string getDisplayName(Account account, string previousTry)
        {
            string displayName = account.Nickname;
            if (previousTry == null)
            {
                if (displayName == null)
                {
                    displayName = account.Name;
                }
            }
            else
            {
                displayName = BIZ_NameManagement.getAdvancedDisplayName(account, previousTry);
            }
            return displayName;
        }


        private static string getAdvancedDisplayName(Account account, string previousTry)
        {
            string advancedDisplayName = account.Name + " " + account.Surname;

            if (previousTry.Equals(account.Name))
            {
                advancedDisplayName = advancedDisplayName.Substring(0, account.Name.Length + 2);
            }
            else
            {
                advancedDisplayName = advancedDisplayName.Substring(0, previousTry.Length);
            }
            advancedDisplayName += ".";
            return advancedDisplayName;
        }
    }
}