﻿using BWSystemServer.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public class DAM_Article : PeriodicDBUpdate<Article>
    {

        private static DBM_Article DBM_Article = new DBM_Article();

        private Func<bool> updatePrizesPreRequirementsMet;

        public DAM_Article(Func<bool> updatePrizesPreRequirementsMet) : base()
        {
            if (updatePrizesPreRequirementsMet == null)
            {
                throw new ArgumentNullException("updatePrizesPreRequirementsMet");
            }
            this.updatePrizesPreRequirementsMet = updatePrizesPreRequirementsMet;
        }


        public override bool isUpdateRequired()
        {
            TimeSpan ts = DateTime.Now - this.lastUpdated;
            return (this.updateRequired || ts > this.updateInterval);
        }

        protected override void updateItemList()
        {
            if (isUpdateRequired())
            {
                this.itemList = DBM_Article.getArticleList();

                this.lastUpdated = DateTime.Now;
                this.updateRequired = false;
            }
        }


        /*
         * Getter
         */ 
        internal List<Article> ArticleList_All
        {
            get
            {
                if (this.isUpdateRequired())
                {
                    this.updateItemList();
                }
                return new List<Article>(itemList);
            }
        }


        internal List<Article> ArticleList_Active
        {
            get
            {
                if (this.isUpdateRequired())
                {
                    this.updateItemList();
                }
                return itemList.Where(x => x.IsAvailable && !x.IsInventory).ToList();
            }
        }

        internal List<Article> ArticleList_Inventory
        {
            get
            {
                if (this.isUpdateRequired())
                {
                    this.updateItemList();
                }
                return itemList.Where(x => x.IsInventory).ToList();
            }
        }


        internal List<Article> getEventArticleList(int eventID)
        {
            List<Article> articles = new List<Article>();
            Event currentEvent = DAM_Event.getEvent(eventID);

            if (currentEvent != null)
            {
                Dictionary<int, Article> eventArticles = new Dictionary<int, Article>();

                //Add all active articles
                if (!currentEvent.ArticlesExclusive)
                {
                    List<Article> activeArticles = this.ArticleList_Active;
                    foreach (Article activeArticle in activeArticles)
                    {
                        eventArticles.Add(activeArticle.ID, activeArticle);
                    }
                }

                //Add cost-absorption articles
                List<Article> allArticles = this.ArticleList_All;
                foreach (int articleID in currentEvent.CostAbsorption)
                {
                    if (!eventArticles.ContainsKey(articleID))
                    {
                        Article articleToAdd = allArticles.First(x => x.ID == articleID);
                        eventArticles.Add(articleToAdd.ID, articleToAdd);
                    }
                }

                articles = eventArticles.Values.ToList();
            }
            else
            {
                string logMsg = "The Articles of EventID: " + eventID.ToString() + " was requested but does not exist!";
                SimpleLog.Log(logMsg);
                throw new ArgumentException(logMsg);
            }

            return articles;
        }

        /*
         * Add Article
         */ 
        public bool addArticle(Article newArticle)
        {
            bool addArticle = true;
            bool updatedArticle = false;

            if (newArticle.ID >= 0)
            {
                List<Article> articles = this.ArticleList_All;
                Article article = articles.FirstOrDefault(x => x.ID == newArticle.ID);

                if (article != null)
                {
                    DBM_Article.updateArticle(newArticle);
                    addArticle = false;
                    updatedArticle = true;
                }
                else
                {
                    throw new ArgumentException("The adding Article seams like it is NOT an update, but NOT new either");
                }
            }

            if (addArticle)
            {
                addArticle = DBM_Article.addArticle(newArticle);
            }

            this.updateRequired = true;
            return (addArticle || updatedArticle);
        }


        /*
         * Update Article
         */ 

        internal void updatePrizes(List<Tuple<int, float>> prizeUpdates)
        {
            if (this.updatePrizesPreRequirementsMet())
            {
                foreach (Tuple<int, float> newPrize in prizeUpdates)
                {
                    DBM_Article.changePrize(newPrize.Item1, newPrize.Item2);
                    Article article = DBM_Article.getArticle(newPrize.Item1);
                    article.Prize = newPrize.Item2;
                    this.addArticle(article);
                }
            }
        }


        public bool changeStock(int articleID, int amountChange)
        {
            Article article = this.itemList.FirstOrDefault(x => x.ID == articleID);
            if (article != null)
            {
                article.Stock -= amountChange;
            }
            return DBM_Article.changeStock(articleID, amountChange);
        }
    }
}