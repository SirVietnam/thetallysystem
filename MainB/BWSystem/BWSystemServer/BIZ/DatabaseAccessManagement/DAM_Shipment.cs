﻿using BWSystemServer.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public class DAM_Shipment : IArchive
    {
        private static DBM_Shipment DBM_Shipment = new DBM_Shipment();
        private static DBM_ShipmentData DBM_ShipmentData = new DBM_ShipmentData();
        private Func<int, int, bool> changeStock;

        public DAM_Shipment(Func<int, int, bool> changeStock)
        {
            if (changeStock == null)
            {
                throw new ArgumentNullException("changeStock");
            }
            this.changeStock = changeStock;
        }


        public void reportShipment(Shipment shipment)
        {
            int shipmentID = DBM_Shipment.addShipment(shipment);
            DBM_ShipmentData.addShipment(shipmentID, shipment.ArticleAmountListInt);

            foreach (Tuple<int, int> shipmentData in shipment.ArticleAmountListInt)
            {
                this.changeStock(shipmentData.Item1, shipmentData.Item2 * (-1));
            }
        }


        public static List<Shipment> getShipmentListWithoutData()
        {
            return DBM_Shipment.getShipmentList();
        }


        public static List<Shipment> getShipmentList()
        {
            List<Shipment> shipments = DAM_Shipment.getShipmentListWithoutData();
            for (int i = 0; i < shipments.Count; i++)
            {
                shipments[i].addArticleList(DBAccessManagement.Instance.getArticleList_All());
                shipments[i] = DBM_ShipmentData.getShipmentData(shipments[i]);
            }
            return shipments;
        }


        public static Shipment getShipment(int shipmentID)
        {
            Shipment shipment = DBM_Shipment.getShipment(shipmentID);
            shipment.addArticleList(DBAccessManagement.Instance.getArticleList_All());
            return DBM_ShipmentData.getShipmentData(shipment);
        }


        public void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            List<Shipment> shipments = DBM_Shipment.getShipmentList(startDateTime);
            shipments = shipments.Where(x => x.Datetime.Ticks <= endDateTime.Ticks).ToList();

            DBM_ShipmentData.copyDataToArchive(shipments);                 //ShipmentData,
            DBM_Shipment.copyDataToArchive(startDateTime, endDateTime);    //Shipment,
        }

        public void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            List<Shipment> shipments = DBM_Shipment.getShipmentList(startDateTime);
            shipments = shipments.Where(x => x.Datetime.Ticks <= endDateTime.Ticks).ToList();

            DBM_ShipmentData.deleteDataFromTable(shipments);                 //ShipmentData,
            DBM_Shipment.deleteDataFromTable(startDateTime, endDateTime);    //Shipment,
        }
    }
}