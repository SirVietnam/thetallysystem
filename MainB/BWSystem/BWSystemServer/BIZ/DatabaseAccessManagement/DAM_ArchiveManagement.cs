﻿using BWSystemServer.Classes;
using BWSystemServer.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{

    public class DAM_ArchiveManagement
    {
        private static DAM_ArchiveManagement instance;

        private DBAccessManagement DAM;
        private DBM_BookingData DB_BookingData;
        private DBM_Cancellation DB_Cancellation;
        private DBM_Event DB_Event;
        private DBM_Settlement DB_Settlement;
        private DBM_Shipment DB_Shipment;
        private DBM_ShipmentData DB_ShipmentData;
        private DBM_Stocktaking DB_Stocktaking;


        public static DAM_ArchiveManagement Instance
        {
            get
            {
                if (instance == null)
                {
                    DAM_ArchiveManagement.instance = new DAM_ArchiveManagement();
                }
                return instance;
            }
        }

        private DAM_ArchiveManagement()
        {
            DAM = DBAccessManagement.Instance;
            this.DB_BookingData = new DBM_BookingData();
            this.DB_Cancellation = new DBM_Cancellation();
            this.DB_Event = new DBM_Event();
            this.DB_Settlement = new DBM_Settlement();
            this.DB_Shipment = new DBM_Shipment();
            this.DB_ShipmentData = new DBM_ShipmentData();
            this.DB_Stocktaking = new DBM_Stocktaking();
        }


        //Get Archive Data
        #region GetData
        internal List<BookingData> getArchiveBookingData(DateTime startDateTime, DateTime endDateTime)
        {
            return this.DB_BookingData.getArchivePostingDataList(startDateTime, endDateTime);
        }

        internal List<CancellationData> getArchiveCancellationData()
        {
            return this.DB_Cancellation.getArchiveCancellationList();
        }

        internal List<Event> getArchiveEventList()
        {
            return this.DB_Event.getArchiveEventList();
        }

        internal List<Settlement> getArchiveSettlementList()
        {
            return this.DB_Settlement.getArchiveSettlementList();
        }

        internal double getArchivePayment(int settlementID, int accountID)
        {
            return this.DB_Settlement.getArchivePaymentData(settlementID, accountID);
        }

        internal List<Shipment> getArchiveShipmentList()
        {
            List<Shipment> shipments = this.DB_Shipment.getArchiveShipmentList();
            List<Article> articles = this.DAM.getArticleList_All();

            foreach (Shipment shipment in shipments)
            {
                shipment.addArticleList(articles);
                this.DB_ShipmentData.getArchiveShipmentData(shipment);
            }
            return shipments;
        }

        internal List<Stocktaking> getArchiveStocktakingList()
        {
            return this.DB_Stocktaking.getArchiveStocktakingList();
        }
        #endregion
    }
}