﻿using BWSystemServer.Classes;
using BWSystemServer.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public class DAM_NonOrgLogin
    {
        private static DBM_NonOrgLogin DBM_NonOrgLogin = new DBM_NonOrgLogin();


        public DAM_NonOrgLogin()
        {
        }

        /*
         * 
         */
        internal static string getMailAddress(int accountID)
        {
            return DBM_NonOrgLogin.getNonOrgLogin(accountID).MailAddress;
        }



        /*
         * Add
         */
        internal bool addLogin(int accountID, string password, string mailAddress)
        {
            bool loginAdded = false;
            NonOrgAuthentication oldNOA = DBM_NonOrgLogin.getNonOrgLogin(accountID);
            if (oldNOA != null)
            {
                SimpleLog.Log("A login to the account: " + accountID + " already exists!");
            }
            else if (this.isValidPassword(password) && ServerUtil.isValidMail(mailAddress))
            {
                byte[] pwSalt = PasswordHashUtil.generateSalt();
                byte[] pwHash = PasswordHashUtil.generateSaltedPasswordHash(password, pwSalt);

                NonOrgAuthentication nonOrgLogin = new NonOrgAuthentication(accountID, pwSalt, pwHash, mailAddress);

                loginAdded = DBM_NonOrgLogin.addNonOrgLogin(nonOrgLogin);
                if (!loginAdded)
                {
                    SimpleLog.Log("An error occoured while adding NonOrgLogin: " + accountID + " !");
                }
            }
            return loginAdded;
        }


        /*
         * Check
         */
        internal static bool isNonOrgAccount(int accountID)
        {
            return DBM_NonOrgLogin.isEntryAvailable(accountID);
        }


        internal static bool isValidLogin(string login)
        {
            InputRegex[] loginRegexs = DAM_NonOrgLogin.getLoginRegex();

            bool validLogin = true;
            foreach (InputRegex loginRegex in loginRegexs)
            {
                Regex currentRegex = new Regex(loginRegex.regexString);
                validLogin = validLogin && currentRegex.IsMatch(login);
            }

            return validLogin;
        }


        internal bool isCorrectPassword(int accountID, string password)
        {
            bool correctPW = false;
            NonOrgAuthentication nonOrgAuth = DBM_NonOrgLogin.getNonOrgLogin(accountID);

            if (nonOrgAuth != null)
            {
                correctPW = PasswordHashUtil.validatePassword(password, nonOrgAuth.PasswordSalt, nonOrgAuth.PasswordHash);
            }

            return correctPW;
        }


        /*
         * Verify
         */
        private bool isValidPassword(string password)
        {
            bool validPassword = true;
            InputRegex[] pwRegexs = PasswordHashUtil.getPasswordRegexWithExplanations();
            foreach (InputRegex pwRegex in pwRegexs)
            {
                Regex regex = new Regex(pwRegex.regexString);
                validPassword &= regex.IsMatch(password);
            }
            return validPassword;
        }


        internal static InputRegex[] getLoginRegex()
        {
            return ServerUtil.getInputRegex("NonOrgLogin_LoginRegex", "NonOrgLogin_LoginRegexExplain");
        }

        internal static InputRegex[] getMailRegexWithExplanations()
        {
            return ServerUtil.getInputRegex("NonOrgLogin_MailRegex", "NonOrgLogin_MailRegexExplain");
        }
    }
}