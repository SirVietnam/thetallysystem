﻿using BWSystemServer.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public class DAM_Booking : IArchive
    {

        private static DBM_BookingData DBM_Booking = new DBM_BookingData();
        private static Func<int, int, bool> changeStock;


        public DAM_Booking(Func<int, int, bool> changeStock) : base()
        {
            if (changeStock == null)
            {
                throw new ArgumentNullException("changeStock");
            }
            DAM_Booking.changeStock = changeStock;
        }


        /*
         * Get Booking
         */
        public static List<BookingData> getBookingDataList(DateTime startTimeStamp)
        {
            return DAM_Booking.DBM_Booking.getPostingDataList(startTimeStamp, true);
        }

        public static List<BookingData> getBookingDataList(DateTime startTimeStamp, DateTime endTimeStamp, bool grouped = true)
        {
            return DAM_Booking.DBM_Booking.getPostingDataList(startTimeStamp, endTimeStamp, grouped);
        }


        public static List<BookingData> getBookingDataList(DateTime startTimeStamp, DateTime endTimeStamp, int customerID, bool grouped = true)
        {
            List<BookingData> bookingData = DAM_Booking.DBM_Booking.getPostingDataList(startTimeStamp, endTimeStamp, grouped);
            IEnumerable<BookingData> bookings = bookingData.Where(x => x.Customer == customerID);
            return bookings.ToList<BookingData>();
        }


        public static List<BookingData> getBookingDataList(int bookingID, bool grouped)
        {
            return DBM_Booking.getPostingDatasToBookingID(bookingID, grouped);
        }


        public static List<BookingData> getEventBookings(int eventID)
        {
            Event currentEvent = DAM_Event.getEvent(eventID);
            if (currentEvent == null)
            {
                throw new ArgumentException("The given EventID does not exist. ID: " + eventID);
            }
            List<BookingData> bookings = DAM_Booking.DBM_Booking.getPostingDataList(eventID);
            return bookings;
        }


        //TODO: Fix this
        //public List<BookingData> getBookingDataListToCancel(DateTime startTimeStamp)
        //{
        //    DateTime lastSettlementEnd = this.getLastSettlementEndDate();
        //    DateTime bookingStart = lastSettlementEnd > startTimeStamp ? lastSettlementEnd : startTimeStamp;
        //    return this.getBookingDataList(bookingStart);
        //}


        /*
         * Add Booking
         */
        public static int addBooking(BookingData bookingData)
        {
            int bookingId = -1;
            if (bookingData.EventBooking == null)
            {
                bookingId = DAM_Booking.addBooking_Normal(bookingData);
            }
            else
            {
                bookingId = DAM_Booking.addBooking_Event(bookingData);
            }
            return bookingId;
        }


        private static int addBooking_Normal(BookingData bookingData)
        {
            int bookingID = -1;

            //Validate BookingAA
            bool allValuesCorrect = (bookingData.Booking != null) && (bookingData.Booking.Count > 0);
            foreach (BookingAA tuple in bookingData.Booking)
            {
                allValuesCorrect = allValuesCorrect && (tuple.Amount > 0);
            }

            bool added = false;
            if (allValuesCorrect)
            {
                bookingID = DBM_Booking.addPostingData(bookingData);
                added = (bookingID != -1);

                if (added && DAM_Booking.changeStock != null)
                {
                    foreach (BookingAA tuple in bookingData.Booking)
                    {
                        //TODO: Fix this
                        added = added & DAM_Booking.changeStock(tuple.Article, tuple.Amount);
                    }
                }
                else
                {
                    SimpleLog.Log("Adding the booking was not successful. Reason Unknonw! Logging BookingData!");
                    DAM_Booking.logErrorBooking(bookingData);
                }
            }
            else
            {
                DAM_Booking.logErrorBooking(bookingData);
            }
            return bookingID;
        }


        private static int addBooking_Event(BookingData bookingData)
        {
            int bookingID = -1;
            Event currentEvent = DAM_Event.getEvent((int)bookingData.EventBooking);

            if (currentEvent != null)
            {
                //Clone & Remove payed for items
                List<BookingAA> normalBookingAAs = new List<BookingAA>();
                List<BookingAA> eventBookingAAs = new List<BookingAA>();
                foreach (BookingAA bookingAA in bookingData.Booking)
                {
                    if (currentEvent.CostAbsorption.Contains(bookingAA.Article))
                    {
                        eventBookingAAs.Add(bookingAA);
                    }
                    else
                    {
                        normalBookingAAs.Add(bookingAA);
                    }
                }


                if (normalBookingAAs.Count > 0)
                {
                    BookingData normalBooking = new BookingData(null, bookingData.User, bookingData.Customer, normalBookingAAs, bookingData.TimeStamp, null, bookingData.DataCancelled);
                    bookingID = DAM_Booking.addBooking_Normal(normalBooking);
                }
                if (eventBookingAAs.Count > 0)
                {
                    BookingData eventBooking = new BookingData(null, bookingData.User, bookingData.Customer, eventBookingAAs, bookingData.TimeStamp, bookingData.EventBooking, bookingData.DataCancelled);
                    bookingID = DAM_Booking.addBooking_Normal(eventBooking);
                }
            }
            return bookingID;
        }


        /*
         * Update Booking
         */
        internal bool enterCancellation(int bookingID, int bookingSize)
        {
            return DBM_Booking.enterCancellation(bookingID, bookingSize);
        }

        /*
         * Archiving
         */
        public void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            DBM_Booking.copyDataToArchive(startDateTime, endDateTime);
        }

        public void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            DBM_Booking.deleteDataFromTable(startDateTime, endDateTime);
        }


        /*
         * Helpers
         */ 
        private static void logErrorBooking(BookingData bookingData)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Invalid Booking recieved:");
            sb.AppendLine("ConsumerID:" + bookingData.Customer);
            if (bookingData.User != null)
                sb.AppendLine("UserID:" + bookingData.User);
            if (bookingData.EventBooking != null)
                sb.AppendLine("EventID:" + bookingData.EventBooking);
            sb.AppendLine("Cancelled: " + bookingData.DataCancelled);
            sb.AppendLine("TimeStamp: " + ServerUtil.DateTimeToString(bookingData.TimeStamp));
            foreach (BookingAA bookingAA in bookingData.Booking)
            {
                sb.AppendLine("-ArticleID: " + bookingAA.Article + " - Amount:" + bookingAA.Amount);
            }
            SimpleLog.Log(sb.ToString());
        }
    }
}