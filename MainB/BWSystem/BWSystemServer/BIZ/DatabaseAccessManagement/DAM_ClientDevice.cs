﻿using BWSystemServer.Classes;
using BWSystemServer.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public class DAM_ClientDevice
    {
        private static DBM_ClientDevice DBM_ClientDevice = new DBM_ClientDevice();
        private static bool useTokenSecurity = (bool)ServerUtil.getConfigValue("UseSecurityTokenEvaluation", typeof(bool));
        private static List<ClientDevice> clientDevices;

        public DAM_ClientDevice()
        {
        }

        /*
         * Devices
         */
        #region Devices
        internal static List<ClientDevice> getClientDevices()
        {
            if (DAM_ClientDevice.clientDevices == null)
            {
                DAM_ClientDevice.clientDevices = DBM_ClientDevice.getClietDeviceList();
            }
            return DAM_ClientDevice.clientDevices;
        }

        internal static string addClientDevice(string username, string password, string ipAddress)
        {
            string clientIDToken = null;
            SimpleLog.Info("Request to add devide: " + ipAddress);

            if (DBAccessManagement.Instance.isValidPassword(DBAccessManagement.Instance.getAccountToLogin(username), password))
            {
                ClientDevice newDevice = new ClientDevice();
                clientIDToken = newDevice.generateNewClient(ipAddress);
                bool deviceAdded = DBM_ClientDevice.addClientDevice(newDevice);

                if (!deviceAdded)
                {
                    SimpleLog.Log("A ClientDevice coun't be added to the DB -- IP: " + ipAddress);
                    Exception ex = new Exception("The device was not added to the DB");
                    throw ex;
                }
                else
                {
                    DAM_ClientDevice.getClientDevices().Add(newDevice);
                    SimpleLog.Info("Request was: " + true);
                }
            }

            return clientIDToken;
        }


        internal static bool deleteClientDevice(int clientID)
        {
            SimpleLog.Info("Request removing devide: " + clientID);
            bool removed = DBM_ClientDevice.removeDevice(clientID);
            if (removed)
            { 
                ClientDevice device = DAM_ClientDevice.getClientDevices().FirstOrDefault(x => x.DeviceID == clientID);
                if (device != null)
                    DAM_ClientDevice.clientDevices.Remove(device);
            }
            SimpleLog.Info("Request was: " + removed);
            return removed;
        }


        internal ClientToken getToken(string loginToken, string ipAddress)
        {
            if (DAM_ClientDevice.useTokenSecurity)
            {
                return this.getDeviceToken(loginToken, ipAddress);
            }
            else
            {
                return this.getFakeToken();
            }
        }


        private ClientToken getFakeToken()
        {
            ClientToken clientToken = new ClientToken();
            clientToken.DeviceID = 1;
            clientToken.ID = 1;
            clientToken.StartDateTime = DateTime.Now;
            clientToken.EndDateTime = DateTime.Now + TimeSpan.FromHours(2.0);
            return clientToken;
        }


        private ClientToken getDeviceToken(string loginToken, string ipAddress)
        {
            ClientToken clientToken = new ClientToken();
            ClientDevice device = DAM_ClientDevice.getClientDevices().FirstOrDefault(x => x.IpAddress == ipAddress);
            if (device == null)
            {
                SimpleLog.Log("An unregisted IP: " + ipAddress + " tried to get a token.");
            }
            else
            {
                clientToken = device.getToken(loginToken);
                if (clientToken.EndDateTime < DateTime.Now)
                {
                    SimpleLog.Log("The IP: " + ipAddress + " used a wrong LoginToken.");
                }
            }
            return clientToken;
        }


        internal bool isValidToken(ClientToken clientToken, string ipAddress)
        {
            bool validToken = false;
            if (DAM_ClientDevice.useTokenSecurity)
            {
                ClientDevice device = DAM_ClientDevice.getClientDevices().FirstOrDefault(x => x.IpAddress == ipAddress);
                if (device != null)
                {
                    validToken = device.isValidToken(clientToken);
                }
                if (!validToken)
                {
                    SimpleLog.Log("The IP: " + ipAddress + " tried accessing with an invalid token!");
                }
            }
            return (!DAM_ClientDevice.useTokenSecurity || validToken);
        }

        #endregion
    }
}