﻿using BWSystemServer.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public class DAM_Event : PeriodicDBUpdate<Event>, IArchive
    {
        private static DBM_Event DBM_Event = new DBM_Event();


        public DAM_Event() : base()
        {
        }


        public List<Event> EventList_All
        {
            get
            {
                if (this.isUpdateRequired())
                {
                    this.updateItemList();
                }
                return new List<Event>(this.itemList);
            }
            
        }

        public List<Event> EventList_Future
        {
            get
            {
                if (this.isUpdateRequired())
                {
                    this.updateItemList();
                }
                return this.itemList.Where(x => x.End > DateTime.Now).ToList();
            }
        }

        public static Event getEvent(int eventID)
        {
            return DAM_Event.DBM_Event.getEvent(eventID);
        }


        public override bool isUpdateRequired()
        {
            TimeSpan ts = DateTime.Now - this.lastUpdated;
            return ts > this.updateInterval || this.updateRequired;
        }

        protected override void updateItemList()
        {
            this.itemList = this.itemList = DBM_Event.getEventList();
        }


        /*
         * Add Event
         */
        internal bool addEvent(Event eventToAdd)
        {
            bool eventAdded = false;
            if (eventToAdd.Name != "" && (eventToAdd.EventID > 0 || eventToAdd.Begin > DateTime.Now) && eventToAdd.End > eventToAdd.Begin)
            {
                IEnumerable<Event> conflictEvents = this.itemList.Where(x => (x.Begin <= eventToAdd.Begin && x.End <= x.Begin)
                                                                            || (x.Begin <= eventToAdd.End && x.End >= eventToAdd.End)
                                                                            || (x.Begin >= eventToAdd.Begin && x.End <= eventToAdd.End));

                if (eventToAdd.EventID < 0 && conflictEvents.Count() == 0)
                {
                    eventAdded = DAM_Event.DBM_Event.addEvent(eventToAdd);
                }
                else if ((conflictEvents.Count() == 0 && eventToAdd.EventID >= 0)
                    || (conflictEvents.Count() == 1 && conflictEvents.First().EventID == eventToAdd.EventID))
                {
                    eventAdded = DAM_Event.DBM_Event.updateEvent(eventToAdd);
                }
            }
            this.updateRequired = true;
            return eventAdded;
        }


        public bool makeEventSettlement(int eventID)
        {
            bool eventSettled = false;
            Event currentEvent = this.EventList_All.FirstOrDefault(x => x.EventID == eventID);
            List<Account> accounts = DBAccessManagement.Instance.getAccountList_All();
            List<Article> articles = DBAccessManagement.Instance.getArticleList_All();


            if (currentEvent != null && currentEvent.End < DateTime.Now)
            {
                List<BookingData> bookings = DAM_Booking.getEventBookings(eventID);
                if (bookings.Count > 0)
                {
                    currentEvent.makeEventSettlement(bookings, articles, accounts);

                    if (currentEvent.eventCalculated())
                    {
                        string configValue = "StandardEventSettlementExportLocation";
                        string absoluteFilePath = ServerUtil.getAbsoluteFilePath(configValue);
                        string fileName = ServerUtil.getFileName(currentEvent);

                        currentEvent.writeToExcelFile(absoluteFilePath + fileName, bookings, articles, accounts);

                        eventSettled = DAM_Event.DBM_Event.updateEvent(currentEvent);
                    }

                }
            }
            return eventSettled;
        }


        public void copyDataToArchive(DateTime startDateTime, DateTime endDateTime)
        {
            DBM_Event.copyDataToArchive(startDateTime, endDateTime);
        }

        public void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime)
        {
            DBM_Event.deleteDataFromTable(startDateTime, endDateTime);
        }
    }
}