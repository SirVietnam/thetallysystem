﻿using BWSystemServer.BIZ.DatabaseAccessManagement;
using BWSystemServer.DAL;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{

    public class DAM_Account : PeriodicDBUpdate<Account>
    {
        private bool useLDAP;
        private bool pushToLDAP_Fotos;
        private bool pushToLDAP_CardNumber;

        private DBM_Account DBM_Acc;
        private Action<List<Account>> mapAccountsAndFavourites;

        public DAM_Account(Action<List<Account>> mapAccountsAndFavourites)
            : base()
        {
            DBM_Acc = new DBM_Account();
            if (mapAccountsAndFavourites == null)
            {
                throw new ArgumentNullException("mapAccountsAndFavourites");
            }
            this.mapAccountsAndFavourites = mapAccountsAndFavourites;

            this.useLDAP = (bool)ServerUtil.getConfigValue("UseLDAP", typeof(bool));
            //this.pushToLDAP_Fotos = this.useLDAP && (bool)ServerUtil.getConfigValue("PushToLDAP_Fotos", typeof(bool));
            this.pushToLDAP_CardNumber = this.useLDAP && (bool)ServerUtil.getConfigValue("PushToLDAP_CardNumber", typeof(bool));
        }


        /*
         * Getter
         */
        internal List<Account> AccountList_All
        {
            get
            {
                if (this.updateRequired)
                {
                    this.updateItemList();
                }
                return new List<Account>(itemList);
            }
        }

        public List<Account> AccountList_Active
        {
            get
            {
                return AccountList_All.Where(x => x.IsAvailable).ToList();
            }
        }


        internal Account getAccount(int id)
        {
            return this.itemList.First(x => x.ID == id);
        }


        internal Tuple<int, string>[] getAccountID_LoginList()
        {
            return this.DBM_Acc.getID_LoginList();
        }



        //////////////  Actual update of itemList
        protected override void updateItemList()
        {
            if (this.isUpdateRequired())
            {
                this.itemList = DBM_Acc.getAccountList();
                this.mapAccountsAndFavourites(this.itemList);
                this.itemList = BIZ_NameManagement.fillWithDisplayNames(this.itemList);

                this.lastUpdated = DateTime.Now;
                this.updateRequired = false;
            }
        }


        /*
         * Add Account
         */
        public bool addAccount(Account newAccount)
        {
            Debug.Assert(newAccount != null);
            bool accountAdded = false;

            //Validate Account
            bool newAccountIsValid = ServerUtil.validateAccount(newAccount);

            if (newAccountIsValid)
            {
                //Check if account already exists
                List<Account> accounts = this.AccountList_All;

                Account oldAccount = accounts.FirstOrDefault(x => x.ID == newAccount.ID);

                if (oldAccount != null)
                {
                    //Update Account
                    accountAdded = this.DBM_Acc.updateAccount(newAccount);
                    if (!newAccountIsValid)
                        SimpleLog.Log("Failed to update account: " + newAccount.ID + " - " + ServerUtil.accountToDisplayWebName(newAccount));
                }
                else
                {
                    //Add Account
                    accountAdded = this.addNewAccount(newAccount);
                }
            }
            else
            {
                SimpleLog.Log("An invalid account: " + newAccount.Name + " " + newAccount.Authentication.LoginName);
            }

            this.updateRequired = true;
            return accountAdded;
        }


        private bool addNewAccount(Account newAccount)
        {
            bool accountAdded = false;

            //Check if equal account already exists (Same content, but different ID)
            ConcurrentBag<int> equalAccounts = new ConcurrentBag<int>();
            Parallel.ForEach(this.AccountList_All, x => this.isAccountEqual(newAccount, x, equalAccounts));

            if (equalAccounts.Count == 0)
            {
                accountAdded = DBM_Acc.addAccount(newAccount);
                if (!accountAdded)
                    SimpleLog.Log("Failed to add new account: " + ServerUtil.accountToDisplayWebName(newAccount));
            }
            else
            {
                SimpleLog.Log("Account " + ServerUtil.accountToDisplayWebName(newAccount) + " was not added, because an equal account was found.");
            }

            return accountAdded;
        }


        /*
         * Helper
         */
        public override bool isUpdateRequired()
        {
            TimeSpan ts = DateTime.Now - this.lastUpdated;
            return (this.updateRequired || ts > this.updateInterval);
        }




        internal bool loginAlreadyExists(string login)
        {
            bool loginIsTaken = false;

            List<Account> currentAccounts = this.AccountList_All;
            IEnumerable<Account> equalAccounts = currentAccounts.Where(x => x.Authentication.LoginName.Equals(login, StringComparison.CurrentCultureIgnoreCase));
            if (equalAccounts.Count() != 0)
            {
                loginIsTaken = true;
            }
            else
            {
                equalAccounts = currentAccounts.Where(x => x.Authentication.LoginName.EndsWith(login, StringComparison.CurrentCultureIgnoreCase));
                if (equalAccounts.Count() != 0)
                    loginIsTaken = true;
            }

            return loginIsTaken;

        }


        public void isAccountEqual(Account newAccount, Account oldAccount, ConcurrentBag<int> equalAccounts)
        {
            bool loginsEqual = newAccount.Authentication.LoginName != null && oldAccount.Authentication.LoginName != null && newAccount.Authentication.LoginName.Equals(oldAccount.Authentication.LoginName, StringComparison.OrdinalIgnoreCase);
            bool nameEqual = oldAccount.Name.Equals(newAccount.Name);
            bool surnameEqual = oldAccount.Surname.Equals(newAccount.Surname);
            bool isEqual = (surnameEqual && nameEqual) || loginsEqual;
            if (isEqual)
                equalAccounts.Add(oldAccount.ID);
        }


    }
}