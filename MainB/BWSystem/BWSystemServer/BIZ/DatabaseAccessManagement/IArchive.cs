﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    interface IArchive
    {
        void copyDataToArchive(DateTime startDateTime, DateTime endDateTime);
        void deleteDataFromTable(DateTime startDateTime, DateTime endDateTime);
    }
}
