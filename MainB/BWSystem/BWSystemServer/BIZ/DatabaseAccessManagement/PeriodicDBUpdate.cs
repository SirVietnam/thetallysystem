﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWSystemServer.BIZ.DatabaseAccessManagement
{
    public abstract class PeriodicDBUpdate<Item>
    {


        protected bool updateRequired;
        protected DateTime lastUpdated;
        protected List<Item> itemList;
        protected TimeSpan updateInterval;

        protected PeriodicDBUpdate()
        {
            this.lastUpdated = new DateTime();
            this.updateRequired = true;
            this.updateInterval = TimeSpan.FromSeconds((int)ServerUtil.getConfigValue("DBListUpdateTimeSpan", typeof(int)));
        }

        protected abstract void updateItemList();
        public abstract bool isUpdateRequired();
    }
}