﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BWSystemServer.Default" %>

<%@ Register Src="~/WebAccess/SubElements/Header.ascx" TagPrefix="SE" TagName="Header" %>
<%@ Register Src="~/WebAccess/SubElements/Sidebar.ascx" TagPrefix="SE" TagName="Sidebar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="Design.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <SE:Header runat="server" ID="Head" HeaderText="BWSystem" SubheaderText="To gain access to any kind of functionality you need to login." />
    <SE:Sidebar runat="server" id="Sidebar" />

    <div style="margin-left: 200px; padding: 1px 16px;">
        <form id="form1" runat="server">
            <div id="div_login">
                <asp:Login ID="Login" runat="server" DisplayRememberMe="False" OnAuthenticate="Login_Authenticate" DestinationPageUrl="~/WebAccess/Account_Page.aspx"></asp:Login>
            </div>
        </form>
    </div>
</body>
</html>
