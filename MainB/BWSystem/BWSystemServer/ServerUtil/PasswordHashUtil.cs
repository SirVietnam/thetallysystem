﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace BWSystemServer
{
    public struct InputRegex
    {
        public string regexString;
        public string descriptionString;
    }

    public class PasswordHashUtil
    {

        /*
         * Salt generation Methdod
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] generateSalt()
        {
            byte[] salt = new byte[32];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(salt);
            return salt;
        }


        /*
         * Hash Generation Methods
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] generateSaltedPasswordHash(string password, byte[] salt)
        {
            byte[] pwInBytes = Encoding.ASCII.GetBytes(password);
            return PasswordHashUtil.generateSaltedPasswordHash(pwInBytes, salt);
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] generateSaltedPasswordHash(int[] password, byte[] salt)
        {
            byte[] pwInBytes = intArrayToBytesArray(password);
            return PasswordHashUtil.generateSaltedPasswordHash(pwInBytes, salt);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] generateSaltedPasswordHash(byte[] password, byte[] salt)
        {
            byte[] saltedPasswordHash = null;
            using (SHA256 hashAlgo = SHA256.Create())
            {
                byte[] saltedPW = salt.Concat(password).ToArray();
                saltedPasswordHash = hashAlgo.ComputeHash(saltedPW);
            }
            return saltedPasswordHash;
        }

        /*
         * Validation Methods
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool validatePassword(string password, byte[] salt, byte[] hashedSaltedPassword)
        {
            byte[] enteredSHWP = PasswordHashUtil.generateSaltedPasswordHash(password, salt);

            bool validPW = true;
            for (int i = 0; i < enteredSHWP.Length; i++)
            {
                validPW = validPW && (enteredSHWP[i] == hashedSaltedPassword[i]);
            }
            return validPW;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool validatePassword(int[] password, byte[] salt, byte[] hashedSaltedPassword)
        {
            byte[] enteredSHWP = PasswordHashUtil.generateSaltedPasswordHash(password, salt);

            bool validPW = true;
            for (int i = 0; i < enteredSHWP.Length; i++)
            {
                validPW = validPW && (enteredSHWP[i] == hashedSaltedPassword[i]);
            }
            return validPW;
        }


        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long getRandomLong()
        {
            byte[] longValues = new byte[8];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetBytes(longValues);
            return BitConverter.ToInt64(longValues, 0);
        }

        /*
         *
         * Helper Methods
         *
         */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte[] intArrayToBytesArray(int[] password)
        {
            byte[] pwInBytes = new byte[password.Length];
            for (int i = 0; i < password.Length; i++)
            {
                pwInBytes[i] = Convert.ToByte(password[i]);
            }

            return pwInBytes;
        }

        /*
         * Password Regex and explanation
         */
        internal static InputRegex[] getPasswordRegexWithExplanations()
        {
            return ServerUtil.getInputRegex("NonOrgLogin_PasswordRegex", "NonOrgLogin_PasswordRegexExplain");
        }
    }
}