﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BWSystemServer
{
    public static class ServerUtil
    {
        #region RegexExpressions
        public const string regexPrizeString = @"^[0-9]*(?:[,.]{1}[0-9]{0,2})?$";
        private static Regex regexPrize;
        public const string regexFloatNumberString = @"^[0-9]*(?:[.,]{1}[0-9]{0,9})?$";
        private static Regex regexFloatNumber;
        public const string regexIntNumberString = @"\d+";
        private static Regex regexIntNumber;
        public const string regexDateString = @"[0-9]{4}\-[0-1][0-9]\-[0-3][0-9]";
        private static Regex regexDate;


        public static Regex RegexPrize
        {
            get
            {
                if (ServerUtil.regexPrize == null)
                {
                    ServerUtil.regexPrize = new Regex(ServerUtil.regexPrizeString);
                }
                return ServerUtil.regexPrize;
            }
        }
        public static Regex RegexFloatNumber
        {
            get
            {
                if (ServerUtil.regexFloatNumber == null)
                {
                    ServerUtil.regexFloatNumber = new Regex(ServerUtil.regexFloatNumberString);
                }
                return ServerUtil.regexFloatNumber;
            }
        }
        public static Regex RegexIntNumber
        {
            get
            {
                if (ServerUtil.regexIntNumber == null)
                {
                    ServerUtil.regexIntNumber = new Regex(ServerUtil.regexIntNumberString);
                }
                return ServerUtil.regexIntNumber;
            }
        }
        public static Regex RegexDate
        {
            get
            {
                if (ServerUtil.regexDate == null)
                {
                    ServerUtil.regexDate = new Regex(ServerUtil.regexDateString);
                }
                return ServerUtil.regexDate;
            }
        }


        internal static InputRegex[] getInputRegex(string configValueConditions, string configValueExplainations)
        {
            string[] regexConditions = ServerUtil.eliminateSeperatorSign_strings((string)ServerUtil.getConfigValue(configValueConditions, typeof(string)));
            string[] regexExplains = ServerUtil.eliminateSeperatorSign_strings((string)ServerUtil.getConfigValue(configValueExplainations, typeof(string)));

            List<InputRegex> regexCondExplains = new List<InputRegex>();
            if (regexConditions.Count() != regexExplains.Count())
            {
                SimpleLog.Log("Error at regexDefinition!\nCounts of Conditions and Explainatories do not match.\n--PW Regex Conditions: " + configValueConditions + ": " + regexConditions.Count() + "\n--PW Regex Explains: " + configValueExplainations + ": " + regexExplains.Count());
            }
            else
            {
                for (int i = 0; i < regexExplains.Count(); i++)
                {
                    InputRegex passwordRegex = new InputRegex();
                    passwordRegex.regexString = regexConditions[i];
                    passwordRegex.descriptionString = regexExplains[i];
                    regexCondExplains.Add(passwordRegex);
                }
            }
            return regexCondExplains.ToArray();
        }
        #endregion

        #region Conversions
        public static double StringToDouble(string numberString)
        {
            if (ServerUtil.RegexFloatNumber.IsMatch(numberString))
            {
                string correctNumberString = numberString.Replace(',', '.');
                return double.Parse(correctNumberString, CultureInfo.InvariantCulture);
            }
            else
            {
                throw new FormatException("The delivered string was not number compatible");
            }
        }



        public static string DateTimeToFileString(DateTime dateTime, string ending)
        {
            string dateTimeString = dateTime.ToString("yyyy-MM-dd");
            return "_" + dateTimeString + ending;
        }

        public static string DateTimeToString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm");
        }

        public static string DateToString(DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        public static string TimeToString(DateTime time)
        {
            return time.ToString("HH:mm");
        }

        public static double convertToDoubleFormDB(int value)
        {
            double valueDouble = (double)value;
            return valueDouble / 100.0;
        }

        public static int convertDoubleForDB(double prize)
        {
            double valueTemp = prize * 100.0;
            return Convert.ToInt32(valueTemp);
        }

        public static string accountToFilename(Account account)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(account.Name);
            sb.Append("_");
            sb.Append(account.Surname);
            sb.Replace(' ', '_');
            return sb.ToString();
        }

        public static string articleToFilename(Article article)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(article.Name);
            sb.Append("_");
            sb.Append(ServerUtil.convertDoubleForDB(article.Size));
            sb.Replace(' ', '_');
            return sb.ToString();
        }

        public static string accountToDisplayWebName(Account account, bool shortVariant = false)
        {
            string accountName = "";
            if (account != null)
            {
                if (shortVariant)
                {
                    if (account.Nickname != null && account.Nickname != "")
                    {
                        accountName = account.Nickname;
                    }
                }

                if (!shortVariant || accountName.Equals(""))
                {
                    accountName = account.Name + " " + account.Surname;
                }
            }
            return accountName;
        }
        #endregion


        #region GetFilepaths
        public static string getAbsoluteFilePath(string configValue)
        {
            string filePath = (string)ServerUtil.getConfigValue(configValue, typeof(string));
            string absoluteFilePath = System.Web.HttpContext.Current.Server.MapPath(filePath);
            if (!Directory.Exists(absoluteFilePath))
            {
                Directory.CreateDirectory(absoluteFilePath);
            }

            return absoluteFilePath;
        }


        public static string getFileName(AnnualStatement annualStatement)
        {
            return annualStatement.AnnualStatementID + ServerUtil.DateTimeToFileString(annualStatement.EndDate, ".xlsx");
        }

        public static string getFileName(Settlement settlement)
        {
            return settlement.SettlementID + ServerUtil.DateTimeToFileString(settlement.EndDate, ".xlsx");
        }

        public static string getFileName(Event currentEvent)
        {
            return currentEvent.EventID + ServerUtil.DateTimeToFileString(currentEvent.Begin, ".xlsx");
        }
        #endregion


        private static AppSettingsReader config = new AppSettingsReader();

        public static Object getConfigValue(string key, Type type)
        {
            return config.GetValue(key, type);
        }


        public static String getConnectionString()
        {
            //TODO: Real implementation?!?!?
            return (string)ServerUtil.getConfigValue("SQLConnectionString", typeof(string));
            //return "Data Source=ZICKE\\SQLEXPRESS;Initial Catalog=master;Integrated Security=True";
        }


        /////////////////// Seperator Sign ///////////////////
        #region SeperatorSign
        public static int[] eliminateSeperatorSign(String integers)
        {
            char seperatorSign = (char)ServerUtil.getConfigValue("SecurityCodeSeperatorSign", typeof(char));
            String[] stringFigures = integers.Split(seperatorSign);
            int[] securityCode = new int[stringFigures.Length];
            for (int i = 0; i < stringFigures.Length; i++)
            {
                try
                {
                    securityCode[i] = Convert.ToInt32(stringFigures[i]);
                } catch (Exception exc)
                {
                    //TODO: correct Excepation handling!
                }
            }

            return securityCode;
        }

        public static String insertSeperatorSign(int[] intArray)
        {
            string stringArrayRep = null;
            if (intArray != null)
            {
                StringBuilder sb = new StringBuilder();
                char seperatorSign = (char)ServerUtil.getConfigValue("SecurityCodeSeperatorSign", typeof(char));
                for (int i = 0; intArray != null && i < intArray.Count(); i++)
                {
                    sb.Append(intArray[i]);
                    if ((i + 1) < intArray.Count())
                    {
                        sb.Append(seperatorSign);
                    }
                }
                stringArrayRep = sb.ToString();
            }

            return stringArrayRep;
        }


        public static string[] eliminateSeperatorSign_strings(string concatString)
        {
            return concatString.Split((char)ServerUtil.getConfigValue("SecurityCodeSeperatorSign", typeof(char)));
        }

        public static string insertSeperatorSign(string[] seperateStrings)
        {
            char seperatorSign = (char)ServerUtil.getConfigValue("SecurityCodeSeperatorSign", typeof(char));

            StringBuilder sb = new StringBuilder();
            foreach (string seperateString in seperateStrings)
            {
                sb.Append(seperateString);
                sb.Append(seperatorSign);
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
        #endregion


        
        public static bool validateAccount(Account account)
        {
            bool nameCorrect = account.Name != null && !account.Name.Equals("");
            bool surnameCorrect = account.Surname != null && !account.Surname.Equals("");
            bool authCorrect = ServerUtil.validateSecurityCode(account.Authentication.SecurityCode) || account.Authentication.CardCode != null;
            bool standardCorrect = account.Standard >= 0; //TODO: make workaround!
            return (nameCorrect && surnameCorrect && authCorrect && standardCorrect);
        }

        internal static Account mergeAccounts(Account baseAccount, Account additionalInfos, bool isAvailable, bool isSuperuser)
        {
            Account mergedAccount = new Account();
            mergedAccount.ID = baseAccount.ID;
            mergedAccount.Name = baseAccount.Name;
            mergedAccount.Surname = baseAccount.Surname;
            mergedAccount.Nickname = (baseAccount.Nickname != null && baseAccount.Nickname != "") ? baseAccount.Nickname : additionalInfos.Nickname;
            mergedAccount.PictureURL = (baseAccount.PictureURL != null && baseAccount.PictureURL != "") ? baseAccount.PictureURL : additionalInfos.PictureURL;
            mergedAccount.Standard = baseAccount.Standard != 0 ? baseAccount.Standard : additionalInfos.Standard;
            mergedAccount.AuthenticateAlways = baseAccount.AuthenticateAlways || additionalInfos.AuthenticateAlways;

            mergedAccount.IsAvailable = isAvailable;
            mergedAccount.IsSuperuser = isSuperuser;

            mergedAccount.Authentication = ServerUtil.mergeAuthentications(baseAccount.Authentication, additionalInfos.Authentication);

            return mergedAccount;
        }


        internal static Authentication mergeAuthentications(Authentication baseAuthentication, Authentication additionalAuthentication)
        {
            Authentication mergedAuthentication = new Authentication();
            mergedAuthentication.LoginName = baseAuthentication.LoginName;
            mergedAuthentication.SecurityCode = baseAuthentication.SecurityCode;
            mergedAuthentication.SecurityCodeHash = baseAuthentication.SecurityCodeHash;
            mergedAuthentication.SecurityCodeSalt = baseAuthentication.SecurityCodeSalt;

            HashSet<Int64> mergedCardCodes = new HashSet<Int64>();
            foreach (Int64 cardCode in baseAuthentication.CardCode)
            {
                mergedCardCodes.Add(cardCode);
            }
            foreach (Int64 cardCode in additionalAuthentication.CardCode)
            {
                mergedCardCodes.Add(cardCode);
            }
            mergedAuthentication.CardCode = mergedCardCodes.ToArray();

            return mergedAuthentication;
        }



        public static bool validateSecurityCode(int[] securityCode)
        {
            bool validCode = false;
            if (securityCode != null)
            {
                validCode = securityCode.Length >= (int)ServerUtil.getConfigValue("MinSecurityCodeCount", typeof(int));
            }
            return validCode;
        }



        public static int getStandardAmount(double size)
        {
            int stdSize = 12;

            if (size >= 1.0)
            {
                stdSize = 12;
            }
            else if (size >= 0.75)
            {
                stdSize = 12;
            }
            else if (size >= 0.5)
            {
                stdSize = 20;
            }
            else if (size >= 0.3)
            {
                stdSize = 24;
            }
            else
            {
                stdSize = 24;
            }

            return stdSize;
        }


        public static bool isValidMail(string emailaddress)
        {
            bool validMail = false;
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                validMail = true;
            } catch (FormatException)
            {
                validMail = false;
            }
            return validMail;
        }
    }
}