﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BWSystemServer.DAL;
using BWSystemServer.BIZ;

namespace BWSystemServer
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            DataBaseManagement DBM = new DataBaseManagement();

            DBAccessManagement DAM = new DBAccessManagement();


            Console.WriteLine("Existing Account:");
            List<Account> accounts = DBM.getAccountList();
            foreach (Account p in accounts)
            {
                Console.WriteLine(p.toString());
            }

            Console.WriteLine(DBM.getArticleList().Count().ToString());
            Article art1 = DBM.getArticleList()[0];
            RegAuthentication aut1 = new RegAuthentication(new int[] { 1, 2, 3, 4, 5, 6, 7 }, "UnknownFormat");

            Account acc1 = new Account(999, "Andreas", "Hasenfuß", null, "HELLO-World", art1.ID, null, aut1, true);

            Console.WriteLine("Account to be added:");
            Console.WriteLine(acc1.toString());
            Boolean accAdded = DBM.addAccount(acc1);
            Console.WriteLine("Account was successfully added: " + accAdded.ToString() + "\n \n");

            String art2Desc = "Ein Softdrink aus Orangen.";
            Article art2 = new Article(999, "Fanta", Article.ArticleType.Softdrink, 0.7, 0.5, 99, 0.0, "Coca Cola", null, art2Desc, false, true);
            Console.WriteLine("Article to be added:");
            Console.WriteLine(art2.toString());
            Boolean artAdded = DBM.addArticle(art2);
            Console.WriteLine("Article was successfully added: " + artAdded.ToString() + "\n \n");  

            /*
            TupleAA bookings = new TupleAA();
            Article art3 = DBM.getArticleList()[1];
            bookings.Add(new TupleAA(art1, 1));
            bookings.Add(new TupleAA(art3, 2));
            PostingData postData = new PostingData(999, null, DBM.getAccountList()[0], bookings, new DateTime(DateTime.Now.Ticks), null);
            
            
            Console.WriteLine("PostingData that will be added:");
            Console.WriteLine(postData.toString());
            Boolean addedSuccess = DBM.addBooking(postData);
            Console.WriteLine("Adding was Successfully: " + addedSuccess.ToString());
            */
            /*
            Console.WriteLine("Readering PostingData:");
            List<PostingData> posts = DBM.getPostingDataList();
            //new DateTime(2012, 11, 05), new DateTime(DateTime.Now.Ticks)
            Console.WriteLine("Amount of postingDatas: " + posts.Count() + "\n");
            Console.WriteLine("Datas Now:");
            foreach (PostingData p in posts)
            {
                Console.WriteLine(p.toString());
            }
            Console.WriteLine("Reading Done!" + "\n \n");

            /*
            Console.WriteLine("Adding Event:");
            Event newEvent = new Event(999, "Saufen", new DateTime(2012, 11, 07, 20, 00, 00), new DateTime(2012, 11, 08, 02, 00, 00));
            Console.WriteLine(newEvent.toString());
            DBM.addEvent(newEvent);
            Console.WriteLine("Event Successfully Added" + "\n \n");
            */
            /*
            Console.WriteLine("Reading Event List:");
            List<Event> events = DBM.getEventList();
            Console.WriteLine("Amount of Events: " + events.Count());
            Console.WriteLine("Event Datas:");
            foreach (Event p in events)
            {
                Console.WriteLine(p.toString());
            }
            Console.WriteLine("Reading Done!" + "\n \n");


            /*
            Console.WriteLine("Adding Settlement:");
            DateTime dateTime = new DateTime(2011, 11, 4, 14, 01, 01);
            Console.WriteLine(dateTime.ToString());
            DBM.addSettlement(dateTime);
            Console.WriteLine("Settlement Successfully Added" + "\n \n");
            */
            /*
            Console.WriteLine("Reading Event List:");
            List<DateTime> settlements = DBM.getSettlementList();
            Console.WriteLine("Amount of Settlements: " + settlements.Count());
            Console.WriteLine("Settlement Datas:");
            foreach (DateTime p in settlements)
            {
                Console.WriteLine(p.ToString());
            }
            Console.WriteLine("Reading Done!" + "\n \n");


            Console.WriteLine("Reading Latest Event:");
            DateTime settlement = DBM.getLastSettlement();
            Console.WriteLine("Settlement Data: " + settlement.ToString());
            Console.WriteLine("Reading Done!" + "\n \n");

            /*
            Console.WriteLine("Adding Inventory:");
            List<Article> articles = DBM.getArticleList();
            Stocktaking stockTaking = new Stocktaking(articles, true);

            for (int i = 0; i < stockTaking.Inventory.Count(); i++)
            {
                Tuple<Article, int> tuple = stockTaking.Inventory[i];
                stockTaking.Inventory[i] = new TupleAA(tuple.Article, 999);
            }
            DBM.addStocktaking(stockTaking);
            Console.WriteLine("StockTaking Successfully Added" + "\n \n");
            */
            /*
            Console.WriteLine("Reading StockTaking:");
            List<Stocktaking> stockTakings = DBM.getStocktakingList();
            Console.WriteLine("Amount of StockTakings: " + stockTakings.Count());
            Console.WriteLine("StockTaking Datas:");
            foreach (Stocktaking p in stockTakings)
            {
                Console.WriteLine(p.toString());
            }
            Console.WriteLine("Reading Done!" + "\n \n");


            /*
            Console.WriteLine("Changing Stock for Booking:");
            Console.WriteLine(posts[0].toString());
            bool bookingChanged = DAM.addBookingData(posts[0]);
            Console.WriteLine("Changing Stock was Successfully: " + bookingChanged.ToString() + "\n");
            */
            /*
            Console.WriteLine("Reading Current Account Datas");
            List<Article> articles = DBM.getArticleList();
            foreach (Article p in articles)
            {
                Console.WriteLine("Article ID:" + p.ID);
                Console.WriteLine("-Name:" + p.Name);
                Console.WriteLine("-Stock:" + p.Stock);
            }
            Console.WriteLine("Reading Done!" + "\n \n");


            Console.WriteLine("Getting Account Favourites:");
            List<Account> accs = DAM.getAccountList();
            Account acc3 = accs[0];
            Console.WriteLine("Getting Account:" + acc3.toString());
            /*
            Console.WriteLine("Stock 0: " + acc3.Favourite[0].Stock);
            Console.WriteLine("Stock 1: " + acc3.Favourite[1].Stock);
            Console.WriteLine("Stock 2: " + acc3.Favourite[2].Stock);
            *//*
            Console.WriteLine("Favourite was added Successfully: " + "\n");

            Console.Write("Press ANY Key to Quit");
            Console.ReadKey();
            */
        }
    }
}
