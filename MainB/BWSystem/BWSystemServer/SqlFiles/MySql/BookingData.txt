CREATE TABLE {0} (
	BookingID    INT      NOT NULL AUTO_INCREMENT,
	BookingSubID INT      NULL,
	UserID       INT      NULL,
	CustomerID   INT      NOT NULL,
	ArticleID    INT      NOT NULL,
	Amount       INT      NOT NULL,
	EventID      INT      NULL,
	TimeStamp    DATETIME NOT NULL,
	Cancellation BIT      NOT NULL,
	PRIMARY KEY (BookingID),
	CONSTRAINT FK_Booking_User FOREIGN KEY (UserID) REFERENCES Account (AccountID),
	CONSTRAINT FK_Booking_Customer FOREIGN KEY (CustomerID) REFERENCES Account (AccountID),
	CONSTRAINT FK_Booking_Article FOREIGN KEY (ArticleID) REFERENCES Article (ArticleID),
	CONSTRAINT FK_Booking_Event FOREIGN KEY (EventID) REFERENCES Event (EventID)
);

