﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

using BWSystemClient2.GUI.GuiElements;
using BWSystemClient2.GUI.SubElements;
using BWSystemClient2.GUI.SubElements.ItemContainer;
using BWSystemClient2.GUI.SubElements.TopSubPanel;
using BWSystemLibrary2;
using System.Windows;

namespace BWSystemClient2.GUI
{
    public delegate void OnOK();
    public delegate void OnCancel();
    public delegate void OnConsumer(int id);
    public delegate void OnUser(int id);
    public delegate void OnAuthentification(int[] securityCode);
    public delegate void OnArticle(int id);
    public delegate void OnAmount(int amount);

    public delegate void OnCardEntered();
    public delegate void OnTimerUp();
    public delegate void OnTimerReset();

    public delegate void OnJoker(int joker);
    public delegate void OnSort(SortingMethod method);
    public delegate void OnSyncData();

    public delegate bool OnAccountLoginCreationComplete(string login, string password, string mailAddress);

    class GuiController
    {
        //Button Pressed Events
        private OnOK onOK;
        private OnCancel onCancel;
        private OnConsumer onConsumer;
        private OnArticle onArticle;
        private OnAmount onAmount;
        private OnUser onUser;
        private OnAuthentification onAuthentification;

        private OnTimerReset onTimerReset;
        private OnTimerUp onTimerUp;
        private OnCardEntered onCardEntered;

        private OnJoker onJoker;
        private OnSort onSort;
        private OnSyncData onSyncData;


        private static GuiController instance;

        private GuiElementStates state;
        private MainWindow mainWindow;

        //SecrutyEvents
        private DispatcherTimer securityTimer;
        private Action nextPanel;

        //TempPanelTimer
        //private DispatcherTimer tempPanelTimer;

        //ItemContainers
        private MainItemContainer consumers;
        private MainItemContainer users;
        private MainItemContainer articles;
        private AmountSelection amountSelection;
        private AuthentificationPanel authentification;
        private CancellationPanel cancellation;

        private AccountPreCreation optionSelection;
        private AccountImportCreation importSelection;
        private AccountLoginCreation loginCreation;
        private FirstUsageAuthentificationPanel firstUsage;
        private AccountCreation accountModification;
        private ArticleInformation articleInformation;

        private BookingSuccessful bookingSuccesful;
        private MessageOverlay messageOverlay;
        private WaitingOverlay waitingOverlay;

        //TopMainContainers
        private StdTopPanel consumerTMPanel;
        private BookingPanel bookingPanel;
        private StdTopPanel eventTopMainPanel;

        //TopSubContainers
        private TopSubPanel_Text consumerTSPanel;
        private TopSubPanel_Text userTSPanel;
        private TopSubPanel_Text articleTSPanel;
        private TopSubPanel_Text amountTSPanel;
        private TopSubPanel_Text authentificationTSPanel;
        private TopSubPanel_Text articleInfoTSPanel;

        private TopSubPanel_Text cancellationTSPanel;

        private TopSubPanel_Text eventArticleTSPanel;

        private TopSubPanel_Text optionTSPanel;
        private TopSubPanel_Text importTSPanel;
        private TopSubPanel_Text loginCreationTSPanel;
        private TopSubPanel_Text modificationTSPanel;
        private TopSubPanel_Text authRecTSPanel;
        private TopSubPanel_Text messageOverlayTSPanel;

        //ClockTimes
        private int clockTime_Product;
        private int clockTime_User;
        private int clockTime_AmountSelection;
        private int clockTime_Authentification;
        private int clockTime_Cancellation;
        private int clockTime_Warning;
        private int ClockTime_AccountEditing;

        private GuiController()
        {
            this.state = GuiElementStates.Instance;

            //Main Panels
            this.consumers = new MainItemContainer();
            this.users = new MainItemContainer();
            this.articles = new MainItemContainer();
            this.amountSelection = new AmountSelection();
            this.authentification = new AuthentificationPanel();
            this.cancellation = new CancellationPanel();
            this.optionSelection = new AccountPreCreation();
            this.importSelection = new AccountImportCreation();
            this.loginCreation = new AccountLoginCreation();
            this.firstUsage = new FirstUsageAuthentificationPanel();
            this.accountModification = new AccountCreation();
            this.articleInformation = new ArticleInformation();
            this.bookingSuccesful = new BookingSuccessful();
            this.messageOverlay = new MessageOverlay();
            this.waitingOverlay = new WaitingOverlay();

            //Top Main Panels
            this.consumerTMPanel = new StdTopPanel();
            this.consumerTMPanel.setText((string)LibUtility.getConfigValue("Title", typeof(string)), (string)LibUtility.getConfigValue("SubTitle", typeof(string)));

            this.bookingPanel = new BookingPanel();

            this.eventTopMainPanel = new StdTopPanel();

            //Top Sub Panels
            this.consumerTSPanel = new TopSubPanel_Text("Select the Customer");
            this.userTSPanel = new TopSubPanel_Text("You need to Authentificate. Who are you?");
            this.articleTSPanel = new TopSubPanel_Text("Select an Article");
            this.amountTSPanel = new TopSubPanel_Text("Select the Amout of Articles");
            this.authentificationTSPanel = new TopSubPanel_Text("Enter your security code");

            this.cancellationTSPanel = new TopSubPanel_Text("Select the Booking to cancel");

            this.eventArticleTSPanel = new TopSubPanel_Text("Select the Article to book");

            this.optionTSPanel = new TopSubPanel_Text("Select the intendent functionality");
            this.importTSPanel = new TopSubPanel_Text("Enter your login credentials");
            this.loginCreationTSPanel = new TopSubPanel_Text("Enter your prefered login credentials");
            this.modificationTSPanel = new TopSubPanel_Text("Fill the fields");
            this.authRecTSPanel = new TopSubPanel_Text("Enter your personal 6 nodes long security wipe code");
            this.articleInfoTSPanel = new TopSubPanel_Text("The article information are:");
            this.messageOverlayTSPanel = new TopSubPanel_Text("Please read the message below carefully");

            this.securityTimer = new DispatcherTimer();
            double timerTime = (double)BWSystemLibrary2.LibUtility.getConfigValue("AuthentificationEnterReset", typeof(double));
            securityTimer.Interval = TimeSpan.FromMilliseconds(timerTime);
            securityTimer.Tick += this.restAuthentification;

            //this.tempPanelTimer = new DispatcherTimer();
            //this.tempPanelTimer.Interval = TimeSpan.FromSeconds(1.5);


            //Read ClockTimers
            this.clockTime_Product = (int)LibUtility.getConfigValue("ClockTime_Product", typeof(int));
            this.clockTime_User = (int)LibUtility.getConfigValue("ClockTime_User", typeof(int));
            this.clockTime_AmountSelection = (int)LibUtility.getConfigValue("clockTime_AmountSelection", typeof(int));
            this.clockTime_Authentification = (int)LibUtility.getConfigValue("clockTime_Authentification", typeof(int));
            this.clockTime_Cancellation = (int)LibUtility.getConfigValue("ClockTime_Cancellation", typeof(int));
            this.clockTime_Warning = (int)LibUtility.getConfigValue("ClockTime_Warning", typeof(int));
            this.ClockTime_AccountEditing = (int)LibUtility.getConfigValue("ClockTime_AccountEditing", typeof(int));
        }


        internal void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }


        public static GuiController Instance
        {
            get
            {
                if (GuiController.instance == null)
                {
                    GuiController.instance = new GuiController();
                }
                return GuiController.instance;
            }
        }


        internal void updateButtonPanels()
        {
            if (!this.mainWindow.CheckAccess())
            {
                this.mainWindow.Dispatcher.BeginInvoke(new Action(() => this.updateButtonPanels()));
            }
            else
            {
                GuiDataStorage guiData = GuiDataStorage.Instance;
                this.consumers.addItems(guiData.ConsumerButtons);
                this.users.addItems(guiData.UserButtons);
                this.articles.addItems(guiData.ArticleButtons);
            }
        }

        internal void setAmountButtonValues(int?[] buttonValues)
        {
            this.amountSelection.updateValues(buttonValues);
        }


        /**
         * Getter
         */
        public BookingPanel BookingPanel
        {
            get { return bookingPanel; }
        }

        public CancellationPanel Cancellation
        {
            get { return cancellation; }
        }

        public void showCustomerSelection(bool showBookingTopPanel = false)
        {
            if (!this.mainWindow.CheckAccess())
            {
                this.mainWindow.Dispatcher.BeginInvoke(new Action(() => this.showCustomerSelection(showBookingTopPanel)));
            }
            else
            {
                this.state.MoreExpander = this.onCancel == null ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                this.state.SortingExpander = System.Windows.Visibility.Visible;
                this.state.Clock = showBookingTopPanel ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                this.subscribeOnSort(new GUI.OnSort(this.onConsumerSort));

                if (!showBookingTopPanel)
                    this.mainWindow.addPanel(this.consumers, this.consumerTMPanel, this.consumerTSPanel, null);
                else
                    this.mainWindow.addPanel(this.consumers, this.bookingPanel, this.consumerTSPanel, this.clockTime_User);
            }
        }


        public void showArticleSelection()
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Visible;
            this.state.Clock = System.Windows.Visibility.Visible;
            this.subscribeOnSort(new GUI.OnSort(this.onArticleSort));

            this.mainWindow.addPanel(this.articles, this.BookingPanel, this.articleTSPanel, this.clockTime_Product);
        }


        public void setEventTopPanelData(string main, string sub)
        {
            this.eventTopMainPanel.setText(main, sub);
        }


        public void showEventArticleSelection(bool showBookingPanel)
        {
            this.state.MoreExpander = showBookingPanel ? System.Windows.Visibility.Hidden : System.Windows.Visibility.Visible;
            this.state.SortingExpander = System.Windows.Visibility.Visible;
            this.state.Clock = showBookingPanel ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
            this.subscribeOnSort(new GUI.OnSort(this.onArticleSort));

            if (showBookingPanel)
            {
                this.mainWindow.addPanel(this.articles, this.bookingPanel, this.eventArticleTSPanel, this.clockTime_Product);
            }
            else
            {
                this.mainWindow.addPanel(this.articles, this.eventTopMainPanel, this.eventArticleTSPanel, null);
            }
        }


        public void showAmountSelection(int articleID, int startAmount, string picturePath, int?[] buttonValues)
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Visible;
            BWSystemLibrary2.BW_WebService.Article article = BWSystemClient2.Domain.DataStorage.Instance.Articles.First(x => x.ID == articleID);
            this.amountSelection.setData(article, startAmount, picturePath, buttonValues);
            this.mainWindow.addAmountOverlay(this.amountSelection, this.amountTSPanel, this.clockTime_AmountSelection);
        }


        public void showArticleInformation(BWSystemLibrary2.BW_WebService.Article article)
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Hidden;
            this.articleInformation.setContent(article);
            this.mainWindow.addPanel(this.articleInformation, this.bookingPanel, this.articleInfoTSPanel, null);
        }

        public void showUserSelection()
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Visible;
            this.state.Clock = System.Windows.Visibility.Visible;
            this.subscribeOnSort(new GUI.OnSort(this.onUserSort));

            this.mainWindow.addPanel(this.users, this.BookingPanel, this.userTSPanel, this.clockTime_User);
        }

        public void showFirstUsage(bool showErrorMsg)
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Visible;
            this.authentification.reset();
            this.firstUsage.showErrorMsg(showErrorMsg);
            this.mainWindow.addPanel(this.firstUsage, this.consumerTMPanel, this.authentificationTSPanel, this.clockTime_Authentification);
        }

        public void showAuthentification()
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Visible;
            this.authentification.reset();
            this.mainWindow.addPanel(this.authentification, this.BookingPanel, this.authentificationTSPanel, this.clockTime_Authentification);
        }

        public void showAuthentificationRecording()
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Hidden;
            this.authentification.reset();
            this.mainWindow.addPanel(this.authentification, this.consumerTMPanel, this.authRecTSPanel, null);
        }

        internal void correctAuthentificationEntered(bool correctCode, Action nextPanel)
        {
            this.nextPanel = nextPanel;
            this.authentification.CorrectCodeEntered = correctCode;
            this.securityTimer.IsEnabled = true;
        }


        private void restAuthentification(object sender, EventArgs e)
        {
            this.securityTimer.IsEnabled = false;
            this.authentification.reset();
            if (this.nextPanel != null)
            {
                this.nextPanel();
            }
        }

        internal void showSuccessfulBookedPanel() //List<ButtonItem_Article> bookedArticles
        {
            ICollection<ButtonItem_ArticleBooking> guiBookings = this.bookingPanel.getBooking();
            this.bookingPanel.resetPanel();
            this.bookingSuccesful.setBookedItems(guiBookings);
            this.mainWindow.addFadeoutCenterPanel(this.bookingSuccesful);
        }


        //Cancellation
        public void showBookingSelection()
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Visible;
            this.mainWindow.addPanel(this.cancellation, this.consumerTMPanel, this.cancellationTSPanel, this.clockTime_Cancellation);
        }


        //Account Import
        internal void showOptionPanel()
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Hidden;
            this.mainWindow.addPanel(this.optionSelection, this.consumerTMPanel, this.optionTSPanel, null);
        }

        internal void showImportPanel(Action<string, string> importFunction, bool retryState = false)
        {
            if (!this.mainWindow.CheckAccess())
            {
                this.mainWindow.Dispatcher.BeginInvoke(new Action(() => this.showImportPanel(importFunction, retryState)));
            }
            else
            {
                this.state.MoreExpander = System.Windows.Visibility.Hidden;
                this.state.SortingExpander = System.Windows.Visibility.Hidden;
                this.state.Clock = System.Windows.Visibility.Hidden;
                this.mainWindow.addPanel(this.importSelection, this.consumerTMPanel, this.importTSPanel, null);
                this.importSelection.reset(importFunction, retryState);
            }
        }


        internal void showAccountLoginCreation(OnAccountLoginCreationComplete onAccountLoginCreationComplete, 
            BWSystemLibrary2.BW_WebService.InputRegex[] loginInputRegexs, 
            BWSystemLibrary2.BW_WebService.InputRegex[] passwordInputRegexs, 
            BWSystemLibrary2.BW_WebService.InputRegex[] mailInputRegexs)
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Visible;

            this.loginCreation.init(onAccountLoginCreationComplete, loginInputRegexs, passwordInputRegexs, mailInputRegexs);

            this.showAccountLoginCreation();
        }


        internal void showAccountLoginCreation()
        {
            this.mainWindow.addPanel(this.loginCreation, this.consumerTMPanel, this.loginCreationTSPanel, this.ClockTime_AccountEditing);
        }


        internal void showAccountModification(BWSystemLibrary2.BW_WebService.Account account)
        {
            if (!this.mainWindow.CheckAccess())
            {
                this.mainWindow.Dispatcher.BeginInvoke(new Action(() => this.showAccountModification(account)));
            }
            else
            {
                this.state.MoreExpander = System.Windows.Visibility.Hidden;
                this.state.SortingExpander = System.Windows.Visibility.Hidden;
                this.state.Clock = System.Windows.Visibility.Visible;
                this.accountModification.setContext(new AccountCreation_Account(account));
                this.mainWindow.addPanel(this.accountModification, this.consumerTMPanel, this.modificationTSPanel, this.ClockTime_AccountEditing);
            }
        }

        internal void showWarningPanel(string warningMsg)
        {
            if (!this.mainWindow.CheckAccess())
            {
                this.mainWindow.Dispatcher.BeginInvoke(new Action(() => this.showWarningPanel(warningMsg)));
            }
            else
            {
                this.state.MoreExpander = System.Windows.Visibility.Hidden;
                this.state.SortingExpander = System.Windows.Visibility.Hidden;
                this.state.Clock = System.Windows.Visibility.Visible;
                this.messageOverlay.setWarningMessage(warningMsg);
                this.mainWindow.addPanel(this.messageOverlay, this.consumerTMPanel, this.messageOverlayTSPanel, this.clockTime_Warning);
            }
        }

        internal void showWaitingOverlay()
        {
            this.state.MoreExpander = System.Windows.Visibility.Hidden;
            this.state.SortingExpander = System.Windows.Visibility.Hidden;
            this.state.Clock = System.Windows.Visibility.Hidden;
            this.mainWindow.addOverlay(this.waitingOverlay);
        }

        internal void showMessageBox(string msg, string headline, MessageBoxButton buttons, MessageBoxImage image)
        {
            MessageBox.Show(msg, headline, buttons, image);
        }


        //Manage Sort Methods
        internal void onConsumerSort(SortingMethod method)
        {
            Domain.WebService.DataSync dataSync = new Domain.WebService.DataSync();
            int[] order = dataSync.getSorting(LibUtility.convertSortingMethod(method), true);
            if (order != null)
                this.consumers.sortItems(order);
        }

        internal void onUserSort(SortingMethod method)
        {
            Domain.WebService.DataSync dataSync = new Domain.WebService.DataSync();
            int[] order = dataSync.getSorting(LibUtility.convertSortingMethod(method), true);
            if (order != null)
                this.users.sortItems(order);
        }

        internal void onArticleSort(SortingMethod method)
        {
            Domain.WebService.DataSync dataSync = new Domain.WebService.DataSync();
            int[] order = dataSync.getSorting(LibUtility.convertSortingMethod(method), false);
            if (order != null)
                this.articles.sortItems(order);
        }


        /**
         * Subscriber Functions
         */
        internal void clearAllSubscriber()
        {
            this.onOK = null;
            this.onCancel = null;
            this.onConsumer = null;
            this.onUser = null;
            this.onAuthentification = null;
            this.onArticle = null;
            this.onAmount = null;
            this.onJoker = null;
            this.onCardEntered = null;
            this.onTimerUp = null;
            this.onSyncData = null;
            this.state.OkButton = System.Windows.Visibility.Hidden;
            this.state.CancelButton = System.Windows.Visibility.Hidden;
        }

        internal void subscribeOnOK(OnOK function)
        {
            this.state.OkButton = System.Windows.Visibility.Visible;
            this.onOK += function;
        }

        internal void unSubscribeOnOK(OnOK function)
        {
            this.state.OkButton = System.Windows.Visibility.Hidden;
            this.onOK -= function;
        }

        internal void subscribeOnCancel(OnCancel function)
        {
            this.state.CancelButton = System.Windows.Visibility.Visible;
            this.onCancel += function;
        }

        internal void unSubscribeOnCancel(OnCancel function)
        {
            this.state.CancelButton = System.Windows.Visibility.Hidden;
            this.onCancel -= function;
        }

        internal void subscribeOnConsumer(OnConsumer function)
        {
            this.onConsumer += function;
        }

        internal void subscribeOnUser(OnUser function)
        {
            this.onUser += function;
        }

        internal void subscribeOnAuthentification(OnAuthentification function)
        {
            this.onAuthentification += function;
        }

        internal void subscribeOnArticle(OnArticle function)
        {
            this.onArticle += function;
        }

        internal void subscribeOnAmount(OnAmount function)
        {
            this.onAmount += function;
        }


        //Card
        internal void subscribeOnCardEntered(OnCardEntered function)
        {
            this.onCardEntered += function;
        }

        internal void unSubscribeCardEntered(OnCardEntered function)
        {
            this.onCardEntered -= function;
        }

        //Timer
        internal void subscribeOnTimerUp(OnTimerUp function)
        {
            this.onTimerUp += function;
        }

        internal void unSubscribeCardEntered(OnTimerUp function)
        {
            this.onTimerUp -= function;
        }

        internal void subscribeOnTimerReset(OnTimerReset function)
        {
            this.onTimerReset += function;
        }

        internal void unSubscribeOnTimerReset(OnTimerReset function)
        {
            this.onTimerReset -= function;
        }

        //Joker
        internal void subscribeOnJoker(OnJoker function)
        {
            this.onJoker += function;
        }

        internal void unSubscribeOnJoker(OnJoker function)
        {
            this.onJoker -= function;
        }

        //Sort
        private void subscribeOnSort(OnSort function)
        {
            this.clearOnSort();
            this.onSort += function;
        }

        private void clearOnSort()
        {
            if (this.onSort != null)
            {
                Delegate[] methods = this.onSort.GetInvocationList();
                foreach (Delegate method in methods)
                {
                    this.onSort -= (OnSort)method;
                }
            }
        }

        //DataSync
        internal void subscribeOnDataSync(OnSyncData function)
        {
            this.onSyncData = null;
            this.onSyncData += function;
        }

        internal void unSubscribeOnDataSync(OnSyncData function)
        {
            this.onSyncData -= function;
        }

        /**
         * Invoke Functions
         */
        internal void OKPressed()
        {
            this.onOK();
        }

        internal void CancelPressed()
        {
            this.onCancel();
        }

        internal void ConsumerPressed(int consumerID)
        {
            this.onConsumer(consumerID);
        }

        internal void UserPressed(int userID)
        {
            this.onUser(userID);
        }

        internal void UserAuthentificationPressed(int[] securityCode)
        {
            this.onAuthentification(securityCode);
        }

        internal void ArticlePressed(int articleID)
        {
            this.onArticle(articleID);
        }

        internal void AmountPressed(int amount)
        {
            this.TimerReset();
            this.onAmount(amount);
        }



        internal void CardEntered()
        {
            if (this.onCardEntered != null)
            {
                this.onCardEntered();
            }
        }

        internal void TimerUp()
        {
            if (this.onTimerUp != null)
            {
                this.onTimerUp();
            }
        }

        internal void TimerReset()
        {
            if (this.onTimerReset != null)
            {
                this.onTimerReset();
            }
        }


        internal void JokerPressed(int jokerID)
        {
            if (this.onJoker != null)
            {
                this.onJoker(jokerID);
            }
        }

        internal void OnSort(SortingMethod method)
        {
            if (this.onSort != null)
            {
                this.onSort(method);
            }
        }

        internal void OnSyncData()
        {
            if (this.onSyncData != null)
            {
                this.onSyncData();
            }
        }
    }
}
