﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BWSystemClient2.GUI.SubElements.ItemContainer;

namespace BWSystemClient2.GUI
{
    public struct GuiBookingData
    {
        public ButtonItem_User user;
        public ButtonItem_Consumer consumer;
        public LinkedList<ButtonItem_ArticleBooking> articles;
    }

    class GuiDataStorage
    {
        private static GuiDataStorage instance;

        private ButtonItem[] consumerButtons;
        private ButtonItem[] userButtons;
        private ButtonItem[] articleButtons;


        private GuiDataStorage()
        {
            this.consumerButtons = new ButtonItem[0];
            this.userButtons = new ButtonItem[0];
            this.articleButtons = new ButtonItem[0];
        }


        internal static GuiDataStorage Instance
        {
            get
            {
                if (instance == null)
                {
                    GuiDataStorage.instance = new GuiDataStorage();
                }
                return GuiDataStorage.instance;
            }
        }


        public ButtonItem[] UserButtons
        {
            get { return userButtons; }
            set { userButtons = value; }
        }

        public ButtonItem[] ConsumerButtons
        {
            get { return consumerButtons; }
            set { consumerButtons = value; }
        }

        public ButtonItem[] ArticleButtons
        {
            get { return articleButtons; }
            set { articleButtons = value; }
        }
    }
}
