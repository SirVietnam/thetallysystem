﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BWSystemClient2.GUI.GuiElements
{
    /// <summary>
    /// Interaction logic for AuthentificationPanel.xaml
    /// </summary>
    public partial class AuthentificationPanel : UserControl
    {
        private bool mouseIsDown;
        private List<Ellipse> elements;
        private List<Line> lines;
        private bool? correctCodeEntered;

        private Style ellipseStandardStyle;
        private Style ellipseSelectedStyle;
        private Style ellipseCorrectStyle;
        private Style ellipseErrorStyle;
        private Style lineStyle;


        public AuthentificationPanel()
        {
            InitializeComponent();

            this.elements = new List<Ellipse>();
            this.lines = new List<Line>();
            this.correctCodeEntered = null;
            this.mouseIsDown = false;

            this.DataContext = this;
            this.lineStyle = this.FindResource("lineStyle") as Style;
            this.ellipseStandardStyle = this.FindResource("stdEllipse") as Style;
            this.ellipseSelectedStyle = this.FindResource("selectedEllipse") as Style;
            this.ellipseCorrectStyle = this.FindResource("correctEllipse") as Style;
            this.ellipseErrorStyle = this.FindResource("errorEllipse") as Style;
        }

        /*
         * Getter
         */
        internal bool? CorrectCodeEntered
        {
            get { return correctCodeEntered; }
            set
            {
                correctCodeEntered = value;
                this.colourLines();
            }
        }


        /*
         * Click Events
         */
        private void onMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.mouseIsDown = true;
        }

        private void onMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.mouseIsDown)
            {
                this.mouseIsDown = false;
                int[] securityCode = this.buildSecurityCode();

                GuiController.Instance.UserAuthentificationPressed(securityCode);
            }
        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            if (this.mouseIsDown)
            {
                Ellipse senderElement = sender as Ellipse;
                senderElement.Style = this.ellipseSelectedStyle;

                string senderName = senderElement.Name;

                if (this.elements.Count == 0 || this.elements.Last() != senderElement)
                {
                    this.elements.Add(sender as Ellipse);

                    if (this.elements.Count > 1)
                    {
                        this.connectLastElements();
                    }
                }
            }
        }


        /*
         * Dawing Methods
         */
        private void connectLastElements()
        {
            Ellipse startElipse = this.elements[this.elements.Count - 2];
            Ellipse endElipse = this.elements[this.elements.Count - 1];
            double deltaX = (startElipse.Width + endElipse.Width) / 4;
            double deltaY = (startElipse.Height + endElipse.Height) / 4;

            Point startPoint = this.securityCanvas.TranslatePoint(new Point(0, 0), startElipse);
            Point endPoint = this.securityCanvas.TranslatePoint(new Point(0, 0), endElipse);

            Line line = new Line();
            line.X1 = Math.Abs(startPoint.X) + deltaX;
            line.Y1 = Math.Abs(startPoint.Y) + deltaY;
            line.X2 = Math.Abs(endPoint.X) + deltaX;
            line.Y2 = Math.Abs(endPoint.Y) + deltaY;
            line.Style = this.lineStyle;
            this.lines.Add(line);

            this.securityCanvas.Children.Add(line);
        }

        internal void reset()
        {
            foreach (Ellipse ellipse in this.elements)
            {
                ellipse.Style = this.ellipseStandardStyle;
            }
            this.elements.Clear();

            foreach (Line line in this.lines)
            {
                this.securityCanvas.Children.Remove(line);
            }
            this.lines.Clear();

        }

        private void colourLines()
        {
            if (this.CorrectCodeEntered != null)
            {
                LinearGradientBrush brush = null;
                if ((bool)this.CorrectCodeEntered)
                {
                    brush = this.FindResource("lineBrushGreen") as LinearGradientBrush;
                }
                else
                {
                    brush = this.FindResource("lineBrushRed") as LinearGradientBrush;
                }

                foreach (Line line in this.lines)
                {
                    line.Stroke = brush;
                }

                Style ellipseStyle = (bool)this.CorrectCodeEntered ? this.ellipseCorrectStyle : this.ellipseErrorStyle;
                foreach (Ellipse ellipse in this.elements)
                {
                    ellipse.Style = ellipseStyle;
                }
            }
        }


        /*
         * Helping Methods
         */
        private int[] buildSecurityCode()
        {
            int[] code = new int[this.elements.Count];

            for (int i = 0; i < this.elements.Count; i++)
            {
                string figureStr = this.elements[i].Name.Replace("f", "");
                code[i] = Convert.ToInt32(figureStr);
            }
            return code;
        }
    }
}
