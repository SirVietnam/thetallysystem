﻿using BWSystemClient2.GUI.SubElements.ItemContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.GuiElements
{
    /// <summary>
    /// Interaction logic for CancellationPanel.xaml
    /// </summary>
    public partial class CancellationPanel : UserControl
    {
        public CancellationPanel()
        {
            InitializeComponent();
        }

        public void setData(CancellationContainer[] containers)
        {
            this.BookingsStack.Children.Clear();
            foreach (CancellationContainer container in containers.Reverse())
            {
                this.BookingsStack.Children.Add(container);
            }
        }
    }
}
