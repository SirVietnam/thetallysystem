﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.GuiElements
{
    /// <summary>
    /// Interaction logic for TopPanel.xaml
    /// </summary>
    public partial class TopPanel : UserControl
    {
        private int clockAppendTime;

        public TopPanel()
        {
            this.DataContext = GuiElementStates.Instance;
            this.clockAppendTime = (int)BWSystemLibrary2.LibUtility.getConfigValue("ClockTime_Append", typeof(int));
            InitializeComponent();
        }

        private void OnClockClick(object sender, MouseButtonEventArgs e)
        {
            int appendSeconds = this.clockAppendTime;
            this.clock.appendTime(appendSeconds);
        }

        internal void setTopPanel(UserControl mainPanel, UserControl subPanel, int? clockStartTimer)
        {
            this.HeadGrid.Children.Clear();
            this.HeadGrid.Children.Add(mainPanel);

            this.HeadSubGrid.Children.Clear();
            this.HeadSubGrid.Children.Add(subPanel);

            this.setClockStartTimer(clockStartTimer);
        }

        internal void setTopSubPanel(UserControl subPanel, int? clockStartTimer)
        {
            this.HeadSubGrid.Children.Clear();
            this.HeadSubGrid.Children.Add(subPanel);

            this.setClockStartTimer(clockStartTimer);
        }

        private void setClockStartTimer(int? clockStartTimer)
        {
            if (clockStartTimer != null)
            {
                this.clock.startTimer((int)clockStartTimer);
            }
        }
    }
}
