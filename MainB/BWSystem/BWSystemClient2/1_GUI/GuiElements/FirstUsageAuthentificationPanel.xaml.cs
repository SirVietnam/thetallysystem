﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.GuiElements
{
    /// <summary>
    /// Interaction logic for FirstUsageAuthentificationPanel.xaml
    /// </summary>
    public partial class FirstUsageAuthentificationPanel : UserControl
    {
        public FirstUsageAuthentificationPanel()
        {
            InitializeComponent();
        }


        public void showErrorMsg(bool showErrorMsg)
        {
            this.errorLB.Visibility = showErrorMsg ? Visibility.Visible: Visibility.Hidden;
        }

        private void onUseLogin(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.JokerPressed(0);
        }
    }
}
