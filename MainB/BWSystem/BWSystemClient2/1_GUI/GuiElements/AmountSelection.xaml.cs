﻿using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI
{
    /// <summary>
    /// Interaktionslogik für AmountSelection.xaml
    /// </summary>
    public partial class AmountSelection : UserControl
    {
        Article article;
        int amount;
        private int startTimer;

        private int?[] buttonValues;


        public AmountSelection()
        {

            InitializeComponent();
        }


        /**
         * Getter
         */
        private int Amount
        {
            get { return amount; }
            set 
            { 
                this.amount = value;
                this.AmoutTextBox.Text = amount.ToString();
            }
        }


        private void OnClick(object sender, MouseButtonEventArgs e)
        {

        }


        internal void setData(Article article, int startAmount, string picturePath, int?[] buttonValues)
        {
            this.article = article;
            this.ArticleNameLabel.Content = article.Name;
            this.prizeLabel.Content = article.Prize.ToString("0.00") + " €/Item";
            this.Amount = startAmount;
            ImageBrush articleImage = new ImageBrush();
            articleImage.Stretch = Stretch.UniformToFill;
            articleImage.ImageSource = new BitmapImage(new Uri(picturePath));
            this.buttonPic.Background = articleImage;
            this.updateValues(buttonValues);
        }


        internal void updateValues(int?[] buttonValues)
        {
            this.buttonValues = buttonValues;

            if (buttonValues[0] == null)
            {
                this.minusBigContainer.Opacity = 0.25;
                this.minusBig.Content = "";
            }
            else
            {
                this.minusBigContainer.Opacity = 1.0;
                this.minusBig.Content = "-" + buttonValues[0].ToString();
            }

            if (buttonValues[1] == null)
            {
                this.minusNormalContainer.Opacity = 0.25;
                this.minusNormal.Content = "";
            }
            else
            {
                this.minusNormalContainer.Opacity = 1.0;
                this.minusNormal.Content = "-" + buttonValues[1].ToString();
            }

            if (buttonValues[2] == null)
            {
                this.minusLittleContainer.Opacity = 0.25;
                this.minusLittle.Content = "";
            }
            else
            {
                this.minusLittleContainer.Opacity = 1.0;
                this.minusLittle.Content = "-" + buttonValues[2].ToString();
            }

            this.plusLittle.Content = "+" + buttonValues[3].ToString();
            this.plusNormal.Content = "+" + buttonValues[4].ToString();
            this.plusBig.Content = "+" + buttonValues[5].ToString();
        }

        /*
         * Amount Listeners
         */
        private void OnPlusBig(object sender, MouseButtonEventArgs e)
        {
            this.Amount += (int)this.buttonValues[5];
            this.reportAmount();
        }

        private void OnPlusNormal(object sender, MouseButtonEventArgs e)
        {
            this.Amount += (int)this.buttonValues[4];
            this.reportAmount();
        }

        private void OnPlusLittle(object sender, MouseButtonEventArgs e)
        {
            this.Amount += (int)this.buttonValues[3];
            this.reportAmount();
        }

        private void OnMinusLittle(object sender, MouseButtonEventArgs e)
        {
            if (this.buttonValues[2] != null)
            {
                this.Amount -= (int)this.buttonValues[2];
            }
            this.reportAmount();
        }

        private void OnMinusNormal(object sender, MouseButtonEventArgs e)
        {
            if (this.buttonValues[1] != null)
            {
                this.Amount -= (int)this.buttonValues[1];
            }
            this.reportAmount();
        }

        private void OnMinusBig(object sender, MouseButtonEventArgs e)
        {
            if (this.buttonValues[0] != null)
            {
                this.Amount -= (int)this.buttonValues[0];
            }
            this.reportAmount();
        }

        private void reportAmount()
        {
            GuiController.Instance.AmountPressed(this.Amount);
        }


        /*
         * Information Listeners
         */
        private void OnInformation(object sender, MouseButtonEventArgs e)
        {
            GuiController.Instance.JokerPressed(this.article.ID);
        }
    }
}
