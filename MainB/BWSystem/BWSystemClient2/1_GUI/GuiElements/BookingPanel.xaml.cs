﻿using BWSystemClient2.GUI.SubElements.ItemContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.GuiElements
{
    /// <summary>
    /// Interaction logic for BookingPanel.xaml
    /// </summary>
    public partial class BookingPanel : UserControl
    {
        private LinkedList<ButtonItem_ArticleBooking> items;
        private Dictionary<int, float> articlePrizeDict;

        public BookingPanel()
        {
            InitializeComponent();

            this.articlePrizeDict = new Dictionary<int, float>();
            this.items = new LinkedList<ButtonItem_ArticleBooking>();
            this.updatePrizeLabel();
        }

        public void updateArticlePrize(int articleID, float prize)
        {
            if (articlePrizeDict.ContainsKey(articleID))
            {
                articlePrizeDict.Remove(articleID);
                articlePrizeDict.Add(articleID, prize);
            }
            else
            {
                articlePrizeDict.Add(articleID, prize);
            }
            this.updatePrizeLabel();
        }

        public void removePrize(int articleID)
        {
            this.articlePrizeDict.Remove(articleID);
            this.updatePrizeLabel();
        }

        private void updatePrizeLabel()
        {
            float prizeTotal = articlePrizeDict.Values.Sum();
            this.prizeCompleteLabel.Content = prizeTotal.ToString("0.00") + " €";
        }


        public void resetPanel()
        {
            this.items.Clear();
            this.BookingsPanel.Children.Clear();
            this.articlePrizeDict.Clear();
            this.updatePrizeLabel();
        }



        public void addBooking(ButtonItem_ArticleBooking item)
        {
            if (!this.items.Contains(item))
            {
                this.items.AddLast(item);
                this.BookingsPanel.Children.Add(item);
            }
        }


        public void removeBooking(int id)
        {
            ButtonItem_ArticleBooking item = this.items.First(x => x.ID == id);
            if (item != null)
            {
                this.items.Remove(item);
                this.BookingsPanel.Children.Remove(item);
                this.removePrize(id);
            }
        }


        public ButtonItem_ArticleBooking[] getBooking()
        {
            return this.items.ToArray<ButtonItem_ArticleBooking>();
        }
    }
}
