﻿using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.GuiElements
{
    /// <summary>
    /// Interaktionslogik für ArticleInformation.xaml
    /// </summary>
    public partial class ArticleInformation : UserControl
    {
        private Article article;

        public ArticleInformation()
        {
            // TODO: Complete member initialization
            InitializeComponent();
        }

        private void OnBack(object sender, MouseButtonEventArgs e)
        {
            GuiController.Instance.JokerPressed(article.ID);
        }

        //internal void setContent(ArticleClassWrapper articleWrapper)
        //{
        //    this.article = articleWrapper;
        //    this.DataContext = articleWrapper;
        //}

        internal void setContent(Article article)
        {
            this.article = article;
            this.DataContext = this.article;
        }
    }
}
