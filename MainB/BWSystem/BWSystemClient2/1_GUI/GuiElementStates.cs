﻿using BWSystemLibrary2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace BWSystemClient2.GUI
{
    public class GuiElementStates : INotifyPropertyChanged
    {
        private static GuiElementStates instance;

        public event PropertyChangedEventHandler PropertyChanged;

        private Visibility okButton;
        private Visibility cancelButton;
        private Visibility clock;
        private Visibility sortingExpander;
        private Visibility moreExpander;

        private string consumerPicture;
        private int consumerBorderWidth;
        private string userPicture;
        private Visibility userPictureVis;

        private string defLogo;
        private string backgroundImage;

        private Color color_1;
        private Color color_2;
        private Color color_3;
        private double expanderOpacity;

        private GuiElementStates()
        {
            this.OkButton = Visibility.Collapsed;
            this.CancelButton = Visibility.Collapsed;
            this.Clock = Visibility.Hidden;
            this.SortingExpander = Visibility.Visible;
            this.MoreExpander = Visibility.Visible;
            this.UserPicture = null;
            this.defLogo = (string)BWSystemLibrary2.LibUtility.getConfigValue("PicturePath", typeof(string));
            this.defLogo += '\\' + (string)BWSystemLibrary2.LibUtility.getConfigValue("DefaultLogo", typeof(string));

            this.backgroundImage = (string)LibUtility.getConfigValue("BackgroundImagePath", typeof(string));
            this.color_1 = (Color)ColorConverter.ConvertFromString((string)LibUtility.getConfigValue("Style_BGColor_1", typeof(string)));
            this.color_2 = (Color)ColorConverter.ConvertFromString((string)LibUtility.getConfigValue("Style_BGColor_2", typeof(string)));
            this.color_3 = (Color)ColorConverter.ConvertFromString((string)LibUtility.getConfigValue("Style_BGColor_3", typeof(string)));
            this.expanderOpacity = (double)LibUtility.getConfigValue("Style_ExpanderOpacity", typeof(double));
        }


        public static GuiElementStates Instance
        {
            get
            {
                if (GuiElementStates.instance == null)
                {
                    GuiElementStates.instance = new GuiElementStates();
                }
                return GuiElementStates.instance;
            }
        }


        /*
         * Getter & Setter
         */
        public Visibility OkButton
        {
            get { return okButton; }
            set
            {
                okButton = value;
                this.onPropertyChanged("OkButton");
            }
        }

        public Visibility CancelButton
        {
            get { return cancelButton; }
            set
            {
                cancelButton = value;
                this.onPropertyChanged("CancelButton");
            }
        }

        public Visibility Clock
        {
            get { return clock; }
            set
            {
                clock = value;
                this.onPropertyChanged("Clock");
            }
        }

        public Visibility SortingExpander
        {
            get { return sortingExpander; }
            set
            {
                sortingExpander = value;
                this.onPropertyChanged("SortingExpander");
            }
        }

        public Visibility MoreExpander
        {
            get { return moreExpander; }
            set
            {
                moreExpander = value;
                this.onPropertyChanged("MoreExpander");
            }
        }

        public string ConsumerPicture
        {
            get { return consumerPicture; }
            set
            {
                if (value != null)
                {
                    consumerPicture = value;
                    this.ConsumerBorderWidth = 3;
                }
                else
                {
                    consumerPicture = this.defLogo;
                    this.ConsumerBorderWidth = 0;
                }
                this.onPropertyChanged("ConsumerPicture");
            }
        }

        public int ConsumerBorderWidth
        {
            get { return consumerBorderWidth; }
            set
            {
                consumerBorderWidth = value;
                this.onPropertyChanged("ConsumerBorderWidth");
            }
        }


        public string UserPicture
        {
            get { return userPicture; }
            set
            {
                if (value != null)
                {
                    userPicture = value;
                    this.UserPictureVis = Visibility.Visible;
                }
                else
                {
                    this.userPicture = "";
                    this.UserPictureVis = Visibility.Collapsed;
                }
                this.onPropertyChanged("UserPicture");

            }
        }


        public Visibility UserPictureVis
        {
            get { return userPictureVis; }
            set
            {
                userPictureVis = value;
                this.onPropertyChanged("UserPictureVis");
            }
        }


        //private abstract Visibility okButton;
        //private abstract Visibility okButton;

        private void onPropertyChanged(string field)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(field));
            }
        }


        // Colors
        public Color Color_1
        {
            get { return color_1; }
        }

        public Color Color_2
        {
            get { return color_2; }
        }

        public Color Color_3
        {
            get { return color_3; }
        }

        public string BackgroundImage
        {
            get { return backgroundImage; }
        }

        public double ExpanderOpacity
        {
            get { return expanderOpacity; }
        }
    }
}
