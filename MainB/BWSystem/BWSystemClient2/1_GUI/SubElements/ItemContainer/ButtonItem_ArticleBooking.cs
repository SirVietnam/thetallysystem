﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    public class ButtonItem_ArticleBooking : ButtonItem_Article
    {

        public ButtonItem_ArticleBooking(int id, string name, string picturePath)
            : base(id, name, picturePath)
        {
            SolidColorBrush brush = (SolidColorBrush)this.FindResource("ItemButton_Article");
            this.buttonPic.BorderBrush = brush;

            this.AmoutTextBox.Visibility = System.Windows.Visibility.Visible;
        }


        public int Amount
        {
            get { return Convert.ToInt32(this.AmoutTextBox.Text); }
            set { this.AmoutTextBox.Text = value.ToString(); }
        }
    }
}
