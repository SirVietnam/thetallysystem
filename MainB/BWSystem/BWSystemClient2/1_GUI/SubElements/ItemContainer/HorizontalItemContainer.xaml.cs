﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    /// <summary>
    /// Interaction logic for HorizontalItemContainer.xaml
    /// </summary>
    public partial class HorizontalItemContainer : UserControl
    {
        private double usedWidth;

        public HorizontalItemContainer()
        {
            InitializeComponent();
            this.usedWidth = 0.0;
        }

        internal int getItemSpaceCount(int itemSize)
        {
            double items = this.Width / (double)itemSize;
            return (int)Math.Floor(items);
        }

        internal int addItems(ButtonItem[] items, int startIndex, double panelWidth)
        {
            int itemIndex = startIndex;
            while ((itemIndex < items.Length) && (this.usedWidth + items[itemIndex].ItemWidth <= panelWidth))
            {
                this.addItem(items[itemIndex++]);
            }
            return itemIndex;
        }

        private void addItem(ButtonItem item)
        {
            this.HorizontalSP.Children.Add(item);
            this.usedWidth += item.ItemWidth;
        }

        internal void clearItems()
        {
            this.HorizontalSP.Children.Clear();
        }
    }
}
