﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    public class ButtonItem_User : ButtonItem
    {

        public ButtonItem_User(int id, string name, string picturePath)
            : base(id, name, picturePath)
        {
            SolidColorBrush brush = (SolidColorBrush)this.FindResource("ItemButton_User");
            this.buttonPic.BorderBrush = brush;
        }

        protected override void OnClick(object sender, MouseButtonEventArgs e)
        {
            GuiController.Instance.UserPressed(this.ID);
        }
    }
}
