﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    public class ButtonItem_Article : ButtonItem
    {

        public ButtonItem_Article(int id, string name, string picturePath)
            : base(id, name, picturePath)
        {
            ImageBrush imageBrush = (ImageBrush)this.buttonPic.Background;
            imageBrush.Stretch = Stretch.UniformToFill;
            SolidColorBrush brush = (SolidColorBrush)this.FindResource("ItemButton_Article");
            this.buttonPic.BorderBrush = brush;

            this.ButtonName.MaxHeight = this.ButtonName.FontSize * 2.0;
            this.ButtonName.TextTrimming = System.Windows.TextTrimming.None;
            this.ButtonName.TextWrapping = System.Windows.TextWrapping.Wrap;

        }

        protected override void OnClick(object sender, MouseButtonEventArgs e)
        {
            GuiController.Instance.ArticlePressed(this.ID);
        }
    }
}
