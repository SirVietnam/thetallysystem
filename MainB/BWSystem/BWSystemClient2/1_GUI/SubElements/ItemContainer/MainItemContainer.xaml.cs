﻿using BWSystemLibrary2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    /// <summary>
    /// Interaction logic for MainItemController.xaml
    /// </summary>
    public partial class MainItemContainer : UserControl
    {
        private ButtonItem[] items;
        private Dictionary<int, ButtonItem> itemDict;

        public MainItemContainer()
        {
            InitializeComponent();
        }

        internal void addItems(ButtonItem[] items)
        {
            this.items = items;
            this.itemDict = new Dictionary<int, ButtonItem>();
            foreach (ButtonItem item in items)
            {
                itemDict.Add(item.ID, item);
            }
        }

        internal void sortItems(int[] idList)
        {
            List<ButtonItem> sortedItems = new List<ButtonItem>();
            foreach (int id in idList)
            {
                if (this.itemDict.ContainsKey(id))
                {
                    sortedItems.Add(this.itemDict[id]);
                }
                else
                {
                    SimpleLog.Log("ID: " + id + " was not found at ButtonIcons but requested to order. It was left out!");
                }
            }
            this.items = sortedItems.ToArray();
        }

        internal void drawItems(double panelWidth)
        {
            Debug.Assert(items != null);
            //double winWidth = this.Width;
            this.VerticalItemController.clearItems();
            if (this.items != null)
                this.VerticalItemController.addItems(this.items, panelWidth - 40); //40 is for Margin!
        }

        internal void clear()
        {
            this.VerticalItemController.clearItems();
        }
    }
}
