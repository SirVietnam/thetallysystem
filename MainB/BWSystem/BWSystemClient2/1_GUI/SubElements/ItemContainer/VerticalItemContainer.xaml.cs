﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    /// <summary>
    /// Interaction logic for VerticalItemContainer.xaml
    /// </summary>
    public partial class VerticalItemContainer : UserControl
    {
        public VerticalItemContainer()
        {
            InitializeComponent();
        }


        public void addItems(ButtonItem[] items, double panelWidth)
        {
            int itemIndex = 0;
            while (itemIndex < items.Length)
            {
                HorizontalItemContainer horizontalContainer = new HorizontalItemContainer();
                this.VerticalSP.Children.Add(horizontalContainer);
                itemIndex = horizontalContainer.addItems(items, itemIndex, panelWidth);
            }
        }

        public void clearItems()
        {
            foreach (HorizontalItemContainer itemContainer in this.VerticalSP.Children)
            {
                itemContainer.clearItems();
            }
            this.VerticalSP.Children.Clear();
        }
    }
}
