﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    /// <summary>
    /// Interaction logic for ButtonItem.xaml
    /// </summary>
    public partial class ButtonItem : UserControl
    {
        private int id;
        private string itemName;
        private string picturePath;


        public ButtonItem(int id, string name, string picturePath)
        {
            this.id = id;
            this.itemName = name;
            this.picturePath = picturePath;
            this.DataContext = this;

            InitializeComponent();
        }

        protected virtual void OnClick(object sender, MouseButtonEventArgs e)
        {

        }


        /**
         * Getter & Setter
         */ 
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }

        public string PicturePath
        {
            get { return picturePath; }
            set { picturePath = value; }
        }

        public double ItemWidth
        {
            get { return this.ContainerGrid.Width; }
        }

        public double ItemHeight
        {
            get { return this.ContainerGrid.Height; }
        }


    }
}
