﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BWSystemLibrary2.BW_WebService;
using System.Globalization;

namespace BWSystemClient2.GUI.SubElements.ItemContainer
{
    /// <summary>
    /// Interaction logic for CancellationContainer.xaml
    /// </summary>
    public partial class CancellationContainer : UserControl
    {
        private static CultureInfo culture = CultureInfo.CurrentCulture;

        private int bookingID;
        private int customerID;
        private int? userID;
        private ButtonItem_ArticleCancellation[] bookings;
        private DateTime timeStamp;
        private bool cancelled;

        private string consumerPicturePath;
        private string userPicturePath;


        public CancellationContainer(int bookingID, int customerID, string customerName, int? userID, string userName, ButtonItem_ArticleCancellation[] bookings, DateTime timeStamp, bool cancelled)
        {
            InitializeComponent();

            this.setData(bookingID, customerID, customerName, userID, userName, bookings, timeStamp, cancelled);
            this.DataContext = this;
        }


        private void setData(int bookingID, int customerID, string customerName, int? userID, string userName, ButtonItem_ArticleCancellation[] bookings, DateTime timeStamp, bool cancelled)
        {
            this.bookingID = bookingID;
            this.BookingIDLabel.Content = bookingID;
            this.CustomerNameLabel.Content = customerName;
            this.customerID = customerID;
            this.userID = userID;

            if (userID != null)
            {
                this.UserNameLabel.Content = userName;
            }
            else
            {
                this.UserPicture.Visibility = System.Windows.Visibility.Collapsed;
            }

            foreach (ButtonItem_ArticleCancellation article in bookings)
            {
                this.ArticlePanel.Children.Add(article);
            }

            this.DateTimeStampLabel.Content = timeStamp.Date.ToString("d", culture);
            this.TimeTimeStampLabel.Content = timeStamp.ToString("H:mm:ss", culture);

            this.cancelled = cancelled;
            if (cancelled)
            {
                this.CancelledVisual.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.CancelledVisual.Visibility = System.Windows.Visibility.Collapsed;
            }
        }


        public int CustomerID
        {
            get { return customerID; }
        }


        public int? UserID
        {
            get { return userID; }
        }

        private void onClick(object sender, MouseButtonEventArgs e)
        {
            if (!this.cancelled)
            {
                GuiController.Instance.JokerPressed(this.bookingID);
            }
        }
    }
}
