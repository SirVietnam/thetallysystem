﻿using BWSystemClient2.GUI.SubElements.ItemContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements
{
    /// <summary>
    /// Interaktionslogik für BookingSuccessful.xaml
    /// </summary>
    public partial class BookingSuccessful : UserControl
    {
        public BookingSuccessful()
        {
            InitializeComponent();
        }

        internal void setBookedItems(ICollection<ButtonItem_ArticleBooking> uiElements)
        {
            this.bookingPanel.Children.Clear();

            foreach (UserControl element in uiElements)
            {
                this.bookingPanel.Children.Add(element);
            }
        }
    }
    
}
