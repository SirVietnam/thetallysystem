﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements
{
    /// <summary>
    /// Interaction logic for OK_Button.xaml
    /// </summary>
    public partial class OK_Button : UserControl
    {
        public OK_Button()
        {
            InitializeComponent();
        }

        private void OnOKClick(object sender, RoutedEventArgs e)
        {

        }
    }
}
