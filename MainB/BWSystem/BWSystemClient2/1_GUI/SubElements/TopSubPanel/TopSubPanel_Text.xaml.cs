﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements.TopSubPanel
{
    /// <summary>
    /// Interaction logic for TopSubPanel_Text.xaml
    /// </summary>
    public partial class TopSubPanel_Text : UserControl
    {
        public TopSubPanel_Text(string text)
        {
            InitializeComponent();

            this.TextLabel.Content = text;
        }
    }
}
