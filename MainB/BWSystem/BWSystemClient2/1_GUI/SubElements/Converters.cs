﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using BWSystemClient2.Domain;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using BWSystemClient2.Domain.WebService;


namespace BWSystemClient2.GUI.SubElements
{
    public class WidthToColumnsConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double width = (double)value;
            int designatedColumnWidth = 150;
            double columnCount = width / (double)designatedColumnWidth;
            return (int)columnCount;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }


    public class HeightConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double height = (double)value;
            string objectHeightString = (string)parameter;
            int objectHeihgt = System.Convert.ToInt32(objectHeightString);
            height = height - objectHeihgt;
            return (int)(height / 3.0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class GridMinHeightWidthConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Grid thisGrid = value as Grid;
            double height = thisGrid.ActualHeight;
            double width = thisGrid.ActualWidth;
            return Math.Min(height, width);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }

    }


    public class Converter_AccountID_To_PicturePath : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string path = "";
            if (value != null)
            {
                int accountID = (int)value;
                if (accountID != 0)
                {
                    path = DataStorage.Instance.getAccountPicturePath(accountID);
                }
                else
                {
                    SimpleLog.Error("Got Account with AccountID: 0");
                }
            }
            return path;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class Converter_ArticleID_To_PicturePath : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int articleID = (int)value;
            string path = "";
            if (articleID != 0)
            {
                path = DataStorage.Instance.getArticlePicturePath(articleID);
            }
            else
            {
                SimpleLog.Error("Got Article with ArticleID: 0");
            }
            return path;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class Converter_ArticleID_To_Name : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int articleID = (int)value;
            string name = "";
            if (articleID >= 0)
            {
                Article article = DataStorage.Instance.Articles.First(x => x.ID == articleID);
                name = article.Name;
            }
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class Converter_Authentication_To_SecurityCode : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Authentication aut = value as Authentication;
            string parameterString = (string)parameter;
            bool invert = System.Convert.ToBoolean(parameterString);

            bool validCode = false;
            if (aut.SecurityCode != null)
            {
                validCode = aut.SecurityCode.Count() >= LibUtility.MinSecurityCodeCount;
            }

            Visibility checkVisible;
            if (validCode)
            {
                checkVisible = Visibility.Visible;
            }
            else
            {
                checkVisible = Visibility.Collapsed;
            }

            if (invert)
            {
                if (checkVisible == Visibility.Visible)
                {
                    checkVisible = Visibility.Collapsed;
                }
                else
                {
                    checkVisible = Visibility.Visible;
                }
            }
            return checkVisible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class Converter_Authentication_To_CardCode : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Authentication aut = value as Authentication;
            string cardCodeString = "";
            if (aut.CardCode != null)
            {
                foreach (Int64 cardCode in aut.CardCode)
                {
                    cardCodeString += cardCode.ToString() + "; ";
                }
            }
            else
            {
                cardCodeString = "No Card Entered";
            }
            return cardCodeString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
