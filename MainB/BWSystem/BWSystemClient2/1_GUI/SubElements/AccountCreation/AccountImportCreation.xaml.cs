﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements
{
    /// <summary>
    /// Interaktionslogik für AccountImportCreation.xaml
    /// </summary>
    public partial class AccountImportCreation : UserControl
    {
        private Action<string, string> importFunction;

        private bool userEntered;
        private bool pwEntered;

        private SolidColorBrush correctColor;
        private SolidColorBrush errorColor;

        public AccountImportCreation()
        {
            InitializeComponent();
            this.DataContext = this;
            this.userEntered = false;
            this.pwEntered = false;
            this.importFunction = null;

            this.correctColor = (SolidColorBrush)FindResource("CorrectColor");
            this.errorColor = (SolidColorBrush)FindResource("ErrorColor");
        }

        /*
         * EventHandlers
         */
        private void onUserIDInput(object sender, KeyEventArgs e)
        {
            this.userEntered = !this.UserID.Text.Equals("");
            if (this.userEntered)
            {
                this.UserID.Background = correctColor;
            }
            else
            {
                this.UserID.Background = errorColor;
            }
            this.onInput();
        }


        private void onPWInput(object sender, KeyEventArgs e)
        {
            this.pwEntered = !this.UserPW.Password.Equals("");
            if (this.pwEntered)
            {
                this.UserPW.Background = correctColor;
            }
            else
            {
                SolidColorBrush backgroundColor = errorColor;
                this.UserPW.Background = backgroundColor;
            }
            this.onInput();
        }


        private void onInput()
        {
            if (this.userEntered && this.pwEntered)
            {
                this.OKButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.OKButton.Visibility = System.Windows.Visibility.Hidden;
            }
        }


        /*
         * Interactin Methods
         */
        internal void reset(Action<string, string> importFunction, bool retryState)
        {
            this.errorLB.Visibility = retryState ? Visibility.Visible : Visibility.Hidden;
            this.importFunction = importFunction;

            this.UserID.Text = "";
            this.UserID.Background = errorColor;

            this.UserPW.Password = "";
            this.UserPW.Background = errorColor;
            this.onInput();
        }


        private void OnOK(object sender, RoutedEventArgs e)
        {
            if (this.importFunction != null)
            {
                this.importFunction(this.UserID.Text, this.UserPW.Password);
            }
        }

        private void UserPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.importFunction != null && e.Key == Key.Return)
            {
                this.OnOK(sender, e);
            }
        }

        private void info_MouseUp(object sender, MouseButtonEventArgs e)
        {
            GuiController.Instance.showMessageBox("Import: If you have an account at this institution\n" +
                   "New: If you do not have an account at this institution\n" +
                   "Modify: If you want to change infos of your account", "INFO", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
