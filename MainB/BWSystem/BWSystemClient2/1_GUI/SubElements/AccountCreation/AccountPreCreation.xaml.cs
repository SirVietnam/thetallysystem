﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements
{
    public enum GuiPreOptions
    {
        ImportAccount,
        CreateAccount,
        ModifyAccount
    }

    /// <summary>
    /// Interaktionslogik für AccountPreCreation.xaml
    /// </summary>
    public partial class AccountPreCreation : UserControl
    {

        public AccountPreCreation()
        {
            InitializeComponent();
        }

        private void onImport(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.JokerPressed((int)GuiPreOptions.ImportAccount);
        }

        private void onNew(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.JokerPressed((int)GuiPreOptions.CreateAccount);
        }

        private void onModify(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.JokerPressed((int)GuiPreOptions.ModifyAccount);
        }

        private void infoBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Choose one of the Following:\n" +
                   "Import: \t If you have already an Account in any other Programm at this Institution\n" +
                   "New: \t If you do not have any Account at this Institution\n" +
                   "Modify: \t If you want to edit your account.", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
