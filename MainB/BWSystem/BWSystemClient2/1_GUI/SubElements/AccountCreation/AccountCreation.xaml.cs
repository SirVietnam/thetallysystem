﻿using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BWSystemClient2.GUI.SubElements;

namespace BWSystemClient2.GUI.SubElements
{
    public enum AccountCreationOption
    {
        SecurityCode,
        StandardArticle,

    }

    /// <summary>
    /// Interaktionslogik für AccountCreation.xaml
    /// </summary>
    public partial class AccountCreation : UserControl
    {
        AccountCreation_Account account;

        public AccountCreation()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        internal void setContext(AccountCreation_Account account)
        {
            this.account = account;
            this.DataContext = this.account;
        }

        private void onSecurityCodeRouting(object sender, MouseButtonEventArgs e)
        {
            this.onSecurityCode(null, null);
        }

        private void onSecurityCode(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.JokerPressed((int)AccountCreationOption.SecurityCode);
        }

        private void onStandard(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.JokerPressed((int)AccountCreationOption.StandardArticle);
        }

        private void onInput(object sender, EventArgs e)
        {
            
        }

        private void onInputKey(object sender, KeyEventArgs e)
        {
            
        }
    }
}
