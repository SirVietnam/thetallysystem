﻿using BWSystemClient2.Domain.WebService;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BWSystemClient2.GUI.SubElements
{
    /// <summary>
    /// Interaktionslogik für AccountLoginCreation.xaml
    /// </summary>
    public partial class AccountLoginCreation : UserControl
    {
        private static string loginDescriptionPrefix = @"The login must ";
        private static string passwordDescriptionPrefix = @"The password must ";
        private static string emailDescriptionPrefix = @""; 


        private struct RegexUI
        {
            internal Regex regex;
            internal Control uiErrorControl;
        }

        private OnAccountLoginCreationComplete onAccountLoginCreationComplete;

        private RegexUI[] loginRegexs;
        private bool validLogin;
        private RegexUI[] mailRegexs;
        private bool validMail;
        private RegexUI[] passwordRegexs;
        private bool validPassword;
        private bool validPasswordRepeat;

        private SolidColorBrush correctColor;
        private SolidColorBrush errorColor;


        public AccountLoginCreation()
        {
            InitializeComponent();

            this.correctColor = (SolidColorBrush)FindResource("CorrectColor");
            this.errorColor = (SolidColorBrush)FindResource("ErrorColor");
        }


        internal void init(OnAccountLoginCreationComplete onAccountLoginCreationComplete, InputRegex[] loginInputRegexs, InputRegex[] passwordInputRegexs, InputRegex[] mailInputRegexs)
        {
            this.onAccountLoginCreationComplete = onAccountLoginCreationComplete;
            this.errorSP.Children.Clear();

            this.addInputRegex(ref this.loginRegexs, ref loginInputRegexs, loginDescriptionPrefix);
            this.addInputRegex(ref this.passwordRegexs, ref passwordInputRegexs, passwordDescriptionPrefix);
            this.addInputRegex(ref this.mailRegexs, ref mailInputRegexs, emailDescriptionPrefix);

            this.userLoginTB.Clear();
            this.mailAddressTB.Clear();
            this.userPWTB.Clear();
            this.userPWRepeatTB.Clear();

            this.onUserIDInput(null, null);
            this.onEmailInput(null, null);
            this.onPWInput(null, null);
            this.onPWInput_Repeat(null, null);
        }


        private void addInputRegex(ref RegexUI[] uiRegexs, ref InputRegex[] inputRegexs, string prefix)
        {
            uiRegexs = new RegexUI[inputRegexs.Length];
            for (int i = 0; i < inputRegexs.Length; i++)
            {
                uiRegexs[i].regex = new Regex(inputRegexs[i].regexString);

                Label uiElement = new Label();
                uiElement.Style =  this.FindResource("RegexErrorLabel") as Style;
                uiElement.Content = prefix + inputRegexs[i].descriptionString;
                errorSP.Children.Add(uiElement);
                uiRegexs[i].uiErrorControl = uiElement;
            }
        }


        private void info_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }


        private void onUserIDInput(object sender, KeyEventArgs e)
        {
            this.validLogin = true;
            foreach(RegexUI regexUI in this.loginRegexs)
            {
                bool currentCorrect = regexUI.regex.IsMatch(userLoginTB.Text);
                regexUI.uiErrorControl.Visibility = currentCorrect ? Visibility.Collapsed : Visibility.Visible;
                this.validLogin = validLogin & currentCorrect;
            }
            this.userLoginTB.Background = validLogin ? this.correctColor : this.errorColor;
            this.onInput();
        }


        private void onEmailInput(object sender, KeyEventArgs e)
        {
            this.validMail = true;
            foreach (RegexUI regexUI in this.mailRegexs)
            {
                bool currentCorrect = regexUI.regex.IsMatch(mailAddressTB.Text);
                regexUI.uiErrorControl.Visibility = currentCorrect ? Visibility.Collapsed : Visibility.Visible;
                this.validMail = this.validMail & currentCorrect;
            }
            this.mailAddressTB.Background = this.validMail ? this.correctColor : this.errorColor;
            this.onInput();
        }


        private void onPWInput(object sender, KeyEventArgs e)
        {
            this.validPassword = true;
            foreach (RegexUI regexUI in this.passwordRegexs)
            {
                bool currentCorrect = regexUI.regex.IsMatch(userPWTB.Password);
                regexUI.uiErrorControl.Visibility = currentCorrect ? Visibility.Collapsed : Visibility.Visible;
                this.validPassword = this.validPassword & currentCorrect;
            }
            this.userPWTB.Background = this.validPassword ? this.correctColor : this.errorColor;

            this.onPWInput_Repeat(sender, e);
        }


        private void onPWInput_Repeat(object sender, KeyEventArgs e)
        {
            this.validPasswordRepeat = this.validPassword && this.userPWTB.Password.Equals(this.userPWRepeatTB.Password, StringComparison.CurrentCulture);
            this.userPWRepeatTB.Background = this.validPasswordRepeat ? this.correctColor : this.errorColor;
            this.onInput();
        }


        private void onInput()
        {
            if (this.validLogin && this.validMail && this.validPassword && this.validPasswordRepeat)
            {
                this.OKButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.OKButton.Visibility = System.Windows.Visibility.Hidden;
            }
        }


        private void UserPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.OnOK(sender, e);
            }
        }


        private void OnOK(object sender, RoutedEventArgs e)
        {
            if (this.validLogin && this.validMail && this.validPassword && this.validPasswordRepeat)
            {
                this.onAccountLoginCreationComplete(this.userLoginTB.Text.TrimEnd(), this.userPWTB.Password, this.mailAddressTB.Text.TrimEnd());
            }
        }
    }
}
