﻿using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.GUI.SubElements
{
    public class AccountCreation_Account : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        Account account;

        public AccountCreation_Account(Account account)
        {
            this.account = account;
        }


        /**
         * Account Getter & Setter
         */
        public string FirstName
        {
            get { return this.account.Name; }
            set
            {
                this.account.Name = value;
                this.onPropertyChanged("FirstName");
            }
        }

        public string Surname
        {
            get { return this.account.Surname; }
            set
            {
                this.account.Surname = value;
                this.onPropertyChanged("Surname");
            }
        }

        public string Nickname
        {
            get { return this.account.Nickname; }
            set
            {
                this.account.Nickname = value;
                this.onPropertyChanged("Nickname");
            }
        }

        public Authentication Authentication
        {
            get { return this.account.Authentication; }
            set
            {
                this.account.Authentication = value;
                this.onPropertyChanged("Authentication");
            }
        }

        public int StandardArticle 
        {
            get { return this.account.Standard; }
            set
            {
                this.account.Standard = value;
                this.onPropertyChanged("StandardArticle");
            }
        }


        public bool AuthenticateAlways 
        {
            get { return this.account.AuthenticateAlways; }
            set
            {
                this.account.AuthenticateAlways = value;
                this.onPropertyChanged("AuthenticateAlways");
            }
        }

        public bool IsSuperuser
        {
            get { return this.account.IsSuperuser; }
            set
            {
                this.account.IsSuperuser = value;
                this.onPropertyChanged("IsSuperuser");
            }
        }


        /**
         * INotifyPropertyChanged!
         */ 
        private void onPropertyChanged(string field)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(field));
            }
        }
    }
}
