﻿using BWSystemLibrary2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BWSystemClient2.GUI.SubElements
{
    /// <summary>
    /// Interaktionslogik für ClockControl.xaml
    /// </summary>
    public partial class ClockControl : UserControl
    {
        private MainWindow mainWindow;
        private DispatcherTimer timer;
        private double ticksLeft;

        int initialTimerTime;

        private Storyboard shakeSB;
        private int shakeThrash;
        private bool shakeStarted;


        public ClockControl()
        {
            this.shakeThrash = 6;
            this.timer = new DispatcherTimer();

            InitializeComponent();

            timer.Tick += this.timer_Tick;
            timer.Interval = TimeSpan.FromMilliseconds(500);

            string clockAnimationType = (string)BWSystemLibrary2.LibUtility.getConfigValue("ClockAnimationType", typeof(string));
            if (clockAnimationType.Equals("Simple", StringComparison.InvariantCultureIgnoreCase))
            {
                this.shakeSB = this.TryFindResource("clockAnimation_simple") as Storyboard;
            }
            else if (clockAnimationType.Equals("Complex", StringComparison.InvariantCultureIgnoreCase))
            {
                this.shakeSB = this.TryFindResource("clockAnimation_complex") as Storyboard;
            }
            else
            {
                this.shakeSB = this.TryFindResource("clockAnimation_simple") as Storyboard;
                SimpleLog.Error("ConfigValue: '" + "ClockAnimationType" + "' was not found!");
            }
            GuiController.Instance.subscribeOnTimerReset(new OnTimerReset(this.resetTimer));
        }

        public MainWindow MainWindow
        {
            set { this.mainWindow = value; }
        }

        public void startTimer(int seconds)
        {
            this.initialTimerTime = seconds;
            this.ticksLeft = seconds * 2;
            if (!this.timer.IsEnabled)
            {
                this.timer.Start();
            }
            this.setTime();
        }

        internal void stopTimer()
        {
            this.timer.Stop();
            this.ticksLeft = 0;
        }

        private void resetTimer()
        {
            this.ticksLeft = this.initialTimerTime * 2;
        }

        public void appendTime(int seconds)
        {
            this.ticksLeft += (seconds * 2);
            if (shakeStarted && (this.ticksLeft > this.shakeThrash))
            {
                this.shakeSB.Stop();
                this.shakeStarted = false;
            }

            this.setTime();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            this.ticksLeft--;
            if (this.ticksLeft <= 0)
            {
                this.timer.Stop();
                this.shakeSB.Stop();
                GuiController.Instance.TimerUp();

            }
            else if (this.ticksLeft <= this.shakeThrash)
            {

                if (!shakeStarted)
                {
                    this.shakeSB.Begin();
                    this.shakeStarted = true;
                }
            }
            else if (this.ticksLeft > this.shakeThrash)
            {
                if (shakeStarted)
                {
                    this.shakeSB.Stop();
                    this.shakeStarted = false;
                }
            }
            this.setTime();
        }

        private void setTime()
        {
            this.clockTextBlock.Text = ((int)(ticksLeft / 2)).ToString();
        }
    }
}
