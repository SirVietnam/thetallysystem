﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BWSystemClient2.GUI;
using BWSystemClient2.GUI.SubElements;
using BWSystemClient2.GUI.SubElements.ItemContainer;
using BWSystemClient2.Domain.WebService;
using BWSystemClient2.Appl;
using BWSystemClient2.GUI.GuiElements;
using BWSystemLibrary2;
using WPFTabTip;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using BWSystemClient2.Appl.Utility;

namespace BWSystemClient2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer;
        private UserControl tempPanel;


        private MainItemContainer itemContainer = null;
        private ApplicationManager am;
        private SortingMethod sortingMethod;
        //private GuiController gs;

        public MainWindow()
        {
            InitializeComponent();

            this.initLogging();

            Application.Current.MainWindow.WindowState = WindowState.Maximized;

            GuiController gs = GuiController.Instance;
            gs.setMainWindow(this);

            this.am = new ApplicationManager(this);
            this.sortingMethod = SortingMethod.alphabetical;
            //TODO: if Application Manager Creation HERE?

            this.DataContext = GuiElementStates.Instance;
            this.SizeChanged += MainWindow_SizeChanged;

            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromSeconds(2.25);
            this.timer.Tick += this.removeTempPanel;

            if ((bool)LibUtility.getConfigValue("UseTouchscreenKeyboard", typeof(bool)))
            {
                this.initAutoKeyboard();
            }
        }


        private void registerCrashHandling()
        {
            //AppDomain.CurrentDomain.UnhandledException += this.CurrentDomain_UnahandledException;
            //Application.Current.DispatcherUnhandledException += this.Application_ThreadException;
            //TaskScheduler.UnobservedTaskException += this.TaskScheduler_UnobservedTaskException;
            //SimpleLog.Log("Crash Handlers Added!");
        }

        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            string preSeperator =
                "-----------------------------------------------------------------------------------------------------------------\n" +
                "-----------------------------------------------------------------------------------------------------------------\n" +
                "--------------------------------------------- GLOBAL CRASH REPORT -----------------------------------------------\n" +
                "------------------------------------------------ TaskScheduler --------------------------------------------------\n" +
                "-----------------------------------------------------------------------------------------------------------------";
            SimpleLog.Log(preSeperator, SimpleLog.Severity.Error, false, 0);
            SimpleLog.Log(e.Exception, false, 0);
        }

        private void Application_ThreadException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            string preSeperator =
                "-----------------------------------------------------------------------------------------------------------------\n" +
                "-----------------------------------------------------------------------------------------------------------------\n" +
                "--------------------------------------------- GLOBAL CRASH REPORT -----------------------------------------------\n" +
                "----------------------------------------------- Thread Exception ------------------------------------------------\n" +
                "-----------------------------------------------------------------------------------------------------------------";
            SimpleLog.Log(preSeperator, SimpleLog.Severity.Error, false, 0);
            SimpleLog.Log(e.Exception, false, 0);
        }

        public void CurrentDomain_UnahandledException(object sender, UnhandledExceptionEventArgs args)
        {
            string preSeperator =
                "-----------------------------------------------------------------------------------------------------------------\n" +
                "-----------------------------------------------------------------------------------------------------------------\n" +
                "--------------------------------------------- GLOBAL CRASH REPORT -----------------------------------------------\n" +
                "----------------------------------------------- Current Domain --------------------------------------------------\n" +
                "-----------------------------------------------------------------------------------------------------------------";
            SimpleLog.Log(preSeperator, SimpleLog.Severity.Error, false, 0);
            SimpleLog.Log((Exception)args.ExceptionObject, false, 0);

            if (args.IsTerminating)
            {
                SimpleLog.Log("Application will TERMINATE!", SimpleLog.Severity.Error, false, 0);
                GUI.GuiController.Instance.showMessageBox("A serious error occoured! The system is terminating. A crash report for your admin was created.\n" + "Please restart the application", "Critical Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }


        private void initLogging()
        {
            string serverLogPath = (string)LibUtility.getConfigValue("ClientLogPath", typeof(string));
            SimpleLog.SetLogFile(serverLogPath, @"ClientLog_");
            SimpleLog.Info("------------------- Starting the Client ------------------------", false);
        }

        private void checkConnection()
        {

            try
            {
                DataSync access = new DataSync();
            } catch (Exception ex)
            {
                SimpleLog.Log(ex, false);
                MessageBoxResult result = MessageBox.Show("Unable to connect to the given WebService. \nTheProgramm will shut down. \nCheck the log for more Informations.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }


        private void initAutoKeyboard()
        {
            TabTipAutomation.IgnoreHardwareKeyboard = HardwareKeyboardIgnoreOptions.IgnoreAll;
            TabTipAutomation.BindTo<TextBox>();
            TabTipAutomation.BindTo<PasswordBox>();
        }


        void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.itemContainer != null)
            {
                itemContainer.drawItems(this.ActualWidth);
            }
        }


        private void init(object sender, RoutedEventArgs e)
        {
            this.registerCrashHandling();

            this.checkConnection();

            this.am.init();

            this.am.startUpComplete();
            this.am.start();
        }


        //Add ItemContainers
        internal void addPanel(MainItemContainer mainPanel, UserControl headMainPanel, UserControl headSubPanel, int? clockStartTimer)
        {
            this.addCenterPanel(mainPanel);
            this.itemContainer = mainPanel;
            this.itemContainer.drawItems(this.ActualWidth);

            this.TopPanel.setTopPanel(headMainPanel, headSubPanel, clockStartTimer);

            GuiController.Instance.OnSort(sortingMethod);
            this.MainWindow_SizeChanged(null, null);
        }

        //Add mainPanel
        internal void addPanel(UserControl mainPanel, UserControl headMainPanel, UserControl headSubPanel, int? clockStartTimer)
        {
            this.addCenterPanel(mainPanel);
            this.itemContainer = null;
            this.TopPanel.setTopPanel(headMainPanel, headSubPanel, clockStartTimer);
        }


        //Add CenterPanel
        private void addCenterPanel(UserControl mainPanel)
        {
            this.CenterPanel.Children.Clear();
            this.CenterPanel.Children.Add(mainPanel);
        }


        //Add Overlay
        internal void addOverlay(UserControl mainPanel)
        {
            Panel.SetZIndex(mainPanel, 99);
            this.CenterPanel.Children.Add(mainPanel);
        }

        internal void removeOverlay(UserControl mainPanel)
        {
            if (this.CenterPanel.Children.Contains(mainPanel))
            {
                this.CenterPanel.Children.Remove(mainPanel);
            }

        }

        internal void addAmountOverlay(AmountSelection amountSelection, UserControl headSubPanel, int? clockStartTimer)
        {
            if (!this.CenterPanel.Children.Contains(amountSelection))
            {
                this.CenterPanel.Children.Add(amountSelection);
            }

            this.TopPanel.setTopSubPanel(headSubPanel, clockStartTimer);
        }


        internal void addFadeoutCenterPanel(UserControl panel)
        {
            if (tempPanel == null)
            {
                this.tempPanel = panel;
                Grid.SetRowSpan(panel, 2);
                this.mainPanel.Children.Add(panel);
                Panel.SetZIndex(panel, 99);
                ((Storyboard)FindResource("FadeOut")).Begin(panel);
                this.timer.IsEnabled = true;
            }
        }


        private void removeTempPanel(object sender, EventArgs e)
        {
            this.timer.IsEnabled = false;
            if (this.mainPanel.Children.Contains(this.tempPanel))
            {
                this.mainPanel.Children.Remove(this.tempPanel);
                this.tempPanel = null;
            }
        }


        ///////////////// Sorting Expander Methods /////////////////
        private void onSortingChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (object item in e.AddedItems)
            {
                if (item == this.alpha)
                {
                    sortingMethod = SortingMethod.alphabetical;
                }
                else if (item == this.consumption)
                {
                    sortingMethod = SortingMethod.consumption;
                }
                //else if (item == this.presence)
                //{
                //  sorting = SortingMethod.presence;
                //}
            }

            GuiController.Instance.OnSort(sortingMethod);
            this.MainWindow_SizeChanged(null, null);
        }

        private void onSortingLoaded(object sender, RoutedEventArgs e)
        {
            this.sortingExpander.SelectedValue = alpha;
        }

        private void ME_About(object sender, MouseButtonEventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.Show();
        }

        //private void ME_Sync(object sender, MouseButtonEventArgs e)
        //{
        //    this.moreExpander.IsExpanded = false;
        //    GuiController.Instance.OnSyncData();
        //}

        //private void ME_Cancellation(object sender, MouseButtonEventArgs e)
        //{
        //    this.moreExpander.IsExpanded = false;

        //    this.am.cancellationClicked();
        //}

        //private void ME_AddAccount(object sender, MouseButtonEventArgs e)
        //{
        //    this.moreExpander.IsExpanded = false;
        //    this.am.addAccountClicked();
        //}


        ///////////////// Button Methods /////////////////
        private void OnOK(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.OKPressed();
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.CancelPressed();
        }


        private void OnAddUser(object sender, RoutedEventArgs e)
        {
            this.am.addAccountClicked();
        }

        private void OnCancellation(object sender, RoutedEventArgs e)
        {
            this.am.cancellationClicked();
        }

        private void OnForceSync(object sender, RoutedEventArgs e)
        {
            GuiController.Instance.OnSyncData();
        }




    }
}
