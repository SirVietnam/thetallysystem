﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System.ServiceModel;

namespace BWSystemClient2.Domain.WebService
{
    class DataSync : WebServiceAccess
    {
        private static bool datasInitialized = false;
        private DataStorage datas;

        public DataSync()
            : base()
        {
            datas = DataStorage.Instance;

            if (!DataSync.datasInitialized)
            {
                this.checkHelloWorld();
                //this.updateData();
                DataSync.datasInitialized = true;
            }
        }


        internal bool checkHelloWorld()
        {
            bool goodCheck = false;

            try
            {
                if (this.openService())
                {
                    string helloWorld = Service.HelloWorld();
                    if (helloWorld.Equals("Hello World"))
                    {
                        goodCheck = Service.checkClientToken(this.CurrentToken);
                    }
                }
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
                Exception outerEx = new Exception("Unable to get a connection to the given WebService: \n"
                    + "----" + this.Service.Endpoint.Address + "\n"
                    + "----" + this.Service.Endpoint.Binding + "\n"
                    + "----" + this.Service.Endpoint.Name, ex);
                throw outerEx;
            }

            return goodCheck;
        }


        internal void pingServer()
        {
            try
            {
                if (this.openService())
                {
                    Service.HelloWorld();
                }
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
            }
        }


        /*
         * Update Datas
         */
        public bool updateData(bool forceUpdate = false)
        {
            bool dataUpdated = false;

            if (this.openService())
            {
                dataUpdated = this.isUpdateRequired() || forceUpdate;
                if (dataUpdated)
                {
                    bool accountsUpdated = this.updateAccountList();
                    bool articlesUpdated = this.updateArticleList();
                    bool eventsUpdated = this.updateEventList();

                    if (accountsUpdated && articlesUpdated && eventsUpdated)
                    {
                        datas.LastDataUpdate = DateTime.Now;
                        dataUpdated = true;
                    }
                    else
                        dataUpdated = false;
                }

            }
            return dataUpdated;
        }

        public bool updateEventData(int eventID, bool eventStart)
        {
            //CARE: change at update!
            bool dataUpdated = false;

            if (this.openService())
            {
                dataUpdated = this.isUpdateRequired() || eventStart;
                if (dataUpdated)
                {
                    bool accountsUpdated = this.updateAccountList(); //CurrentToken
                    bool articlesUpdated = this.updateArticleEventList(eventID);
                    bool eventsUpdated = this.updateEventList();

                    if (accountsUpdated && articlesUpdated && eventsUpdated)
                    {
                        datas.LastDataUpdate = DateTime.Now;
                        dataUpdated = true;
                    }
                    else if (eventStart)
                    {
                        throw new Exception("The event data couldn't load during event start. See Logs!");
                    }
                    else
                        dataUpdated = false;
                }

            }
            return dataUpdated;
        }

        internal BookingData[] getBookings()
        {
            BookingData[] bookings = null;

            if (this.openService())
            {
                DateTime cancelDateTime = DateTime.Now - TimeSpan.FromHours((double)LibUtility.getConfigValue("CancellationIntervalHours", typeof(double)));
                bookings = this.Service.getPostingDataList(CurrentToken, cancelDateTime);
            }
            return bookings;
        }

        /*
         * Bookings
         */
        internal bool bookBooking(BookingData booking)
        {
            bool booked = false;

            if (this.openService())
            {
                try
                {
                    booked = this.Service.addPostingData(CurrentToken, booking);
                    if (!booked)
                    {
                        SimpleLog.Error("Booking was not accepted by server!");
                    }
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);
                    booked = false;
                } catch (TimeoutException e)
                {
                    SimpleLog.Log(e);
                    booked = false;
                }
            }
            else
            {
                SimpleLog.Error("WebService could not be opend. Booking was not send to server!");
            }

            return (booked);
        }


        internal bool bookCancellation(CancellationData cancellation)
        {
            bool booked = false;

            if (this.openService())
            {
                try
                {
                    booked = this.Service.cancelBooking(CurrentToken, cancellation);
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);
                    booked = false;
                }
            }

            return (booked);
        }


        /*
         * Accounts
         */
        internal Account importAccount(string userID, string userPW)
        {
            Account importedAcc = null;

            if (this.openService())
            {
                try
                {
                    importedAcc = this.Service.getAccount(CurrentToken, userID, userPW);
                } catch (System.ServiceModel.FaultException ex)
                {
                    SimpleLog.Log(ex);
                }
            }
            return importedAcc;
        }

        internal bool saveAccount(Account account)
        {
            bool added = false;
            if (this.openService())
            {
                try
                {
                    added = this.Service.addAccount(CurrentToken, account);
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);
                    added = false;
                }
            }
            return added;
        }


        internal bool saveAccount(Account account, string password, string mailAddress)
        {
            bool added = false;
            if (this.openService())
            {
                try
                {
                    added = this.Service.addNonOrgAccount(CurrentToken, account, password, mailAddress);
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);
                    added = false;
                }
            }
            return added;
        }


        /*
         * Others
         */
        internal int[] getSorting(BWSystemLibrary2.BW_WebService.SortingMethod method, bool user)
        {
            int[] sorting = null;

            if (this.openService())
            {
                try
                {
                    ArrayOfInt sortingTemp = this.Service.getOrder(method, user);
                    if (sortingTemp != null)
                        sorting = sortingTemp.ToArray();
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);
                } catch (TimeoutException e)
                {
                    SimpleLog.Log(e);
                }
            }
            return sorting;
        }

        /*
         * HELPERS 
         */
        private bool isUpdateRequired()
        {
            bool updateRequired = false;
            try
            {
                updateRequired = Service.updateRequired(datas.LastDataUpdate);
            } catch (CommunicationException e)
            {
                SimpleLog.Log(e);
                updateRequired = false;
            }

            return updateRequired;
        }


        private bool updateAccountList()
        {
            bool accUpdated = false;
            if (this.openService())
            {
                try
                {
                    Account[] accounts = Service.getAccountList(CurrentToken); //CurrentToken
                    if (accounts != null)
                    {
                        datas.Accounts = accounts;
                        accUpdated = true;
                    }
                    else
                    {
                        SimpleLog.Error("Got Null when account list was requrested!");
                    }
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);

                }
            }
            return accUpdated;
        }


        private bool updateArticleEventList(int eventID)
        {
            bool articlesUpdated = false;
            if (this.openService())
            {
                try
                {
                    Article[] articles = Service.getEventArticleList(eventID);
                    if (articles != null)
                    {
                        datas.Articles = articles;
                        articlesUpdated = true;
                    }
                    else
                    {
                        SimpleLog.Error("Got Null when article event list was requrested!");
                    }
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);

                }
            }
            return articlesUpdated;

        }


        private bool updateArticleList()
        {
            bool articlesUpdated = false;
            if (this.openService())
            {
                try
                {
                    Article[] articles = Service.getArticleList(); //CurrentToken
                    if (articles != null)
                    {
                        datas.Articles = articles;
                        articlesUpdated = true;
                    }
                    else
                    {
                        SimpleLog.Error("Got Null when article list was requrested!");
                    }
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);

                }
            }
            return articlesUpdated;
        }

        private bool updateEventList()
        {
            bool eventsUpdated = false;
            if (this.openService())
            {
                try
                {
                    Event[] events = Service.getFutureEvents(); //CurrentToken
                    if (events != null)
                    {
                        datas.Events = events;
                        eventsUpdated = true;
                    }
                    else
                    {
                        SimpleLog.Error("Got Null when event list was requrested was");
                    }
                } catch (CommunicationException e)
                {
                    SimpleLog.Log(e);
                }
            }
            return eventsUpdated;
        }
    }
}
