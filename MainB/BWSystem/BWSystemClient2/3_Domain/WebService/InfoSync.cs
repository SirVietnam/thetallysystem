﻿using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Domain.WebService
{
    public class InfoSync : WebServiceAccess
    {


        public InfoSync() : base()
        {
        }


        internal InputRegex[] getLoginRegexs()
        {
            InputRegex[] loginRegex = null;
            try
            {
                if (this.openService())
                {
                    loginRegex = this.Service.getNonOrgLoginPrefix(this.CurrentToken);
                }
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
            }

            return loginRegex;
        }


        internal InputRegex[] getPasswordRegexs()
        {
            InputRegex[] passwordRegex = null;
            try
            {
                if (this.openService())
                {
                    passwordRegex = this.Service.getPasswordRegex(this.CurrentToken);
                }
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
            }

            return passwordRegex;
        }


        internal InputRegex[] getMailAddressRegexs()
        {
            InputRegex[] mailRegex = null;
            try
            {
                if (this.openService())
                {
                    mailRegex = this.Service.getMailRegex(this.CurrentToken);
                }
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
            }

            return mailRegex;
        }


        internal bool isValidLogin(string login)
        {
            bool validLogin = false;
            try
            {
                if (this.openService())
                {
                    validLogin = this.Service.isValidLogin(this.CurrentToken, login);
                }
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
            }

            return validLogin;
        }
    }
}
