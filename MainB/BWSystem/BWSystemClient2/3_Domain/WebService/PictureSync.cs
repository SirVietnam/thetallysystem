﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;

namespace BWSystemClient2.Domain.WebService
{
    public class PictureSync : WebServiceAccess
    {
        private static PictureSync instance;

        private DataStorage storage;
        private bool clearPicturesOnStartup;

        private int maxSize;
        private string standardPicture;

        private string accountPicturesPath;
        private string articlePicturesPath;


        private PictureSync()
            : base()
        {
            AppSettingsReader config = new AppSettingsReader();
            string maxPicSizeString = (string)config.GetValue("MaxPictureSize", typeof(string));
            this.maxSize = Convert.ToInt32(maxPicSizeString);
            this.standardPicture = (string)config.GetValue("StandardPicture", typeof(string));
            this.accountPicturesPath = (string)config.GetValue("PicturePath", typeof(string)) + Path.DirectorySeparatorChar + "Users";
            this.articlePicturesPath = (string)config.GetValue("PicturePath", typeof(string)) + Path.DirectorySeparatorChar + "Articles";
            this.clearPicturesOnStartup = (bool)config.GetValue("ClearPicturesOnStartup", typeof(bool));

            this.storage = DataStorage.Instance;
            this.initFolders();
        }


        public static PictureSync Instance
        {
            get
            {
                if (PictureSync.instance == null)
                {
                    PictureSync.instance = new PictureSync();
                }
                return PictureSync.instance;
            }
        }


        private void initFolders()
        {
            if (!Directory.Exists(this.accountPicturesPath))
            {
                Directory.CreateDirectory(this.accountPicturesPath);
            }
            else if (this.clearPicturesOnStartup)
            {
                IEnumerable<string> files1 = Directory.EnumerateFiles(this.accountPicturesPath);
                foreach (string file in files1)
                {
                    File.Delete(file);
                }
            }

            if (!Directory.Exists(this.articlePicturesPath))
            {
                Directory.CreateDirectory(this.articlePicturesPath);
            }
            else if (this.clearPicturesOnStartup)
            {
                IEnumerable<string> files2 = Directory.EnumerateFiles(this.articlePicturesPath);
                foreach (string file in files2)
                {
                    File.Delete(file);
                }
            }
        }


        public string getDefaultAccountImage()
        {
            string picturePath = this.accountPicturesPath + Path.DirectorySeparatorChar + "DefaultAccount";

            if (this.openService())
            {
                byte[] pictureBytes = Service.getDefaultUserImage(this.maxSize);
                File.WriteAllBytes(picturePath, pictureBytes);
            }
            else
            {
                picturePath = null;
            }
            return picturePath;
        }

        public string getDefaultArticleImage()
        {
            string picturePath = this.articlePicturesPath + Path.DirectorySeparatorChar + "DefaultArticle";

            if (this.openService())
            {
                byte[] pictureBytes = Service.getDefaultArticleImage(this.maxSize);
                File.WriteAllBytes(picturePath, pictureBytes);
            }
            else
            {
                picturePath = null;
            }
            return picturePath;
        }


        //public void loadImages(Account[] accounts, Article[] article)
        //{
        //    foreach (Account account in accounts)
        //    {
        //        this.getAccountImage(account);
        //    }

        //    foreach (Article product in article)
        //    {
        //        this.getArticleImage(product);
        //    }
        //}


        public string getAccountImage(Account account)
        {
            string picturePath = null;
            string userPicture = null;
            if (account != null)
            {
                userPicture = account.PictureURL;
            }

            if (userPicture != null)
            {
                picturePath = this.accountPicturesPath + Path.DirectorySeparatorChar + account.PictureURL;

                if ((!File.Exists(picturePath) || this.updateReuqired(account)) && this.openService())
                {
                    byte[] pictureBytes = Service.getUserImage(userPicture, this.maxSize);
                    if (pictureBytes != null)
                        File.WriteAllBytes(picturePath, pictureBytes);
                    else
                        SimpleLog.Log("A picture for account: " + account.ID + " - " + account.Name + " was expected, but returned NULL.");
                }
            }
            return picturePath;
        }



        public string getArticleImage(Article article)
        {
            string picturePath = null;
            string productPicture = null;
            if (article != null)
            {
                productPicture = article.PictureURL;
            }

            if (productPicture != null)
            {
                picturePath = this.articlePicturesPath + Path.DirectorySeparatorChar + article.PictureURL;

                if ((!File.Exists(picturePath) || this.updateReuqired(article)) && this.openService())
                {
                    byte[] pictureBytes = Service.getArticleImage(productPicture, this.maxSize);
                    if (pictureBytes != null)
                        File.WriteAllBytes(picturePath, pictureBytes);
                    else
                        SimpleLog.Log("A picture for article: " + article.ID +  " - " + article.Name + " was expected, but returned NULL.");
                }

            }
            return picturePath;
        }




        private bool updateReuqired(Account account)
        {
            bool required = false;

            FileInfo fileInfo = new FileInfo(this.accountPicturesPath + Path.DirectorySeparatorChar + account.PictureURL);
            if (this.openService())
            {
                DateTime serverDateTime = this.Service.getAccountImageDateTime(account.ID);
                if (serverDateTime.Ticks >= fileInfo.LastWriteTime.Ticks)
                {
                    required = true;
                }
            }
            return required;
        }

        private bool updateReuqired(Article article)
        {
            bool required = false;

            FileInfo fileInfo = new FileInfo(this.articlePicturesPath + Path.DirectorySeparatorChar + article.PictureURL);
            if (this.openService())
            {
                DateTime serverDateTime = this.Service.getArticleImageDateTime(article.ID);
                if (serverDateTime.Ticks >= fileInfo.LastWriteTime.Ticks)
                {
                    required = true;
                }
            }
            return required;
        }


        public string getImagePath(Account account)
        {
            string picturePath = account == null || account.PictureURL == null ? this.standardPicture : account.PictureURL;
            return this.accountPicturesPath + Path.DirectorySeparatorChar + picturePath;
        }

        public string getImagePath(Article article)
        {
            string picturePath = article == null || article.PictureURL == null ? this.standardPicture : article.PictureURL;
            return this.articlePicturesPath + Path.DirectorySeparatorChar + picturePath;
        }

    }
}
