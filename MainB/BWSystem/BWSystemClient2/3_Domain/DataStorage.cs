﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System.Drawing;
using System.Runtime.Serialization;
using BWSystemClient2.Appl.CardReader;
using BWSystemClient2.GUI.SubElements.ItemContainer;

namespace BWSystemClient2.Domain
{
    public delegate void OnEventUpdated(List<Event> newEvents);

    class DataStorage
    {
        private static DataStorage instance;

        private DateTime lastDataUpdate;

        private BookingData bookingData;
        private CancellationData cancellationData;

        private Account[] accounts;
        private Dictionary<int, Account> accountsHM;

        private Article[] articles;
        private Dictionary<int, Article> articlesHM;

        private Event[] events;
        private Dictionary<int, Event> eventsHM;
        private OnEventUpdated onEventUpdated;

        private BookingData[] bookings;
        private CancellationData[] cancellations;

        private string defaultAccountPicturePath;
        private string defaultArticlePicturePath;

        private Dictionary<int, string> accountPicturePath;
        private Dictionary<int, string> articlePicturePath;

        private Dictionary<int, ButtonItem_Consumer> consumerIcons;
        private Dictionary<int, ButtonItem_User> userIcons;
        private Dictionary<int, ButtonItem_Article> articleIcons;

        private LinkedList<BookingData> finishedBookings;
        private LinkedList<BookingData> unbookedBookings;

        private CardDatasEventArgs cardDatas;


        private DataStorage()
        {
            this.LastDataUpdate = new DateTime();
            this.accounts = new Account[0];
            this.accountsHM = new Dictionary<int, Account>();

            this.articles = new Article[0];
            this.articlesHM = new Dictionary<int, Article>();

            this.events = new Event[0];
            this.eventsHM = new Dictionary<int, Event>();

            this.bookings = new BookingData[0];
            this.cancellations = new CancellationData[0];

            this.accountPicturePath = new Dictionary<int, string>();
            this.articlePicturePath = new Dictionary<int, string>();

            this.consumerIcons = new Dictionary<int, ButtonItem_Consumer>();
            this.userIcons = new Dictionary<int, ButtonItem_User>();
            this.articleIcons = new Dictionary<int, ButtonItem_Article>();

            this.finishedBookings = new LinkedList<BookingData>();
            this.unbookedBookings = new LinkedList<BookingData>();
        }


        public static DataStorage Instance
        {
            get
            {
                if (DataStorage.instance == null)
                {
                    DataStorage.instance = new DataStorage();
                }
                return DataStorage.instance;
            }
        }


        /**
         * Getter & Setter for Storage Datas
         */
        //Account
        public Account[] Accounts
        {
            get { return accounts; }
            set
            {
                accounts = value;

                this.accountsHM.Clear();
                foreach (var item in value)
                {
                    this.accountsHM.Add(item.ID, item);
                }
            }
        }

        public Account getAccount(int accountID)
        {
            //Account account = null;
            //bool containsKey = this.accountsHM.ContainsKey(accountID);
            //if (containsKey)
            //{
            //    account = this.accountsHM[accountID];
            //}
            //return account;
            return this.accounts.FirstOrDefault(x => x.ID == accountID);
        }


        //Article
        public Article[] Articles
        {
            get { return articles; }
            set
            {
                articles = value;

                this.articlesHM.Clear();
                foreach (var item in value)
                {
                    this.articlesHM.Add(item.ID, item);
                }
            }
        }

        public Article getArticle(int articleID)
        {
            //Article article = null;
            //bool containsKey = this.accountsHM.ContainsKey(articleID);
            //if (containsKey)
            //{
            //    article = this.articlesHM[articleID];
            //}
            //return article;
            return this.articles.FirstOrDefault(x => x.ID == articleID);
        }


        //Event
        public void subscribeOnEventUpdated(OnEventUpdated onEventUpdated)
        {
            this.onEventUpdated += onEventUpdated;
        }

        public Event[] Events
        {
            get { return events; }
            set
            {
                //events = value.OrderBy(x => x.Begin).ToArray();

                //this.eventsHM.Clear();
                //foreach (var item in value)
                //{
                //    this.eventsHM.Add(item.EventID, item);
                //}

                List<Event> newEvents = new List<Event>();

                foreach (Event currentEvent in value)
                {
                    if (this.eventsHM.ContainsKey(currentEvent.EventID))
                    {
                        Event oldEvent = this.eventsHM[currentEvent.EventID];
                        oldEvent.Name = currentEvent.Name;
                        oldEvent.Begin = currentEvent.Begin;
                        oldEvent.End = currentEvent.End;
                        oldEvent.CostAbsorption = currentEvent.CostAbsorption;
                        //oldEvent. = currentEvent.Begin; //TODO: Add EventCosts?
                    }
                    else
                    {
                        newEvents.Add(currentEvent);
                        this.eventsHM.Add(currentEvent.EventID, currentEvent);
                    }

                }
                if (this.onEventUpdated != null)
                {
                    this.onEventUpdated(newEvents);
                }
                newEvents.AddRange(this.events);
                this.events = newEvents.OrderBy(x => x.Begin).ToArray();
            }
        }


        public Event getEvent(int eventID)
        {
            Event _event = null;
            bool containsKey = this.accountsHM.ContainsKey(eventID);
            if (containsKey)
            {
                _event = this.eventsHM[eventID];
            }
            return _event;
        }


        //Booking
        public BookingData[] Bookings
        {
            get { return bookings; }
            set { bookings = value; }
        }

        public BookingData BookingData
        {
            get { return bookingData; }
            set { bookingData = value; }
        }

        //Cancellation
        public CancellationData[] Cancellations
        {
            get { return cancellations; }
            set { cancellations = value; }
        }

        public CancellationData CancellationData
        {
            get { return cancellationData; }
            set { cancellationData = value; }
        }

        //Default Pictures
        public string DefaultAccountPicturePath
        {
            get { return defaultAccountPicturePath; }
            set { defaultAccountPicturePath = value; }
        }

        public string DefaultArticlePicturePath
        {
            get { return defaultArticlePicturePath; }
            set { defaultArticlePicturePath = value; }
        }

        //Account Pictures
        public void setAccountPicturePath(int accountID, string picturePath)
        {
            if (this.accountPicturePath.ContainsKey(accountID))
            {
                this.accountPicturePath.Remove(accountID);
            }
            this.accountPicturePath.Add(accountID, picturePath);
        }

        public string getAccountPicturePath(int accountID)
        {
            string picturePath = this.accountPicturePath.ContainsKey(accountID) ? this.accountPicturePath[accountID] : this.DefaultAccountPicturePath;
            return picturePath;
        }

        //Article Pictures
        public void setArticlePicturePath(int articleID, string picturePath)
        {
            if (this.articlePicturePath.ContainsKey(articleID))
            {
                this.articlePicturePath.Remove(articleID);
            }
            this.articlePicturePath.Add(articleID, picturePath);
        }

        public string getArticlePicturePath(int articleID)
        {
            string picturePath = this.articlePicturePath.ContainsKey(articleID) ? this.articlePicturePath[articleID] : this.DefaultArticlePicturePath;
            return picturePath;
        }

        //Customer Icon
        public void setCustomerIcon(ButtonItem_Consumer icon)
        {
            if (this.consumerIcons.ContainsKey(icon.ID))
            {
                this.consumerIcons.Remove(icon.ID);
            }
            this.consumerIcons.Add(icon.ID, icon);
        }

        public ButtonItem_Consumer getCustomerIcon(int accountID)
        {
            ButtonItem_Consumer icon = null;
            if (this.consumerIcons.ContainsKey(accountID))
            {
                icon = this.consumerIcons[accountID];
            }
            return icon;
        }


        //User Icon
        public void setUserIcon(ButtonItem_User icon)
        {

            if (this.userIcons.ContainsKey(icon.ID))
            {
                this.userIcons.Remove(icon.ID);
            }
            this.userIcons.Add(icon.ID, icon);
        }

        public ButtonItem_User getUserIcon(int accountID)
        {
            ButtonItem_User icon = null;
            if (this.userIcons.ContainsKey(accountID))
            {
                icon = this.userIcons[accountID];
            }
            return icon;
        }


        //Article Icon
        public void setArticleIcon(ButtonItem_Article icon)
        {
            if (this.articleIcons.ContainsKey(icon.ID))
            {
                this.articleIcons.Remove(icon.ID);
            }
            this.articleIcons.Add(icon.ID, icon);
        }

        public ButtonItem_Article getArticleIcon(int articleID)
        {
            ButtonItem_Article icon = null;
            if (this.articleIcons.ContainsKey(articleID))
            {
                icon = this.articleIcons[articleID];
            }
            return icon;
        }


        //Finished Bookings
        public LinkedList<BookingData> FinishedBookings
        {
            get { return new LinkedList<BookingData>(this.finishedBookings); }
        }

        public void addFinishedBooking(BookingData booking)
        {
            this.finishedBookings.AddLast(booking);
        }

        //Unbooked Bookings
        public LinkedList<BookingData> UnbookedBookings
        {
            get { return unbookedBookings; }
            set { unbookedBookings = value; }
        }

        //Card Datas
        public CardDatasEventArgs CardDatas
        {
            get { return cardDatas; }
            set { cardDatas = value; }
        }

        public DateTime LastDataUpdate
        {
            get { return lastDataUpdate; }
            set { lastDataUpdate = value; }
        }



        /*
         * Helper Methods
         */

    }
}
