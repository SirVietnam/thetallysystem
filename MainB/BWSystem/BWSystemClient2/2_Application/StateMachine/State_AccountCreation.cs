﻿using BWSystemClient2.Appl;
using BWSystemClient2.Appl.Utility;
using BWSystemClient2.Domain;
using BWSystemClient2.GUI;
using BWSystemClient2.GUI.SubElements;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace BWSystemClient2.Appl.StateMachine
{
    class State_AccountCreation : StateMachine
    {
        private Tuple<string, string, string> currentAccountLogin;
        private Account currentAccount;
        private BackgroundWorker accountSaver;
        private BackgroundWorker accountImporter;

        public State_AccountCreation()
            : base()
        {
            this.accountSaver = new BackgroundWorker();
            this.accountSaver.DoWork += this.asyncSaveAccount;
            this.accountSaver.RunWorkerCompleted += this.accountSaved;

            this.accountImporter = new BackgroundWorker();
            this.accountImporter.DoWork += this.asyncImportPressed;
            this.accountImporter.RunWorkerCompleted += this.accountImported;
        }


        internal override void start()
        {
            this.currentAccountLogin = null;
            this.currentAccount = null;
            this.initSubscribe();
            this.preSelection();
        }


        internal override bool saveState()
        {
            return this.currentAccount == null;
        }


        protected override void initSubscribe()
        {
            guiController.clearAllSubscriber();
            guiController.subscribeOnArticle(new OnArticle(this.ArticleSelected));
            guiController.subscribeOnUser(new OnUser(this.UserSelected));
            guiController.subscribeOnAuthentification(new OnAuthentification(this.UserAuthentificated));
        }




        /**
         * Modify Import Create Selection
         */
        #region PreSeclection
        private void preSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_PreSelection), null, null, new OnTimerUp(this.onTimeUp_PreSelection));

            this.guiController.showOptionPanel();
            this.currentOnJoker = new OnJoker(optionSelected);
            this.guiController.subscribeOnJoker(currentOnJoker);
        }


        public void optionSelected(int optionID)
        {
            GuiPreOptions option = (GuiPreOptions)optionID;

            this.guiController.unSubscribeOnJoker(this.currentOnJoker);
            this.currentOnJoker = null;

            switch (option)
            {
                case GuiPreOptions.ModifyAccount:
                    this.UserSelection();
                    break;

                case GuiPreOptions.CreateAccount:
                    this.LoginCreation();
                    break;

                case GuiPreOptions.ImportAccount:
                    this.ImportAccount();
                    break;
                default:
                    break;
            }
        }


        public void onCancel_PreSelection()
        {
            ApplicationManager.reset();
        }

        private void onTimeUp_PreSelection()
        {
            this.onCancel_PreSelection();
        }
        #endregion


        /**
         * Import Account
         */
        #region Import Account
        public void ImportAccount(bool retryState = false)
        {
            this.changeSubscription(null, new OnCancel(onCancel_ImportAccount));
            this.guiController.showImportPanel(this.ImportPressed, retryState);
        }

        public void ImportPressed(string userID, string userPassword)
        {
            SimpleLog.Log("Starting import. For user: " + userID);
            this.changeSubscription(null, null);
            this.guiController.showWaitingOverlay();

            try
            {
                this.accountImporter.RunWorkerAsync(new Tuple<string, string>(userID, userPassword));
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
                this.guiController.showMessageBox("An error occoured during account import.\nPlease contact your admin if this continues to occour.", "Load Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                ApplicationManager.reset();
            }

        }

        private void asyncImportPressed(object sender, DoWorkEventArgs e)
        {
            Tuple<string, string> userData = (Tuple<string, string>)e.Argument;
            this.currentAccount = this.dim.importAccount(userData.Item1, userData.Item2);
        }

        private void accountImported(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                SimpleLog.Log(e.Error);
                this.guiController.showMessageBox("Am Error occoured during the account import. If this repeats contact your admin.\nReturning to main screen.", "Import Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                ApplicationManager.reset();
            }
            else if (!e.Cancelled && this.currentAccount != null)
            {
                SimpleLog.Log("Account successfully imported!");
                if (this.currentAccount.Standard <= 0)
                    this.currentAccount.Standard = -1;
                this.AccountEditing(this.currentAccount);
            }
            else
            {
                SimpleLog.Log("Account import failed. Wrong ID or PW?");
                this.ImportAccount(true);
            }
        }


        public void onCancel_ImportAccount()
        {
            SimpleLog.Info("User cancelled user import");
            this.start();
        }
        #endregion


        /*
         * First Account Usage
         */
        #region First Account Usage
        public void AccountAuth(int userID)
        {
            SimpleLog.Log("Starting first account usage for: " + userID);
            this.initSubscribe();
            this.changeSubscription(null, new OnCancel(this.onCancel_AccountAuth), new OnJoker(this.onJoker_AccountAuth), new OnCardEntered(this.onCardEntered_AccountAuth), new OnTimerUp(onCancel_AccountAuth));
            Account user = this.dataStorage.Accounts.First(x => x.ID == userID);
            user.Standard = -1;
            this.unauthentificatedUser = user;
            this.guiController.showFirstUsage(false);

            if (dataStorage.CardDatas != null && ((DateTime.Now - dataStorage.CardDatas.TimeStamp) < TimeSpan.FromSeconds(3.0)))
            {
                onCardEntered_AccountAuth();
            }
        }

        public void onCancel_AccountAuth()
        {
            this.unauthentificatedUser = null;
            ApplicationManager.reset();
        }

        public void onCardEntered_AccountAuth()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null && account.ID == this.unauthentificatedUser.ID)
            {
                this.currentAccount = this.unauthentificatedUser;
                this.unauthentificatedUser = null;
                this.AccountEditing();
            }
            else
            {
                this.guiController.showFirstUsage(true);
            }
        }

        public void onJoker_AccountAuth(int id)
        {
            this.unauthentificatedUser = null;
            this.ImportAccount(false);
        }
        #endregion


        /*
         * Login Creation
         */
        #region LoginCreation
        public void LoginCreation(bool initUI = true)
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_LoginCreation), null, null, new OnTimerUp(onCancel_LoginCreation));

            if (initUI)
            {
                Domain.WebService.InfoSync infoSync = new Domain.WebService.InfoSync();
                InputRegex[] loginRegexs = infoSync.getLoginRegexs();
                InputRegex[] passwordRegexs = infoSync.getPasswordRegexs();
                InputRegex[] mailAddressRegexs = infoSync.getMailAddressRegexs();

                this.guiController.showAccountLoginCreation(new OnAccountLoginCreationComplete(this.setAccountLoginDetails), loginRegexs, passwordRegexs, mailAddressRegexs);
            }
            else
            {
                this.guiController.showAccountLoginCreation();
            }
        }

        public void onOK_LoginCreation()
        {
            Account newAccount = new Account();
            newAccount.Standard = -1;
            newAccount.Authentication = new Authentication();
            if (this.currentAccountLogin != null)
                newAccount.Authentication.LoginName = this.currentAccountLogin.Item1;
            else
            {
                throw new NullReferenceException("The current account login is null, but not supposed to be!");
            }
            newAccount.IsAvailable = true;
            this.AccountEditing(newAccount);
        }

        public void onCancel_LoginCreation()
        {
            SimpleLog.Log("The login creation was cancelled!");
            this.currentAccountLogin = null;
            this.currentAccount = null;
            ApplicationManager.reset();
        }


        private bool setAccountLoginDetails(string login, string password, string mailAddress)
        {
            Domain.WebService.InfoSync infoSync = new Domain.WebService.InfoSync();
            bool validLogin = infoSync.isValidLogin(login);
            if (validLogin)
            { 
                this.currentAccountLogin = new Tuple<string, string, string>(login, password, mailAddress);
                this.onOK_LoginCreation();
            }
            else
            {
                this.WarningOverlay("You entered a non valid login. Maybe your login is already taken or you have a wrong prefix. Try again.");
            }
            return validLogin;
        }
        #endregion

        /**
         * Account Editing
         */
        #region Account Editing
        public void AccountEditing()
        {
            this.AccountEditing(this.currentAccount);
        }

        public void AccountEditing(Account account)
        {
            OnOK onOK = SecurityUtility.isValidAccount(account) ? new OnOK(onOK_AccountEditing) : null;
            this.changeSubscription(onOK, new OnCancel(this.onCancel_AccountEditing), null, new OnCardEntered(this.onCardEntered_AccountEditing), new OnTimerUp(onTimeUp_AccountEditing));
            this.currentAccount = this.cloneAccount(account);
            this.currentOnJoker = new OnJoker(this.onAccountEditingOption);
            this.guiController.subscribeOnJoker(this.currentOnJoker);
            this.guiController.showAccountModification(this.currentAccount);
        }


        public void onOK_AccountEditing()
        {
            SimpleLog.Log("Starting saving account:" + this.currentAccount.Surname + ", " + this.currentAccount.Name);
            this.guiController.showWaitingOverlay();

            try
            {
                this.accountSaver.RunWorkerAsync();
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
                this.guiController.showMessageBox("An error occoured during account saving.\nPlease contact your admin if this continues to occour.", "Sync Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                ApplicationManager.reset();
            }
        }


        private void asyncSaveAccount(object sender, DoWorkEventArgs e)
        {
            bool added = false;
            if (this.currentAccountLogin == null && this.currentAccount.ID >= 0)
            { 
                added = this.dim.saveAccount(this.currentAccount);
            }
            else
            {
                added = this.dim.saveNonOrgAccount(this.currentAccount, this.currentAccountLogin.Item2, this.currentAccountLogin.Item3);
            }

            if (added)
            {
                this.currentAccount = null;
                this.currentAccountLogin = null;
            }
            else
            {
                SimpleLog.Log("The account: " + this.currentAccount.Name + " " + this.currentAccount.Surname + " could not be saved!");
            }
        }

        private void accountSaved(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                SimpleLog.Log(e.Error);
                this.guiController.showMessageBox("A critical error occoured while saving the account. If this happens again please contact your system admin.", "CRITICAL ERROR", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
            else if (this.currentAccount == null)
            {
                SimpleLog.Log("Account successfully saved!");
                ApplicationManager.reset(true);
            }
            else
            {
                SimpleLog.Log("Account Saving failed. Reason unkown, but no exception was thrown by worker thread.");
                this.guiController.showMessageBox("An error occourd.\nIt seams your account was not saved.\nTry again, if this occours again contact your system admin.", "ERROR", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                this.AccountEditing();
            }
        }


        public void onCancel_AccountEditing()
        {
            SimpleLog.Log("The account creation for: " + this.currentAccount.Name + " " + this.currentAccount.Surname + " was cancelled!");
            this.currentAccount = null;
            ApplicationManager.reset();
        }


        internal void onAccountEditingOption(int optionID)
        {
            this.guiController.unSubscribeOnJoker(this.currentOnJoker);
            this.currentOnJoker = null;

            AccountCreationOption option = (AccountCreationOption)optionID;
            switch (option)
            {
                case AccountCreationOption.SecurityCode:
                    this.AuthentificationSate();
                    break;

                case AccountCreationOption.StandardArticle:
                    this.ArticleSelection();
                    break;

                default:
                    break;
            }
        }


        private void onCardEntered_AccountEditing()
        {
            Int64 enteredCardNumber = this.dataStorage.CardDatas.CardNumber;
            Account userToAcc = this.secUtil.validCardEntered(enteredCardNumber);

            if (userToAcc == null)
            {
                Authentication auth = this.currentAccount.Authentication;
                if (auth.CardCode == null)
                {
                    auth.CardCode = new ArrayOfLong();
                }
                if (!auth.CardCode.Contains(enteredCardNumber))
                {
                    auth.CardCode.Add(enteredCardNumber);
                }
                this.guiController.showAccountModification(this.currentAccount);
            }
        }

        private void onTimeUp_AccountEditing()
        {
            this.onCancel_AccountEditing();
        }
        #endregion


        /**
         * Account Selection
         * Using UserSelection for the Account Selection (Account to Edit) 
         */
        #region Account Selection
        protected override void UserSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_UserSelection), null, null, new OnTimerUp(onTimeUp_UserSelection));
            this.guiController.showUserSelection();
        }

        protected override void UserSelected(int userID)
        {
            Account user = this.dataStorage.Accounts.First(x => x.ID == userID);
            this.unauthentificatedUser = user;
            this.AuthentificationSate(user);
        }

        protected override void onCancel_UserSelection()
        {
            this.start();
        }

        protected override void onCardEntered_UserSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.currentAccount = account;
                this.unauthentificatedUser = null;
                this.AccountEditing();
            }
        }

        protected override void onTimeUp_UserSelection()
        {
            this.onCancel_UserSelection();
        }
        #endregion


        /**
         * Article Selection
         */
        #region Article Selection
        protected override void ArticleSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_ArticleSelection));
            this.guiController.showArticleSelection();
        }

        protected override void ArticleSelected(int articleID)
        {
            this.currentAccount.Standard = articleID;
            this.AccountEditing();
        }


        protected override void onOK_ArticleSelection()
        {
            throw new NotImplementedException();
        }


        protected override void onCancel_ArticleSelection()
        {
            this.AccountEditing();
        }

        protected override void onCardEntered_ArticleSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_ArticleSelection()
        {
            throw new NotImplementedException();
        }
        #endregion


        /**
         * Authentification
         */
        #region Authentification
        protected override void AuthentificationSate(Account account)
        {
            //TODO add auth
            OnCardEntered onCardEntered = this.unauthentificatedUser != null ? new OnCardEntered(this.onCardEntered_Authentification) : null;
            this.changeSubscription(null, new OnCancel(this.onCancel_Authentification), null, onCardEntered, new OnTimerUp(onTimeUp_Authentification));
            this.unauthentificatedUser = account;
            this.guiController.showAuthentification();
        }

        protected void AuthentificationSate()
        {
            //TODO add auth
            this.changeSubscription(null, new OnCancel(this.onCancel_AuthentificationRecording));
            this.guiController.showAuthentificationRecording();
        }


        protected override void UserAuthentificated(int[] inputSecurityCode)
        {
            if (this.unauthentificatedUser != null)
            {
                this.userAuthentificacted_normal(inputSecurityCode);
            }
            else
            {
                this.userAuthentificacted_recording(inputSecurityCode);
            }
        }


        private void userAuthentificacted_normal(int[] inputSecurityCode)
        {
            bool correctSecurityCode = this.secUtil.correctSecurityCode(this.unauthentificatedUser, inputSecurityCode);

            if (correctSecurityCode)
            {
                this.guiController.correctAuthentificationEntered(correctSecurityCode, this.AccountEditing);
                this.currentAccount = this.unauthentificatedUser;
                this.unauthentificatedUser = null;
            }
            else
            {
                this.guiController.correctAuthentificationEntered(correctSecurityCode, null);
            }
        }


        private void userAuthentificacted_recording(int[] inputSecurityCode)
        {
            if (SecurityUtility.ValidSecurityCode(inputSecurityCode))
            {
                //Convert to WebService!
                ArrayOfInt newSecCode = new ArrayOfInt();
                foreach (int number in inputSecurityCode)
                {
                    newSecCode.Add(number);
                }
                //Generat Salt & Hash
                byte[] salt = BWSystemLibrary2.PasswordHashUtil.generateSalt();
                this.currentAccount.Authentication.SecurityCodeSalt = salt;
                this.currentAccount.Authentication.SecurityCodeHash = BWSystemLibrary2.PasswordHashUtil.generateSaltedPasswordHash(inputSecurityCode, salt);

                this.guiController.correctAuthentificationEntered(true, this.AccountEditing);
                this.currentAccount.Authentication.SecurityCode = newSecCode;
            }
            else
            {
                this.guiController.correctAuthentificationEntered(false, this.AuthentificationSate);
            }
        }


        protected override void onCancel_Authentification()
        {
            this.unauthentificatedUser = null;
            this.UserSelection();
        }

        protected void onCancel_AuthentificationRecording()
        {
            this.AccountEditing();
        }

        protected override void onCardEntered_Authentification()
        {
            if (this.unauthentificatedUser != null)
            {
                Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

                if (account != null && account == this.unauthentificatedUser)
                {
                    this.currentAccount = this.unauthentificatedUser;
                    this.unauthentificatedUser = null;
                    this.AccountEditing();
                }
            }
        }

        protected override void onTimeUp_Authentification()
        {
            this.onCancel_Authentification();
        }
        #endregion






        /**
         * Amount Selection
         */
        #region Amount Selection
        protected override void AmountSelection()
        {
            throw new NotImplementedException();
        }

        protected override void AmountSelection(BookingAA bookingAA)
        {
            throw new NotImplementedException();
        }

        protected override void AmoutSelected(int amount)
        {
            throw new NotImplementedException();
        }

        protected override void onOK_AmountSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCancel_AmountSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCardEntered_AmountSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_AmountSelection()
        {
            throw new NotImplementedException();
        }
        #endregion


        /**
         * Customer Selection
         */
        #region Customer Selection
        protected override void CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void ConsumerSelected(int consumerID)
        {
            throw new NotImplementedException();
        }

        protected override void onOK_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCancel_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCardEntered_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_CustomerSelection()
        {
            throw new NotImplementedException();
        }
        #endregion


        /**
         * Warning Msg Overlay
         */
        #region Msg Overlay
        protected override void WarningOverlay(string message)
        {
            this.changeSubscription(new OnOK(this.onOK_WarningOverlay), null, null, null, new OnTimerUp(this.onTimeUp_WarningOverlay));

            this.guiController.showWarningPanel(message);
        }

        protected override void onOK_WarningOverlay()
        {
            this.LoginCreation(false);
        }

        protected override void onCancel_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onCardEntered_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_WarningOverlay()
        {
            throw new NotImplementedException();
        }
        #endregion


        /*
         * Helper
         */
        private Account cloneAccount(Account account)
        {
            Account cloneAccount = new Account();
            cloneAccount.ID = account.ID;
            cloneAccount.Surname = account.Surname;
            cloneAccount.Name = account.Name;
            cloneAccount.Nickname = account.Nickname;
            cloneAccount.PictureURL = account.PictureURL;
            cloneAccount.Standard = account.Standard;
            cloneAccount.AuthenticateAlways = account.AuthenticateAlways;
            cloneAccount.IsAvailable = account.IsAvailable;
            cloneAccount.IsSuperuser = account.IsSuperuser;

            Authentication cloneAuth = new Authentication();
            cloneAccount.Authentication = cloneAuth;
            cloneAuth.LoginName = account.Authentication.LoginName;
            if (account.Authentication.CardCode != null)
            {
                cloneAuth.CardCode = new ArrayOfLong();
                foreach (long cardCode in account.Authentication.CardCode)
                {
                    cloneAuth.CardCode.Add(cardCode);
                }
            }
            cloneAuth.SecurityCodeHash = account.Authentication.SecurityCodeHash;
            cloneAuth.SecurityCodeSalt = account.Authentication.SecurityCodeSalt;
            cloneAuth.SecurityCode = account.Authentication.SecurityCode;

            return cloneAccount;
        }
    }
}


