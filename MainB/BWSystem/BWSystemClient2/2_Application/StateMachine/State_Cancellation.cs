﻿using BWSystemClient2.Appl;
using BWSystemClient2.Appl.Utility;
using BWSystemClient2.Domain;
using BWSystemClient2.GUI;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.StateMachine
{
    class State_Cancellation : StateMachine
    {

        private CancellationData currentCancellation;

        private BackgroundWorker bookingsLoader;

        public State_Cancellation()
            : base()
        {
            this.bookingsLoader = new BackgroundWorker();
            this.bookingsLoader.DoWork += this.loadBookings;
            this.bookingsLoader.RunWorkerCompleted += this.bookingsLoaded;
        }



        internal override bool saveState()
        {
            return (this.currentCancellation == null);
        }


        internal override void start()
        {
            this.initSubscribe();
            this.BookingSelection();
        }


        protected override void initSubscribe()
        {
            guiController.clearAllSubscriber();
            //guiController.subscribeOnConsumer(new OnConsumer(this.ConsumerSelected));
            guiController.subscribeOnArticle(new OnArticle(this.ArticleSelected));
            guiController.subscribeOnAmount(new OnAmount(this.AmoutSelected));
            guiController.subscribeOnUser(new OnUser(this.UserSelected));
            guiController.subscribeOnAuthentification(new OnAuthentification(this.UserAuthentificated));
        }


        /**
         * CustomerState
         */
        #region BookingSelection
        private void BookingSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_BookingSelection), new OnJoker(this.onBookingSelected_BookingSelection), null, new OnTimerUp(this.onTimeUp_BookingSelection));
            this.guiController.showWaitingOverlay();
            //TODO Update Required? Where to put it?
            //this.updateData(); 
            try
            {
                this.bookingsLoader.RunWorkerAsync();
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
                //this.guiController.showMessageBox("An error occoured during accoutn saving.\nPlease contact your admin if this continues to occour.", "Sync Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                //ApplicationManager.reset();
            }
        }


        private void loadBookings(object sender, DoWorkEventArgs e)
        {
            SimpleLog.Log("Starting loading bookings for cancellation.");
            
            this.dataStorage.Bookings = this.dim.getBookings().OrderByDescending(x => x.TimeStamp).Take(30).OrderBy(x => x.TimeStamp).ToArray();
        }

        private void bookingsLoaded(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                SimpleLog.Log(e.Error);
                this.guiController.showMessageBox("Failed to load bookings. Retry!\nIf this error presits, contact the system admin.", "ERROR", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                ApplicationManager.reset();
            }
            else
            {
                DataMapper.syncCancellationPanel(this.guiController.Cancellation, this.dataStorage.Bookings);
                this.guiController.showBookingSelection();
            }
        }


        public void onBookingSelected_BookingSelection(int bookingID)
        {
            this.currentBooking = this.dataStorage.Bookings.First(x => x.ID == bookingID);
            this.setupCancellation();

            DataMapper.syncBookingDataToGui(this.guiController.BookingPanel, this.currentBooking.Booking);
            this.UserSelection();
        }

        public void onCancel_BookingSelection()
        {
            ApplicationManager.reset();
        }

        private void onTimeUp_BookingSelection()
        {
            this.onCancel_BookingSelection();
        }

        private void setupCancellation()
        {
            this.dataStorage.BookingData = this.currentBooking;
            this.currentCancellation = new CancellationData();
            this.currentCancellation.NewBooking = this.currentBooking;
            this.dataStorage.CancellationData = this.currentCancellation;
            this.currentCancellation.CancelledBookingID = (int)this.currentBooking.ID;

            this.currentBooking.ID = null;
            this.currentBooking.User = null;
        }
        #endregion


        /**
         * ArticleState
         */
        #region ArticleSelection
        protected override void ArticleSelection()
        {
            this.changeSubscription(new OnOK(this.onOK_ArticleSelection), new OnCancel(this.onCancel_ArticleSelection), null, null, new OnTimerUp(onTimeUp_ArticleSelection));

            if (secUtil.authentificationRequired())
            {
                this.UserSelection();
            }
            else
            {
                this.guiController.showArticleSelection();
            }
        }


        protected override void ArticleSelected(int articleID)
        {
            BookingAA bookingAA = this.currentBooking.Booking.FirstOrDefault(x => x.Article == articleID);
            if (bookingAA == null)
            {
                bookingAA = new BookingAA();
                bookingAA.Article = articleID;
                bookingAA.Amount = 1;
                BookingAA[] bookingAAs = new BookingAA[this.currentBooking.Booking.Length + 1];
                this.currentBooking.Booking.CopyTo(bookingAAs, 0);
                bookingAAs[bookingAAs.Length - 1] = bookingAA;
                this.currentBooking.Booking = bookingAAs;

                DataMapper.addBookingDataFromGui(this.guiController.BookingPanel, bookingAA);
            }

            this.AmountSelection(bookingAA);
        }


        protected override void onOK_ArticleSelection()
        {
            bool bookingCancelled = this.dim.bookCancellation(this.currentCancellation);
            if (bookingCancelled)
                this.guiController.showSuccessfulBookedPanel();
            //TODO: Do else!
            ApplicationManager.reset();
        }

        protected override void onCancel_ArticleSelection()
        {
            this.start();
        }

        protected override void onCardEntered_ArticleSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_ArticleSelection()
        {
            this.onOK_ArticleSelection();
        }
        #endregion


        /**
         * AmountState
         */
        #region AmountSelection
        protected override void AmountSelection()
        {
            throw new NotImplementedException();
        }

        protected override void AmountSelection(BookingAA bookingAA)
        {
            this.changeSubscription(new OnOK(this.onOK_AmountSelection), new OnCancel(this.onCancel_AmountSelection), new OnJoker(this.ArticleInformation));

            this.currentBookingAA = bookingAA;
            Article currentArticle = this.dataStorage.Articles.First(x => x.ID == this.currentBookingAA.Article);
            string picturePath = this.dataStorage.getArticlePicturePath(currentArticle.ID);
            this.guiController.showAmountSelection(bookingAA.Article, bookingAA.Amount, picturePath, this.guiUtil.getAmountButtonValues(bookingAA.Amount, currentArticle.Size));
        }

        protected override void AmoutSelected(int amount)
        {
            this.currentBookingAA.Amount = amount;
            DataMapper.syncBookingDataToGui(this.guiController.BookingPanel, this.currentBookingAA);
            Article currentArticle = this.dataStorage.Articles.First(x => x.ID == this.currentBookingAA.Article);
            this.guiController.setAmountButtonValues(guiUtil.getAmountButtonValues(amount, currentArticle.Size));
        }


        protected override void onOK_AmountSelection()
        {
            if (this.currentBookingAA.Amount == 0)
            {
                this.removeArticle(this.currentBookingAA);
                DataMapper.removeBookingDataFromGui(this.guiController.BookingPanel, this.currentBookingAA);
            }

            this.currentBookingAA = null;
            //this.guiController.removeAmountSelection();
            this.ArticleSelection();
        }

        protected override void onCancel_AmountSelection()
        {
            this.removeArticle(this.currentBookingAA);
            DataMapper.removeBookingDataFromGui(this.guiController.BookingPanel, this.currentBookingAA);
            //this.guiController.removeAmountSelection();
            this.ArticleSelection();
        }


        protected override void onCardEntered_AmountSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_AmountSelection()
        {
            throw new NotImplementedException();
        }
        #endregion


        /**
         * UserSelection
         */
        #region UserSelection
        protected override void UserSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_UserSelection), null, new OnCardEntered(this.onCardEntered_UserSelection), null);
            this.guiController.showUserSelection();
        }

        protected override void UserSelected(int userID)
        {
            Account user = this.dataStorage.Accounts.First(x => x.ID == userID);
            this.unauthentificatedUser = user;
            this.AuthentificationSate(user);
        }


        protected override void onCancel_UserSelection()
        {
            this.start();
        }

        protected override void onCardEntered_UserSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.UserSelected(account.ID);
                this.UserAuthentificated();
            }
        }

        protected override void onTimeUp_UserSelection()
        {
            throw new NotImplementedException();
        }
        #endregion


        /**
         * Authentification
         */
        #region Authentification
        protected override void AuthentificationSate(Account account)
        {
            //TODO add auth
            this.changeSubscription(null, new OnCancel(this.onCancel_Authentification), null, new OnCardEntered(this.onCardEntered_Authentification), null);
            this.unauthentificatedUser = account;
            this.guiController.showAuthentification();
        }


        protected override void UserAuthentificated(int[] inputSecurityCode)
        {
            bool correctSecurityCode = this.secUtil.correctSecurityCode(this.unauthentificatedUser, inputSecurityCode);

            if (correctSecurityCode)
            {
                this.guiController.correctAuthentificationEntered(correctSecurityCode, this.ArticleSelection);//guiController.showArticleSelection
                this.currentBooking.User = this.unauthentificatedUser.ID;
                this.currentCancellation.User = this.unauthentificatedUser.ID;

                this.guiUtil.setUserPicture(this.unauthentificatedUser);
                this.unauthentificatedUser = null;
            }
            else
            {
                this.guiController.correctAuthentificationEntered(correctSecurityCode, null);
            }
        }

        protected override void UserAuthentificated()
        {
            this.currentBooking.User = this.unauthentificatedUser.ID;
            this.currentCancellation.User = this.unauthentificatedUser.ID;

            this.guiUtil.setUserPicture(this.unauthentificatedUser);
            this.unauthentificatedUser = null;
            if (this.currentBookingAA == null)
            {
                this.guiController.showArticleSelection();
            }
        }


        protected override void onCancel_Authentification()
        {
            this.unauthentificatedUser = null;
            this.UserSelection();
        }


        protected override void onCardEntered_Authentification()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.UserSelected(account.ID);
                this.UserAuthentificated();
            }
        }
        #endregion


        /**
         * Customer Selection
         */
        #region Customer Selection
        protected override void CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void ConsumerSelected(int consumerID)
        {
            throw new NotImplementedException();
        }

        protected override void onOK_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCancel_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCardEntered_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_CustomerSelection()
        {
            throw new NotImplementedException();
        }
        #endregion


        /**
         * Warning Msg Overlay
         */
        #region Msg Overlay
        protected override void WarningOverlay(string message)
        {
            throw new NotImplementedException();
        }

        protected override void onOK_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onCancel_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onCardEntered_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_WarningOverlay()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
