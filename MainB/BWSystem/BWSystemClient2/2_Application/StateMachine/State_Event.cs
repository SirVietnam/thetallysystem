﻿using BWSystemClient2.Appl.Utility;
using BWSystemClient2.GUI;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.StateMachine
{
    class State_Event : StateMachine
    {
        private bool eventStarted;
        private Event currentEvent;
        private int? notPayedArticleTemp;

        public State_Event(Event currentEvent) :
            base()
        {
            this.currentEvent = currentEvent;
            this.eventStarted = false;
            this.guiController.setEventTopPanelData(currentEvent.Name, GuiUtility.dateTime_to_String(currentEvent.Begin) + " - " + GuiUtility.dateTime_to_String(currentEvent.End));
        }


        internal override void start()
        {
            this.initSubscribe();
            guiController.BookingPanel.resetPanel();
            this.notPayedArticleTemp = null;
            this.currentBooking = null;
            this.currentBookingAA = null;
            this.guiUtil.setConsumerPicture(null);
            this.guiUtil.setUserPicture(null);
            this.ArticleSelection();
        }

        internal override bool saveState()
        {
            return this.currentBooking == null;
        }


        internal override void updateData(bool forceUpdate = false)
        {
            this.dim.updateEventData((int)this.currentEvent.EventID, !this.eventStarted);
            this.eventStarted = true;
        }


        protected override void initSubscribe()
        {
            guiController.clearAllSubscriber();
            guiController.subscribeOnConsumer(new OnConsumer(this.ConsumerSelected));
            guiController.subscribeOnArticle(new OnArticle(this.ArticleSelected));
            guiController.subscribeOnAmount(new OnAmount(this.AmoutSelected));
            guiController.subscribeOnUser(new OnUser(this.UserSelected));
            guiController.subscribeOnAuthentification(new OnAuthentification(this.UserAuthentificated));
        }



        /**
         * ArticleState
         */
        #region ArticleSelectionState
        protected override void ArticleSelection()
        {
            if (this.currentBooking == null)
            {
                guiController.showEventArticleSelection(false);
                this.changeSubscription(null, null);
            }
            else if (this.notPayedArticleTemp != null)
            {
                int articleSelectedTemp = (int)this.notPayedArticleTemp;
                this.notPayedArticleTemp = null;
                this.ArticleSelected(articleSelectedTemp);
            }
            else
            {
                guiController.showEventArticleSelection(true);
                this.changeSubscription(new OnOK(this.onOK_ArticleSelection), new OnCancel(this.onCancel_ArticleSelection), null, null, new OnTimerUp(this.onTimeUp_ArticleSelection));
            }
        }


        private DateTime lastClick = new DateTime();

        protected override void ArticleSelected(int articleID)
        {
            if (this.currentBooking == null)
            {
                this.initBookingData();
                this.currentBooking.EventBooking = this.currentEvent.EventID;
                this.currentBooking.Customer = this.currentEvent.CreatorAccountID;
            }

            if (this.currentEvent.CostAbsorption.Contains(articleID))
            {
                this.ArticleSelected_Payed(articleID);
            }
            else
            {
                this.ArticleSelected_NonPayed(articleID);
            }

            this.lastClick = DateTime.Now;
        }



        private void ArticleSelected_Payed(int articleID)
        {
            if (this.currentBookingAA != null && this.currentBookingAA.Article == articleID && (this.lastClick > DateTime.Now - TimeSpan.FromSeconds(0.5)))
            {
                this.AmountSelection();
            }
            else
            {
                this.currentBookingAA = this.currentBooking.Booking.FirstOrDefault(x => x.Article == articleID);
                if (this.currentBookingAA != null)
                {
                    this.currentBookingAA.Amount++;
                }
                else
                {
                    this.newBookingAA(articleID);
                }
                DataMapper.syncBookingDataToGui(this.guiController.BookingPanel, this.currentBookingAA);
                this.ArticleSelection();
            }
        }


        private void ArticleSelected_NonPayed(int articleID)
        {
            if (this.currentBooking.User == null)
            {
                this.notPayedArticleTemp = articleID;
                this.WarningOverlay("The Article you selected ist not payed for by the current event. You need pay for (only) this article yourself. Please select the customer.");
            }
            else
            {
                this.ArticleSelected_Payed(articleID);
            }
        }


        protected override void onOK_ArticleSelection()
        {
            bool booked = this.dim.bookBooking(this.currentBooking);
            if (booked)
            {
                this.guiController.showSuccessfulBookedPanel();
            }
            this.start();
        }

        protected override void onCancel_ArticleSelection()
        {
            this.start();
        }

        protected override void onCardEntered_ArticleSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_ArticleSelection()
        {
            this.onOK_ArticleSelection();
        }
        #endregion


        /**
         * AmountState
         */
        #region AmountSelectionState
        protected override void AmountSelection(BookingAA bookingAA)
        {
            throw new NotImplementedException();
        }


        protected override void onOK_AmountSelection()
        {
            if (this.currentBookingAA.Amount == 0)
            {
                this.removeArticle(this.currentBookingAA);
                DataMapper.removeBookingDataFromGui(this.guiController.BookingPanel, this.currentBookingAA);
            }

            this.currentBookingAA = null;
            this.ArticleSelection();
        }

        protected override void onCardEntered_AmountSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                if (this.currentBooking.User == null)
                {
                    this.UserSelected(account.ID);
                    this.UserAuthentificated();
                }
                this.AmountSelection();
            }
        }

        protected override void onTimeUp_AmountSelection()
        {
            this.onOK_AmountSelection();
            this.ArticleSelection();
        }
        #endregion


        /**
         * UserSelection
         */
        #region UserSelectionState
        protected override void onCancel_UserSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_UserSelection), null, new OnCardEntered(this.onCardEntered_UserSelection), new OnTimerUp(this.onTimeUp_UserSelection));
            this.CustomerSelection();
        }

        protected override void onTimeUp_UserSelection()
        {
            this.onCancel_UserSelection();
        }
        #endregion


        /**
         * Authentification
         */
        #region AuthentificationState
        protected override void UserAuthentificated(int[] inputSecurityCode)
        {
            bool correctSecurityCode = this.secUtil.correctSecurityCode(this.unauthentificatedUser, inputSecurityCode);

            if (correctSecurityCode)
            {

                this.guiController.correctAuthentificationEntered(correctSecurityCode, this.ArticleSelection);

                this.currentBooking.User = this.unauthentificatedUser.ID;

                this.guiUtil.setUserPicture(this.unauthentificatedUser);
                this.unauthentificatedUser = null;
            }
            else
            {
                this.guiController.correctAuthentificationEntered(correctSecurityCode, null);
            }
        }

        protected override void UserAuthentificated()
        {
            base.UserAuthentificated();
            this.ArticleSelection();
        }

        protected override void onCardEntered_Authentification()
        {
            this.UserAuthentificated();
        }
        #endregion


        /**
         * CustomerState
         */
        #region CustomerSelectionState
        protected override void CustomerSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_CustomerSelection), null, new OnCardEntered(this.onCardEntered_CustomerSelection), this.onTimeUp_CustomerSelection);
            //guiData.setNewBookingData();
            guiController.showCustomerSelection(true);
        }

        protected override void ConsumerSelected(int consumerID)
        {
            this.currentBooking.Customer = consumerID;
            Account consumer = this.dataStorage.Accounts.First(x => x.ID == consumerID);
            //If the account was created automatically => reroute to account creation
            if (SecurityUtility.isValidAccount(consumer)) // consumer.Authentication.SecurityCodeHash != null && consumer.Authentication.SecurityCodeHash.Length != 0
            {
                this.guiUtil.setConsumerPicture(consumer);
                this.UserSelection();
            }
            else
            {
                this.onCompleteAccount(consumerID);
            }
        }


        protected override void onOK_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCancel_CustomerSelection()
        {
            this.ArticleSelection();
        }

        protected override void onCardEntered_CustomerSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.ConsumerSelected(account.ID);
                this.UserSelected(account.ID);
                this.UserAuthentificated();
            }
        }

        protected override void onTimeUp_CustomerSelection()
        {
            this.onCancel_CustomerSelection();
        }
        #endregion


        /**
         * ArticleInformation
         */
        #region WarningOverlay
        protected override void WarningOverlay(string message)
        {
            this.changeSubscription(new OnOK(this.onOK_WarningOverlay), new OnCancel(this.onCancel_WarningOverlay), null, new OnCardEntered(this.onCardEntered_WarningOverlay), new OnTimerUp(this.onTimeUp_WarningOverlay));

            this.guiController.showWarningPanel(message);
        }


        protected override void onOK_WarningOverlay()
        {
            this.CustomerSelection();
        }

        protected override void onCancel_WarningOverlay()
        {
            this.notPayedArticleTemp = null;
            this.ArticleSelection();
        }

        protected override void onCardEntered_WarningOverlay()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.ConsumerSelected(account.ID);
                this.UserSelected(account.ID);
                this.ArticleSelection();
            }
        }

        protected override void onTimeUp_WarningOverlay()
        {
            this.onCancel_AmountSelection();
        }
        #endregion
    }
}
