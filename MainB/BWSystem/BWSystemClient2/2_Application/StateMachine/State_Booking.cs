﻿using BWSystemClient2.Appl;
using BWSystemClient2.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BWSystemLibrary2.BW_WebService;
using BWSystemClient2.GUI.SubElements.ItemContainer;
using BWSystemClient2.Domain;
using System.Threading;
using BWSystemClient2.Appl.Utility;
using System.Windows.Threading;
using BWSystemLibrary2;

namespace BWSystemClient2.Appl.StateMachine
{
    public delegate void OnCompleteAccount(int accountID);

    class State_Booking : StateMachine
    {


        public State_Booking()
            : base()
        {
        }


        internal override void start()
        {
            this.initBookingData();
            this.initSubscribe();
            this.CustomerSelection();
            guiController.BookingPanel.resetPanel();
        }

        internal override bool saveState()
        {
            return (this.currentBooking != null && this.currentBooking.Customer == -1);
        }


        protected override void initSubscribe()
        {
            guiController.clearAllSubscriber();
            guiController.subscribeOnConsumer(new OnConsumer(this.ConsumerSelected));
            guiController.subscribeOnArticle(new OnArticle(this.ArticleSelected));
            guiController.subscribeOnAmount(new OnAmount(this.AmoutSelected));
            guiController.subscribeOnUser(new OnUser(this.UserSelected));
            guiController.subscribeOnAuthentification(new OnAuthentification(this.UserAuthentificated));
            guiController.subscribeOnDataSync(new OnSyncData(this.onSyncData_Clicked));
        }


        /**
         * CustomerState
         */
        #region CustomerSelectionState
        protected override void CustomerSelection()
        {
            this.changeSubscription(null, null, null, new OnCardEntered(this.onCardEntered_CustomerSelection), null);
            this.guiUtil.setConsumerPicture(null);
            this.guiUtil.setUserPicture(null);
            //guiData.setNewBookingData();
            guiController.showCustomerSelection();
        }

        protected override void ConsumerSelected(int consumerID)
        {
            this.currentBooking.Customer = consumerID;
            Account consumer = DataStorage.Instance.Accounts.First(x => x.ID == consumerID);
            //If the account was created automatically => reroute to account creation
            if (SecurityUtility.isValidAccount(consumer)) // consumer.Authentication.SecurityCodeHash != null && consumer.Authentication.SecurityCodeHash.Length != 0
            {
                this.guiUtil.setConsumerPicture(consumer);
                this.ArticleSelection();
            }
            else
            {
                this.onCompleteAccount(consumerID);
            }
        }

        protected override void onOK_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onCancel_CustomerSelection()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_CustomerSelection()
        {
            throw new NotImplementedException();
        }


        protected override void onCardEntered_CustomerSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                if (SecurityUtility.isValidAccount(account))
                {
                    this.ConsumerSelected(account.ID);
                    this.UserSelected(account.ID);
                    this.UserAuthentificated();
                    this.ArticleSelected(account.Standard);
                }
                else
                {
                    this.onCompleteAccount(account.ID);
                }
            }
        }
        #endregion

        /**
         * ArticleState
         */
        #region ArticleSelectionState
        protected override void ArticleSelection()
        {
            OnOK onOK = SecurityUtility.containsValidBookingAA(this.currentBooking) ? new OnOK(this.onOK_ArticleSelection) : null;
            this.changeSubscription(onOK, new OnCancel(this.onCancel_ArticleSelection), null, new OnCardEntered(this.onCardEntered_ArticleSelection), new OnTimerUp(this.onTimeUp_ArticleSelection));

            if (secUtil.authentificationRequired())
            {
                this.UserSelection();
            }
            else
            {
                this.guiController.showArticleSelection();
            }
        }


        protected override void ArticleSelected(int articleID)
        {
            this.currentBookingAA = this.currentBooking.Booking.FirstOrDefault(x => x.Article == articleID);
            if (this.currentBookingAA == null)
            {
                this.newBookingAA(articleID);

                DataMapper.addBookingDataFromGui(this.guiController.BookingPanel, this.currentBookingAA);
            }

            this.AmountSelection(this.currentBookingAA);
        }


        protected override void onOK_ArticleSelection()
        {
            if (SecurityUtility.containsValidBookingAA(this.currentBooking))
            {
                bool booked = this.dim.bookBooking(this.currentBooking);
                if (booked)
                {
                    this.guiController.showSuccessfulBookedPanel();
                }
                //TODO: Else => failed booking!
            }
            else
            {
                SimpleLog.Log("Illegal BookingAA was tried to book!");
                SecurityUtility.logErrorBooking(this.currentBooking);
                //TODO: Show error!
            }

            ApplicationManager.reset();
        }

        protected override void onCancel_ArticleSelection()
        {
            ApplicationManager.reset();
        }

        protected override void onCardEntered_ArticleSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.UserSelected(account.ID);
                this.UserAuthentificated();
            }
        }

        protected override void onTimeUp_ArticleSelection()
        {
            this.onOK_ArticleSelection();
        }
        #endregion

        /**
         * AmountState
         */
        #region AmountSelectionState
        protected override void AmountSelection()
        {
            this.AmountSelection(this.currentBookingAA);
        }

        protected override void AmountSelection(BookingAA bookingAA)
        {
            this.changeSubscription(new OnOK(this.onOK_AmountSelection), new OnCancel(this.onCancel_AmountSelection), new OnJoker(this.ArticleInformation), new OnCardEntered(this.onCardEntered_AmountSelection), new OnTimerUp(this.onTimeUp_AmountSelection));

            this.currentBookingAA = bookingAA;
            Article currentArticle = this.dataStorage.Articles.First(x => x.ID == this.currentBookingAA.Article);
            string picturePath = this.dataStorage.getArticlePicturePath(currentArticle.ID);
            this.guiController.showAmountSelection(bookingAA.Article, bookingAA.Amount, picturePath, this.guiUtil.getAmountButtonValues(bookingAA.Amount, currentArticle.Size));
        }

        protected override void AmoutSelected(int amount)
        {
            this.currentBookingAA.Amount = amount;
            DataMapper.syncBookingDataToGui(this.guiController.BookingPanel, this.currentBookingAA);

            if (this.secUtil.authentificationRequired())
            {
                this.UserSelection();
                //this.guiController.removeAmountSelection();
            }
            else
            {
                Article currentArticle = this.dataStorage.Articles.First(x => x.ID == this.currentBookingAA.Article);
                this.guiController.setAmountButtonValues(guiUtil.getAmountButtonValues(amount, currentArticle.Size));
            }
        }


        protected override void onOK_AmountSelection()
        {
            if (this.currentBookingAA.Amount == 0)
            {
                this.removeArticle(this.currentBookingAA);
                DataMapper.removeBookingDataFromGui(this.guiController.BookingPanel, this.currentBookingAA);
            }

            this.currentBookingAA = null;
            this.ArticleSelection();
        }

        protected override void onCancel_AmountSelection()
        {
            this.removeArticle(this.currentBookingAA);
            DataMapper.removeBookingDataFromGui(this.guiController.BookingPanel, this.currentBookingAA);
            this.ArticleSelection();
        }

        protected override void onCardEntered_AmountSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.UserSelected(account.ID);
                this.UserAuthentificated();
                this.AmountSelection(this.currentBookingAA);
            }
        }

        protected override void onTimeUp_AmountSelection()
        {
            this.onOK_AmountSelection();
            if (SecurityUtility.containsValidBookingAA(this.currentBooking))
                this.onOK_ArticleSelection();
            else
                this.onCancel_ArticleSelection();
        }
        #endregion


        /**
         * UserSelection
         */
        #region UserSelectionState
        //protected override void UserSelection()
        //{
        //    this.changeSubscription(null, new OnCancel(this.onCancel_UserSelection), null, new OnCardEntered(this.onCardEntered_UserSelection), new OnTimerUp(this.onTimeUp_UserSelection));
        //    this.guiController.showUserSelection();
        //}

        protected override void UserSelected(int userID)
        {
            Account user = this.dataStorage.Accounts.First(x => x.ID == userID);
            this.unauthentificatedUser = user;
            this.AuthentificationSate(user);
        }


        protected override void onCancel_UserSelection()
        {
            this.start();
        }

        //protected override void onCardEntered_UserSelection()
        //{
        //    Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

        //    if (account != null)
        //    {
        //        this.UserSelected(account.ID);
        //        this.UserAuthentificated();
        //    }
        //}

        protected override void onTimeUp_UserSelection()
        {
            this.onCancel_UserSelection();
        }
        #endregion

        /**
         * Authentification
         */
        #region AuthentificationState
        protected override void UserAuthentificated()
        {
            base.UserAuthentificated();
            if (this.currentBookingAA == null)
            {
                this.guiController.showArticleSelection();
            }
            else
            {
                this.AmountSelection();
            }
        }
        #endregion


        /**
         * SyncData
         */
        #region SyncData
        //private void onSyncData_Clicked()
        //{
        //    guiController.clearAllSubscriber();
        //    this.guiController.showWaitingOverlay();
        //    System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => this.async_OnSyncData_Clicked()));
        //}

        //private void async_OnSyncData_Clicked()
        //{
        //    ApplicationManager.reset(true);
        //}
        #endregion


        /**
         * Warning Msg Overlay
         */
        #region WarningMsgOverlay
        protected override void WarningOverlay(string message)
        {
            throw new NotImplementedException();
        }

        protected override void onOK_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onCancel_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onCardEntered_WarningOverlay()
        {
            throw new NotImplementedException();
        }

        protected override void onTimeUp_WarningOverlay()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}