﻿using BWSystemClient2.Appl.Utility;
using BWSystemClient2.Domain;
using BWSystemClient2.GUI;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace BWSystemClient2.Appl.StateMachine
{
    abstract class StateMachine : GuiDelegateManager
    {
        protected GuiDataStorage guiData;

        protected DataInteractionManager dim;
        protected DataStorage dataStorage;

        protected SecurityUtility secUtil;
        protected GuiUtility guiUtil;

        //Private temporary stoarge for InputData
        protected BookingData currentBooking;
        protected Account unauthentificatedUser;
        protected BookingAA currentBookingAA;


        public StateMachine()
            : base()
        {
            this.dim = DataInteractionManager.Instance;
            this.guiData = GuiDataStorage.Instance;

            this.dataStorage = DataStorage.Instance;
            this.secUtil = new SecurityUtility();
            this.guiUtil = new GuiUtility();
        }


        /**
         * Startup Methods
         */
        internal abstract void start();

        protected abstract void initSubscribe();

        internal abstract bool saveState();

        internal virtual void updateData(bool forceUpdate = false)
        {
            this.dim.updateData(forceUpdate);
        }


        //Change Helpers
        protected virtual void initBookingData()
        {
            this.currentBooking = new BookingData();
            this.currentBooking.Booking = new BookingAA[0];
            this.dataStorage.BookingData = this.currentBooking;
            this.currentBooking.Customer = -1;
            this.unauthentificatedUser = null;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected virtual void newBookingAA(int articleID)
        {
            this.currentBookingAA = new BookingAA();
            this.currentBookingAA.Article = articleID;
            this.currentBookingAA.Amount = 1;
            BookingAA[] bookingAAs = new BookingAA[this.currentBooking.Booking.Length + 1];
            this.currentBooking.Booking.CopyTo(bookingAAs, 0);
            bookingAAs[bookingAAs.Length - 1] = this.currentBookingAA;
            this.currentBooking.Booking = bookingAAs;
        }


        protected void removeArticle(BookingAA bookingAA)
        {
            BookingAA[] newBookingAAs = new BookingAA[this.currentBooking.Booking.Length - 1];
            int offset = 0;
            for (int i = 0; i < newBookingAAs.Length; i++)
            {
                if (this.currentBooking.Booking[i].Article == bookingAA.Article)
                {
                    offset++;
                }
                newBookingAAs[i] = this.currentBooking.Booking[i + offset];
            }
            this.currentBooking.Booking = newBookingAAs;
        }

        /**
         * CustomerState
         */
        #region CustomerSelectionState
        protected abstract void CustomerSelection();

        protected abstract void ConsumerSelected(int consumerID);

        protected abstract void onOK_CustomerSelection();

        protected abstract void onCancel_CustomerSelection();

        protected abstract void onCardEntered_CustomerSelection();

        protected abstract void onTimeUp_CustomerSelection();
        #endregion


        /**
         * ArticleState
         */
        #region ArticleSelectionState
        protected abstract void ArticleSelection();

        protected abstract void ArticleSelected(int articleID);

        protected abstract void onOK_ArticleSelection();

        protected abstract void onCancel_ArticleSelection();

        protected abstract void onCardEntered_ArticleSelection();

        protected abstract void onTimeUp_ArticleSelection();
        #endregion


        /**
         * AmountState
         */
        #region AmountSelectionState
        protected virtual void AmountSelection()
        {
            this.changeSubscription(new OnOK(this.onOK_AmountSelection), new OnCancel(this.onCancel_AmountSelection), new OnJoker(this.ArticleInformation), new OnCardEntered(this.onCardEntered_AmountSelection), new OnTimerUp(this.onTimeUp_AmountSelection));

            Article currentArticle = this.dataStorage.Articles.First(x => x.ID == this.currentBookingAA.Article);
            string picturePath = this.dataStorage.getArticlePicturePath(currentArticle.ID);
            this.guiController.showAmountSelection(this.currentBookingAA.Article, this.currentBookingAA.Amount, picturePath, this.guiUtil.getAmountButtonValues(this.currentBookingAA.Amount, currentArticle.Size));
        }

        protected abstract void AmountSelection(BookingAA bookingAA);

        protected virtual void AmoutSelected(int amount)
        {
            this.currentBookingAA.Amount = amount;
            DataMapper.syncBookingDataToGui(this.guiController.BookingPanel, this.currentBookingAA);

            if (this.secUtil.authentificationRequired())
            {
                this.UserSelection();
            }
            else
            {
                Article currentArticle = this.dataStorage.Articles.First(x => x.ID == this.currentBookingAA.Article);
                this.guiController.setAmountButtonValues(guiUtil.getAmountButtonValues(amount, currentArticle.Size));
            }
        }

        protected abstract void onOK_AmountSelection();

        protected virtual void onCancel_AmountSelection()
        {
            this.removeArticle(this.currentBookingAA);
            DataMapper.removeBookingDataFromGui(this.guiController.BookingPanel, this.currentBookingAA);
            this.ArticleSelection();
        }

        protected abstract void onCardEntered_AmountSelection();

        protected abstract void onTimeUp_AmountSelection();
        #endregion


        /**
         * ArticleInformation
         */
        #region ArticleInformationState
        protected virtual void ArticleInformation(int articleID)
        {
            this.changeSubscription(null, null, new OnJoker(removeInformation));

            Article article = this.dataStorage.Articles.First(x => x.ID == articleID);
            this.guiController.showArticleInformation(article);
        }

        protected virtual void removeInformation(int articleID)
        {
            this.AmountSelection(this.currentBookingAA);
        }
        #endregion


        /**
         * UserSelection
         */
        #region UserSelectionState
        protected virtual void UserSelection()
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_UserSelection), null, new OnCardEntered(this.onCardEntered_UserSelection), new OnTimerUp(this.onTimeUp_UserSelection));
            this.guiController.showUserSelection();
        }

        protected virtual void UserSelected(int userID)
        {
            Account user = this.dataStorage.Accounts.First(x => x.ID == userID);
            this.unauthentificatedUser = user;
            this.AuthentificationSate(user);
        }

        protected abstract void onCancel_UserSelection();

        protected virtual void onCardEntered_UserSelection()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.UserSelected(account.ID);
                this.UserAuthentificated();
            }
        }

        protected abstract void onTimeUp_UserSelection();
        #endregion


        /**
         * Authentification
         */
        #region AuthentificationState
        protected virtual void AuthentificationSate(Account account)
        {
            this.changeSubscription(null, new OnCancel(this.onCancel_Authentification), null, new OnCardEntered(this.onCardEntered_Authentification), new OnTimerUp(this.onTimeUp_Authentification));
            this.unauthentificatedUser = account;
            this.guiController.showAuthentification();
        }


        protected virtual void UserAuthentificated(int[] inputSecurityCode)
        {
            bool correctSecurityCode = this.secUtil.correctSecurityCode(this.unauthentificatedUser, inputSecurityCode);

            if (correctSecurityCode)
            {
                if (this.currentBookingAA == null)
                {
                    this.guiController.correctAuthentificationEntered(correctSecurityCode, this.ArticleSelection);
                }
                else
                {
                    this.guiController.correctAuthentificationEntered(correctSecurityCode, this.AmountSelection);
                }
                this.currentBooking.User = this.unauthentificatedUser.ID;

                this.guiUtil.setUserPicture(this.unauthentificatedUser);
                this.unauthentificatedUser = null;
            }
            else
            {
                this.guiController.correctAuthentificationEntered(correctSecurityCode, null);
            }
        }

        protected virtual void UserAuthentificated()
        {
            this.currentBooking.User = this.unauthentificatedUser.ID;
            this.guiUtil.setUserPicture(this.unauthentificatedUser);
            this.unauthentificatedUser = null;
        }


        protected virtual void onCancel_Authentification()
        {
            this.unauthentificatedUser = null;
            this.UserSelection();
        }

        protected virtual void onCardEntered_Authentification()
        {
            Account account = this.secUtil.validCardEntered(dataStorage.CardDatas.CardNumber);

            if (account != null)
            {
                this.UserSelected(account.ID);
                this.UserAuthentificated();
            }
        }

        protected virtual void onTimeUp_Authentification()
        {
            this.onCancel_Authentification();
        }
        #endregion


        /**
         * SyncData
         */
        #region SyncData
        protected virtual void onSyncData_Clicked()
        {
            guiController.clearAllSubscriber();
            this.guiController.showWaitingOverlay();
            System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => this.async_OnSyncData_Clicked()));
        }

        protected virtual void async_OnSyncData_Clicked()
        {
            ApplicationManager.reset(true);
        }
        #endregion


        /**
         * ArticleInformation
         */
        #region WarningOverlay
        protected abstract void WarningOverlay(string message);

        protected abstract void onOK_WarningOverlay();

        protected abstract void onCancel_WarningOverlay();

        protected abstract void onCardEntered_WarningOverlay();

        protected abstract void onTimeUp_WarningOverlay();
        #endregion


    }
}
