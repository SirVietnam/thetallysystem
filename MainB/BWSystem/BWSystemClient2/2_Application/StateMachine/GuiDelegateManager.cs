﻿using BWSystemClient2.GUI;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.StateMachine
{
    abstract class GuiDelegateManager
    {
        protected GuiController guiController;

        private OnOK currentOnOK;
        private OnCancel currentOnCancel;
        protected OnJoker currentOnJoker;
        private OnCardEntered currentOnCardEntered;
        private OnTimerUp currentOnTimerUp;

        protected OnCompleteAccount onCompleteAccount;


        public GuiDelegateManager()
        {
            this.guiController = GuiController.Instance;
        }


        /*
         * Change current methods
         */
        protected void changeSubscription(OnOK newOnOK, OnCancel newOnCancel)
        {
            this.changeSubscription(newOnOK, newOnCancel, null, null, null);

        }

        protected void changeSubscription(OnOK newOnOK, OnCancel newOnCancel, OnJoker newOnJoker)
        {
            this.changeSubscription(newOnOK, newOnCancel, newOnJoker, null, null);
        }

        protected void changeSubscription(OnOK newOnOK, OnCancel newOnCancel, OnJoker newOnJoker, OnCardEntered newOnCardEntered, OnTimerUp newOnTimerUp)
        {
            this.changeOnOK(newOnOK);
            this.changeOnCancel(newOnCancel);
            this.changeOnJoker(newOnJoker);
            this.changeOnCardEntered(newOnCardEntered);
            this.changeOnTimerUp(newOnTimerUp);
        }



        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void changeOnOK(OnOK newOnOK)
        {
            //OK
            if (this.currentOnOK != null)
            {
                this.guiController.unSubscribeOnOK(this.currentOnOK);
            }
            if (newOnOK != null)
            {
                this.guiController.subscribeOnOK(newOnOK);
            }
            this.currentOnOK = newOnOK;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void changeOnCancel(OnCancel newOnCancel)
        {
            //Cancel
            if (this.currentOnCancel != null)
            {
                this.guiController.unSubscribeOnCancel(this.currentOnCancel);
            }
            if (newOnCancel != null)
            {
                this.guiController.subscribeOnCancel(newOnCancel);
            }
            this.currentOnCancel = newOnCancel;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void changeOnJoker(OnJoker newOnJoker)
        {
            //Joker
            if (this.currentOnJoker != null)
            {
                this.guiController.unSubscribeOnJoker(this.currentOnJoker);
            }
            if (newOnJoker != null)
            {
                this.guiController.subscribeOnJoker(newOnJoker);
            }
            this.currentOnJoker = newOnJoker;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void changeOnCardEntered(OnCardEntered newOnCardEntered)
        {
            //Card
            if (this.currentOnCardEntered != null)
            {
                this.guiController.unSubscribeCardEntered(this.currentOnCardEntered);
            }
            if (newOnCardEntered != null)
            {
                this.guiController.subscribeOnCardEntered(newOnCardEntered);
            }
            this.currentOnCardEntered = newOnCardEntered;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void changeOnTimerUp(OnTimerUp newOnTimerUp)
        {
            //Card
            if (this.currentOnTimerUp != null)
            {
                this.guiController.unSubscribeCardEntered(this.currentOnTimerUp);
            }
            if (newOnTimerUp != null)
            {
                this.guiController.subscribeOnTimerUp(newOnTimerUp);
            }
            this.currentOnTimerUp = newOnTimerUp;
        }

        /**
         * Subscribe on Single Actions
         */
        internal void subscribeOnCompleteAccount(OnCompleteAccount onCompleteAccount)
        {
            this.onCompleteAccount += onCompleteAccount;
        }
    }
}
