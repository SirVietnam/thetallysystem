﻿using BWSystemClient2.Domain;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BWSystemClient2.Appl.Utility
{
    class SecurityUtility
    {
        private double authentificationTimeSpan;
        private double authentificationCost;

        public SecurityUtility()
        {
            this.authentificationTimeSpan = (double)BWSystemLibrary2.LibUtility.getConfigValue("ValidationSecTimeSpan", typeof(double));
            this.authentificationCost = (double)BWSystemLibrary2.LibUtility.getConfigValue("CashAutorisationThreash", typeof(double));
        }


        public bool authentificationRequired()
        {
            bool alreadyAuth = (DataStorage.Instance.BookingData.User != null);
            bool authReq = false;

            if (!alreadyAuth)
            {
                int consumerID = DataStorage.Instance.BookingData.Customer;
                Account consumer = DataStorage.Instance.Accounts.First(x => x.ID == consumerID);
                authReq = consumer.AuthenticateAlways;

                if (!authReq)
                {
                    long authTicks = DateTime.Now.Ticks - TimeSpan.FromSeconds(authentificationTimeSpan).Ticks;
                    IEnumerable<BookingData> bookingDatas = DataStorage.Instance.FinishedBookings.Where(x => x.TimeStamp.Ticks >= authTicks);
                    double cost = this.calculateCosts(bookingDatas);
                    cost += this.calculateCost(DataStorage.Instance.BookingData);
                    authReq = cost >= authentificationCost;
                }
            }
            return authReq;
        }


        private double calculateCosts(IEnumerable<BookingData> bookingDatas)
        {
            double cost = 0.0f;
            foreach (BookingData bookingData in bookingDatas)
            {
                cost += this.calculateCost(bookingData);
            }

            return cost;
        }


        private double calculateCost(BookingData bookingData)
        {
            double cost = 0.0;

            foreach (BookingAA bookingAA in bookingData.Booking)
            {
                Article article = DataStorage.Instance.Articles.First(x => x.ID == bookingAA.Article);
                cost += ((double)bookingAA.Amount) * article.Prize;
            }
            return cost;
        }


        internal bool correctSecurityCode(Account account, int[] inputSecurityCode)
        {
            bool correctCode = false;

            if (SecurityUtility.isValidAccount(account))
            {
                correctCode = BWSystemLibrary2.PasswordHashUtil.validatePassword(inputSecurityCode, account.Authentication.SecurityCodeSalt, account.Authentication.SecurityCodeHash);
            }

            return correctCode;
        }


        internal static bool ValidSecurityCode(int[] inputSecurityCode)
        {
            bool validCode = inputSecurityCode != null && inputSecurityCode.Length >= (int)BWSystemLibrary2.LibUtility.MinSecurityCodeCount;
            return validCode;
        }


        internal Account validCardEntered(long enteredCardNumber)
        {
            Account accountToCard = null;

            foreach (Account acc in DataStorage.Instance.Accounts)
            {
                if (acc.Authentication.CardCode != null)
                {
                    foreach (long userCardCode in acc.Authentication.CardCode)
                    {
                        if (userCardCode == enteredCardNumber)
                        {
                            accountToCard = acc;
                            break;
                        }
                    }

                    if (accountToCard != null)
                    {
                        break;
                    }
                }
            }

            return accountToCard;
        }

        internal static bool isValidAccount(Account account)
        {
            bool nameCorrect = account.Name != null && !account.Name.Equals("");
            bool surnameCorrect = account.Surname != null && !account.Surname.Equals("");
            bool hashCorrect = account.Authentication.SecurityCodeHash != null && account.Authentication.SecurityCodeHash.Length > 0;
            bool saltCorrect = account.Authentication.SecurityCodeSalt != null && account.Authentication.SecurityCodeSalt.Length > 0;
            bool standardCorrect = account.Standard >= 0; //TODO: make workaround!
            return (nameCorrect && surnameCorrect && hashCorrect && saltCorrect && standardCorrect);
        }


        internal static bool containsValidBookingAA(BookingData booking)
        {
            bool validBooking = booking.Booking != null;
            validBooking = validBooking && booking.Booking.Length > 0;
            if (validBooking)
            {
                foreach (BookingAA bookingAA in booking.Booking)
                {
                    validBooking = validBooking && bookingAA.Amount > 0;
                }
            }
            return validBooking;
        }


        internal static void logErrorBooking(BookingData bookingData)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Invalid Booking recieved:");
            sb.AppendLine("ConsumerID:" + bookingData.Customer);
            if (bookingData.User != null)
                sb.AppendLine("UserID:" + bookingData.User);
            if (bookingData.EventBooking != null)
                sb.AppendLine("EventID:" + bookingData.EventBooking);
            sb.AppendLine("Cancelled: " + bookingData.DataCancelled);
            sb.AppendLine("TimeStamp: " + bookingData.TimeStamp.ToString());
            if (bookingData.Booking != null)
            {
                foreach (BookingAA bookingAA in bookingData.Booking)
                {
                    sb.AppendLine("-ArticleID: " + bookingAA.Article + " - Amount:" + bookingAA.Amount);
                }
            }
            else
            {
                sb.AppendLine("BookingAA is null!");
            }
            SimpleLog.Log(sb.ToString());
        }
    }
}
