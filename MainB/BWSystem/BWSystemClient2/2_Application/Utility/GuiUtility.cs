﻿using BWSystemClient2.Domain;
using BWSystemClient2.Domain.WebService;
using BWSystemClient2.GUI;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.Utility
{
    class GuiUtility
    {

        public GuiUtility()
        {
        }

        public int?[] getAmountButtonValues(int amount, double botteSize)
        {
            int?[] buttonValues = new int?[6];
            int refSize = LibUtility.getStandardAmount(botteSize);

            int?[] plusValues = this.plusSizes(amount, refSize);
            plusValues.CopyTo(buttonValues, 3);

            int?[] minValues = this.minSizes(amount, refSize);
            minValues.CopyTo(buttonValues, 0);

            return buttonValues;
        }


        //private int getStandardAmount(double size)
        //{
        //    int stdSize = 12;

        //    if (size >= 1.0)
        //    {
        //        stdSize = 12;
        //    }
        //    else if (size >= 0.75)
        //    {
        //        stdSize = 12;
        //    }
        //    else if (size >= 0.5)
        //    {
        //        stdSize = 20;
        //    }
        //    else if (size >= 0.3)
        //    {
        //        stdSize = 24;
        //    }
        //    else
        //    {
        //        stdSize = 24;
        //    }

        //    return stdSize;
        //}

        private int?[] plusSizes(int amount, int baseSize)
        {
            int?[] plusSizes = new int?[3];
            plusSizes[0] = 1;

            int relativeBase = amount % baseSize;
            int max = (relativeBase == 0) ? baseSize : (baseSize - relativeBase);
            int min = max / 2;
            max = (max <= 2) ? (baseSize + max) : max;
            min = (min <= 1) ? 2 : min;

            plusSizes[1] = min;
            plusSizes[2] = max;
            return plusSizes;
        }

        private int?[] minSizes(int amount, int baseSize)
        {
            int?[] minSizes = new int?[3];
            int relativeBase = amount % baseSize;
            if (relativeBase == 0 && amount > 0)
            {
                relativeBase = baseSize;
            }

            int? minusOne = 1;

            int? max = null;
            if (relativeBase > 2)
            {
                max = relativeBase;
            }
            else if (relativeBase <= 2 && amount > 2)
            {
                max = relativeBase + baseSize;
            }


            int? min = null;
            if (relativeBase >= 12)
            {
                min = relativeBase / 4;
            }
            else if (relativeBase >= 2 || amount >= 2)
            {
                min = 2;
            }
            else
            {
                min = null;
            }

            if (amount <= 0)
            {
                minusOne = null;
                min = null;
                max = null;
            }

            minSizes[2] = minusOne;
            minSizes[1] = min;
            minSizes[0] = max;
            return minSizes;
        }


        internal void setConsumerPicture(Account consumer)
        {
            if (consumer == null)
            {
                GuiElementStates.Instance.ConsumerPicture = null;
            }
            else
            {
                GuiElementStates.Instance.ConsumerPicture = DataStorage.Instance.getAccountPicturePath(consumer.ID);
            }
        }

        internal void setUserPicture(Account user)
        {
            if (user == null)
            {
                GuiElementStates.Instance.UserPicture = null;
            }
            else
            {
                GuiElementStates.Instance.UserPicture = DataStorage.Instance.getAccountPicturePath(user.ID);
            }   
        }


        public string AccountToName(Account account)
        {
            return (account.Nickname != null && !account.Nickname.Equals("") ? account.Nickname : account.Name);
        }


        public static string dateTime_to_String(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm");
        }
    }
}
