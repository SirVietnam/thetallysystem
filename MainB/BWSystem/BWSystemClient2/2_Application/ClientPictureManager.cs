﻿using BWSystemClient2.Domain;
using BWSystemClient2.Domain.WebService;
using BWSystemClient2.GUI.SubElements.ItemContainer;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;

namespace BWSystemClient2.Appl
{
    internal delegate void OnPictureSyncComplete();

    class ClientPictureManager
    {
        private OnPictureSyncComplete onPicSyncComplete;

        private PictureSync pictureSync;
        private BackgroundWorker pictureAccountSyncWorker;
        private BackgroundWorker pictureArticleSyncWorker;

        private int? lastAccountID_synced;
        private int? lastArticleID_synced;


        public ClientPictureManager()
        {
            this.pictureSync = PictureSync.Instance;
            this.pictureAccountSyncWorker = new BackgroundWorker();
            this.pictureAccountSyncWorker.DoWork += this.syncAccountPictures;
            this.pictureAccountSyncWorker.RunWorkerCompleted += this.pictureSyncCompleted;
            this.pictureArticleSyncWorker = new BackgroundWorker();
            this.pictureArticleSyncWorker.DoWork += this.syncArticlePictures;
            this.pictureArticleSyncWorker.RunWorkerCompleted += this.pictureSyncCompleted;

            this.lastAccountID_synced = null;
            this.lastArticleID_synced = null;
        }


        internal void initPictures()
        {
            DataStorage ds = DataStorage.Instance;

            ds.DefaultAccountPicturePath = this.pictureSync.getDefaultAccountImage();
            if (ds.DefaultAccountPicturePath == null)
            {
                string errorString = "Default Account Picture could not be loaded. This is a major error! Need to shut down!";
                SimpleLog.Error(errorString, false);
                throw new SynchronizationLockException(errorString);
            }

            ds.DefaultArticlePicturePath = this.pictureSync.getDefaultArticleImage();
            if (ds.DefaultArticlePicturePath == null)
            {
                string errorString = "Default Article Picture could not be loaded. This is a major error! Need to shut down!";
                SimpleLog.Error(errorString, false);
                throw new SynchronizationLockException(errorString);
            }
        }


        internal void syncPictures()
        {
            this.syncAccountPictures();
            this.syncArticlePictures();
        }

        internal void syncAccountPictures()
        {
            try
            {
                this.pictureAccountSyncWorker.RunWorkerAsync();
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
            }
        }

        internal void syncArticlePictures()
        {
            try
            {
                this.pictureArticleSyncWorker.RunWorkerAsync();
            } catch (InvalidOperationException ex)
            {
                SimpleLog.Log(ex);
            }
        }



        private void syncAccountPictures(object sender, DoWorkEventArgs e)
        {
            SimpleLog.Log("Picture Sync - Account - Starting at ID: " + this.lastAccountID_synced);

            DataStorage dataStorage = DataStorage.Instance;
            IOrderedEnumerable<Account> accounts = dataStorage.Accounts.OrderBy(x => x.ID);

            if (this.lastAccountID_synced == null)
            {
                this.lastAccountID_synced = accounts.First().ID - 1;
            }

            foreach (Account account in accounts)
            {
                if (this.lastAccountID_synced < account.ID)
                {
                    string picturePath = pictureSync.getAccountImage(account);
                    if (picturePath != null)
                    {
                        dataStorage.setAccountPicturePath(account.ID, picturePath);
                        this.lastAccountID_synced = account.ID;
                    }
                }
            }
            this.lastAccountID_synced = null;
        }


        private void syncArticlePictures(object sender, DoWorkEventArgs e)
        {
            SimpleLog.Log("Picture Sync - Article - Starting at ID: " + this.lastArticleID_synced);

            DataStorage dataStorage = DataStorage.Instance;
            IOrderedEnumerable<Article> articles = dataStorage.Articles.OrderBy(x => x.ID);

            if (this.lastArticleID_synced == null)
            {
                this.lastArticleID_synced = articles.First().ID - 1;
            }

            foreach (Article article in articles)
            {
                if (this.lastArticleID_synced < article.ID)
                {
                    string picturePath = pictureSync.getArticleImage(article);
                    if (picturePath != null)
                    {
                        dataStorage.setArticlePicturePath(article.ID, picturePath);
                        this.lastArticleID_synced = article.ID;
                    }
                }
            }
            this.lastArticleID_synced = null;
        }


        private void pictureSyncCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                SimpleLog.Log(e.Error);
                SimpleLog.Log("Additional Infos:"
                    + "\n\tLast Account Synced ID:" + this.lastAccountID_synced
                    + "\n\tLast Article Synced ID:" + this.lastArticleID_synced);
            }
            else if (e.Cancelled)
            {
                SimpleLog.Log("Picture Sync - Cancelled. The Reason: UNKOWN"
                    + "\n\tLast Account Synced ID:" + this.lastAccountID_synced
                    + "\n\tLast Article Synced ID:" + this.lastArticleID_synced);
            }
            else
            {
                SimpleLog.Log("Picture Sync - Complete");
            }

            if (this.onPicSyncComplete != null)
            {
                this.onPicSyncComplete();
            }
        }


        public void subscribeOnPictureSyncComplete(OnPictureSyncComplete function)
        {
            this.onPicSyncComplete += function;
        }

        public void unsubscribeOnPictureSyncComplete(OnPictureSyncComplete function)
        {
            onPicSyncComplete = (OnPictureSyncComplete)Delegate.Remove(onPicSyncComplete, function);
        }
    }
}
