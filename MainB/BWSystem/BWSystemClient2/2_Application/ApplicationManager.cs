﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BWSystemClient2.Appl.StateMachine;
using BWSystemClient2.Domain.WebService;
using BWSystemClient2.GUI;
using BWSystemClient2.Appl.CardReader;
using BWSystemLibrary2;
using System.Windows;
using System.Windows.Threading;
using BWSystemLibrary2.BW_WebService;

namespace BWSystemClient2.Appl
{
    class ApplicationManager
    {
        private static ApplicationManager am;

        private MainWindow mainWindow;

        private DataInteractionManager dim;
        private ClientPictureManager picManger;
        private GuiController guiController;
        private BWSCardWrapper cardReader;

        private State_Booking bookingSM;
        private State_AccountCreation accountSM;
        private State_Cancellation cancellationSM;
        private State_Event eventSM;

        private StateMachine.StateMachine currentSM;
        private StateMachine.StateMachine nextSM;
        private DispatcherTimer dispatcherTimer;


        public ApplicationManager(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            ApplicationManager.am = this;

            this.dim = DataInteractionManager.Instance;
            this.picManger = new ClientPictureManager();
            this.guiController = GuiController.Instance;

            this.accountSM = new State_AccountCreation();
            this.bookingSM = new State_Booking();
            this.cancellationSM = new State_Cancellation();
            this.bookingSM.subscribeOnCompleteAccount(new OnCompleteAccount(this.accountSM.AccountAuth));

            this.picManger.subscribeOnPictureSyncComplete(new OnPictureSyncComplete(this.dim.updateUIPanels));

            this.dispatcherTimer = new DispatcherTimer();
            this.dispatcherTimer.Interval += TimeSpan.FromSeconds(1.0);
            this.dispatcherTimer.Tick += changeSM;
        }


        internal void init()
        {
            this.startSmartCardReader();

            this.dim.init();
            this.picManger.syncPictures();
        }


        private void startSmartCardReader()
        {
            if ((bool)LibUtility.getConfigValue("UseCardReader", typeof(bool)))
            {
                try
                {
                    this.cardReader = new BWSCardWrapper();
                } catch (Exception ex)
                {
                    SimpleLog.Log(ex);
                    MessageBoxResult result = MessageBox.Show("The usage of the smart card reader is activated, but failed to initialize.\nThe system will terminate.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    if (result == MessageBoxResult.OK)
                    {
                        Application.Current.Shutdown();
                    }
                }
            }
        }



        public static void reset(bool forceUpdate = false)
        {
            if (am != null)
            {
                am.start(forceUpdate);
            }
        }

        internal void startUpComplete()
        {
            if (this.currentSM == null)
            {
                this.currentSM = this.bookingSM;
            }
        }

        public static void updateData()
        {
            if (am != null && am.currentSM != null)
            {
                am.currentSM.updateData();
            }
        }


        public void start(bool forceUpdate = false)
        {
            if (this.currentSM != null)
            {
                if (forceUpdate)
                    this.currentSM.updateData(forceUpdate);
                this.currentSM.start();
            }
        }

        internal void cancellationClicked()
        {
            //this.currentSM.updateData();
            this.cancellationSM.start();
        }

        internal void addAccountClicked()
        {
            this.currentSM.updateData();
            this.accountSM.start();
        }


        internal static void startEvent(Event currentEvent)
        {
            if (am != null)
            {
                am.eventSM = new State_Event(currentEvent);
                am.eventSM.subscribeOnCompleteAccount(new OnCompleteAccount(am.accountSM.AccountAuth));
                am.nextSM = am.eventSM;

                am.dispatcherTimer.Start();
            }
        }


        internal static void endEvent(Event currentEvent)
        {
            if (am != null)
            {
                am.nextSM = am.bookingSM;
                am.dispatcherTimer.Start();
            }
        }


        private void changeSM(object sender, EventArgs e)
        {
            if (am.currentSM != null && am.currentSM.saveState())
            {
                this.dispatcherTimer.Stop();
                this.currentSM = this.nextSM;
                this.currentSM.updateData(true);
                this.currentSM.start();
            }
        }

    }
}
