﻿using BWSystemLibrary2;
using GS.SCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;

namespace BWSystemClient2.Appl.CardReader
{
    class BWSCardReader
    {
        private event CardDatasEventHandler recieverCardDatas;

        private int cardReaderExceptionsCount;
        private WinSCard scard;
        private string readerString;

        private Timer timer;

        public BWSCardReader()
        {
            this.scard = new WinSCard();
            this.scard.EstablishContext();

            this.timer = new Timer();
            this.timer.Elapsed += this.timer_tick;
            this.timer.Interval = 2500;

            string[] readers = this.scard.ListReaders();

            this.cardReaderExceptionsCount = 0;

            if (readers.Count() == 1)
            {
                this.readerString = readers[0];
            }
            else
            {
                //TODO how to react?
            }
        }

        private void timer_tick(object sender, EventArgs e)
        {
            this.timer.Stop();
            try
            {
                this.cardListener();
                this.cardReaderExceptionsCount = 0;
            } catch (Exception ex)
            {
                SimpleLog.Log(ex);
                this.cardReaderExceptionsCount++;
            } finally
            {
                if (this.cardReaderExceptionsCount < 5)
                {
                    this.timer.Start();
                }
                else
                {
                    GUI.GuiController.Instance.showMessageBox("Lost connection to the Smart Card Reader!\nCheck the connection and restart the application.The Card Reader will be disabled for now.\nReport to your admin if this continues.", "Card Reader Connection Lost", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                }
            }
        }


        public void startTimer()
        {
            this.timer.Start();
        }


        //private void connectSCard()
        //{
        //    this.scard.Connect(readerString, GS.SCard.Const.SCARD_SHARE_MODE.Exclusive, GS.SCard.Const.SCARD_PROTOCOL.Undefined);
        //}


        internal void cardListener()
        {
            this.scard.WaitForCardPresent(readerString);
            this.scard.Connect(readerString);

            byte[] cmdApdu = { 0xFF, 0xCA, 0x00, 0x00, 00 }; // Get Card UID ...
            byte[] respApdu = new byte[256];
            int respLength = respApdu.Length;
            this.scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

            byte[] sRespApdu = new byte[respLength];
            Array.Copy(respApdu, sRespApdu, respLength);
            Array.Reverse(sRespApdu);

            long? cardNumber = null;

            if (sRespApdu[0] == 0x00 && sRespApdu[1] == 0x90)
            {
                string hex = BitConverter.ToString(sRespApdu, 2);
                cardNumber = Int64.Parse(hex.Replace("-", String.Empty), System.Globalization.NumberStyles.HexNumber);
            }

            if (cardNumber != null && recieverCardDatas != null)
            {
                this.recieverCardDatas(this, new CardDatasEventArgs((long)cardNumber));
            }
        }


        public void subscribe(CardDatasEventHandler handler)
        {
            this.recieverCardDatas += handler;
        }
    }
}
