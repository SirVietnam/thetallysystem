﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GS.SCard;
using GS.Util.Hex;
using GS.Util.ByteArray;
using System.Windows.Threading;
using System.Timers;
using System.Threading;
using BWSystemClient2.Appl.CardReader;
using BWSystemClient2.GUI;
using BWSystemClient2.Domain;
using System.Windows;

namespace BWSystemClient2.Appl.CardReader
{
    public class BWSCardWrapper
    {
        private Thread readerThread;
        private BWSCardReader reader;
        //private bool waitingForCard;

        public BWSCardWrapper()
        {
            this.reader = new BWSCardReader();
            this.readerThread = new Thread(this.reader.startTimer);

            reader.subscribe(new CardDatasEventHandler(this.onCardRead));
            this.readerThread.Start();
        }

        private void onCardRead(object o, CardDatasEventArgs datas)
        {
            DataStorage.Instance.CardDatas = datas;
            Application.Current.Dispatcher.Invoke(GuiController.Instance.CardEntered, DispatcherPriority.Send);
        }
    }
}
