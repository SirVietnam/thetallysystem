﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.CardReader
{
    public delegate void CardDatasEventHandler(object o, CardDatasEventArgs datas);

    public class CardDatasEventArgs : EventArgs
    {

        private readonly long cardNumber;
        private readonly DateTime timeStamp;



        public CardDatasEventArgs(long cardNumber)
        {
            this.cardNumber = cardNumber;
            this.timeStamp = new DateTime(DateTime.Now.Ticks);
        }


        public long CardNumber
        {
            get { return cardNumber; }
        }


        public DateTime TimeStamp
        {
            get { return timeStamp; }
        }
    }
}
