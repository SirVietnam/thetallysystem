﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BWSystemClient2.Domain;
using BWSystemClient2.Domain.WebService;
using BWSystemLibrary2.BW_WebService;
using BWSystemLibrary2;
using System.Windows;
using BWSystemClient2.GUI;
using BWSystemClient2.Appl.Timers;
using System.Windows.Threading;

namespace BWSystemClient2.Appl
{
    class DataInteractionManager
    {
        private static DataInteractionManager dim;

        private TimerManager timer;
        private DataSync dataSync;
        private DataStorage data;
        private ClientPictureManager pictureManager;


        private DataInteractionManager()
        {
            this.pictureManager = new ClientPictureManager();
            this.pictureManager.subscribeOnPictureSyncComplete(new OnPictureSyncComplete(this.updateUIPanels));
            this.data = DataStorage.Instance;
        }


        internal static DataInteractionManager Instance
        {
            get
            {
                if (DataInteractionManager.dim == null)
                {
                    DataInteractionManager.dim = new DataInteractionManager();
                }
                return DataInteractionManager.dim;
            }
        }


        internal void init()
        {
            this.dataSync = new DataSync();
            this.initTimers();
            this.readBackupBookings();
            this.pictureManager.initPictures();
            this.updateData();
        }


        /*
         * Timer Helper Methods
         */
        private void initTimers()
        {
            this.timer = new TimerManager();
            this.data.subscribeOnEventUpdated(new OnEventUpdated(this.onNewEventsRecieved));
            this.timer.addTimers(new UpdateTimer((double)BWSystemLibrary2.LibUtility.getConfigValue("UpdateTimer_UserData", typeof(double)), new Action(ApplicationManager.updateData)));
            this.timer.addTimers(new UpdateTimer((double)BWSystemLibrary2.LibUtility.getConfigValue("UpdateTimer_Pictures", typeof(double)), new Action(this.pictureManager.syncAccountPictures)));
            this.timer.addTimers(new UpdateTimer((double)BWSystemLibrary2.LibUtility.getConfigValue("UpdateTimer_Pictures", typeof(double)), new Action(this.pictureManager.syncArticlePictures)));
            //this.timer.addTimers(new UpdateTimer(1.5, new Action(this.dataSync.pingServer)));
        }


        private void onNewEventsRecieved(List<Event> newEvents)
        {
            foreach (Event currentEvent in newEvents)
            {
                EventTimer newEventTimer = new EventTimer(currentEvent);
                this.timer.addTimers(newEventTimer);
            }
        }


        internal void updateData(bool forceUpdate = false)
        {
            if (this.dataSync.updateData(forceUpdate))
            {
                this.updateUIPanels();
            }
        }


        internal void updateEventData(int eventID, bool eventStart = false)
        {
            if (this.dataSync.updateEventData(eventID, eventStart))
            {
                this.updateUIPanels();
            }
        }

        internal void updateUIPanels()
        {
            DataMapper.buildButtonItems();
            DataMapper.mapDataToGui();
            GuiController.Instance.updateButtonPanels();
        }


        internal bool bookBooking(BookingData booking)
        {
            booking.TimeStamp = DateTime.Now;

            this.data.addFinishedBooking(booking);

            bool booked = this.dataSync.bookBooking(booking);
            if (!booked)
            {
                this.backupBooking(booking);
            }
            else
            {
                //Book backup while it can be booked
                while (this.data.UnbookedBookings.Count > 0 && booked)
                {
                    BookingData unbookedBooking = this.data.UnbookedBookings.First();
                    this.data.UnbookedBookings.RemoveFirst();
                    booked = this.bookBooking(unbookedBooking);
                }
                //Update backup!
                booked = LibUtility.serializeBookings(this.data.UnbookedBookings.ToList());
            }
            return booked;
        }

        private void backupBooking(BookingData booking)
        {
            this.data.UnbookedBookings.AddLast(booking);
            LibUtility.serializeBookings(this.data.UnbookedBookings.ToList());
        }

        private void readBackupBookings()
        {
            List<BookingData> unbookedBookingsList = LibUtility.deserializeBookings();

            LinkedList<BookingData> unbookedBookingsLL = new LinkedList<BookingData>();
            foreach (BookingData booking in unbookedBookingsList)
            {
                unbookedBookingsLL.AddLast(booking);
            }

            this.data.UnbookedBookings = unbookedBookingsLL;
        }


        internal bool bookCancellation(CancellationData cancellation)
        {
            cancellation.CancellationTime = DateTime.Now;

            bool booked = this.dataSync.bookCancellation(cancellation);
            if (!booked)
            {
                GuiController.Instance.showMessageBox("The Booking couldn't be cancelled. Please retry.", "Cancellation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                //TODO: Save Cancellations?
                //this.data.UnbookedCancellations.AddLast(cancellation);
            }
            return booked;
        }


        internal Account importAccount(string userID, string userPassword)
        {
            return this.dataSync.importAccount(userID, userPassword);
        }

        internal bool saveAccount(Account account)
        {
            return this.dataSync.saveAccount(account);
        }

        internal bool saveNonOrgAccount(Account account, string password, string mailAddress)
        {
            return this.dataSync.saveAccount(account, password, mailAddress);
        }

        internal BookingData[] getBookings()
        {
            return this.dataSync.getBookings();
        }
    }
}
