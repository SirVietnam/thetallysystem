﻿using BWSystemClient2.Appl.Utility;
using BWSystemClient2.Domain;
using BWSystemClient2.Domain.WebService;
using BWSystemClient2.GUI;
using BWSystemClient2.GUI.GuiElements;
using BWSystemClient2.GUI.SubElements.ItemContainer;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BWSystemClient2.Appl
{
    class DataMapper
    {
        private static GuiUtility guiUtil = new GuiUtility();

        public DataMapper()
        {
        }

        internal static void mapDataToGui()
        {
            if (!System.Windows.Threading.Dispatcher.CurrentDispatcher.CheckAccess())
            {
                BWSystemLibrary2.SimpleLog.Log("Dispatcher required. This should not happen!");
                System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => DataMapper.mapDataToGui()));
            }
            else
            {

                DataStorage dataStorage = DataStorage.Instance;
                GuiDataStorage guiDataStorage = GuiDataStorage.Instance;

                Account[] accounts = dataStorage.Accounts;

                List<ButtonItem_User> guiUsers = new List<ButtonItem_User>();

                for (int i = 0; i < accounts.Length; i++)
                {
                    Account currentAccount = accounts[i];
                    if (SecurityUtility.isValidAccount(currentAccount))
                        guiUsers.Add(dataStorage.getUserIcon(currentAccount.ID));
                }

                ButtonItem_Consumer[] guiConsumers = new ButtonItem_Consumer[accounts.Length];
                for (int i = 0; i < accounts.Length; i++)
                {
                    guiConsumers[i] = dataStorage.getCustomerIcon(accounts[i].ID);
                }

                Article[] articles = dataStorage.Articles;
                ButtonItem_Article[] guiArticles = new ButtonItem_Article[articles.Length];
                for (int i = 0; i < articles.Length; i++)
                {
                    guiArticles[i] = dataStorage.getArticleIcon(articles[i].ID);
                }

                guiDataStorage.ConsumerButtons = guiConsumers;
                guiDataStorage.UserButtons = guiUsers.ToArray();
                guiDataStorage.ArticleButtons = guiArticles;
            }
        }


        internal static void buildButtonItems()
        {
            SimpleLog.Log("Building ButtonItems");

            DataStorage dataStorage = DataStorage.Instance;
            Account[] accounts = dataStorage.Accounts;
            Article[] articles = dataStorage.Articles;

            foreach (Account account in accounts)
            {
                string picturePath = dataStorage.getAccountPicturePath(account.ID);

                ButtonItem_User userIcon = new ButtonItem_User(account.ID, account.DisplayName, picturePath);
                dataStorage.setUserIcon(userIcon);

                ButtonItem_Consumer customerIcon = new ButtonItem_Consumer(account.ID, account.DisplayName, picturePath);
                dataStorage.setCustomerIcon(customerIcon);

            }

            foreach (Article article in articles)
            {
                string picturePath = dataStorage.getArticlePicturePath(article.ID);
                ButtonItem_Article articleIcon = new ButtonItem_Article(article.ID, article.Name, picturePath);
                dataStorage.setArticleIcon(articleIcon);
            }
        }


        internal static void syncBookingDataToGui(BookingPanel bookingPanel, BookingAA[] bookingAAs)
        {
            bookingPanel.resetPanel();
            foreach (BookingAA bookingAA in bookingAAs)
            {
                DataMapper.syncBookingDataToGui(bookingPanel, bookingAA);
            }
        }


        public static void syncBookingDataToGui(BookingPanel bookingGuiPanel, BookingAA bookingData)
        {
            ButtonItem_ArticleBooking[] bookingGui = bookingGuiPanel.getBooking();

            bool bookingFound = false;
            ButtonItem_ArticleBooking bookingGuiItem = bookingGui.FirstOrDefault(x => x.ID == bookingData.Article);
            if (bookingGuiItem != null)
            {
                bookingGuiItem.Amount = bookingData.Amount;
                bookingFound = true;
                DataMapper.updateBookingPrize(bookingGuiPanel, bookingData);
            }

            if (!bookingFound)
            {
                DataMapper.addBookingDataFromGui(bookingGuiPanel, bookingData);
            }

        }


        public static void addBookingDataFromGui(BookingPanel bookingGuiPanel, BookingAA bookingData)
        {
            Article article = DataStorage.Instance.Articles.First(x => x.ID == bookingData.Article);
            ButtonItem_ArticleBooking newBooking = new ButtonItem_ArticleBooking(bookingData.Article, article.Name, DataStorage.Instance.getArticlePicturePath(article.ID));
            newBooking.Amount = bookingData.Amount;
            bookingGuiPanel.addBooking(newBooking);
            DataMapper.updateBookingPrize(bookingGuiPanel, bookingData);
        }

        private static void updateBookingPrize(BookingPanel bookingGuiPanel, BookingAA bookingData)
        {
            double itemsPrize = ((double)bookingData.Amount) * DataStorage.Instance.getArticle(bookingData.Article).Prize;
            bookingGuiPanel.updateArticlePrize(bookingData.Article, (float)itemsPrize);
        }


        public static void removeBookingDataFromGui(BookingPanel bookingGuiPanel, BookingAA bookingData)
        {
            bookingGuiPanel.removeBooking(bookingData.Article);
        }


        public static void syncCancellationPanel(CancellationPanel cancelPanel, BookingData[] bookings)
        {
            CancellationContainer[] guiBookings = new CancellationContainer[bookings.Length];
            for (int i = guiBookings.Length - 1; i >= 0; i--)
            {
                guiBookings[i] = DataMapper.BookingDataToCancellationConatainer(bookings[i]);
            }
            cancelPanel.setData(guiBookings);
        }


        private static CancellationContainer BookingDataToCancellationConatainer(BookingData bookingData)
        {
            ButtonItem_ArticleCancellation[] bookingAAs = new ButtonItem_ArticleCancellation[bookingData.Booking.Length];
            for (int i = 0; i < bookingAAs.Length; i++)
            {
                bookingAAs[i] = DataMapper.BookingAAToItemContainer((int)bookingData.ID, bookingData.Booking[i]);
            }
            Account consumer = DataStorage.Instance.getAccount(bookingData.Customer);
            string consumerName = consumer.DisplayName;

            Account user = null;
            string userName = null;
            if (bookingData.User != null)
            {
                user = DataStorage.Instance.getAccount((int)bookingData.User);
                userName = user.DisplayName;
            }
            return new CancellationContainer((int)bookingData.ID, bookingData.Customer, consumerName, bookingData.User, userName, bookingAAs, bookingData.TimeStamp, bookingData.DataCancelled);
        }


        private static ButtonItem_ArticleCancellation BookingAAToItemContainer(int bookingID, BookingAA bookingAA)
        {
            Article article = DataStorage.Instance.getArticle(bookingAA.Article);
            ButtonItem_ArticleCancellation newBooking = new ButtonItem_ArticleCancellation(bookingID, bookingAA.Article, article.Name, DataStorage.Instance.getArticlePicturePath(article.ID));
            newBooking.Amount = bookingAA.Amount;
            return newBooking;
        }
    }
}
