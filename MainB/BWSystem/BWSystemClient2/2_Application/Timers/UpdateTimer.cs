﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.Timers
{
    class UpdateTimer : ITimeable
    {
        private static Random randomizer = new Random();

        protected DateTime nextExecutionDateTime;
        protected Action updateAction;
        private TimeSpan updateInterval;
        private bool executed;

        public UpdateTimer(double updateIntervalMinutes, Action updateAction)
        {
            this.updateInterval = TimeSpan.FromMinutes(updateIntervalMinutes);
            this.nextExecutionDateTime = DateTime.Now - TimeSpan.FromSeconds(5.0);
            this.updateAction = updateAction;
            this.executed = true;
        }

        public void execute()
        {
            if (updateAction != null)
                this.updateAction();
            this.executed = true;
            this.getUpcommingDateTime();
        }

        public DateTime getUpcommingDateTime()
        {
            while (this.executed && this.nextExecutionDateTime <= DateTime.Now)
            {
                nextExecutionDateTime += updateInterval + TimeSpan.FromSeconds(10.0 * UpdateTimer.randomizer.NextDouble());
            }
            this.executed = false;
            return nextExecutionDateTime;
        }

        public bool isSingleShot()
        {
            return false;
        }
    }
}
