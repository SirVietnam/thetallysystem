﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.Timers
{
    interface ITimeable
    {
        DateTime getUpcommingDateTime();

        void execute();

        bool isSingleShot();
    }
}
