﻿using BWSystemClient2.Domain;
using BWSystemClient2.Domain.WebService;
using BWSystemLibrary2;
using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Threading;

namespace BWSystemClient2.Appl.Timers
{
    class TimerManager
    {
        private DispatcherTimer eventTimer;
        DateTime lastSync;

        ITimeable currentTimer;

        List<ITimeable> timers;

        public TimerManager()
        {
            lastSync = DateTime.Now;

            timers = new List<ITimeable>();

            this.eventTimer = new DispatcherTimer();
            this.eventTimer.Tick += timerTick;
            this.eventTimer.Interval = TimeSpan.FromSeconds(30.0);
            this.eventTimer.Start();
        }


        internal void addTimers(ITimeable timer)
        {
            if (timer.getUpcommingDateTime() >= DateTime.Now)
            {
                this.timers.Add(timer);

                this.setNextTimer();
            }
        }


        private void timerTick(object sender, EventArgs e)
        {
            this.eventTimer.Stop();
            SimpleLog.Log("Starting Timer Tick");

            if (this.currentTimer != null)
            {
                this.currentTimer.execute();

                if (this.currentTimer.isSingleShot())
                {
                    this.timers.Remove(this.currentTimer);
                }
            }

            this.setNextTimer();
        }

        private void setNextTimer()
        {
            this.timers = timers.OrderBy(x => x.getUpcommingDateTime()).ToList();

            this.currentTimer = this.timers.FirstOrDefault();
            if (this.currentTimer != null)
            {
                DateTime upcomming = this.currentTimer.getUpcommingDateTime();
                TimeSpan nextInterval = (upcomming - DateTime.Now);
                this.eventTimer.Interval = nextInterval.TotalSeconds > 0.0 ? nextInterval : TimeSpan.FromSeconds(1.0);
                this.eventTimer.Start();
            }
        }


        //public void executeTimer()
        //{

        //}

    }
}
