﻿using BWSystemLibrary2.BW_WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSystemClient2.Appl.Timers
{
    class EventTimer : ITimeable
    {
        private Event currentEvent;


        public EventTimer(Event currentEvent)
        {
            this.currentEvent = currentEvent;
            this.execute();
        }


        public DateTime getUpcommingDateTime()
        {
            DateTime upcommingDateTime = currentEvent.End;
            if (this.preEventState())
            {
                upcommingDateTime = currentEvent.Begin;
            }
            return upcommingDateTime;
        }

        public void execute()
        {
            if (!this.preEventState() && !this.postEventState())
            {
                ApplicationManager.startEvent(this.currentEvent);
            }
            else if (!this.preEventState() && this.postEventState())
            {
                ApplicationManager.endEvent(this.currentEvent);
            }
        }

        private bool preEventState()
        {
            return DateTime.Now < (this.currentEvent.Begin); // - TimeSpan.FromSeconds(2.0)
        }

        private bool postEventState()
        {
            return DateTime.Now >= (this.currentEvent.End); // - TimeSpan.FromSeconds(0.15)
        }

        bool ITimeable.isSingleShot()
        {
            return this.postEventState();
        }
    }
}
